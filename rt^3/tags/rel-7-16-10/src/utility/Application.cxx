/*                 A P P L I C A T I O N . C X X
 * BRL-CAD
 *
 * Copyright (c) 2010 United States Government as represented by
 * the U.S. Army Research Laboratory.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 */
/** @file Application.cxx
 *
 * Brief description
 *
 */

// interface header
#include "Application.h"

// implementation system headers
#include <time.h>
#include <algorithm>

// implementation headers
#include "../date/buildStamp.h"


Application::Application(int argc, char*argv[])
{
}


Application::~Application()
{
}


bool Application::run()
{
}


Date Application::buildDate() const
{
  return Date(std::string(::buildDate));
}


Time Application::buildTime() const
{
  return Time(std::string(::buildTime));
}


Date Application::runDate() const
{
  return _runDate;
}


Time Application::runTime() const
{
  return _runTime;
}


std::string Application::uptime() const
{
}


// Local Variables: ***
// mode: C++ ***
// tab-width: 8 ***
// c-basic-offset: 2 ***
// indent-tabs-mode: t ***
// End: ***
// ex: shiftwidth=2 tabstop=8
