/* c++ specifies a particular assumed format for __DATE__ and __TIME__ 
 * __DATE__ should be Mmm dd yyyy using the local clock date
 * __TIME__ should be hh:mm::ss in 24 hour local clock time
 */
extern const char *buildDate;
extern const char *buildTime;

// Local Variables: ***
// mode: C++ ***
// tab-width: 8 ***
// c-basic-offset: 2 ***
// indent-tabs-mode: t ***
// End: ***
// ex: shiftwidth=2 tabstop=8
