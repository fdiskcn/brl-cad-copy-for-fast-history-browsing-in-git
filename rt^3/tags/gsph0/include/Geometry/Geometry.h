#ifndef __GEOMETRY_H__
#define __GEOMETRY_H__

namespace Geometry {
  int init();
}

#include "Geometry/Triangle.h"
#include "Geometry/SceneFactory.h"
#include "Geometry/Scene.h"
#include "Geometry/Primitive.h"


#endif  /* __GEOMETRY_H__ */

// Local Variables: ***
// mode: C++ ***
// tab-width: 8 ***
// c-basic-offset: 2 ***
// indent-tabs-mode: t ***
// End: ***
// ex: shiftwidth=2 tabstop=8
