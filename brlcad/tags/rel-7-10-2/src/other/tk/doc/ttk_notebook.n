'\"
'\" Copyright (c) 2004 Joe English
'\"
'\" See the file "license.terms" for information on usage and redistribution
'\" of this file, and for a DISCLAIMER OF ALL WARRANTIES.
'\" 
'\" RCS: @(#) $Id$
'\" 
.so man.macros
.TH ttk_notebook n 8.5 Tk "Tk Themed Widget"
.BS
.\" Use _ instead of :: as the name becomes a filename on install
.SH NAME
ttk_notebook \- Multi-paned container widget
.SH SYNOPSIS
\fBttk::notebook\fR \fIpathName \fR?\fIoptions\fR?
.br
\fIpathName \fBadd\fR \fIpathName\fR.\fIsubwindow\fR ?\fIoptions...\fR?
\fIpathName \fBinsert\fR \fIindex\fR \fIpathName\fR.\fIsubwindow\fR ?\fIoptions...\fR?
.BE

.SH DESCRIPTION
A \fBttk::notebook\fR widget manages a collection of subpanes 
and displays a single one at a time.
Each pane is associated with a tab, which the user
may select to change the currently-displayed pane.
.SO
\-class	\-cursor	\-takefocus	\-style
.SE

.SH "WIDGET-SPECIFIC OPTIONS"
.OP \-height height Height
If present and greater than zero, 
specifies the desired height of the pane area
(not including internal padding or tabs).
Otherwise, the maximum height of all panes is used.
.OP \-padding padding Padding
Specifies the amount of extra space to add around the outside
of the notebook.
The padding is a list of up to four length specifications 
\fIleft top right bottom\fR.
If fewer than four elements are specified, 
\fIbottom\fR defaults to \fItop\fR,
\fIright\fR defaults to \fIleft\fR, and 
\fItop\fR defaults to \fIleft\fR.
.OP \-width width Width
If present and greater than zero, 
specifies the desired width of the pane area
(not including internal padding).
Otherwise, the maximum width of all panes is used.
.SH "TAB OPTIONS"
The following options may be specified for individual notebook panes:
.OP \-state state State
Either \fBnormal\fR, \fBdisabled\fR or \fBhidden\fR.  
If \fBdisabled\fR, then the tab is not selectable. If \fBhidden\fR,
then the tab is not shown.
.OP \-sticky sticky Sticky
Specifies how the child pane is positioned within the pane area.
Value is a string containing zero or more of the characters
\fBn, s, e,\fR or \fBw\fR.
Each letter refers to a side (north, south, east, or west) 
that the child window will "stick" to,
as per the \fBgrid\fR geometry manager.
.OP \-padding padding Padding
Specifies the amount of extra space to add between the notebook and this pane.
Syntax is the same as for the widget \fB-padding\fR option.
.OP \-text text Text
Specifies a string to be displayed in the tab.
.OP \-image image Image
Specifies an image to display in the tab,
which must have been created with the \fBimage create\fR command.
.OP \-compound compound Compound
Specifies how to display the image relative to the text,
in the case both \fB-text\fR and \fB-image\fR are present.
See \fIlabel(n)\fR for legal values.
.OP \-underline underline Underline
Specifies the integer index (0-based) of a character to underline 
in the text string.
The underlined character is used for mnemonic activation
if \fBttk::notebook::enableTraversal\fR is called.

.SH "WIDGET COMMAND"
.TP
\fIpathname \fBadd \fIchild\fR ?\fIoptions...\fR?
Adds a new tab to the notebook.
When the tab is selected, the \fIchild\fR window
will be displayed.
\fIchild\fR must be a direct child of the notebook window.
See \fBTAB OPTIONS\fR for the list of available \fIoptions\fR.
.TP
\fIpathname \fBconfigure\fR ?\fIoptions\fR?
See \fIttk_widget(n)\fR.
.TP
\fIpathname \fBcget\fR \fIoption\fR
See \fIttk_widget(n)\fR.
.TP
\fIpathname \fBforget\fR \fItabid\fR
Removes the tab specified by \fItabid\fR,
unmaps and unmanages the associated child window.
.TP
\fIpathname \fBindex\fR \fItabid\fR
Returns the numeric index of the tab specified by \fItabid\fR,
or the total number of tabs if \fItabid\fR is the string "\fBend\fR".
.TP
\fIpathname \fBinsert\fR \fIpos\fR \fIsubwindow\fR \fIoptions...\fR
Inserts a pane at the specified position.
\fIpos\fR is either the string \fBend\fR, an integer index, 
or the name of a managed subwindow.
If \fIsubwindow\fR is already managed by the notebook, 
moves it to the specified position.
See \fBTAB OPTIONS\fR for the list of available options.
.TP
\fIpathname \fBinstate\fR \fIstatespec \fR?\fIscript...\fR?
See \fIttk_widget(n)\fR.
.TP
\fIpathname \fBselect\fR ?\fItabid\fR?
Selects the specified tab.  The associated child pane will be displayed,
and the previously-selected pane (if different) is unmapped.
If \fItabid\fR is omitted, returns the widget name of the
currently selected pane.
.TP
\fIpathname \fBstate\fR ?\fIstatespec\fR?
See \fIttk_widget(n)\fR.
.TP
\fIpathname \fBtab\fR \fItabid\fR ?\fI-options \fR?\fIvalue ...\fR
Query or modify the options of the specific tab.
If no \fI-option\fR is specified, returns a dictionary of the tab option values.
If one \fI-option\fR is specified, returns the value of that \fIoption\fR.
Otherwise, sets the \fI-option\fRs to the corresponding \fIvalue\fRs.
See \fBTAB OPTIONS\fR for the available options.
.TP
\fIpathname \fBtabs\fR
Returns a list of all windows managed by the widget.
.\" Perhaps "panes" is a better name for this command?

.SH "KEYBOARD TRAVERSAL"
To enable keyboard traversal for a toplevel window
containing a notebook widget \fI$nb\fR, call:
.CS
ttk::notebook::enableTraversal $nb
.CE
.PP
This will extend the bindings for the toplevel widget
containing the notebook as follows:
.IP \(bu
\fBControl-Tab\fR selects the tab following the currently selected one.
.IP \(bu
\fBShift-Control-Tab\fR selects the tab preceding the currently selected one.
.IP \(bu
\fBAlt-K\fR, where \fBK\fR is the mnemonic (underlined) character
of any tab, will select that tab.
.PP
Multiple notebooks in a single toplevel may be enabled for traversal,
including nested notebooks.  
However, notebook traversal only works properly if all panes
are direct children of the notebook.

.SH "TAB IDENTIFIERS"
The \fItabid\fR argument to the above commands may take
any of the following forms:
.IP \(bu
An integer between zero and the number of tabs;
.IP \(bu
The name of a child pane window;
.IP \(bu
A positional specification of the form "@\fIx\fR,\fIy\fR",
which identifies the tab 
.IP \(bu
The literal string "\fBcurrent\fR",
which identifies the currently-selected tab; or:
.IP \(bu
The literal string "\fBend\fR",
which returns the number of tabs 
(only valid for "\fIpathname \fBindex\fR").

.SH "VIRTUAL EVENTS"
The notebook widget generates a \fB<<NotebookTabChanged>>\fR
virtual event after a new tab is selected.

.SH "EXAMPLE"
.CS
notebook .nb
\.nb add [frame .nb.f1] -text "First tab"
\.nb add [frame .nb.f2] -text "Second tab"
\.nb select .nb.f2
ttk::notebook::enableTraversal .nb
.CE

.SH "SEE ALSO"
ttk_widget(n), grid(n)

.SH "KEYWORDS"
pane, tab
