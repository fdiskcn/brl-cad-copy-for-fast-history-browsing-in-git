/*
 *  Authors -
 *	John R. Anderson
 *	Susanne L. Muuss
 *	Earl P. Weaver
 *
 *  Source -
 *	VLD/ASB Building 1065
 *	The U. S. Army Ballistic Research Laboratory
 *	Aberdeen Proving Ground, Maryland  21005
 *  
 *  Copyright Notice -
 *	This software is Copyright (C) 1990 by the United States Army.
 *	All rights reserved.
 */

/*	These functions evaluate a Rational B-Spline Curve */

#include "conf.h"

#include <stdio.h>

#include "machine.h"
#include "vmath.h"
#include "raytrace.h"		/* for declaration of rt_calloc() */

#undef W		/* from vmath.h */

static fastf_t *knots=(fastf_t *)NULL;
static int numknots=0;

/* Set the knot values */
void
Knot( n , values )
int n;		/* number of values in knot sequence */
fastf_t values[];	/* knot values */
{
	int i;

	if( n < 2 )
	{
		rt_log( "Knot: ERROR %d knot values\n" , n );
		rt_bomb( "Knot: cannot have less than 2 knot values\n" );
	}

	if( numknots )
		rt_free( (char *)knots , "Knot: knots" );

	knots = (fastf_t *)rt_calloc( n , sizeof( fastf_t ) , "Knot: knots" );

	numknots = n;

	for( i=0 ; i<n ; i++ )
		knots[i] = values[i];

}

Freeknots()
{
	rt_free( (char *)knots , "Freeknots: knots" );
	numknots = 0;
}


/* Evaluate the Basis functions */
fastf_t
Basis( i , k , t )
fastf_t t;	/* parameter value */
int i;		/* interval number ( 0 through k ) */
int k;		/* degree of basis function */
{
	fastf_t denom1,denom2,retval=0.0;

	if( (i+1) > (numknots-1) )
	{
		fprintf( stderr , "Error in evaluation of a B-spline Curve\n" );
		fprintf( stderr, "attempt to access knots out of range: numknots=%d i=%d, k=%d\n" , numknots , i , k );
		return( 0.0 );
	}

	if( k == 1 )
	{
		if( t >= knots[i] && t < knots[i+1] )
			return( 1.0 );
		else
			return( 0.0 );
	}
	else
	{
		denom1 = knots[i+k-1] - knots[i];
		denom2 = knots[i+k] - knots[i+1];

		if(denom1 != 0.0 )
			retval += (t - knots[i])*Basis( i , k-1 , t )/denom1;

		if( denom2 != 0.0 )
			retval += (knots[i+k] - t)*Basis( i+1 , k-1 , t )/denom2;

		return( retval );
	}
}

/* Evaluate a B-Spline curve */
void
B_spline( t , m , k , P , W , pt )
fastf_t t;	/* parameter value */
int k;		/* order */
int m;		/* upper limit of sum (number of control points - 1) */
point_t P[];	/* Control Points (x,y,z) */
fastf_t W[];	/* Weights for Control Points */
point_t pt;	/* Evaluated point on spline */
{

	fastf_t tmp,numer[3],denom=0.0;
	int i,j;

	for( j=0 ; j<3 ; j++ )
		numer[j] = 0.0;

	for( i=0 ; i<=m ; i++ )
	{
		tmp = W[i]*Basis( i , k , t );
		denom += tmp;
		for( j=0 ; j<3 ; j++ )
			numer[j] += P[i][j]*tmp;
	}

	for( j=0 ; j<3 ; j++ )
		pt[j] = numer[j]/denom;
}


	
