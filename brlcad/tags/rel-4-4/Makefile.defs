#################################################################
#								#
#			Makefile.defs				#
#								#
#  This Makefile contains definitions common to most of the	#
#  software packages contained in this distribution.		#
#  Combined with Makefile.rules and a directory-specific	#
#  Makefile.loc to create full MAKE input.			#
#								#
#  Each source directory contains a Makefile with these		#
#  contents:							#
#								#
#everything:							#
#	make $(MFLAGS) -$(MAKEFLAGS) \				#
#		-f ../Makefile.defs -f Makefile.loc \		#
#		-f ../Makefile.rules				#
#.DEFAULT:							#
#	make $(MFLAGS) -$(MAKEFLAGS) \				#
#		-f ../Makefile.defs -f Makefile.loc \		#
#		-f ../Makefile.rules $@				#
#								#
#  so that the local rules and definitions for each directory	#
#  are combined with the common rules and definitions.		#
#								#
#  Author -							#
#	Michael John Muuss					#
#								#
#  Source -							#
#	SECAD/VLD Computing Consortium, Bldg 394		#
#	The U. S. Army Ballistic Research Laboratory		#
#	Aberdeen Proving Ground, Maryland  21005		#
#								#
#  Acknowledgements -						#
#	This Makefile draws heavily on Doug Gwyn's		#
#	VLD Standard Makefile, and Doug Kingston's		#
#	MMDF Global Makefile.					#
#								#
#  $Header$		#
#								#
#################################################################

#################################################################
#								#
#  Operating System Definition and Compiler Flags		#
#								#
#  Either "BSD" or "SYSV" *MUST* be defined			#
#								#
#  Oddball compiler/loader directives should also go here	#
#  On parallel machines, if h/machine.h does not define		#
#  the PARALLEL flag, it should be done here.			#
#								#
#################################################################

#VAX#SYSTEM	= -DBSD
#SYSV#SYSTEM	= -DSYSV
#VLD#SYSTEM	= -DSYSV -DVLDSYSV
# Silicon Graphics, -Zf if Weitek FP chip installed
#SGI#SYSTEM	= -DSYSV -Zf
#SGI#SYSTEM	= -DSYSV
#ALLIANT#SYSTEM	= -DBSD -ce
#GOULD#SYSTEM	= -DBSD -m 8
#XMP#SYSTEM	= -DSYSV -DPARALLEL
#XMP2.1#SYSTEM	= -DSYSV -DPARALLEL -holevel_0
#CRAY2#SYSTEM	= -DSYSV -DPARALLEL -Dcray
# Sun3, select correct floating point flag, or -fswitch if unsure (slower)
#SUN3#SYSTEM	= -DBSD -f68881
#SUN3#SYSTEM	= -DBSD -ffpa
#SUN3#SYSTEM	= -DBSD -fswitch
#PYRAMID#SYSTEM	= -DBSD -Dvoid=int
#ULTRIX1#SYSTEM	= -DBSD -Dvoid=int

#################################################################
#								#
#  System-Version information, for special handling within one	#
#  type of $SYSTEM.						#
#								#
#  Currently meaningful settings are:				#
#								#
#	SGI34	SGI release 3.4 and below, with EXCELAN TCP.	#
#	SGI35	SGI release 3.5 and above, with real TCP.	#
#	UNICOS20	Cray UNICOS 2.0, with TCP		#
#	SUN3		SUN UNIX version 3.2 or above		#
#								#
#################################################################

#SGI#SYSVERS	= SGI34
#SGI#SYSVERS	= SGI35
#4D#SYSVERS	= 4D32
#XMP#SYSVERS	= UNICOS20
#CRAY2#SYSVERS	= UNICOS20
#SUN#SYSVERS	= SUN3

#################################################################
#								#
#  Definitions specific to one portion of the CAD distribution,	#
#  but collected here for ease of configuration			#
#								#
#################################################################

#################################################################
#								#
#		Configuration for libpkg			#
#								#
#################################################################

#SGI34#LIBPKG_CONFIG	= -I/usr/include/EXOS -DSGI_EXCELAN
#SGI35#LIBPKG_CONFIG	= -I/usr/include/bsd -DBSD
#4D32#LIBPKG_CONFIG	= -I/usr/include/bsd -DBSD
#XMP#LIBPKG_CONFIG	= -I/usr/win-include -DBSD -DSHORT_IDENT -DCRAY
#CRAY2#LIBPKG_CONFIG	= -I/usr/win-include -DBSD -DSHORT_IDENT -DCRAY

#################################################################
#								#
#		Configuration for remrt				#
#								#
#################################################################

#SGI34#REMRT_CONFIG	= -I/usr/include/EXOS -DSGI_EXCELAN
#SGI35#REMRT_CONFIG	= -I/usr/include/bsd -DBSD
#4D32#REMRT_CONFIG	= -I/usr/include/bsd -DBSD
#XMP#REMRT_CONFIG	= -I/usr/win-include -DBSD -DSHORT_IDENT -DCRAY
#CRAY2#REMRT_CONFIG	= -I/usr/win-include -DBSD -DSHORT_IDENT -DCRAY

#################################################################
#								#
#		Configuration for rfbd				#
#								#
#################################################################

#SGI35#RFBD_CONFIG	= -I/usr/include/bsd -DBSD
#4D32#RFBD_CONFIG	= -I/usr/include/bsd -DBSD

#################################################################
#								#
#		Configuration for libfb				#
#								#
#  Specify all OPTIONAL framebuffer interfaces to attach.	#
#  Select from:							#
#	if_remote	Network access to remote display	#
#	if_adage	Adage RDS-3000 ("Ikonas")		#
#	if_sgi		SGI IRIS display			#
#	if_ptty		AT&T 5620 w/special Moss layer		#
#	if_sun		SUNView interface			#
#	if_ug		Ultra Graphics device			#
#	if_rat		Raster Technology One/80 (24-bit RGB)	#
#	if_4d		SGI 4D/60T "clover" workstation
#								#
#  Edit all three LIBFB_* lines below similarly.		#
#  NOTES:							#
#  1)  When TCP exists and if_remote is desired, set		#
#	LIBFB_OBJS = if_remote.o pkg.o				#
#	to gain access to the PKG routines.			#
#								#
#  2)  For SGI release 3.4 and below, but not 3.5 and above:	#
#	LIBFB_OBJS = if_remote.o ../libpkg/pkg.o sgiselect.o	#
#								#
#  3)  For SGIs which have MC 68010 CPUs (not Turbos or		#
#	30xx machines), be certain to uncomment -DMC_68010,	#
#	because the default is a 68020				#
#								#
LIBFB_CFILES	= if_remote.c \
		  if_ptty.c
#VAX#		  if_adage.c
#SGI#		  if_sgi.c
#SUN#		  if_sun.c
#4D32#		  if_4d.c
LIBFB_OBJS	= if_remote.o pkg.o \
		  if_ptty.o
#VAX#		  if_adage.o
#SGI35#		  if_sgi.o
#SGI34#		  if_sgi.o sgiselect.o
#SUN#		  if_sun.o
#4D32#		  if_4d.o
LIBFB_CONFIG	= -DIF_REMOTE \
		  -DIF_PTTY
#VAX#		  -DIF_ADAGE
#SGI#		  -DIF_SGI	# -DMC_68010
#SUN#		  -DIF_SUN
#4D32#		  -DIF_4D
#USNA#		  -DIF_ADAGE -DGM256
#								#
#################################################################

#################################################################
#								#
#		Configuration for librt				#
#								#
# Select one of:						#
#	timer42		4.2 BSD, 4.3 BSD (Berkeley UNIX)	#
#	timerunix	Generic UNIX, System III, V, etc.	#
#	timer52brl	For BRL's System 5-under-4.n BSD	#
#	timerhep	Denelcor HEP				#
#	timercray	Cray-X/MP UNICOS			#
LIBRT_TIMER	= timer42
#SYSV#LIBRT_TIMER	= timerunix
#XMP#LIBRT_TIMER	= timercray
#								#
#################################################################

#################################################################
#								#
#		Configuration for RT				#
#								#
#	Mostly useful for specification of multi-processing	#
#	versions of libc, etc.					#
#								#
#XMP#RT_CONFIG	= -L/lib/multi -lu
#CRAY2#RT_CONFIG	= -lmt
#4D32#RT_CONFIG	= 	-lbsd
#								#
#################################################################

#################################################################
#								#
#		Configuration for mged				#
#								#
# If you wish MGED usage logging, add				#
#	-DLOGFILE=/file/name to MGED_CONFIG			#
#								#
# At present, MGED operates on these types of			#
# display hardware by default:					#
#	dm-plot		any UNIX-plot filter			#
#	dm-tek		Tektronix 4014 and family		#
#	dm-tek4109	Tektronix 4109				#
# and also supports these optional display devices:		#
#	dm-vg		Vector General 3300			#
#	dm-mg		Megatek 7250				#
#	dm-ir		SGI IRIS 2400,3030  (uses lib -lgl2)	#
#	dm-sun		SUN 3.2 Pixwin				#
#	dm-rat		Raster Tech One/180,380			#
#	dm-rat80	Raster Tech One/80			#
#	dm-ps		E&S Picture System 300			#
#	dm-4d		Silicon Graphics 4D/60T
#	dm-mer		Megatek Merlin 9200	[ Marginal ]	#
#	dm-sab		Saber w/GOPS		[ Marginal ]	#
#								#
#  Specify all OPTIONAL display managers to attach.		#
#								#
#VAX#MGED_CFILES	= dm-mg.c dm-vg.c
#VAX#MGED_OBJS		= dm-mg.o dm-vg.o
#VAX#MGED_CONFIG	= -DDM_MG -DDM_VG
#VAX#MGED_LIBES		=
#
#4D32#MGED_CFILES	= dm-4d.c
#4D32#MGED_OBJS		= dm-4d.o
#4D32#MGED_CONFIG	= -G 64 -DDM_4D
#4D32#MGED_LIBES	= -lbsd -lgl
#
#SGI#MGED_CFILES	= dm-ir.c
#SGI#MGED_OBJS		= dm-ir.o
#SGI#MGED_CONFIG	= -DDM_IR	# -DMC_68010
#SGIXNS#MGED_CONFIG	= -DDM_IR -DNONBLOCK	# -DMC_68010
#SGI35#MGED_LIBES	= -lbsd -lgl2
#SGI34#MGED_LIBES	= ../libfb/sgiselect.o -lgl2
#								#
#SUN#MGED_CFILES	= dm-sun.c
#SUN#MGED_OBJS	= dm-sun.o
#SUN#MGED_CONFIG	= -DDM_SUNPW
#SUN#MGED_LIBES	= -lsuntool -lsunwindow -lpixrect
#								#
#  Pick the right script for generating the version string:	#
#  Depends on differing behaviors of "echo" command		#
#								#
MGED_VERS	= newvers.sh
#SYSV#MGED_VERS	= newvers5.sh
#VLD#MGED_VERS	= newversVLD.sh
#								#
#################################################################

#################################################################
#								#
#  Paths for programs, libraries, etc				#
#  Defined here to provide "optimal managerial flexibility"	#
#								#
#  Note:  Don't add comments after a definition, 		#
#	the extra white space can ruin some of the expansions.	#
#								#
#################################################################

CC		= /bin/cc
#4D32#CC	= /usr/bin/cc
#VLD#CC		= /usr/5bin/cc

SHELL		= /bin/sh

MAKE		= /bin/make

# Makefiles to print
MAKEFILE	= ../Makefile.defs ./Makefile.loc ../Makefile.rules ./Makefile

# Program to get name of current system
GETHOST		= hostname
#SYSV#GETHOST	= uname -n
#4D32#GETHOST	= hostname

# If BSD42, use ranlib on archives.  For SYSV, set to "echo" (except SGI).
RANLIB		= ranlib
#SYSV#RANLIB	= echo
#VLD#RANLIB	= ranlib
#SGI#RANLIB	= ranlib
#4D32#RANLIB	= ar ts

# Place to install binary executable
BINDIR		= /usr/brlcad/bin
#VLD#BINDIR	= /vld/bin

# Place to install (and find) libraries
LIBDIR		= /usr/brlcad/lib
#VLD#LIBDIR	= /vld/lib

# Place to install daemons and other non-user-command type programs
ETCDIR		= /usr/brlcad/etc

# Suffix for archives.  Won't change for most UNIX machines, but...
ARCH_SUF	= a
#XMP#ARCH_SUF	= o

# For utilities that wish to USE our libraries, here are the popular ones:
LIBFB		= ../libfb.'`sh ../mtype.sh`'/libfb.${ARCH_SUF}
LIBRT		= ../librt.'`sh ../mtype.sh`'/librt.${ARCH_SUF}
LIBPLOT		= ../libplot3.'`sh ../mtype.sh`'/libplot3.${ARCH_SUF}
LIBRLE		= ../librle.'`sh ../mtype.sh`'/librle.${ARCH_SUF}
LIBPKG		= ../libpkg.'`sh ../mtype.sh`'/libpkg.${ARCH_SUF}
LIBWDB		= ../libwdb.'`sh ../mtype.sh`'/libwdb.${ARCH_SUF}
LIBSPL		= ../libspl.'`sh ../mtype.sh`'/libspl.${ARCH_SUF}
LIBTERMLIB	= -ltermlib
#XMP#LIBTERMLIB	= -lcurses
LIBTERMIO	= ../libtermio.'`sh ../mtype.sh`'/libtermio.${ARCH_SUF}
LIBCURSOR	= ../libcursor.'`sh ../mtype.sh`'/libcursor.${ARCH_SUF}
LIBSYSV		= ../libsysv.'`sh ../mtype.sh`'/libsysv.${ARCH_SUF}
#SYSV#LIBSYSV	=

# For strictly serial machines, the Princeton malloc() in libmalloc
# uses address space and physical memory more efficiently than the
# Berkeley/CalTech malloc(), but with the same performance advantage.
# It works well on VAXen and Goulds and SGIs.  Sadly, it does not work
# on the SUNs.
# For parallel machines, the vendor's malloc() typically needs to be
# used, to handle parallel stack frame expansion, etc.  This is
# significant on the Cray and HEP, and perhaps others.
LIBMALLOC	= ../libmalloc/libmalloc.${ARCH_SUF}
#SUN#LIBMALLOC	=	# Forced to use vendor malloc
#XMP#LIBMALLOC	=	# Forced to use vendor malloc
#CRAY2#LIBMALLOC	=	# Forced to use vendor malloc

# Directory within CAD Package for header files
INCDIR		= ../h

# Directory where users can find installed copies of header files.
INCLUDE_DIR	= /usr/include/brlcad
#VLD#INCLUDE_DIR	= /vld/include

# Directory for Manual pages, when ${MANSECTION} appended
MANDIR		= /usr/man/man
#SYSV#MANDIR	= /usr/man/u_man/man
#VLD#MANDIR	= /usr/5lib/man/local/man

# Permissions for files to be installed
BINPERM		= 775
MANPERM		= 664
LIBPERM		= 664
DIRPERM		= 775

# Compiler Flags
PROFILE		=
CFLAGS		= -O ${PROFILE} -I${INCDIR} ${SYSTEM} ${CONFIGDEFS}
#4D#CFLAGS	= -O2 ${PROFILE} -I${INCDIR} ${SYSTEM} ${CONFIGDEFS}

# Loader Flags.  Note that ${CC} is used to invoke the loader
LDFLAGS 	= ${SYSTEM} ${PROFILE}
#4D#LDFLAGS 	= -O2 ${SYSTEM} ${PROFILE}

# Lint Flags
LINT		= lint
LFLAGS		= -hbvxac ${SYSTEM} ${CONFIGDEFS}
#SYSV#LFLAGS	= ${SYSTEM} ${CONFIGDEFS}

# Name of the object module archiver, for use with the loader.
AR		= ar
#XMP#AR	= ../cray-ar.sh

# Regular libraries.  Everybody gets the math library;  most need it.
LIBES		= -lm

# Lint libraries
LLIBES		= -lm

# Program to install executables
INS		= cp

# Command string to typeset manual pages
# Usage is:   ${TYPESET_MAN1} *.${MANSECTION} ${TYPESET_MAN2}
#
TYPESET_MAN1	= tbl /usr/lib/tmac/tmac.an
TYPESET_MAN2	= | /usr/5lib/troff -Ti300 | /usr/5bin/dimp -t | \
		    qpr -q im231
#TYPESET_MAN1	= tbl /usr/lib/tmac/tmac.an 
#TYPESET_MAN2	= | /usr/5lib/troff -Ti10 | /usr/5bin/di10 -t | \
		    qpr -q im328-raw
#TYPESET_MAN1	= troff -man
#TYPESET_MAN2	= 

#################################################################
#								#
#  Definition of master source machine, and master distribution	#
#  directory.  Please don't change these values.		#
#  When the current host is == MASTERHOST, the "make install"	#
#  rules will also handle the distribution source management.	#
#  Even if you have lots of disk space, you don't want that	#
#  to happen.							#
#								#
#################################################################
MASTERHOST	= brl-vgr
DISTRIBUTION	= /m/dist

#################################################################
#								#
#  Variables that will be defined in xxx/Makefile.loc		#
#								#
#################################################################

# Full names of final products
PRODUCTS	= product

# Names of local headers that contribute to product
HEADERS 	=

# Names of C source files that contribute to product
CFILES		= cfiles.c

# Names of Shell files that contribute to product
SHFILES		=

# Section of manual to install pages in.
# All the manual pages in a given directory MUST be in the same section.
MANSECTION	= 99

# ASCII Test files:  input, and expected output
TFILES		=

# Any Configuration-specific definitions to pass to ${CC}, ie, -Dfoo=1
CONFIGDEFS	=

# Name of directory that holds the sources
SRCDIR		= xxx

# If special things (like ranlib) need doing after "make install",
# xxx/Makefile.loc should set AFTERINSTALL=${RANLIB}
AFTERINSTALL	= echo
#								#
#################################################################
