#################################################################
#								#
#			Makefile.rules				#
#								#
#  Author -							#
#	Michael John Muuss					#
#								#
#  Source -							#
#	SECAD/VLD Computing Consortium, Bldg 394		#
#	The U. S. Army Ballistic Research Laboratory		#
#	Aberdeen Proving Ground, Maryland  21005		#
#								#
#  $Header$	#
#								#
#################################################################

#  Define default list of targets to be made
all:		${PRODUCTS}

#	Generic SCCS rule:
#.DEFAULT:
#	$(GET) $(GFLAGS) -p s.$@ > $@
#	touch $@

# This is how to make the product, but may not always apply (eg, libs).
# Also, this construct only works with the SysV make.
#${PRODUCTS}:	$$@.o ${OBJS}
#	$(CC) -o $@ ${LDFLAGS} $@.o ${OBJS} ${LIBES} 
#	size $@

# Format sources for printing on stdout.
# The invoker is responsible for diverting to their printer of choice.
print:		${MAKEFILE} ${HEADERS} ${CFILES} ${TFILES}
	@pr ${MAKEFILE} ${HEADERS} ${CFILES} ${TFILES} *.${MANSECTION}
	@nroff -Tlp -man *.${MANSECTION}

# Don't bother doing the stub pages
typeset:
	${TYPESET_MAN1} *.${MANSECTION} ${TYPESET_MAN2}

lint:		${HEADERS} ${CFILES}
	${LINT} ${LFLAGS} -I${INCDIR} ${CFILES} ${LLIBES} > ${SRCDIR}.lint


tags:		${HEADERS} ${CFILES}
	ctags ${HEADERS} ${CFILES}

flow:		${HEADERS} ${CFILES}
	cflow -I${INCDIR} ${CFILES} > ${SRCDIR}.flow

xref:		${HEADERS} ${CFILES}
	cxref -c -s -w132 -I${INCDIR} ${CFILES} > ${SRCDIR}.xref

compare:	all
	for prod in ${PRODUCTS} ; \
		do	cmp ${BINDIR}/$$prod $$prod; \
		done

uninstall:	all
	-@if test -n "${PRODUCTS}" ; \
	then	$(MAKE) $(MFLAGS) -$(MAKEFLAGS) uninst-prod ; \
	fi
	-@files=`echo *.${MANSECTION}` ; \
	if test "$$files" != '*'.${MANSECTION} ; \
	then	$(MAKE) $(MFLAGS) -$(MAKEFLAGS) uninst-man ; \
	fi
	-@files=`echo *.${MANSECTION}.stub` ; \
	if test "$$files" != '*'.${MANSECTION}.stub ; \
	then	$(MAKE) $(MFLAGS) -$(MAKEFLAGS) uninst-stub ; \
	fi

uninst-prod:
	-@for prod in ${PRODUCTS} ; \
	do	rm -f ${BINDIR}/$$prod ${BINDIR}/$$prod.bak 2>/dev/null; \
	done

uninst-man:
	-@for man in *.${MANSECTION} ; \
	do	rm -f ${MANDIR}${MANSECTION}/$$man \
		    ${MANDIR}${MANSECTION}/$$man.bak 2>/dev/null ; \
	done

uninst-stub:
	-@for stub in *.${MANSECTION}.stub ; \
	do	man=`basename $$stub .stub`; \
		rm -f ${MANDIR}${MANSECTION}/$$man \
		    ${MANDIR}${MANSECTION}/$$man.bak 2>/dev/null ; \
	done

# Handle all phases of final installation
install:	all
	-@if test -n "${PRODUCTS}" ; \
	then	$(MAKE) $(MFLAGS) -$(MAKEFLAGS) inst-prod ; \
	fi
	-@files=`echo *.${MANSECTION}` ; \
	if test "$$files" != '*'.${MANSECTION} ; \
	then	$(MAKE) $(MFLAGS) -$(MAKEFLAGS) inst-man ; \
	fi
	-@files=`echo *.${MANSECTION}.stub` ; \
	if test "$$files" != '*'.${MANSECTION}.stub ; \
	then	$(MAKE) $(MFLAGS) -$(MAKEFLAGS) inst-stub ; \
	fi
	-@sh -c "if test `${GETHOST}` = ${MASTERHOST} ; then $(MAKE) inst-dist; else exit 0 ; fi"

# Install all products
inst-prod:	${PRODUCTS}
	-@for prod in ${PRODUCTS} ; \
	do	if cmp -s ${BINDIR}/$$prod $$prod; \
		 then	echo $$prod unchanged; \
		 else	mv -f ${BINDIR}/$$prod ${BINDIR}/$$prod.bak 2>/dev/null; \
			${INS} $$prod ${BINDIR} && \
			${AFTERINSTALL} ${BINDIR}/$$prod && \
			echo "++++++INSTALLED" ${BINDIR}/$$prod && \
			${AFTERINSTALL} ${BINDIR}/$$prod.bak && \
			chmod ${BINPERM} ${BINDIR}/$$prod; \
		 fi \
	done

# Install the actual manual pages
inst-man:
	-@for man in *.${MANSECTION} ; \
	do	if cmp -s ${MANDIR}${MANSECTION}/$$man $$man; \
		 then	echo $$man unchanged; \
		 else	mv -f ${MANDIR}${MANSECTION}/$$man \
			    ${MANDIR}${MANSECTION}/$$man.bak 2>/dev/null ; \
			${INS} $$man ${MANDIR}${MANSECTION} && \
			echo "++++++INSTALLED" ${MANDIR}${MANSECTION}/$$man && \
			chmod ${MANPERM} ${MANDIR}${MANSECTION}/$$man; \
		 fi \
	done

# Install the "stub" manual pages, which just source some other page
inst-stub:
	-@for stub in *.${MANSECTION}.stub ; \
	do	man=`basename $$stub .stub`; \
		if cmp -s ${MANDIR}${MANSECTION}/$$man $$stub; \
		 then	echo $$man unchanged; \
		 else	mv -f ${MANDIR}${MANSECTION}/$$man \
			    ${MANDIR}${MANSECTION}/$$man.bak 2>/dev/null ; \
			${INS} $$stub ${MANDIR}${MANSECTION}/$$man && \
			echo "++++++INSTALLED" ${MANDIR}${MANSECTION}/$$man && \
			chmod ${MANPERM} ${MANDIR}${MANSECTION}/$$man; \
		 fi \
	done


# It might be better to say ${HEADERS} ${CFILES} ${SFILES},
# but that would miss routines that were not configured in a particular system.
inst-dist:
	-rm -fr ${DISTRIBUTION}/${SRCDIR}/*
	-${INS} ./Makefile.loc Makefile *.[hcfs] ${SHFILES} \
		*.${MANSECTION} *.${MANSECTION}.stub \
		*.doc \
		${DISTRIBUTION}/${SRCDIR}/.

#  running "make clobber" or "make clean noprod" will remove all binary files.
#  "clean" is intended to remove .o files and by-products.
#  "noprod" is intended to remove the final executables.
#  Either can be used without the other, depending on the effect desired.
clobber:	clean noprod

clean:
	-rm -f core *~ *.o \
		${SRCDIR}.out ${SRCDIR}.err ${SRCDIR}.xref \
		${SRCDIR}.flow ${SRCDIR}.lint

noprod:
	-rm -f ${PRODUCTS}


#
#  Automaticly build #include dependencies.
#
#  This code can't be simply replaced by the 4.3 BSD "cc -M".
#
#  The convention for writing #include directives is:
#	#include "./file"	gives file in current directory
#	#include "/file"	gives file at absolute path
#	#include "file"		gives file in ${INCDIR}
#	#include <file>		leaves to CPP
#
#  Note that there MUST be a SPACE after the "#include" in order to
#  be seen;  this allows conditional includes (if needed) to be indicated
#  by a #include TAB, so as to not cause trouble.
depend:
	-@if test -n "${CFILES}" ; \
	then	$(MAKE) $(MFLAGS) -$(MAKEFLAGS) do-depend ; \
	fi

do-depend:
	cd ../${SRCDIR}; \
	mv -f Makefile.loc Makefile.bak; \
	cp Makefile.bak Makefile.loc; \
	chmod 664 Makefile.loc; \
	for k in ${CFILES}; do \
		i=`basename $$k .c`; \
		echo $$i.o: ../${SRCDIR}/$$k >>makedep; \
		grep '^#include ' /dev/null ../${SRCDIR}/$$k | sed \
			-e 's,^../'${SRCDIR}'/,,' \
			-e 's,c:[^"]*"\./\([^"]*\)".*,o: \1,' \
			-e 's,c:[^"]*"/\([^"]*\)".*,o: /\1,' \
			-e 's,c:[^"]*"\([^"]*\)".*,o: '${INCDIR}'/\1,' \
			-e '/debug.h/d' \
			-e '/c:[^<]*<\(.*\)>/d' \
			>>makedep; \
		echo '	$$(CC) $$(CFLAGS) -c ../'${SRCDIR}/$$k >>makedep; \
	done; \
	echo '# DO NOT DELETE THIS LINE -- make depend uses it' >>Makefile.loc; \
	echo '# IF YOU PUT STUFF HERE IT WILL GO AWAY' >> Makefile.loc; \
	echo '/^# DO NOT DELETE THIS LINE/+1,$$d' >eddep; \
	echo '$$r makedep' >>eddep; \
	echo 'w' >>eddep; \
	ed - Makefile.loc < eddep; \
	rm eddep makedep; \
	echo '# DEPENDENCIES MUST END AT END OF FILE' >> Makefile.loc; \
	echo '# IF YOU PUT STUFF HERE IT WILL GO AWAY' >> Makefile.loc; \
	echo '# see make depend above' >> Makefile.loc
