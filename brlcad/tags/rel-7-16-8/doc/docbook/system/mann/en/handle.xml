<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2008-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR AS IS'' AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='handle1'>

<refmeta>
  <refentrytitle>HANDLE</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>handle</refname>
  <refpurpose>
    Creates a BRL-CAD .g file containing handles.
  </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>handle</command>    
    <arg choice='opt' rep='repeat'><replaceable>options</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>
    <command>handle</command> is a program to create a BRL-CAD database of
    handles.  Up to twenty-six handles of the same dimensions may be created.
    <command>handle</command> uses libwdb to create a BRL-CAD database file.
    The handles are composed of three cylinders, two tori, and two arb8s.  This
    program may be run interactively or the user may specify options on a command
    line.  If the user chooses to run the program interactively he answers the
    questions as the program prompts him.  Below are the options that can be used
    on the command line.
 </para>
</refsect1>


<refsect1 id='options'>
  <title>OPTIONS</title>
  <variablelist remap='TP'>
    <varlistentry>
      <term><option>-fname.g</option></term>
      <listitem>
	<para>
         BRL-CAD file name.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-n#</option></term>
      <listitem>
	<para>
          The number of handles to be creates (must be less than or equal to 26).
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-l#</option></term>
      <listitem>
	<para>
         Length of handle in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-h#</option></term>
      <listitem>
	<para>
         Height of handle in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-r1#</option></term>
      <listitem>
	<para>
         Radius, r1, of the tori in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-r2#</option></term>
      <listitem>
	<para>
         Radius, r2, of the tori in millimeters.
        </para>
      </listitem>
    </varlistentry>
  </variablelist>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <example>
    <title>Interactive <command>handle</command> Session</title>
    <para>
    <literallayout>
This program constructs a handle with the base centered
at (0, 0, 0) and the height extending in the positive z-
direction.  The handle will be composed of 3 cylinders, 
2 tori, and 2 arb8s.

Enter the name of the mged file to be created (25 char max).
        handle.g
Enter number of handles to create (26 max).
        3
Enter the length and height of handle in mm.
        200 100
Enter the radius of the tori in mm.
        20 
Enter the radius of the cylinders in mm.
        10

mged file name:  handle.g
length:  200.000000 mm
height:  100.000000 mm
radius of tori:  20.000000 mm
radius of cylinders:  10.000000 mm
number of handles:  3
   </literallayout>
    </para>
  </example>

  <example>
    <title>Single-Line <command>handle</command> Command</title>
    <para>
      <userinput>handle -fhandle.g -n3 -l200 -h100 -r120 -r210</userinput>
    </para>
  </example>

  <para>
   Both of these examples produce the same result - a database called
   handle.g containing three handles of length 200mm, height 100mm, radius
   20mm fo rthe rounding, and radius 10mm for the cylinders.
  </para>

</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>Susan A. Coates</para>
</refsect1>

<refsect1 id='copyright'>
  <title>COPYRIGHT</title>
  <para>
    This software is Copyright (c) 2005-2010 United States Government as
    represented by the U.S. Army Research Laboratory. All rights reserved.
  </para>
</refsect1>

<refsect1 id='see_also'>
  <title>SEE ALSO</title>
  <para>
   bolt(1), window(1), window_frame(1), gastank(1)
  </para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

