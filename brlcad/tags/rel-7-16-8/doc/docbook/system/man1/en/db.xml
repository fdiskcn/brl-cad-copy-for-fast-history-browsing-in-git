<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='db'>
  
  <refmeta>
    <refentrytitle>DB</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>db</refname>
    <refpurpose>
      Provides an interface to a number of database manipulation
      routines.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>db</command>    
      <arg choice='req'><replaceable>command</replaceable></arg>
      <arg choice='opt'><replaceable>arguments</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      Provides an interface to a number of database manipulation
      routines. Note that this command always operates in units of millimeters. The
      <emphasis>command</emphasis> must be one of the following with appropriate arguments:
    </para>
    
    <variablelist>
      <varlistentry>
	<term><emphasis>match [regular_exp]</emphasis></term>
	<listitem>
	  <para>
	    Returns a list of all objects in that database that match the list of regular
	    expressions.
	  </para>
	</listitem>
      </varlistentry>
      
      <varlistentry>
	<term><emphasis>get shape_or_path [attribute]</emphasis></term>
	<listitem>
	  <para>
	    Returns information about the primitive shape at the end of the <emphasis>shape_or_path</emphasis>.
	    If a path is specified, the transformation matrices encountered along that path will
	    be accumulated and applied to the leaf shape before displaying the information. If
	    no <emphasis>attribute</emphasis> is specified, all the details about the shape are returned. 
	    If a specific <emphasis>attribute</emphasis> is listed, then only that information is returned.
	  </para>
	</listitem>
      </varlistentry>
      
      <varlistentry>
	<term><emphasis>put shape_name shape_type attributes</emphasis></term>
	<listitem>
	  <para>
	    Creates shape named <emphasis>shape_name</emphasis> of type <emphasis>shape_type</emphasis>
	    with attributes as listed in <emphasis>attributes</emphasis>. The arguments to the 
	    <emphasis>put</emphasis> command are the same as those returned by the <emphasis>get</emphasis> 
	    command.
	  </para>
	</listitem>
      </varlistentry>
      
      <varlistentry>
	<term><emphasis>adjust shape_name attribute new_value1 [new_value2 new_value3...]</emphasis></term>
	<listitem>
	  <para>
	    Modifies the shape named <emphasis>shape_name</emphasis> by adjusting the value of its 
	    <emphasis>attribute</emphasis> to the <emphasis>new_values</emphasis>.
	  </para>
	</listitem>
      </varlistentry>
      
      <varlistentry>
	<term><emphasis>form object_type</emphasis></term>
	<listitem>
	  <para>
	    Displays the format used to display objects of type <emphasis>object_type</emphasis>.
	  </para>
	</listitem>
      </varlistentry>
      
      <varlistentry>
	<term><emphasis>tops</emphasis></term>
	<listitem>
	  <para>Returns all top-level objects.</para>
	</listitem>
      </varlistentry>
      
      <varlistentry>
	<term><emphasis>close</emphasis></term>
	<listitem>
	  <para>Closes the previously opened database and deletes the associated command.</para>
	</listitem>
      </varlistentry>
    </variablelist> 
  </refsect1>
  
  <refsect1 id='examples'>
    <title>EXAMPLES</title>
    <para>
      The following examples show the uses of the <command>db</command> command to list various 
      objects and attributes from the database, as well as to create new TGC shapes, adjust a vertex attribute,
      and display command formats.
    </para>
    <example>
      <title>List all objects in the database that end with ".s"</title>
      <para><prompt>mged></prompt> <userinput>db match *.s</userinput></para>
      <para>Gets a list of all objects in the database that end with ".s".</para>
    </example>
    
    <example>
      <title>List all attributes and their values for <emphasis>cone.s</emphasis></title>
      <para><prompt>mged></prompt><userinput>db get cone.s</userinput></para>
      <para>
	Gets a list of all the attributes and their values for shape <emphasis>cone.s</emphasis>.
      </para>
    </example>
    
    <example>
      <title>Get the value of the vertex attribute of <emphasis>cone.s</emphasis></title>
      <para><prompt>mged></prompt><userinput>db get cone.s V</userinput></para>
      <para>
	Gets the value of the <emphasis>V</emphasis> (vertex) attribute of shape <emphasis>cone.s</emphasis>
      </para>  
    </example>
    
    <example>
      <title>Create a new TGC shape named <emphasis>new_cone.s</emphasis> with specified attributes.</title>
      <para><prompt>mged></prompt><userinput>db put new_cone.s tgc V {0 0 0} H {0 0 1} A {1 0 0} B {0 1 0} C {5 0 0} D {0 5 0}</userinput></para>
      <para>
	Creates a new TGC shape named <emphasis>new_cone.s</emphasis> with the specified attributes.
      </para> 
    </example>
    
    <example>
      <title>Adjust the vertex attribute of <emphasis>new_cone.s</emphasis> to a given value.</title>
      <para><prompt>mged></prompt><userinput>db adjust new_cone.s V {0 0 10}</userinput></para>
      <para>
	Adjusts the <emphasis>V</emphasis> (vertex) attribute of <emphasis>new_cone.s</emphasis> 
	to the value {0 0 10}.
      </para> 
    </example>
    
    <example>
      <title>Display the format used by the <command>get</command> and <command>put</command> commands 
      for the TGC shape type.</title>
      <para><prompt>mged></prompt><userinput>db form tgc</userinput></para>
      <para>
	Displays the format used by the <command>get</command> and <command>put</command> commands 
	for the TGC shape type.
      </para> 
    </example>
  </refsect1>
  
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>BRL-CAD Team</para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
    </para>
  </refsect1>
</refentry>

