/*                           S Y N . C
 * BRL-CAD
 *
 * Copyright (C) 2004-2005 United States Government as represented by
 * the U.S. Army Research Laboratory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this file; see the file named COPYING for more
 * information.
 */
/** @file syn.c
 *  Multi Sine Synthesis
 */
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include "./complex.h"

#define TABSIZE	512
double	sintab[TABSIZE];

static char usage[] = "\
Usage: syn samples_per_set [ratio] > doubles\n";

void makesintab(void);

int main(int argc, char **argv)
{
	int	i;
	double	d;
	double	period, stepsize, findex;
	int	setsize;

	if( isatty(fileno(stdout)) || argc < 2 ) {
		fprintf( stderr, usage );
		exit( 1 );
	}

	makesintab();
	fprintf(stderr, "init done\n");

	setsize = atoi(argv[1]);

	findex = 0;
	stepsize = 0;
	while( fread(&period, sizeof(period), 1, stdin) == 1 ) {
		if( period > 0 )
			stepsize = TABSIZE / period;
		for( i = setsize; i > 0; i-- ) {
			d = sintab[(int)findex];
			d *= 0.4;
			fwrite( &d, sizeof(d), 1, stdout );
			findex += stepsize;
			if( findex > TABSIZE )
				findex -= TABSIZE;
		}
	}
	return 0;
}

void
makesintab(void)
{
	int	i;
	double	theta;

	for( i = 0; i < TABSIZE; i ++ ) {
		theta = i / (double)TABSIZE * TWOPI;
		sintab[i] = sin(theta);
	}
}

/*
 * Local Variables:
 * mode: C
 * tab-width: 8
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 * ex: shiftwidth=4 tabstop=8
 */
