/*
 *			librt/Cakefile
 */
#define SRCDIR	librt
#define PRODUCTS	librt.ARCH_SUF
#define	SRCSUFF	.c
#define MANSECTION	3

#include "../Cakefile.defs"

/* Separate into orderly groupings:
 *	Raytrace logic
 *	Database handling
 *	Geometry routines
 *	Utility routines
 */
#define FILES	\
	shoot \
	cut bool \
	bomb \
	db_open db_scan db_lookup db_io db_alloc db_walk \
	db_path db_tree db_comb db_anim db_match \
	db5_scan \
	memalloc \
	table dir tree prep \
	g_arb g_ars g_ebm g_ell g_half g_pg g_rec g_sph g_nurb \
	g_tgc g_torus g_vol g_arbn g_part g_pipe g_nmg \
	g_rpc g_rhc g_epa g_ehy g_eto g_grip g_hf g_dsp g_sketch g_extrude \
	g_submodel g_cline g_bot \
	cmd mater roots spectrum \
	rtlex vlist regionfix htbl \
	nmg_mk nmg_misc nmg_bool nmg_eval nmg_inter nmg_plot nmg_ck \
	nmg_mesh nmg_fcut nmg_class nmg_mod nmg_index \
	nmg_rt_isect nmg_rt_segs nmg_manif \
	nmg_visit nmg_info nmg_pr \
	nmg_extrude nmg_tri nmg_fuse nmg_pt_fu \
	wdb fortray tcl \
	mkbundle bundle many \
	pr LIBRT_TIMER storage global \
	rt_dspline \
        nurb_basis nurb_bound nurb_diff nurb_eval nurb_flat \
        nurb_knot nurb_norm nurb_poly nurb_ray nurb_refine \
        nurb_solve nurb_split nurb_util nurb_xsplit nurb_copy \
        nurb_c2 oslo_calc oslo_map nurb_plot nurb_bezier nurb_trim \
	nurb_interp nurb_reverse nurb_tess nurb_trim_util pmalloc \
	wdb_obj view_obj dg_obj vdraw wdb_comb_std

/* Oddly, this line needs to be present to keep IBM AIX cpp happy. */
#ifdef FAST_SQRT
#	define	EXTRA_OBJ	FAST_SQRT
#else
#	define	EXTRA_OBJ	/**/
#endif

#define VERSION_VARIABLE	rt_version
#define LIBRARY_TITLE		"The BRL-CAD Ray-Tracing Library"

#include "../Cakefile.lib"
#include "../Cakefile.rules"
