WCHAR_ERROR_MESSAGE
ERROR: wchar_t has incorrect size (%0d). Should be: %1d

VERSION_BUILD
BRL-CAD Export option added to Pro/E Version %0s, build %1s

OPEN_FILE_ERROR
ERROR: Cannot open file (%0s)

NO_CURRENT_OBJECT
ERROR: There is no current object

NO_INFO_FOR_OBJECT
ERROR: Cannot get information on current object

USER_CURRENT_OBJECT_NAME
Current object is %0s

USER_GET_OUTPUT_FILE_NAME
Enter name for output file [%0s]:

UNKNOWN_OBJECT_TYPE
ERROR: Unknown object type (%0s) for object (%1s)

CANNOT_OPEN_TMP_FILE
ERROR: Cannot open temporary file (%0s) for reading

PROE_TO_BRL_DONE
Finished, use BRL-CAD tool "proe-g" to convert ascii file to BRL-CAD

USER_GET_QUALITY
Enter quality for facetization (1-10) [%0d]:

USER_EXPORT_FAILED
ERROR: Export of Render format to file (%0s) for part (%1s) failed

USER_TEMPNAM
ERROR: Failed to create temporary file for part %0s

NO_UNITS_FOR_MODEL
ERROR: Failed to get model units (assuming mm)

ASSEM_CUTS
WARNING: part %0s has %1d assembly cuts (you may need to edit this part)

USER_GET_CHORD_HEIGHT
Enter maximum chord height error for facetization (in model units) [%0f]:

USER_GET_ANGLE_CONTROL
Enter angle control value for additional improvement on small radii curves [%0f]:

