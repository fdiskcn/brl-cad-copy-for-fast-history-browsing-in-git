/*
 *			D M - P L O T . C
 *
 *  Routines specific to MGED's use of LIBDM's Plot display manager.
 *
 *  Author -
 *	Robert G. Parker
 *  
 *  Source -
 *	SLAD CAD Team
 *	The U. S. Army Research Laboratory
 *	Aberdeen Proving Ground, Maryland  21005
 */

#ifndef lint
static const char RCSid[] = "@(#)$Header$ (BRL)";
#endif

#include "conf.h"

#include <stdio.h>
#include <sys/time.h>		/* for struct timeval */
#include "machine.h"
#include "externs.h"
#include "bu.h"
#include "vmath.h"
#include "mater.h"
#include "raytrace.h"
#include "./ged.h"
#include "./mged_dm.h"
#include "dm-plot.h"

extern void dm_var_init();

int
Plot_dm_init(o_dm_list, argc, argv)
struct dm_list *o_dm_list;
int argc;
char *argv[];
{
  dm_var_init(o_dm_list);

  if((dmp = dm_open(interp, DM_TYPE_PLOT, argc, argv)) == DM_NULL)
    return TCL_ERROR;

  return TCL_OK;
}
