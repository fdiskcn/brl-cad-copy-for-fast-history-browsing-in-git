.\" $Header$
.\"	groff -ms README | print-postscript
.TL
README
.sp
The BRL-CAD Package
.br
Release 4.4
.br
(October-1994)
.RT
.IP "VERSION"
.sp
This document is printed from the README file in the BRL-CAD distribution.
The version in the printed manuals are usually somewhat older
than the version on the actual distribution tape.
When attempting to
install the software, please refer to the documentation
versions actually contained on the distribution tape.
.sp
.IP CONTENTS
.sp
This README file exists only to provide you a brief roadmap to
the contents of the BRL-CAD distribution tape.
The information on how to read the tape,
install and operate the software,
and perform the benchmark tests have become sufficiently large
that they are now each a standalone document.
.sp
Please note that
this distribution package does \fInot\fR include
the various military-specific model
analysis tools such as MUVES, GIFT, SAR, SLAVE, VAST, etc., nor does it
include any military databases.
If you desire to have access to this material,
please contact Keith Applin at (410)-278-6647.
.sp
.IP "INSTALLATION"
.sp
To install the BRL-CAD Package see the instructions in doc/install.doc.
This file includes directions on the best way to read the distribution
tape,
as well as details on how to configure and install the software.
This file also includes directions on how to compile and run
the benchmark tests.
.sp
.IP "BENCHMARK"
.sp
See the paper in doc/benchmark.doc for information on
analyzing the results of the benchmark tests.
Comparisons between various different computers are listed.
.sp
.IP "BACKGROUND MATERIAL"
.sp
For overview information on the whole package, consult
the overview paper in the file papers/overview, and
the brlcad(1) manual page in the file util/brlcad.1.
For a discussion of the significance of this software,
read the paper "Understanding the Preparation and Analysis of Solid
Models", included in the file papers/solid-models.
.sp
.IP "DISTRIBUTION DETAILS"
.sp
This software is governed by a limited distribution agreement.
To obtain a copy of the BRL-CAD Package distribution materials, there
are two options:
.sp
1.  Free distribution with no support privileges:  Those users with
online access to the InterNet may obtain the BRL-CAD Package via
FTP file transfer, at no cost, after completing and returning a signed
copy of the printed distribution agreement. A blank agreement form is
available only via anonymous FTP from host ftp.arl.mil (address
128.63.16.158) from file "brl-cad/agreement". One printed set of BRL-CAD
documentation will be mailed to you at no cost. No installation
assistance or telephone support will be available.
.sp
2.  Full service distribution:  The Survivability/Vulnerability
Information Analysis Center (SURVIAC) administers the supported BRL-CAD
distributions and information exchange programs for ARL.  Full service
distributions a copy of the full distribution materials on your choice
of magnetic tape media, or you may elect to obtain your copy via FTP.
One printed set of BRL-CAD documentation will be mailed to you.
Software upgrades will be provided at no additional charge,
and you will have access to full technical assistance by phone, FAX,
letter, or E-mail.  Agencies of the Federal Government may acquire the
full service distribution with a simple MIPR or OGA funds transfer.
.sp
.nf
For further details, contact:
.sp .5
	BRL-CAD Distribution
	SURVIAC Aberdeen Satellite Office
	1003 Old Philadelphia Road
	Suite 103
	Aberdeen MD  21001  USA
.fi
.sp .5
or call Ms. Carla Moyer at USA (410)-273-7794, send
E-mail to <cad-dist@arl.mil>, FAX your letter to USA (410)-272-6763,
or write to the above address.
.fi
.sp
.ne 1i
.IP "COMMUNICATION"
.sp
You are invited to participate in the InterNet (aka "ARPANET")
electronic mailing list on the BRL-CAD software, which is called
.sp .5
.ti +1i
<CAD @ ARL.MIL>.
.sp .5
Bug reports and discussions of new features
are the main topics;  volume of messages has been light (so far).
Direct your bug reports to this address.  Request a subscription by
sending to
.sp .5
.ti +1i
<CAD-REQUEST @ ARL.MIL>.
.sp .5
If you find bugs, please report your experiences via E-mail.
.sp
A variety of information on BRL-CAD is available via the World-Wide-Web.
Papers and documentation can be found at
.sp .5
.ti +1i
http://web.arl.mil/software/brl-cad/
.sp .5
The software repository can be found at
.sp .5
.ti +1i
http://ftp.arl.mil/ftp/brl-cad/
.sp
.IP "FUTURE EVENTS"
.sp
It is expected that new releases of this software will be issued
between once and twice a year.
Information about new releases will be routinely provided
by both paper and electronic mail
to recipients of this software.
If your address changes, please let us
know, so we can update our records.
.sp
.KS
.nf
Best Wishes,
 -Mike Muuss
.sp
Leader, Advanced Computer Systems Team
The US Army Research Laboratory
APG, MD  21005-5068  USA
.sp
E-mail:  <Mike @ ARL.MIL>
.KE
.fi
