.\" Set the interparagraph spacing to 1 (default is 0.4)
.PD 1v
.\"
.\" The man page begins...
.\"
.TH LIBREDBLACK 3 BRL/CAD
.\"
.SH NAME
libredblack \- red-black tree operations
.\"
.SH SYNOPSIS
\fB#include "machine.h"
.br
\fB#include "rtlist.h"
.br
\fB#include "redblack.h"
.\"
.PP
.B rb_tree *rb_create (description, nm_orders, order_funcs)
.br
.B char *description;
.br
.B int nm_orders;
.br
.B int (**order_funcs)();
.\"
.PP
.B rb_tree *rb_create1 (description, order_func)
.br
.B char *description;
.br
.B int (*order_func)();
.\"
.PP
.B void *rb_curr (tree, order)
.br
.B rb_tree *tree;
.br
.B int order;
.\"
.PP
.B void rb_delete (tree, order)
.br
.B rb_tree *tree;
.br
.B int order;
.\"
.PP
.B void rb_diagnose_tree (tree, order, trav_type)
.br
.B rb_tree *tree;
.br
.B int order;
.br
.B int trav_type;
.\"
.PP
.B void *rb_extreme (tree, order, sense)
.br
.B rb_tree *tree;
.br
.B int order;
.br
.B int sense;
.\"
.PP
.B void rb_free (tree, free_data)
.br
.B rb_tree *tree;
.br
.B void (*free_data)();
.\"
.PP
.B int rb_insert (tree, data)
.br
.B rb_tree *tree;
.br
.B void *data;
.\"
.PP
.B int rb_is_uniq (tree, order)
.br
.B rb_tree *tree;
.br
.B int order;
.\"
.PP
.B void *rb_neighbor (tree, order, sense)
.br
.B rb_tree *tree;
.br
.B int order;
.br
.B int sense;
.\"
.PP
.B int rb_rank (tree, order)
.br
.B rb_tree *tree;
.br
.B int order;
.\"
.PP
.B void *rb_search (tree, order, data)
.br
.B rb_tree *tree;
.br
.B int order;
.br
.B void *data;
.\"
.PP
.B void *rb_select (tree, order, k)
.br
.B rb_tree *tree;
.br
.B int order;
.br
.B int k;
.\"
.PP
.B void rb_set_uniqv (tree, vec)
.br
.B rb_tree *tree;
.br
.B bitv_t vec;
.\"
.PP
.B void rb_summarize_tree (tree)
.br
.B rb_tree *tree;
.\"
.PP
.B void rb_uniq_all_off (tree)
.br
.B rb_tree *tree;
.\"
.PP
.B void rb_uniq_all_on (tree)
.br
.B rb_tree *tree;
.\"
.PP
.B int rb_uniq_off (tree, order)
.br
.B rb_tree *tree;
.br
.B int order;
.\"
.PP
.B int rb_uniq_on (tree, order)
.br
.B rb_tree *tree;
.br
.B int order;
.\"
.PP
.B void rb_walk (tree, order, visit, trav_type)
.br
.B rb_tree *tree;
.br
.B int order;
.br
.B void (*visit)();
.br
.B int trav_type;
.\"
.\"
.SH DESCRIPTION
These routines implement red-black trees,
a form of balanced binary trees,
in such a way that all the basic dynamic set operations
(e.g., insertion, deletion, search, minimum, maximum,
predecessor, and successor)
and order-statistic operations
(i.e., select and rank)
require no more than
.IR "O(" "log " "n)"
time,
where
.I n
is the number of nodes.
They allow storage of arbitrary data structures
at tree nodes
and also support multiple simultaneous orders (trees)
on the same nodes.
The trees are based on comparison functions
like those used by
.I qsort(3).
.PP
.I rb_create
allocates storage for
and initializes
the tree header.
.I Description
is an explanatory comment that
.I libredblack
prints in its diagnostics,
.I nm_orders
is the number of simultaneous orders,
and
.I order_funcs
points to the table of comparison functions
(one for each order).
These are called with two arguments
that point to the application data blocks being compared.
Each function must return an integer
less than, equal to, or greater than zero
according as the first argument is to be considered
less than, equal to, or greater than the second.
.I rb_create
returns a pointer to
an
.I rb_tree.
This pointer must be saved,
as it is a required argument to all the other routines.
.I rb_create1
is similar,
except that it creates a tree that supports only the single order
specified by
.I order_func.
.PP
The application can specify that
.I libredblack
may not insert new nodes that compare equal in any of the orders
to an existing node.
Such uniqueness enforcement is switch selectable
and may be controlled independently for each order
and modified dynamically.
The default behavior is not to enforce any uniqueness.
.PP
.I rb_free
relinquishes the storage used by
.I tree,
calling
.I free_data
to dispose of the application data in the nodes.
If
.I free_data
equals
.I RB_RETAIN_DATA
(defined in \fI"redblack.h"\fR),
then the application data blocks are left unaffected.
Otherwise,
.IR rb_free " calls " free_data
once for each block of application data,
passing a pointer to the data.
Since
.I rb_create1
allocates its own table of comparison functions,
a memory leak will result if
a tree returned by
.I rb_create1
is freed before this table is freed.
For this reason,
.I "redblack.h"
provides the macro
.I rb_free1(tree, free_data),
which should be used instead of
.I rb_free
when relinquishing a tree created by
.I rb_create1.
.PP
.I rb_insert
creates a new node containing
.I data
and adds it to
.I tree,
provided that doing so would not violate current uniqueness requirements.
If a uniqueness requirement would be violated,
.I rb_insert
does nothing but return a negative integer,
the absolute value of which is the first order for which a violation exists.
Otherwise,
the node is inserted in the appropriate place
in each order,
as determined by the comparison functions,
and
.I rb_insert
returns the number of orders
for which the new node compared equal to an existing node in the tree.
.PP
.I rb_uniq_on
specifies that subsequent insertion of nodes into
.I tree
should enforce uniqueness on
.I order,
and returns the previous setting of the switch.
.I rb_uniq_off
specifies that subsequent insertion of nodes into
.I tree
should proceed without regard for uniqueness on
.I order,
and returns the previous setting of the switch.
The macros
.I rb_uniq_on1(tree)
and
.I rb_uniq_off1(tree)
available in
\fI"redblack.h"\fR,
are similar,
except that they control the first (perhaps only) order.
.I rb_is_uniq
returns 1 if uniqueness is currently enforced
for
.I order
in
.I tree,
and 0 otherwise.
The macro
.I rb_is_uniq1(tree)
available in
\fI"redblack.h"\fR,
is similar,
except that it queries the first (perhaps only) order.
.I rb_uniq_all_on
and
.I rb_uniq_all_off
set all
.I nm_orders
orders identically on or off,
and
.I rb_set_uniqv
sets the orders according to the bit vector
.I vec.
.PP
.I rb_extreme
searches through
.I tree
to find a minimum or maximum node in one of the orders
as determined by the corresponding comparison function.
.I Sense
is either
.I SENSE_MIN
or
.I SENSE_MAX,
and
.I order
specifies which order to search.
.I rb_extreme
returns a pointer to the extreme data.
The macros
.I rb_min(tree, order)
and
.I rb_max(tree, order),
available in
\fI"redblack.h"\fR,
are implemented in terms of
.I rb_extreme
in the obvious way.
.PP
.I rb_search
traverses
.I tree
searching for a node of which the contents equals
.I data
according to the comparison function
specified by
.I order.
On success,
.I rb_search
returns a pointer to the data in the
matching node.
Otherwise, it returns
.I NULL.
The macro
.I rb_search1(tree, data),
available in
\fI"redblack.h"\fR,
is similar,
except that it searches the first (perhaps only) order.
.PP
.I rb_select
traverses
.I tree
to retrieve the \fIk\fRth order statistic
(i.e.,
the data block of rank
.I k,
the \fIk\fRth-smallest data block)
according to the comparison function
specified by
.I order,
where
.I k
is between 1 and the number of nodes in
.I tree,
inclusive.
On success,
.I rb_select
returns a pointer to the block of data of rank
.I k.
Otherwise, it returns
.I NULL.
The macro
.I rb_select1(tree, k),
available in
\fI"redblack.h"\fR,
is similar,
except that it uses the first (perhaps only) order.
.PP
.I rb_walk
traverses
.I tree
according to the comparison function specified by
.I order.
The function
.I visit
is called for each node in turn,
being passed two arguments:
a pointer to the data at that node
and the depth of the node in the tree for the specified order.
The type of tree traversal to perform,
specified by
.I trav_type,
may be any one of
.I PREORDER, INORDER,
and
.I POSTORDER.
The macro
.I rb_walk1(tree, visit, trav_type),
available in
\fI"redblack.h"\fR,
is similar,
except that it walks the first (perhaps only) order.
.PP
.I rb_diagnose_tree
traverses
.I tree
according to the comparison function specified by
.I order,
printing information about the various structures.
The application may optionally store in the
.I rbt_print
member of the
.I rb_tree
structure
the address of an application-specific print routine.
If this pointer is nonzero,
.I rb_diagnose_tree
dereferences it to print information for the data at each node.
The type of tree traversal to perform,
specified by
.I trav_type,
may be any one of
.I PREORDER, INORDER,
and
.I POSTORDER.
.PP
The
.I rb_tree
structure contains a pointer to
the node most recently accessed
(e.g., inserted, discovered in a search, or selected by rank).
When the most recent access failed,
this current node is undefined.
The following commands make use of
the current node:
.PP
.I rb_curr
returns a pointer to the data in the current node in 
.I order,
or
.I NULL
if the current node is undefined.
The macro
.I rb_curr1(tree),
available in
\fI"redblack.h"\fR,
is similar,
except that it returns a pointer to the data in the current node
in the first (perhaps only) order.
.PP
.I rb_delete
removes a block of application data from
.I tree.
Because the algorithms sometimes cause a single block of data
to be stored in different nodes for the different orders,
the application specifies
.I order,
which indicates the block of data
(in the current node) to be removed.
If the current node is defined,
.I rb_delete
removes this block of data from every order.
Otherwise,
it prints a warning and returns.
The macro
.I rb_delete1(tree),
available in
\fI"redblack.h"\fR,
is similar,
except that it removes the block of data in the first (perhaps only) order.
.PP
.I rb_neighbor
returns a pointer to the data in the node adjacent (in \fIorder\fR) to
the current node,
or
.I NULL
if the current node is undefined.
.I sense,
which may be one of
.I SENSE_MIN
and
.I SENSE_MAX,
specifies either predecessor or successor, respectively.
The macros
.I rb_pred(tree, order)
and
.I rb_succ(tree, order),
available in
\fI"redblack.h"\fR,
are implemented in terms of
.I rb_neighbor
in the obvious way.
.\"
.PP
.I rb_rank
returns the the rank
(i.e., position expressed as an integer between
1 and the number of nodes in
.I tree,
inclusive)
of the current node in
.I order,
or
.I NULL
if the current node is undefined.
The macro
.I rb_rank1(tree),
available in
\fI"redblack.h"\fR,
is similar,
except that it uses the first (perhaps only) order.
.\"
.PP
The members
of the
.I rb_tree
structure,
as defined in
\fI"redblack.h"\fR,
are classified into three classes
based on their suitability for direct manipulation by applications.
Class I,
members that applications may read directly,
includes
.PP
    long rbt_magic;     /* Magic no. for integrity check */
    int  rbt_nm_nodes;  /* Number of nodes */
.PP
Class II,
members that applications may read or write directly
as necessary,
includes
.PP
    void (*rbt_print)();   /* Data pretty-print function */
    int  rbt_debug;        /* Debug bits */
    char *rbt_description; /* Comment for diagnostics */
.PP
Class III comprises
members that applications should not manipulate directly;
any access should be through the routines provided by
.I libredblack.
They include
.PP
    int            rbt_nm_orders;   /* Number of orders */
    int            (**rbt_order)(); /* Comparison funcs */
    struct rb_node **rbt_root;      /* The actual trees */
    char           *rbt_unique;     /* Uniqueness flags */
    struct rb_node *rbt_current;    /* Current node */
    struct rb_list rbt_nodes;       /* All nodes */
    struct rb_list rbt_packages;    /* All packages */
    struct rb_node *rbt_empty_node; /* Sentinel for nil */
.PP
The distinction between classes I and III is not critical,
but any direct modification of members in either class
will result in unpredictable (probably dire) results.
The order of the members within the
.I rb_tree
structure
is subject to change in future releases.
.PP
Diagnostic output may be requested
by setting the debug bits in the
.I rb_tree
structure
using the debug bit flags defined in
\fI"redblack.h"\fR.
.\"
.SH SEE ALSO
qsort(3)
.\"
.SH AUTHOR
Paul Tanenbaum
