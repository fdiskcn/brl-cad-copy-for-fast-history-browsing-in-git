Date:     Wed, 25 Nov 87 11:00:03 EST
From:     "Gary S. Moss" (VLD/VMB) <moss@BRL.ARPA>

	The only way (I believe) to pick an object on a Tektronix is to
use the crosshairs to point to it, and hit (I believe) the space bar to
select it.  If this does not work, than either the software is at fault
or your emulators are imperfect.  We don't have every conceivable device
here to test it out on, and barely enough time to answer your questions.
I would suggest looking at the mged/dm-tek.c and mged/dm-tek4109.c and
see if the proper escape codes are being sent to your device to handle
the crosshairs.  We have pretty much gotten rid of all Tektronix style
devices here in favor of IRIS work stations and Suns, so there is not
much interest in maintaining support for Tektronix, though we will be
happy to propogate any fixes you may have.

Here are MGED commands as requested:
< run mged with a new model (e.g., newmod0.g)
	mged newmod0.g
< create an arb5 primitive name 'prim1'
	make prim1 arb5
< translate, rotate, and scale 'prim1' to slightly off center
	press oill
	<select "prim1" with crosshairs>
	translate 1.0 1.0 1.0
	rotobj 5.0 5.0 5.0
	scale 1.5
< create an arb7 primitive name 'prim2'
	make prim2 arb7
< combine 'prim1' and 'prim2' into an object named 'prims'
	g prims prim1 prim2
< enter object edit state, editing 'prims'
	press oill
	<select "prims" with crosshairs>
< translate, rotate, and scale object 'prims' to slightly off center
	translate 1.0 1.0 1.0
	rotobj 5.0 5.0 5.0
	scale 2.0
< leave object edit state
	press accept
< create an arbN named 'my_1'
< create another arbN named 'my_2'
	<There are no arbN's.>
< create a region named 'both' which includes both (u?) 'my_1' and 'my_2'
	r both + my_1 u my_2
< create a region named 'first' which subtracts 'my_2' from 'my_1' (1-2?)
	r first + my_1 - my_2
< create a region named 'second' which is the intersection (+?)
< of 'my_2' and 'my_1'
	r second + my_1 + my_2
< assign a different item, material, color, air to each of the three 
< regions
	edcodes both first second
< combine 'prims', 'both', 'first', and 'second' into an object named
< 'obj2'
	g obj2 prims both first second
< save the model
< quit mged
	q

Also, in case you haven't discovered it, typing a command name with no
arguments will often give a usage message, and typing a '?' as an
argument will sometimes give a list of possible ones:

mged> press
Usage: press button_label
        (emulate button press)
mged> press ?
 ?: no match
 top, bottom, right, left, front, 
 rear, restore, save, adc, reset, 
 45,45, 35,25, oill, sill, oscale, 
 oxscale, oyscale, ozscale, ox, oy, 
 oxy, orot, accept, reject, slice, 
 sedit, srot, sxy, sscale, mged>

Hope this helps,
moss
========================================
Date: Wed, 25 Nov 87 08:12:56 CST
From: Jim Bozek <bozek%uxe.cso.uiuc.edu@UXC.CSO.UIUC.EDU>

	I recently installed the BRL CAD distribution on a Pyramid 90x. The
bench mark images were successfully generated and other software (i.e., the
utilities, mged, lgt, etc.) seem to be working properly. Thank you for this.

	I am having a problem in that I can not seem to get mged to go into
object state. I use either a Tektronix 4014 emulator on the Macintosh or a
real honest to goodness Tektronix 4207 which emulates a 4109. Attachment to
the Tek 4109 is done properly, pictures are drawn, etc. Solid edit state can
be entered using 'sed', etc. I can't seem to find an object analogy to 'sed'
other than 'press oill' which opens up the door to the pick, path, states, 
etc. from which I can't seem to do anything except 'press reject'. I am not
using a tablet or a pen (is a pen required or can this also be emulated with
key presses?). The only cursor that ever comes up on the screen is the
angle/distance cursor when I toggle it on with 'press adc'.

	Could you please give me some advice? Even better, could you send me,
or us on the network, a (very simple) script of mged commands which would 
lead us through an editing session. For example:

	run mged with a new model (e.g., newmod0.g)
	create an arb5 primitive name 'prim1'
	translate, rotate, and scale 'prim1' to slightly off center
	create an arb7 primitive name 'prim2'
	combine 'prim1' and 'prim2' into an object named 'prims'
	enter object edit state, editing 'prims'
	translate, rotate, and scale object 'prims' to slightly off center
	leave object edit state
	create an arbN named 'my_1'
	create another arbN named 'my_2'
	create a region named 'both' which includes both (u?) 'my_1' and 'my_2'
	create a region named 'first' which subtracts 'my_2' from 'my_1' (1-2?)
	create a region named 'second' which is the intersection (+?)
		of 'my_2' and 'my_1'
	assign a different item, material, color, air to each of the three 
		regions
	combine 'prims', 'both', 'first', and 'second' into an object named
		'obj2'
	save the model
	quit mged

	Such an example would illustrate most of what is required to create 
a model, and would help me immeasurably. Thank you for your time and, again,
thank you for the software.

						Jim Bozek
========================================
Date:     Wed, 27 Jan 88 4:04:16 EST
From:     Mike Muuss <mike@BRL.ARPA>

Howard Walter has decided that the BRL CAD Package is not software that
the BRL Software Support crew is willing to support, or even attempt to
answer questions about.  Therefore, he has ruled that all the installed
programs that comprise the BRL CAD Package should be moved from their
current directory of /usr/brl/bin to some other place.

That other place will be /usr/brlcad/bin.  Associated with it will be
/usr/brlcad/lib and /usr/brlcad/etc for related pieces. The directory
/usr/include/brlcad will remain, unchanged.

After 1-March-88, BRL CAD Package commands (including MGED, RT, LGT,
RLE-FB, etc) will no longer be found in /usr/brl/bin, but will instead
be found only in /usr/brlcad/bin.  For the intervening weeks, existing
versions will continue to be found in /usr/brl/bin, while new versions
of the software will begin to be installed in /usr/brlcad/bin.

If you do not use any of this software, you do not need to do anything.
If you DO use this software, you need only to make a one-time change.
Please do it today.  The change is as follows:

- = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - =

*)  For Bourne Shell (/bin/sh) users [this is the default Shell], you need
to edit the file ".profile" in your home directory.  It will have a line
in it that looks something like:

PATH=/usr/brl/bin:/usr/ucb:/bin:/usr/bin:/usr/local/bin:/usr/aos/bin:/usr/5bin::

You want to insert the string "/usr/brlcad/bin:" at the very front,
(note the trailing colon ":")  right after the "=" sign, so that
the string now reads:

PATH=/usr/brlcad/bin:/usr/brl/bin:/usr/ucb:/bin:/usr/bin:/usr/local/bin:/usr/aos/bin:/usr/5bin::

     ^^^^^^^^^^^^^^^^ -- here is where the insertion was done.

- = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - =

*)  For C-Shell (/bin/csh) and T-C-Shell (/usr/brl/bin/tcsh) users, you need
to edit the file ".cshrc" in your home directory.  It will have a line in
it that looks something like:

set path=(/usr/brl/bin /usr/ucb /bin /usr/bin /usr/local/bin . /usr/5bin)

You want to insert the string "/usr/brlcad/bin " at the very front,
(note the trailing space) right after the open paren "(", so that
the string now reads:

set path=(/usr/brlcad/bin /usr/brl/bin /usr/ucb /bin /usr/bin /usr/local/bin . /usr/5bin)

          ^^^^^^^^^^^^^^^^ -- here is where the insertion was done.

- = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - = - =

For either Shell, after you have made this change, log out, and log back
in, and you will be all set.

Please be careful not to delete any of the other directories named in
the list.  If you encounter any difficulties, contact the system
administrator on the machine that you are having trouble with.
Also note that you will have to perform this editing operation on
each and every machine that you use.

I hope that you will find this Administration-mandated change fairly
easy to cope with.  If this presents undue hardship, please notify
Howard.

	Best,
	 -Mike
========================================
Date: Wed, 13 Apr 88 4:50:17 EDT
From: Mike Muuss <mike@BRL.ARPA>

The BRLCAD Package had quite a bit of software in it, all written in C,
and targeted mainly for UNIX.  It includes:

Hardware independent and network transparent framebuffer library,
image processing tools,
full constructive solid geometry (CSG) modeling package, including:
	editor,
	ray-tracing library
	two rendering packages
	procedural database interface

and a variety of other good things.  It has been distributed to over 300
sites now, and is well received.
	Best,
	 -Mike
========================================
Date:     Tue, 3 Nov 87 4:36:13 EST
From:     Mike Muuss <mike@BRL.ARPA>

Release 2.3 of the BRL CAD Package has been finalized this evening.
The new executables, header files, and libraries have been isntalled on
the VAX (VGR), the Cray-2 (Bob), the XMP (Patton), all the Suns served
by SPARK, and the Goulds (VMB and SEM).  As usual, RANLIB on the SGI
blew up, and the Alliant is *still* compiling, so they don't have this
new release installed yet.

Now begins the tedious chore of registering and producing the zillions
of distribution requests currently outstanding. To help in this process,
I have formalized the location of the tape list on VGR as
/m/cad/tapes/tapes, with a (draft) version of the procedure for
checking, registering, writing, and sending a distribution in the
file /m/cad/tapes/procedure.  BRL folks, please send me your comments.

UPGRADING FROM RELEASE 2.0

Primarily, this release is a maintenance release, with lots of little
nits resolved, along with a few new features.  The major weak area is
still Sun display support.  In particular, using RT from within MGED
on a Sun, fails miserably.

This release contains different and better support for multiple lights
in RT, along with a standardized shader interface, and stackable
shaders. Support for polygonal objects is now correct, but slow. The
spline code has been significantly improved. Some contributed code for
the Raster Technologies One/80 has been included, but is untested.  The
library for procedurally generating databases ``libwdb'' is now
included, along with some example uses in the directory ``proc-db''.
The program ``rtwalk'' generates a viewpoint animation control script
that takes the eye between given start and end points, without walking
through any geometry.  This is especially interesting when used on
complex scenes like those made by proc-db/clutter.c.

WORK IN PROGRESS

We are hard at work on a variety of new features, the most significant are:

*) a substantial package for animation specification and preview,

*) code to replace the "big-E" command, to convert the CSG model into
a polygonal approximation, for certain analysis codes, and to drive
display hardware with fast polygon rendering,

*) additional primitive types,

*) MGED improvements, including merging common database-handling code
with librt, and a consolidation of the MGED and LIBRT geometry modules.

*) Significant signal and image-processing capabilities

Many of these should be out in the fabled "next release" :-)

	Best,
	 -Mike
========================================
Date: Sat, 27 Feb 88 17:52:28 EDT
From: Graphics Devlopment Group <munnari!cidam.rmit.oz.au!graphics@uunet.UU.NET>

I have have some difficulty understanding exactly what to expect
as the result of combinations.

take the following example:

	units inches
	in box1 box 0 0 0 10 0 0 0 5 0 0 0 5
	in box2 box 8 2.5 2.5 4 0 0 0 2.5 0 0 0 2.5
	in cyl rcc 10 -1.0 2.5 0 10 0 1

as the input to mged.

The intention is to make an object which is the union of the two boxes
then subtract the cylinder from that union.

Now we've tried a number of variations including:

	r foo u box1 u box2 - cyl

but this merely subtracts the cylinder from box2, not a hole through
the union.

	r foo u box2 u box1 - cyl

give the inverse.

It was thought that perhaps by defing a sub-group to define the union

	g foo box1 box2
	r bar u foo - cyl

may give the correct answer, but alas it doesn't.

Using a sub-region, not only fails to give the correct answer, but 
gives an obviously bug-related bogus answer.

	r foo u box1 u box2
	r bar u foo - cyl

The correct answer can be achieved by:

	r test u box1 - cyl u box2 - cyl

This is not a very elegant solution, in fact it is rather clumsy. In a
complicated model, it could cause a high overhead.

So, please tell me what is wrong here  It is obvious that the solution
is out there as I cannot believe that those complicated models in the
sample pixel files were built in this (clumbsy) way.  

The crux of the matter appears to me to be how to (easily) define
the hierarchy/precedence of modeling operations.
========================================
Date:     Tue, 1 Mar 88 8:35:30 EST
From:     Keith Applin (VLD/VMB) <keith@BRL.ARPA>

The problem here is how regions are to be evaluated.  By definition, the
first operation of a region must be intersection "+" or union "u". If
"+" is first, then evaluation of the region's operation/member combinations
proceeds sequentially.  Unfortunately, for historical reasons, if the
first operation is a "u", then the evaluation becomes a sequence of
nested evaluations between unions.  For example:

	r foo u s1 - s2 - s3 u s5 - s2 u s6 - s3

is evaluated as

	u ( + s1 - s2 - s3 ) u ( + s5 - s2 ) u ( + s6 - s3 )

Given this framework, your results are as expected and the region:

	r test u box1 - cyl u box2 - cyl

gives the desired result.  I am puzzled that your other "approaches"
of first creating a union of the boxes (either a group or a region)
and then subtracting the cylinder didn't work....I believe they should.

