/*
 *	S H _ S C L O U D . C
 *
 *	A 3D "solid" cloud shader
 */
#include "conf.h"

#include <stdio.h>
#include <math.h>
#include "machine.h"
#include "vmath.h"
#include "raytrace.h"
#include "./material.h"
#include "./mathtab.h"
#include "./rdebug.h"
#define M_PI            3.14159265358979323846

#define CLAMP(_x,_a,_b)	(_x < _a ? _a : (_x > _b ? _b : _x))
#define FLOOR(x)	(  (int)(x) - (  (x) < 0 && (x) != (int)(x)  )  )
#define CEIL(x)		(  (int)(x) + (  (x) > 0 && (x) != (int)(x)  )  )

struct scloud_specific {
	double	lacunarity;
	double	h_val;
	double	octaves;
	double	thresh;
	vect_t	delta;
	point_t	scale;	/* scale coordinate space */
	mat_t	xform;
};

static struct scloud_specific scloud_defaults = {
	2.1753974,	/* lacunarity */
	1.0,		/* h_val */
	4.0,		/* octaves */
	1.0,		/* threshold for opacity */
	{ 0.0, 0.0, 0.0 },	/* delta */
	{ 1.0, 1.0, 1.0 },	/* scale */
	};

#define SHDR_NULL	((struct scloud_specific *)0)
#define SHDR_O(m)	offsetof(struct scloud_specific, m)
#define SHDR_AO(m)	offsetofarray(struct scloud_specific, m)

struct structparse scloud_pr[] = {
	{"%f",	1, "lacunarity",	SHDR_O(lacunarity),	FUNC_NULL },
	{"%f",	1, "H", 		SHDR_O(h_val),	FUNC_NULL },
	{"%f",	1, "octaves", 		SHDR_O(octaves),	FUNC_NULL },
	{"%f",	1, "thresh",		SHDR_O(thresh),	FUNC_NULL },
	{"%f",  3, "scale",		SHDR_AO(scale),	FUNC_NULL },
	{"%f",  3, "delta",		SHDR_AO(delta),	FUNC_NULL },
	{"",	0, (char *)0,		0,			FUNC_NULL }
};
struct structparse scloud_parse[] = {
	{"%f",	1, "lacunarity",	SHDR_O(lacunarity),	FUNC_NULL },
	{"%f",	1, "H", 		SHDR_O(h_val),	FUNC_NULL },
	{"%f",	1, "octaves", 		SHDR_O(octaves),	FUNC_NULL },
	{"%f",	1, "thresh",		SHDR_O(thresh),	FUNC_NULL },
	{"%f",  3, "scale",		SHDR_AO(scale),	FUNC_NULL },
	{"%f",  3, "delta",		SHDR_AO(delta),	FUNC_NULL },
	{"%f",	1, "l",			SHDR_O(lacunarity),	FUNC_NULL },
	{"%f",	1, "o", 		SHDR_O(octaves),	FUNC_NULL },
	{"%f",	1, "t",			SHDR_O(thresh),	FUNC_NULL },
	{"%f",  3, "s",			SHDR_AO(scale),	FUNC_NULL },
	{"%f",  3, "d",			SHDR_AO(delta),	FUNC_NULL },
	{"",	0, (char *)0,		0,			FUNC_NULL }
};

HIDDEN int	scloud_setup(), scloud_render();
HIDDEN void	scloud_print(), scloud_free();

struct mfuncs scloud_mfuncs[] = {
	{"scloud",	0,		0,		MFI_NORMAL|MFI_HIT|MFI_UV,
	scloud_setup,	scloud_render,	scloud_print,	scloud_free },

	{(char *)0,	0,		0,		0,
	0,		0,		0,		0 }
};



/*
 *	S C L O U D _ S E T U P
 */
HIDDEN int
scloud_setup( rp, matparm, dpp, mfp, rtip )
register struct region *rp;
struct rt_vls	*matparm;
char	**dpp;	/* pointer to reg_udata in *rp */
struct mfuncs		*mfp;
struct rt_i		*rtip;
{
	register struct scloud_specific *scloud;
	struct db_full_path full_path;
	mat_t	region_to_model;
	mat_t	model_to_region;
	mat_t	tmp;

	RT_VLS_CHECK( matparm );
	GETSTRUCT( scloud, scloud_specific );
	*dpp = (char *)scloud;

	memcpy(scloud, &scloud_defaults, sizeof(struct scloud_specific) );
	if( rdebug&RDEBUG_SHADE)
		rt_log("scloud_setup\n");

	if( rt_structparse( matparm, scloud_parse, (char *)scloud ) < 0 )
		return(-1);

	if( rdebug&RDEBUG_SHADE)
		rt_structprint( rp->reg_name, scloud_parse, (char *)scloud );

	/* get transformation between world and "region" coordinates */
	if (db_string_to_path( &full_path, rtip->rti_dbip, rp->reg_name) ) {
		/* bad thing */
		rt_bomb("db_string_to_path() error");
	}
	if(! db_path_to_mat(rtip->rti_dbip, &full_path, region_to_model, 0)) {
		/* bad thing */
		rt_bomb("db_path_to_mat() error");
	}

	/* get matrix to map points from model (world) space
	 * to "region" space
	 */
	mat_inv(model_to_region, region_to_model);


	/* add the noise-space scaling */
	mat_idn(tmp);
	tmp[0] = 1. / scloud->scale[0];
	tmp[5] = 1. / scloud->scale[1];
	tmp[10] =  1. / scloud->scale[2];

	mat_mul(scloud->xform, tmp, model_to_region);

	/* add the translation within noise space */
	mat_idn(tmp);
	tmp[MDX] = scloud->delta[0];
	tmp[MDY] = scloud->delta[1];
	tmp[MDZ] = scloud->delta[2];
	mat_mul2(tmp, scloud->xform);

	return(1);
}

/*
 *	S C L O U D _ P R I N T
 */
HIDDEN void
scloud_print( rp, dp )
register struct region *rp;
char	*dp;
{
	rt_structprint( rp->reg_name, scloud_pr, (char *)dp );
}

/*
 *	S C L O U D _ F R E E
 */
HIDDEN void
scloud_free( cp )
char *cp;
{
	rt_free( cp, "scloud_specific" );
}

/*
 *	S C L O U D _ R E N D E R
 */
int
scloud_render( ap, pp, swp, dp )
struct application	*ap;
struct partition	*pp;
struct shadework	*swp;
char	*dp;
{
	register struct scloud_specific *scloud_sp =
		(struct scloud_specific *)dp;
	point_t in_pt, out_pt, pt;
	vect_t	v_cloud;
	double	thickness;
	int	steps;
	double	step_delta;
	int	i;
	double  val;
	double	transmission;

	RT_CHECK_PT(pp);
	RT_AP_CHECK(ap);
	RT_CK_REGION(pp->pt_regionp);

#if 0
	MAT4X3PNT(in_pt, scloud_sp->xform, swp->sw_hit.hit_point);
		val = noise_fbm(in_pt, scloud_sp->h_val, 
			scloud_sp->lacunarity, scloud_sp->octaves );
 	VSET(swp->sw_color, val, val, val);
	return 1;
#endif
	VJOIN1(in_pt, ap->a_ray.r_pt, pp->pt_inhit->hit_dist, ap->a_ray.r_dir);
	VJOIN1(out_pt, ap->a_ray.r_pt, pp->pt_outhit->hit_dist, ap->a_ray.r_dir);

	/* transform point into "noise-space coordinates" */
	MAT4X3PNT(in_pt, scloud_sp->xform, in_pt);
	MAT4X3PNT(out_pt, scloud_sp->xform, out_pt);

	VSUB2(v_cloud, out_pt, in_pt);
	thickness = MAGNITUDE(v_cloud);
	steps = (int)(thickness * 3.0);

	step_delta = thickness / (double)steps;

	VUNITIZE(v_cloud);

	VMOVE(pt, in_pt);
	transmission = 1.0;
	for (i=0 ; i < steps ; i++ ) {
		/* compute the next point in the cloud space */
		VJOIN1(pt, in_pt, i*step_delta, v_cloud);

		val = noise_fbm(pt, scloud_sp->h_val, 
			scloud_sp->lacunarity, scloud_sp->octaves );

		val = (val+1.) * .5;

		if (val > scloud_sp->thresh) {

			transmission -= 1-val;
			if (transmission < 0.000001) {
				transmission = 0.;
				break;
			}
		}
	}

	transmission = CLAMP(transmission, 0.0, 1.0);

	swp->sw_transmit = transmission;

/* 	VSET(swp->sw_color, transmission, transmission, transmission); */

	return(1);
}
