#ifndef _RISE_H
#define _RISE_H

#include "tie.h"
#include "adrt_common.h"

#define	RISE_OP_CAMERA		0

#define	RISE_PIXEL_FMT		0	/* 0 == unsigned char, 1 ==  tfloat */

#define	RISE_OBSERVER_PORT	1982

#define	RISE_USE_COMPRESSION	0

#define	RISE_NET_OP_INIT	0
#define	RISE_NET_OP_FRAME	1
#define	RISE_NET_OP_QUIT	2
#define	RISE_NET_OP_SHUTDOWN	3

#define RISE_VER_KEY		0
#define RISE_VER		"0.1.3"
#define RISE_VER_DETAIL		"RISE 0.1.3 - Copyright (C) U.S Army Research Laboratory (2003 - 2005)"

#endif
