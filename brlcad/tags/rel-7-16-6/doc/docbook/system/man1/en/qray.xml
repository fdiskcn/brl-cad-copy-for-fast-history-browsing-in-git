<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='qray'>

<refmeta>
  <refentrytitle>QRAY</refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>qray</refname>
  <refpurpose>Gets/sets <emphasis>query ray</emphasis> characteristics.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>qray</command>    
   <arg><replaceable>subcommand</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Gets/sets <emphasis>query ray</emphasis> characteristics. Without a subcommand, the usage message is
printed. The <emphasis>qray</emphasis> command accepts the following subcommands:</para>
<variablelist>
  <varlistentry><term><command>vars</command></term> 
	<listitem>
	  <para> Prints a list of all query ray variables.  
	 </para> 
     </listitem> 
  </varlistentry>

  <varlistentry><term><command>basename</command> [<emphasis>str</emphasis>]</term> 
    <listitem>
       <para> If <emphasis>str</emphasis> is specified, then set basename to <emphasis>str</emphasis>. 			Otherwise, return the basename. Note that the basename is the name used to create the fake shape 		names corresponding to the query ray. There will be one fake shape for every color used along the 		ray.
  	 </para> 
   </listitem>
  </varlistentry>
  
<varlistentry><term><command>effects</command>[<emphasis>t|g|b</emphasis>]</term> 
   <listitem>
      <para>Set or get the type of <emphasis>effects</emphasis> that will occur when firing a query ray. The 		effects of firing a ray can be either <emphasis>t</emphasis> for textual output, <emphasis>g			</emphasis> for graphical output, or <emphasis>b</emphasis> for both textual and graphical.   
      </para> 
   </listitem> 
  </varlistentry>

<varlistentry><term><command>echo</command> [<emphasis>0|1</emphasis>]</term> 
   <listitem>
 	<para> Sets or gets the value of <emphasis>echo</emphasis>.  If set to 1, the actual <command>nirt		</command> command used will be echoed to the screen.  
	</para>
   </listitem> 
</varlistentry>

<varlistentry><term><command>oddcolor</command> [<emphasis>r g b</emphasis>]</term> 
   <listitem>
      <para>Sets or gets the color of odd partitions.
	</para>
   </listitem> 
</varlistentry>

<varlistentry><term><command>evencolor</command> [<emphasis>r g b</emphasis>]</term> 
   <listitem>
 	<para>Sets or gets the color of even partitions. 
	</para> 
   </listitem> 
</varlistentry>

<varlistentry><term><command>voidcolor</command> [<emphasis>r g b</emphasis>]</term> 
   <listitem>
 	<para>Sets or gets the color of areas where the ray passes through nothing.
	</para>
   </listitem> 
</varlistentry>

<varlistentry><term><command>overlapcolor</command> [<emphasis>r g b</emphasis>]</term> 
   <listitem>
	<para>Sets or gets the color of areas that overlap.
	</para> 
   </listitem> 
</varlistentry>

<varlistentry><term><command>fmt</command> [<emphasis>r|h|p|f|m|o</emphasis> [<emphasis>str</emphasis>]]</term> 
   <listitem>
  	<para>Sets or gets the format string(s). See the <emphasis>man</emphasis> page of <command>nirt</command> 	for more details. </para> 
   </listitem>
</varlistentry>

<varlistentry><term><command>script</command> [<emphasis>str</emphasis>]</term> 
   <listitem>
  	<para>Sets or gets the <command>nirt</command> script string.
	</para> 
   </listitem> 
</varlistentry>

<varlistentry><term><command>help</command></term> 
   <listitem>
 	<para>Prints the usage message.
	</para> 
   </listitem> 
</varlistentry>

</variablelist>

</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The four examples show the use of the <command>qray</command> command without a subcommand and with three of the listed subcommands.
    </para>
  
       
  <example>
    <title>Print the usage message.</title>
    <para>
      <prompt>mged></prompt><userinput>qray</userinput>
    </para>
    <para>Prints the usage message.
    </para>
  </example>
 
 <example>
    <title>Return the overlap format string.</title>
    <para>
      <prompt>mged></prompt><userinput>qray fmt o</userinput>
    </para>
    <para>Returns the overlap format string.
    </para>
  </example>

<example>
    <title>Return the rgb color used to color odd partitions.</title>
    <para>
      <prompt>mged></prompt><userinput>qray oddcolor</userinput>
    </para>
    <para>Returns the rgb color used to color odd partitions.
    </para>
  </example>

<example>
    <title>Set the odd partition color to red.</title>
    <para>
      <prompt>mged></prompt><userinput>qray oddcolor 255 0 0</userinput>
    </para>
    <para>Sets the odd partition color to red.
     </para>
  </example>



</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

