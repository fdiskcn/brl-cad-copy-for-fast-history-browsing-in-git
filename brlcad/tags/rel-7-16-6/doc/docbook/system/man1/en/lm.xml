<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='lm'>

<refmeta>
  <refentrytitle>LM</refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>lm</refname>
  <refpurpose>The command with no values argument lists the name of every region in the
	database (in alphabetical order), except for those marked as hidden with the <command>hide</command>
	command. If the values argument is supplied, only those regions with a
	�MUVES_Component� attribute having one of the values are listed.
  </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>lm</command>    
    
    <arg>-l</arg>
    <arg><replaceable>values</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>The command with no values argument lists the name of every region in the
	database (in alphabetical order), except for those marked as hidden with the <command>hide</command>
	command. If the values argument is supplied, only those regions with a
	�MUVES_Component� attribute having one of the values are listed. The �l option
	specifies to use a long format showing object name, object type, major type, minor
	type, and length.
  </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The first example shows the use of the <command>lm</command> command to list all regions having a 	MUVES_Component attribute with a specified value. The second example shows the use of the <command>lm	</command> command to list all regions having a "MUVES_Component" attribute and one of two specified values. 	The -l option is also used to show the long format.  
  </para>
  <example>
    <title>List all regions having a "MUVES_Component" attribute and a specific value.</title>
    <variablelist>
      <varlistentry>
	   <term><prompt>mged></prompt> <userinput>lm engine</userinput></term>
	   <listitem>
	     <para>Lists all regions with a �MUVES_Component� attribute having a value of �engine.�
	      </para>
	   </listitem>
      </varlistentry>
    </variablelist>
  </example>
       
  <example>
    <title>List (using the long format) all regions having a "MUVES_Component" attribute and one of two specified 	values.</title>
    <para>
      <prompt>mged></prompt><userinput>lm �1 engine pilot</userinput>
    </para>
    <para>Lists (in the long format) all regions with a �MUVES_Component� attribute having a value of �engine� or
	�pilot.� 
    </para>
  </example>
 
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

