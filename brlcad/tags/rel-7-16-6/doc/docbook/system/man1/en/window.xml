<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2008-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR AS IS'' AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='window1'>

<refmeta>
  <refentrytitle>WINDOW</refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>window</refname>
  <refpurpose>
    Creates a BRL-CAD .g file containing windows.
  </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>window</command>    
    <arg choice='opt' rep='repeat'><replaceable>options</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>
    <command>window</command> is a program to create a BRL-CAD database of 
    windows.  Up to twenty-six windows of the same size may be created.  The 
    windows are made with rounded corners.  <command>window</command> uses 
    libwdb to create a BRL-CAD database.  The windows are composed of two 
    arb8s and four cylinders.   This program may be run interactively or 
    the user may specify options on a command line.  If the user chooses to 
    run the program interactively he answers the questions as the program 
    prompts him.  Below are the options that can be used on the command line.
 </para>
</refsect1>


<refsect1 id='options'>
  <title>OPTIONS</title>
  <variablelist remap='TP'>
    <varlistentry>
      <term><option>-fname.g</option></term>
      <listitem>
	<para>
        BRL-CAD file name.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-n#</option></term>
      <listitem>
	<para>
         The number of windows to be created (must be less than or equal to 26.)
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-h#</option></term>
      <listitem>
	<para>
         Height of window in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-w#</option></term>
      <listitem>
	<para>
         Width of window in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-d#</option></term>
      <listitem>
	<para>
         Depth of window in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-r#</option></term>
      <listitem>
	<para>
         Radius of the corner in millimeters.
        </para>
      </listitem>
    </varlistentry>
  </variablelist>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <example>
    <title>Interactive <command>window</command> Session</title>
    <para>
    <literallayout>
The windows are composed of 2 arb8s and 4 cylinders.
The front of the window is centered at (0, 0, 0) and
extends in the negative x-direction the depth of the
window.

Enter the mged file to be created (25 char max).
        window.g
Enter the number of windows to create (26 max).
        3
Enter the height, width, and depth of the window.
        200 300 20
Enter the radius of the corner.
        30

mged file:  window.g
height of window:  200.000000 mm
width of window:  300.000000 mm
depth of window:  20.000000 mm
radius of corner:  30.000000 mm
number of windows:  3
   </literallayout>
    </para>
  </example>

  <example>
    <title>Single-Line <command>window</command> Command</title>
    <para>
      <userinput>window -fwindow.g -n3 -h200 -w300 -d20 -r30</userinput>
    </para>
  </example>

  <para>
   Both of these examples create a database named window.g containing
   three windows with a height of 200mm, width of 300mm, depth of 20mm,
   and corner radius of 30mm.
  </para>

</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>Susan A. Coates</para>
</refsect1>

<refsect1 id='copyright'>
  <title>COPYRIGHT</title>
  <para>
    This software is Copyright (c) 2005-2010 United States Government as
    represented by the U.S. Army Research Laboratory. All rights reserved.
  </para>
</refsect1>

<refsect1 id='see_also'>
  <title>SEE ALSO</title>
  <para>
  bolt(1), handle(1), window_frame(1), gastank(1)
  </para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

