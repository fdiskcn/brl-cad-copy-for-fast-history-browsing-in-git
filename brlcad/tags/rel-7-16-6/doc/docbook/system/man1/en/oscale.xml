<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='oscale'>

<refmeta>
  <refentrytitle>OSCALE</refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>oscale</refname>
  <refpurpose>This command of matrix edit mode modifies the matrix to perform a uniform
scale operation.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>oscale</command>    
      <arg choice='req'><replaceable>scale_factor</replaceable></arg>
    </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>This command of matrix edit mode modifies the matrix to perform a uniform
scale operation. A <emphasis>scale_factor</emphasis> of 2 doubles the size of the associated object, and a
<emphasis>scale_factor</emphasis> of 0.5 reduces it by half.
   </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The example shows the use of the <command>oscale</command> command to increase the size of a currently edited object by a specified factor.
   </para>
       
  <example>
    <title>Increase a currently edited object by a specified factor.</title>
    <para>
      <prompt>mged></prompt><userinput>oscale 3</userinput>
    </para>
    <para>Increases the size of the currently edited object by a factor of 3.
     </para>
  </example>
 
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

