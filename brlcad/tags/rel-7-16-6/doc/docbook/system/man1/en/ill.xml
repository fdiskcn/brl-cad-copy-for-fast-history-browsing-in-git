<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='ill'>

<refmeta>
  <refentrytitle>ILL</refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>ill</refname>
  <refpurpose>Performs the function of selecting an object after entering <emphasis>solid</emphasis>
(i.e., primitive)<emphasis>illuminate</emphasis> or <emphasis>object illuminate</emphasis> mode.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>ill</command>    
        <arg choice='req'><replaceable>obj_name</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Performs the function of selecting an object after entering <emphasis>solid</emphasis>
	(i.e., primitive) <emphasis>illuminate</emphasis> or <emphasis>object illuminate</emphasis> mode.
	In <emphasis>solid illuminate</emphasis> mode, this command selects the specific shape for editing. In 	<emphasis>object illuminate</emphasis> mode, this command selects the leaf object for the object path, 	then the user may use the mouse to select where along the object path the editing should be applied. In 	both modes,the <emphasis>ill</emphasis> command will only succeed if the specified <emphasis>obj_name	</emphasis> is only referenced once in the displayed objects; otherwise a <emphasis>multiply referenced	</emphasis> message will be displayed. If the <command>ill</command> command fails, the user must resort 	to either using the mouse to make the selection, or using aip and M 1 0 0.  
  </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
    <para>The example shows the use of the <command>ill</command> command to select a specified object for 	editing.
    </para>
  <example>
   <title>Select an object for editing.</title>
    <variablelist>
      <varlistentry>
	   <term><prompt>mged></prompt> <userinput>ill shapea</userinput></term>
	   <listitem>
	     <para><emphasis>Shapea></emphasis> is selected for editing.
	      	     </para>
	   </listitem>
      </varlistentry>
    </variablelist>
  </example>
       
 </refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

