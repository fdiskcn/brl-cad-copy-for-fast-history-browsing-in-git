BRL-CAD Bugs
============

Bugs should be reported to the bug tracker on the project website at
http://sourceforge.net/tracker/?atid=640802&group_id=105292

The bugs and issues listed in here may or may not be the same as or
related to the bugs reported to the bug tracker.  In general, users
should not look to this file for information regarding the status of
bugs.  Informal bug-related information that is intended for
developers will generally be found here.  This may include short term
issues that are in active development as well as long term and
on-going issues.

Recent Bugs
-----------

* mged rrt command is not respecting the view size, just does
  something similar to autoview but even gets az/el wrong too.

* reported that fbclear is not working on Windows build in mged.

* Windows mged build is not generating tclIndex files with all
  entries.  notably the pattern_gui.tcl file is not getting included
  as well as a variety of others.  see missing entries in
  tclscripts/mged/tclIndex for examples:
  https://sourceforge.net/tracker/?func=detail&atid=640802&aid=2923199&group_id=105292

* mged-invoked EDITOR locks up mged without a controlling terminal due
  to lacking knowledge of needing to invoke xterm.

* vdeck truncates the region names in the 'regions' file, probably
  wrong bu_strlcpy() length.

* hyp tesselation routine creates bot that does not raytrace

* if you attach a display manager in tk-mged, kill the window, then
  quit, mged crashes on exit.

* CMD-b and CMD-f navigates the mged command window text widget on Mac
  and trivally causes problems with the cursor and text injection.

* ogl display manager still crashes on attach in mged on Mac OS X 10.5

* mged segfaults when drawing lots of solids at once (e.g. draw *.s)

* mged command mode doesn't display the output of multiple
  semicolon-separated commands.  Only the output of the very last mged
  command is displayed.  Example:

  mged -c moss.g "tops ; title"
  mged -c moss.g "puts OK ; tops ; puts OK2 ; title ; puts DONE"

* kill command in archer deletes object but then reports a failure and
  mouse is stuck in a busy state.

* mged rotation halts after a few events and after zooming in/out at
  least once (Mac OS X 10.5)

* rtedge fills in nearly entire faces of an object when executing on a
  model that is in perspective mode. Effects really bad on small sized
  renders, and gets better the larger the render size. Does not happen
  in non-perspective ray-traces.

* infinite light sources close to objects result in unexpected/bad
  light behavior.  example is a 1m sph light about 5m from a box about
  1k x 1k x 1k in size.  setting make each light be red, green, and
  blue respectively, set to infinite (and fraction 10) and sides to
  the cube will render sides with a fixed color (green, blue) and top
  magenta.  On one test with all lights at fraction 1.0 and only one
  inf, was getting interleaved partial flat/no color all around cube.

* fastgen plate mode bots are reporting invalid shotline segments.
  depending on whether you shoot from the front or back can result in
  bogus lists visiable with nirt in mged, entirely missing the bot, or
  mismatched entry/exit/overlap segments.

* coil -S ... generates bad pipes with values previously known to
  work (see man page for one example).

* nirt -e "xyz val val val" doesn't work due to the space after the -e
  and how the nirt/rt commands parse quoted args.

* tab completion in classic/command mode sometimes doesn't work
  (presumably if mged can't find tcl resources).  was finally able to
  reproduce this with the result given as:

  invalid command name "bind"

* mged 'facetize -n' command is giving many errors:
    ERROR: color index out of range (-128 > 255)
  and it shouldn't.  seems to be happening on 3-manifold surfaces but
  only for particuar faces.  looks like the painting algorithm that is
  being used is flawed (and an incomplete implementation).

* running 'keep' on too many objects (hundreds is causing a usage
  statement (and not doing the keep)

* rtwizard won't display a line overlay if you line object is obscured
  by another.  Example being to display a vehicle as ghost, crew as
  solid, and crew as line.  confirmed even when using the other line
  occlusion settings.

* fbserv is receiving image data very slowly if the framebuffer is a
  remote X24 interface.  could reproduce on mac with the following:

  fbserv -s1024 1 /dev/X &
  rt -F1 -s1024 db/moss.g all.g
  rt -F1 -i -s1024 db/moss.g all.g  # 8x worse

* ls -A and/or e -A reportedly crashes on use, unverified

* File->Export seems to not work on Windows

* xpush fails to properly create a hierarchy leaving some as top-level
  objects and killing other existing objects resulting in dangling
  references.

* mged on Mac OS X will crash on an X11 call if compiled against
  system Tcl/Tk (at least on 10.4 linking against 8.4), see TODO

* the ray tracer fails to report LOS and sometimes even hits on a BoT
  that is marked as unoriented or is inverted (even though it
  sometimes will report the segments, just without LOS).  this seems
  to happen for bots that are neither lh or rh (they're mixed) and
  causes the raytrace to fail if they're set to anything other than
  'no' for orientation.  this affects rt, nirt, et al.

* g-vrml ignores bots that are created with dxf-g

* Mac OS X installer doesn't include the symbolic links, only
  including the versioned installation directory.

* Mouse cursor turns into a huge yellow-translucent arrow when running
  MGED through remote X11.

  Running this fixes the bug: .id_0t config -cursor "xterm black"

* mged (on debian) is reporting this when the user presses tab:
  Error in bgerror: invalid command name "::swidgets::togglearrow"
  the second time they press tab, they get:
  Error in bgerror: window name "_secErrorDialog" already exists in parent

  The problem was noticed on 7.12.4 and happens regardless of there
  being a db opened or not.  The problem does not occur in classic
  mode.

* mged command window is showing up as an empty 0x0 dimensioned window
  on freebsd using xfce and metacity (single display, 1680x1050 res).
  dm shows up just fine.

* wireframe is failing to draw when framebuffer is displayed (underlay
  or overlay) in mged.  problem is specific to dm-ogl (dm-x is fine).

* g2asc of a v4 is apparently writing out the wrong s_cgtypes and
  otherwise parsing v4 .g files incorrectly

* bump shader seems to be busted.  only renders as flat by itself and
  stacked with phong it seems to ignore the uv settings.

* rtweight prints incorrect units when units are set to ft.  Appears
  to trace back to unit handling in view_end in viewweight.c.  This
  logic should be replaced using the proper libbu functions.

* the graphics window does not acquire keybinding focus in mged on
  ubuntu linux.  key events go to the command window even with the
  graphics window has focus.

* rt is consuming 100% CPU in fb_close() on a lingering ogl
  framebuffer waiting to close.  needs to sleep or select for a while.

* /dev/ogl framebuffer is not refreshing/updating on mac os x,
  resulting in the window remaining blank until you give expose events

* report of mged being locked up on startup taking full cpu, works
  fine with with -c with X dm.

* MGED File->Preferences->Fonts results in error "named font "button
  _font" doesn't exist on Linux.

* mged tab-completion doesn't work if the object names have spaces

* OpenGL display manager doesn't refresh automatically when the
  context is invalidated (on Mac OS X) any more

* Open dialog in MGED seems rather busted for scrolling and selection
  (seems to be Mac OS X specific)

* turning on Framebuffer->Rectangle Area in mged disables the
  embedded framebuffer diplay on Mac OS X when you sweep a raytrace
  rectangle.  sweeping still works if you leave it at All.

* invalid shaders result in an "Unable to locate where BRL-CAD ... is
  installed" message that refers to being unable to find a
  /lib/libSHADER.so file.  Quite a bogus message and probably not
  something that should abort the ray-trace.

* rtedge seems to get stuck in a loop at the end of rendering, never
  terminates (unless you rtabort/kill the process); works with -P1

* nirt/query_ray reports intersection messages in triplicate if the
  shot routines miss but still print out messages

* mged's plot tool (the gui one, not the 'plot' command) outputs a
  plot file that has an erase as the last command, causing empty plots

* interactive mouse edits in mged can go haywire on Mac OS X with
  snap-to-grid enabled

* overlap tool in mged encourages very inefficient CSG operations
  (e.g. a simple bolt with an entire hull armor subtracted)

* a character is (still) sometimes captured by the cursor box

* permute (in mged) requires apply or save before 'new' vertex values
  will function properly as keypoints.

* facedef then move face undoes the facedef in mged

* dsp primitive is going into what seems to be an unbounded memory
  allocation loop during prep, inside dsp_layers().

* crashes during photonmap raytrace of m35 where right node is invalid
  rt -o test.pix -V 1.0 -P4 -J0 -l7 -A0 ~/Desktop/m35.g all.g box.r sph.r

* libfb cannot utilize standard output/error "device" files without
  path trickery.  it recognizes the /dev/ prefix and presumes it's a
  libfb device instead of a filesystem device.

   OK: rt -F/./dev/stdout
  BAD: rt -F/dev/stdout

* libfb cannot open a file descriptor when piping/redirecting output.

   OK: rt -F/./dev/stdout moss.g all.g
  BAD: rt -F/./dev/stdout moss.g all.g > test.pix

* ray-tracers assume a seekable output stream.  libfb reports numerous
  fseek errors and outputs slightly corrupted pix data.

   OK: rt -o test.pix moss.g all.g && pix-png test.pix > test.png && rm -f test.pix
  BAD: rt -o /dev/stdout moss.g all.g | pix-png > test.png

* running any of the various mged commands (e.g. clicking the raytrace
  button many times quickly, or running rtarea on a large model) that
  output a lot of data will hang mged (idle cpu utilization).

* solids.sh regression test fails on Mac OS X with one pixel
  off-by-many on the edge of the middle ARB8 when using an optimized
  -O3 compile (-fno-unsafe-math-optimizations makes no difference)

* triangulation of some pipe in toyjeep.g fails (rather reliably) on
  some given bend.  fails in nmg_triangulate model().

* tgc reports one hit errors though hit point has 0,0,0 direction
  implying that maybe the point was supposedly deleted.

* rtedge is not respecting the -c"set" options.  needs to give some
  indication that they were parsed too since it ignores unrecognized
  values apparently.

* fbhelp sends some of the output to stdout and some to stderr...

* debugbu 2 immediately reports a bu_vls_free() error.. apparently
  been a problem since 4.5 days.  this occurs because
  bu_memdebug_add() is not called unless memory checking is enabled
  via bu_debug.  so when bu_debug is set to 2 or 3, it ends up
  reporting errors during bu_free() for items that were allocated and
  were not yet being tracked.

* mged View menu says I and O are keybindings for zoom in/out -- they
  are apparently not.

* raytracers report the wrong amount of cumulative time (reports as
  0.0 seconds elapsed) on amd64 linux; most likely the same pthread
  accounting bug visited a couple years ago.

* Create menu.. create "part" boo hiss..

* wavelet isn't working correctly on a simple image decomposition &
  reconstruction.

* query_ray can't find nirt if not installed

* fbed is looking for /usr/lib/vfont/nonie.r.12

* mged doesn't check if html_dir does not exist when searching for
  docs/browser.

* permute then translate doesn't refresh the graphics window
    permute 4378
    translate menus
    p 0 0 0
    p 0 0 0

* constrained axis translation bindings on OS X don't work
    object/matrix edit
    shift-alt mouse (left/mid/right for x/y/z respectively) should work

* mged documentation doesn't open on OS X

* the findfont directive in gv postscript is reporting font not found

* concave arbs give correct wireframe but do not raytrace correctly.

* rtwizard/rtedge creates incorrect "fuzzy" edge overlay when creating
  a "Ghost Image with Insert and Lines" that includes a light source
  and object close to surface (e.g. moss.g; select plane, light, and
  cone for ghost; select cone for insert and lines.)

* mged's solid illuminate doesn't work when Lighting is turned on

* raytrace of zoomed images (e.g., -s64) doesn't work for if_X24
  framebuffers.  zooming logic seems to be broken (was crashing in
  memcpy, but that was fixed) for at least -s96 and smaller:

  ./libtool --mode=execute gdb --args src/rt/rt -F/dev/X -P1 -s64 db/moss.g all.g

* g2asc exports attr lines but will not grok them on asc2g

* mged's matrix selection does not modify the correct matrix if there
  are multiply referenced non-unique paths (e.g. referencing the same
  object N times in a combination).

* facetize -t doesn't work (tnurb support)

* on os x, the -fast option results in an odd optimization/aliasing
  bug where rtip contents are lost in rt after returning from
  rt_gettrees().

* tra in console mode outputs a warning about mged_players

* mged primitive editor doesn't accept/apply values to disk for
  certain primitives (e.g. sph)

* photon map cache file doesn't work (seems to crash rt on use)

* enabling perspective mode horks shaded mode, also Z clipping
  shouldn't be required to get the shading.

* an underlay framebuffer does not underlay with shaded mode

* rtweight chokes on a .density file that has less than 3 values per
  line (infinite loop).  also gives infinite areas when presented with
  an empty .density file.

* setting src/librt/db5_io.c's AVS_ADD off in db5_import_attributes
  causes bad things to happen and it really shouldn't.

* vrml exporter (and probably others) doesn't export primitives
  directly as one might expect like the ray-tracers, only regions.

* sketches extruded with non-square AB vectors results in inner sketch
  components not getting scaled properly.

* rtcheck is ignoring fastgen tagged geometry since no multioverlap
  handler is set.

* bot-bldxf fails in db_walk_tree(), interface may have changed

* you can't run a make benchmark unless you're compiling in place


Older Bugs
----------

* several manpages are missing or out of date for various tools

* X11 framebuffer often fails to display on certain middle bit depth
  displays. (e.g. 15 or 16 bit depth)

* mk_lcomb fails on large combinations due to mk_tree_pure() building
  a left-heavy tree and wdb_put_internal() then recursively performing
  lookup and put calls.


Annoyances
----------

* matrix edits on objects in mged require you to select a primitive
  for a coordinate system reference.  this can be counterintuitive and
  confusing.

* there are frequently modality errors when entering edit mode as
  control and middle mouse rebind to model edit instead of view edit.

---
Bugs should be reported to the bug tracker on the project website at
http://sourceforge.net/tracker/?atid=640802&group_id=105292

BUGS should be formatted to column 70 (M-q in emacs), no tabs.

// Local Variables:
// mode: Text
// fill-column: 70
// End:
