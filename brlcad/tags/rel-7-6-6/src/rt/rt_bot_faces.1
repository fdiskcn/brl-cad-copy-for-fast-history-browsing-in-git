.TH RT_BOT_FACES 1 BRL-CAD
./"                 R T _ B O T _ F A C E S . 1
./" BRL-CAD
./"
./" Copyright (c) 2005 United States Government as represented by
./" the U.S. Army Research Laboratory.
./"
./" This document is made available under the terms of the GNU Free
./" Documentation License or, at your option, under the terms of the
./" GNU General Public License as published by the Free Software
./" Foundation.  Permission is granted to copy, distribute and/or
./" modify this document under the terms of the GNU Free Documentation
./" License, Version 1.2 or any later version published by the Free
./" Software Foundation; with no Invariant Sections, no Front-Cover
./" Texts, and no Back-Cover Texts.  Permission is also granted to
./" redistribute this document under the terms of the GNU General
./" Public License; either version 2 of the License, or (at your
./" option) any later version.
./"
./" You should have received a copy of the GNU Free Documentation
./" License and/or the GNU General Public License along with this
./" document; see the file named COPYING for more information.
./"
./"./"./"
.UC 4
.SH NAME
rt_bot_faces \(em raytrace a model, and write a file listing all the BOT facets hit first on any shotline.
.SH SYNOPSIS
.B rt_bot_faces
[ options ... ]
model.g
objects ...
.SH DESCRIPTION
.I Rt_Bot_Faces
operates on the indicated
.I objects
in the input
.I model.g
and fires rays (the same as RT) keeping a list of all the first-hit BOT facets.
.LP
The orientation of the rays to be fired may be specified by
the
.B \-a
and
.B \-e
options, in which case the model will be autosized, and the grid
will be centered on the centroid of the model, with ray spacing
chosen to span the entire set of
.I objects.
Alternatively,
with the
.B \-M
option, a transformation matrix may be provided on standard input
which maps model-space to view-space.
In this case, the grid ranges from -1.0 <= X,Y <= +1.0 in view space,
with the size of the grid (number of rays fired) specified with
.BR \-s .
This option is most useful when
.B rt_bot_faces
is being invoked from a shell script created by an
.IR mged (1)
\fIsaveview\fR command.  Note that it conflicts with
.B \-G#
and
.BR \-g# ;
negative hit distances have been seen when this
conflict was not avoided.
.LP
The following options are recognized.
.TP
.B \-s#
Number of rays to fire in X and Y directions (square grid).
Default is 512 (512x512).
.TP
.B \-a#
Select azimuth in degrees.  Used with
.B \-e
and conflicts with
.BR \-M .
.TP
.B \-e#
Select elevation in degrees.  Used with
.B \-a
and conflicts with
.BR \-M .
.TP
.B \-M
Read model2view matrix from standard input.
Conflicts with
.B \-a
and
.BR \-e .
.TP
.B \-g#
Select grid cell width.  Do not use with the
.B \-s#
option.
.TP
.B \-G#
Select grid cell height.  If \fInot\fR specified, cell height equals
cell width.  Do not use with the
.B \-s#
option.
.TP
.B \-U #
sets the Boolean variable
.I use_air
to the given value.
The default for
.IR rt_bot_faces (1)
is on, i.e.
.BR \-U1 ,
but the effect of air regions may be disabled with
.BR \-U0 .
.TP
.B \-o output_bot_faces
specifies a named file to receive the list of BOT facets that appear as first surface hit on any ray.
By default, the data are written to stdout. The format of the output is a header line for each BOT primitive that appeared as the first object on any ray of the form:
.nf
	BOT: bot_primitive_name
.fi
This is followed by a list of facet numbers for that BOT primitive (one per line). The
facet numbers are indices into the list of faces (starting from zero) for that BOT primitive.
If the output file is not empty, it is expected to contain data from a previous execution of
.I rt_bot_faces,
and the data in the file is read, and stored, new data is added to it during the execution,
and the output file is overwritten with the accumulated data.
.TP
.B \-x#
Set librt debug flags to (hexadecimal) number.
.TP
.B \-c """set rt_cline_radius=#"""
Tells
.IR rt_bot_faces (1)
to use the provided number (in millimeters) as the additional radius to add to CLINE
solids.
.TP
.B \-c """set save_overlaps=1"""
Tells
.IR rt_bot_faces (1)
to act like the
.IR FASTGEN4
raytracer. Specifically, overlap handling is changed to agree with that of
.IR FASTGEN4.
.LP
The
.B rt_bot_faces
program is a simple front-end to
.IR librt (3).
.SH EXAMPLE
This section will acquaint the user with the ordinary usage of \fIrt_bot_faces\fR.
the typical use is:

.nf
.ce
\fIrt_bot_faces -s32 model.g all.g > file\fR
.fi

This will fire a square grid of 32 by 32 rays at the named model and the resulting list of BOT facets
will be placed in "file".

.SH "OVERLAP REPORTING"
\fIRT_BOT_FACES\fR shares overlap handling and reporting with the rest of the
\fIRT\fR family of applications.  A brief description of the overlap
reporting follows.
.LP
The first one hundred (100) overlaps are individually reported.  Thereafter,
only one out of each additional one hundred overlaps are reported, and
the user is notified that overlap printouts are being omitted.  This
alerts the user that more overlaps exist, but that they are too numerous to
list individually.  The general overlap message takes the following form:

.nf
.ce
 \fI"OVERLAP1: reg=region_name isol=solid_name"\fR
.ce
 \fI"OVERLAP2: reg=region_name isol=solid_name"\fR
.ce
 \fI"OVERLAP depth #mm at (x, y, z) sx# sy# lvl#"\fR
.fi

This means that OVERLAP1 and OVERLAP2 share a common space (depth) of
#millimeters starting at the point x, y, z in model coordinates, and
at the ray coordinates (screen coordinates) sx and sy.  Level refers to the
level of recursion, and will typically be zero unless glass or mirror
objects are present.
.SH "SEE ALSO"
 mged(1),
rt(1), rtray(1),
librt(3), plot3(5), ray(5V).
.SH DIAGNOSTICS
Numerous error conditions are possible, usually due to errors in
the geometry database.
Descriptive messages are printed on standard error (file descriptor 2).
.SH AUTHORS
John R. Anderson
.SH SOURCE
SLAD/BVLD/VMB Advanced Computer Systems Team
.br
The U. S. Army Research Laboratory
.br
Aberdeen Proving Ground, MD  21005
.SH "BUG REPORTS"
Reports of bugs or problems should be submitted via electronic
mail to <devs@brlcad.org>.
