.TH LIBCURSOR 3 BRL-CAD
./"                    L I B C U R S O R . 3
./" BRL-CAD
./"
./" Copyright (c) 2005 United States Government as represented by
./" the U.S. Army Research Laboratory.
./"
./" This document is made available under the terms of the GNU Free
./" Documentation License or, at your option, under the terms of the
./" GNU General Public License as published by the Free Software
./" Foundation.  Permission is granted to copy, distribute and/or
./" modify this document under the terms of the GNU Free Documentation
./" License, Version 1.2 or any later version published by the Free
./" Software Foundation; with no Invariant Sections, no Front-Cover
./" Texts, and no Back-Cover Texts.  Permission is also granted to
./" redistribute this document under the terms of the GNU General
./" Public License; either version 2 of the License, or (at your
./" option) any later version.
./"
./" You should have received a copy of the GNU Free Documentation
./" License and/or the GNU General Public License along with this
./" document; see the file named COPYING for more information.
./"
./"./"./"
'\"	Originally extracted from SCCS archive:
'\"		created:	02-Jul-1986	G S Moss
'\"		last edit:	86/07/08	D A Gwyn
'\"		SCCS ID:	@(#)libcursor.3	1.5
.SH NAME
libcursor \- cursor control library
.SH SYNOPSIS
.nf
.B #include <stdio.h>
.P
.B int InitTermCap(fp);
.B FILE *fp;
.P
.B int MvCursor(x, y);
.B int x, y;
.P
.B int HmCursor(\|);
.P
.B int ClrEOL(\|);
.P
.B int ClrText(\|);
.P
.B int ClrStandout(\|);
.P
.B int SetStandout(\|);
.P
.B int SetScrlReg(top, bottom);
.B int top, bottom;
.P
.B int ResetScrlReg(\|);
.P
.B int ScrollUp(\|);
.P
.B int ScrollDn(\|);
.P
.B int DeleteLn(\|);
.P
.B int PutChr(c);
.B int c;
.P
.fi
.B extern char termName[\|];
.br
\fBextern int LI;\fP		/* number of lines on screen (or layer) */
.br
\fBextern int CO;\fP	/* number of columns on screen (or layer) */
.br
\fBextern char *BC;\fP	/* backspace */
.br
\fBextern char *PC;\fP	/* padding character */
.br
\fBextern char *UP;\fP	/* move cursor up one line */
.br
\fBextern char *CS;\fP	/* change scrolling region */
.br
\fBextern char *SO;\fP	/* begin standout mode */
.br
\fBextern char *SE;\fP	/* end standout mode */
.br
\fBextern char *CE;\fP	/* clear to end of line */
.br
\fBextern char *CL;\fP	/* clear display (also homes cursor) */
.br
\fBextern char *HO;\fP	/* home cursor */
.br
\fBextern char *CM;\fP	/* screen-relative cursor motion */
.br
\fBextern char *TI;\fP	/* initialize terminal */
.br
\fBextern char *DL;\fP	/* delete current line */
.br
\fBextern char *SR;\fP	/* scroll text down (backward) */
.br
\fBextern char *SF;\fP	/* scroll text up (forward) */
.SH DESCRIPTION
These routines are designed to provide a terminal-independent means of controlling
cursor movement, character attributes, text scrolling, and erasure of text
which is a level above the
.IR termlib\^ (3)
library.
It is similar to the
.IR curses\^ (3)
library, but does not address the problem of maintaining windows, so it has much
less overhead and avoids the associated bugs.
.P
.I InitTermCap\^
must be passed the output stream pointer and invoked before any other functions
in this library.
This function reads the
.IR termcap\^ (4)
capability data base to extract terminal-specific parameters
and control strings,
then initializes the terminal, returning 1 for success and 0 for failure.
After
.I InitTermCap\^
is used, the global buffer
.I termName\^
will contain the name of the terminal device, or
``\fB\s-2UNKNOWN\s0\fP''
in the event of failure
.RI ( e.g. ,
the environment variable
.B
.SM $TERM
was not set or exported (see
.IR sh\^ (1)));
the global variables
.I LI\^
and
.I CO\^
will contain the number of lines and columns, respectively, of the
terminal screen or window layer attached to the specified output stream;
and the terminal control string pointers will be set, or
.B
.SM NULL
if their respective capabilities are not found.
The terminal control strings are not generally suitable for direct use
as they require the
.I tputs\^
or
.I tgoto\^
macros from the
.IR termlib\^ (3)
library, but they may be checked for being
.B
.SM NULL
to test in advance for a capability,
rather than testing the respective function for
success or failure.
The function
.I PutChr\^
will place the specified character on the terminal's output stream.
It is not intended
for use by the application (although such use is permissible),
but is defined for the
.IR termlib\^ (3)
library.
.P
All the following functions will return 1 for success and 0 if the capability
is not described in the
.IR termcap\^ (4)
entry.
.I MvCursor\^
will move the cursor to the specified column and row
.RI ( x\^
and
.IR y\^ )
screen location specified.
.I HmCursor\^
will move the cursor to the origin (top left) of the screen and is equivalent
to
.IR MvCursor(1,1)\^ .
To erase from the cursor position to the end of the line, use
.IR ClrEOL\^ ,
and to erase the entire screen call
.IR ClrText\^
(typically has the side effect of homing the cursor).
To initiate the output of reverse video or emboldened text (depending
upon the terminal's capabilities), use
.IR SetStandout\^ ;
.I ClrStandout\^
will restore the normal mode.
For terminals such as the
.SM "DEC VT100"
which have scrolling region capability, the functions
.I SetScrlReg\^
and
.I ResetScrlReg\^
can be used.
.I SetScrlReg\^
must be invoked with the top and bottom line numbers of the region of the
screen to be scrolled.
The functions
.I ScrollUp\^
and
.I ScrollDn\^
will scroll the text on the screen up and down
(forward and backward) respectively,
by one line per invocation.
The function
.I DeleteLn\^
will delete the line containing the cursor, causing the text
below to scroll up to fill the void.
.SH EXAMPLE
.I Libcursor\^
can be loaded with any C program:
.P
.RS
$ \|\fIcc \|program.c \|\-lcursor \|\-ltermlib\fP
.RE
.SH FILES
/usr/brl/lib/libcursor.a
.br
/usr/lib/libtermlib.a
.br
/etc/termcap
.SH "SEE ALSO"
curses(3X), termlib(3), termcap(4).
.SH AUTHOR
Gary S. Moss, BRL/VLD-VMB
