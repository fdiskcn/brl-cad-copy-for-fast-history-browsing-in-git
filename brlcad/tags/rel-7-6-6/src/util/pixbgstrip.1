.TH PIXBGSTRIP 1 BRL-CAD
./"                   P I X B G S T R I P . 1
./" BRL-CAD
./"
./" Copyright (c) 2005 United States Government as represented by
./" the U.S. Army Research Laboratory.
./"
./" This document is made available under the terms of the GNU Free
./" Documentation License or, at your option, under the terms of the
./" GNU General Public License as published by the Free Software
./" Foundation.  Permission is granted to copy, distribute and/or
./" modify this document under the terms of the GNU Free Documentation
./" License, Version 1.2 or any later version published by the Free
./" Software Foundation; with no Invariant Sections, no Front-Cover
./" Texts, and no Back-Cover Texts.  Permission is also granted to
./" redistribute this document under the terms of the GNU General
./" Public License; either version 2 of the License, or (at your
./" option) any later version.
./"
./" You should have received a copy of the GNU Free Documentation
./" License and/or the GNU General Public License along with this
./" document; see the file named COPYING for more information.
./"
./"./"./"
.SH NAME
pixbgstrip \- strip smooth shaded background from a pix(5) file
.SH SYNOPSIS
.B pixbgstrip
.RB [ \-ah ]
.RB [ \-t\  thresh]
.RB [ \-x\  x_offset_for_bg_pixel]
.RB [ \-s\  squarefilesize]
.RB [ \-w\  file_width]
.RB [ \-n\  file_height]
[file.pix]
.SH DESCRIPTION
.I pixbgstrip
reads a
.IR pix (5)
format file from the named file, or from
standard input if no file is specified.
For each scanline read, all pixels that match the color of the
background pixel are converted to black =0/0/0,
and then the new scanline is written on stdout.
.PP
The default X offset for the background pixel is zero (e.g.
.BI \-x\ 0
), but if the pixel at X offset zero is not the desired background
(for example, if a border exists around the actual image), then
another offset can be specified.
.PP
By default, a pixel will be replaced with black only if it exactly
matches the value of the background pixel
(e.g., the matching threshold is zero
.BI \-t\  0
).
However, if the background is slightly noisy, it may be necessary to
perform a slightly looser matching.  The threshold variable
specifies how large a difference between an input pixel and the background
pixel will still result in black being output.
.PP
By default, the
.I pix
file is assumed to be 512x512 pixels.
Specifying the
.B \-a
flag causes the program to attempt to autosize.
A table of common image sizes is consulted, and if any match
the size of the input file, then the width and height values
associated with that size will be used.
Specifying the
.B \-h
flag changes the size to 1024x1024.
.PP
The
.BI \-w\  file_width
flag specifies the width of each scanline in the input file, in pixels.
.PP
The
.BI \-n\  file_height
flags specifies the height in scanlines of the input file.
.PP
.BI \-s\  squarefilesize
sets both the height and width to the size given.
.SH "SEE ALSO"
pixbackgnd(1), pix-fb(1), fb-pix(1), libfb(3), pix(5)
.SH DIAGNOSTICS
If the
.I pix
file is shorter than expected, the program exits silently.
.SH AUTHOR
Michael John Muuss
.SH SOURCE
SECAD/VLD Computing Consortium, Bldg 394
.br
The U. S. Army Ballistic Research Laboratory
.br
Aberdeen Proving Ground, Maryland  21005
.SH "BUG REPORTS"
Reports of bugs or problems should be submitted via electronic
mail to <devs@brlcad.org>.
