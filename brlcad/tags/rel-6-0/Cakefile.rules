/*
 *			C A K E F I L E . R U L E S
 */

/*
 *  Patterns to handle the installation and uninstallation
 *  of products, manuals, and manual stubs.
 *  This is complicated by the fact that not all directories
 *  will contain all three types of items.
 */
#define	MAN_EXISTS	{{ls ../SRCDIR|grep -s .MANSECTION'$'}}
#define STUB_EXISTS	{{ls ../SRCDIR|grep -s .MANSECTION.stub'$'}}
#define ARG_EXISTS	{{ARGS="%"; test '%' != "$ARGS" -o -f %}}

/* Format sources for printing on stdout.  Use macro to bypass ANSI cpp "bug" */
#define PRINT_STUFF	[[SUB X X.print ../SRCDIR/Cakefile ]] \
	[[SUB X X.print ../SRCDIR/''*.[h''MANSECTION] ]] \
	[[SUB X X.nroff ../SRCDIR/''*.MANSECTION ]] \
	[[SUB X ../SRCDIR/X''SRCSUFF.print FILES]]

print&:	PRINT_SUFF

%.print&:  do-%.print			if ARG_EXISTS
%.print&:				if not ARG_EXISTS

do-%.print&:
	@pr %

/*
 */
nroff&:  do-nroff			if MAN_EXISTS
nroff&:					if not MAN_EXISTS

do-nroff&:  [[SUB X X.nroff ../SRCDIR/''*.MANSECTION ]]

%.nroff&: ../SRCDIR/%
	@nroff -Tlp -man ../SRCDIR/%

/*
 *  Typeset all the non-stub manual pages.  Do them all at once,
 *  to prevent lots of extra header sheets on the printer
 *
 *	do-typeset-a	creates the file "troff.out" with the macros in it
 *	%.troff		adds each manual page to "troff.out"
 */
typeset&: do-typeset			if MAN_EXISTS
typeset&:				if not MAN_EXISTS

do-typeset&:  do-typeset-a troff.out
	( TYPESET_MAN2 ) < troff.out
	@rm -f troff.out

do-typeset-a&:
	TYPESET_MAN0 TYPESET_MAN1 > troff.out

troff.out&:  [[SUB X X.troff ../SRCDIR/''*.MANSECTION ]]

%.troff&:				if ARG_EXISTS
	TYPESET_MAN0 % >> troff.out

#define SRC_LIST	`SUB X ../SRCDIR/X''SRCSUFF FILES`

lint&:
	LINT LFLAGS -I''INCDIR SRC_LIST LLIBES > SRCDIR.lint

tags:	[[echo SRC_LIST]]
	ctags SRC_LIST

TAGS:	[[echo SRC_LIST]]
	etags SRC_LIST

flow&:	[[echo SRC_LIST]]
	cflow -I''INCDIR SRC_LIST > SRCDIR.flow

xref&:	[[echo SRC_LIST]]
	cxref -c -s -w132 -I''INCDIR SRC_LIST > SRCDIR.xref

compare&: [[SUB X X.compare PRODUCTS]]

%.compare&:	%
	cmp BINDIR/% %

#if !defined(DONT_INSTALL)
uninstall&:  uninstall-prod uninstall-man uninstall-stub
#else
uninstall&:
#endif

#ifdef PRODUCTS
uninstall-prod&: [[SUB X X.uninstall-prod PRODUCTS]]
#else
uninstall-prod&:
#endif

%.uninstall-prod&:
	rm -f BINDIR/% BINDIR/%.bak

uninstall-man&:	do-uninstall-man	if MAN_EXISTS
uninstall-man&:				if not MAN_EXISTS

do-uninstall-man&:  [[SUB ../SRCDIR/X X.ui-man ../SRCDIR/''*.MANSECTION]]

%.ui-man&:
	rm -f MANDIR''MANSECTION/% MANDIR''MANSECTION/%.bak

uninstall-stub&:  do-uninstall-stub	if STUB_EXISTS
uninstall-stub&:			if not STUB_EXISTS

do-uninstall-stub&:  [[SUB ../SRCDIR/X.stub X.uninstall-stub ../SRCDIR/''*.stub]]

%.uninstall-stub&:
	rm -f MANDIR''MANSECTION/% MANDIR''MANSECTION/%.bak

#if !defined(EXTRA_INSTALL_TARGETS)
#	define EXTRA_INSTALL_TARGETS	/**/
#endif

#if !defined(DONT_INSTALL)
install&: install-prod install-man install-stub

install-nobak&: install-prod-nobak install-man install-stub
#else
install&:

install-nobak&:
#endif

#if defined(SHARED_PRODUCT)
install-prod&: [[SUB X X.iprod SHARED_PRODUCT]] EXTRA_INSTALL_TARGETS

install-prod-nobak&: [[SUB X X.iprod-nobak SHARED_PRODUCT]] EXTRA_INSTALL_TARGETS

#else
#  if defined(PRODUCTS)
install-prod&: [[SUB X X.iprod PRODUCTS]] EXTRA_INSTALL_TARGETS

install-prod-nobak&: [[SUB X X.iprod-nobak PRODUCTS]] EXTRA_INSTALL_TARGETS
#  else
install-prod&:

install-prod-nobak&:
#  endif
#endif

/* Here is where we depend on having /bin/sh as the shell */
%.iprod&: %
	@echo ......checking % 1>&2 ; \
	if cmp -s BINDIR/% %; \
	then	exit 0; fi; \
	mv -f BINDIR/% BINDIR/%.bak 2>/dev/null; \
	if INS % BINDIR/%; \
	then	if test x''AFTERINSTALL != x'AFTERINSTALL'; \
		then	AFTERINSTALL BINDIR/%; fi; \
		echo "++++++INSTALLED" BINDIR/% 1>&2; \
		chmod BINPERM BINDIR/%; \
	fi

%.iprod-nobak&: %
	@echo ......checking % 1>&2 ; \
	if cmp -s BINDIR/% %; \
	then	exit 0; fi; \
	rm -f BINDIR/% 2>/dev/null; \
	if INS % BINDIR/%; \
	then	if test x''AFTERINSTALL != x'AFTERINSTALL'; \
		then	AFTERINSTALL BINDIR/%; fi; \
		echo "++++++INSTALLED" BINDIR/% 1>&2; \
		chmod BINPERM BINDIR/%; \
	fi

install-man&:  do-install-man	if MAN_EXISTS
install-man&:			if not MAN_EXISTS

do-install-man&: [[SUB ../SRCDIR/X X.i-man ../SRCDIR/''*.MANSECTION ]]

%.i-man&:	../SRCDIR/%
	@if cmp -s MANDIR''MANSECTION/% ../SRCDIR/%; \
	then	exit 0; fi; \
	mv -f MANDIR''MANSECTION/% MANDIR''MANSECTION/%.bak 2>/dev/null; \
	if INS ../SRCDIR/% MANDIR''MANSECTION/%; \
	then	echo "++++++INSTALLED" MANDIR''MANSECTION/% 1>&2; \
		chmod MANPERM MANDIR''MANSECTION/%; \
	fi

install-stub&:  do-install-stub	if STUB_EXISTS
install-stub&:			if not STUB_EXISTS

do-install-stub&: [[SUB ../SRCDIR/X.stub X.install-stub ../SRCDIR/''*.stub]]

%.install-stub&:  ../SRCDIR/%.stub
	@if cmp -s MANDIR''MANSECTION/% ../SRCDIR/%.stub; \
	then	exit 0; fi; \
	mv -f MANDIR''MANSECTION/% MANDIR''MANSECTION/%.bak 2>/dev/null; \
	if INS ../SRCDIR/%.stub MANDIR''MANSECTION/%; \
	then	echo "++++++INSTALLED" MANDIR''MANSECTION/% 1>&2; \
		chmod MANPERM MANDIR''MANSECTION/%; \
	fi

/*
 *  Force installation of copies of all sources,
 *  whether configured in or not, into the distribution tree.
 */
#ifndef	STATIC_PRODUCTS
#	define STATIC_PRODUCTS	/**/
#endif
#ifdef NFS
#define	INSTALL_PATTERNS	STATIC_PRODUCTS \
	../SRCDIR/Cakefile ../SRCDIR/\\\\*.[hcsfyl] \
	../SRCDIR/\\\\*.sh \
	../SRCDIR/\\\\*.MANSECTION ../SRCDIR/\\\\*.MANSECTION.stub \
	../SRCDIR/\\\\*.doc
#else
#define	INSTALL_PATTERNS	STATIC_PRODUCTS \
	./Cakefile \\\\*.[hcsfyl] \\\\*.sh \
	\\\\*.MANSECTION \\\\*.MANSECTION.stub \\\\*.doc
#endif

/* inst-dist is never run automatically, it has to be run by hand. */
/* inst-dist is run from gen.sh as part of the final packaging of a distribution */
inst-dist&: dist-clobber [[SUB X X.do-inst-dist INSTALL_PATTERNS]]

dist-clobber&:
	-rm -fr DISTRIBUTION/SRCDIR
	mkdir DISTRIBUTION/SRCDIR; chmod 775 DISTRIBUTION/SRCDIR

%.do-inst-dist&:  %.inst-dist	if ARG_EXISTS
%.do-inst-dist&:		if not ARG_EXISTS

%.inst-dist&:
	-INS % DISTRIBUTION/SRCDIR/.

#ifndef EXTRA_CLEAN
#	define	EXTRA_CLEAN	/**/
#endif

/*
 *	clean	remove all .o files and by-products
 *	noprod	remove the final executables
 *	clobber	perform clean & noprod
 */
clobber&:	clean noprod

clean&:
	-rm -f core *.core *~ *.o EXTRA_CLEAN SRCDIR.out SRCDIR.err SRCDIR.xref SRCDIR.flow SRCDIR.lint

#if defined(LIBRARY_TITLE)
/*
 *  The asterisk is to glob all the variations of (shared) library names.
 *  This depends on the fact that for libraries, PRODUCTS is a single name,
 *  not a list of file names, and that there will be no source file called
 *  PRODUCTS.c, e.g., no librt.c file for making librt.so.10.
 *
 *  The "SRCDIR.*" version is to catch the case where we're changing
 *  from shared libraries to non-shared libraries.
 */
noprod&:
	-rm -f PRODUCTS* SRCDIR.*
#else
noprod&:
	-rm -f PRODUCTS
#endif

#ifdef never
/*
 *  Insist that, by default, Cake work on making the products.
 *  THIS SHOULD BE THE LAST THING CAKE SEES!
 *  The double-colon marks this as the main target.
 */
"!MAINCAKE!"&::	PRODUCTS
#endif
