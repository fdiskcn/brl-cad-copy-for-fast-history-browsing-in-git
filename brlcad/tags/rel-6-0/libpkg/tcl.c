/*
 *				T C L . C
 *
 * LIBPKG's tcl interface.
 * 
 * Source -
 *	SLAD CAD Team
 *	The U. S. Army Research Laboratory
 *	Aberdeen Proving Ground, Maryland  21005
 *
 * Author -
 *	Robert G. Parker
 */
#include "conf.h"
#include "tcl.h"

int
Pkg_Init(interp)
     Tcl_Interp *interp;
{
	return TCL_OK;
}
