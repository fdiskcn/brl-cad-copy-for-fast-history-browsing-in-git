The BRL-CAD Package includes a powerful solid modeling capability and
a network-distributed image-processing capability. This software is now
running at over 400 sites.  It has been distributed to 42 acedemic
institutions in twenty states and four contries. 75 different businesses
have requested and received the software including 23 Fortune 500
companies. 16 government organizations representing all three services,
NSA, NASA, NBS and the Veterns Administration are running the code.
Three of the four national laboratories have copies of the BRL CAD
package.

BRL-CAD started in 1979 as a task to provide an interactive graphics
editor for the BRL vehicle-description data base. Today the package
totals more than 150,00 lines of "C" source code. It runs under UNIX and
is supported over more than a dozen product lines from Sun Workstations
to the Cray 2. The package includes:

	A Solid geometric editor
	The Ray tracing library
	Two Lighting models
	Many image-handling, data-comparison, and other supporting utilities

In terms of geometrical representation of data, BRL-CAD supports:

	The original Constructive Solid Geometry (CSG) BRL database.

	Extensions to include solids made from collections of 
	Uniform B-Spline Surfaces as well as
	Non-Uniform Rational B-Spline [NURB] Surfaces.

	A facetted data representation.

It supports association of material (and other attribute properties)
with geometry which is critical to subsequent applications codes.
It supports a set of extensible interfaces by means of which geometry
(and attribute data) are passed to applications.

Applications linked to BRL-CAD:

o Weights and Moments-of-Inertia
o Optical Image Generation (including specular/diffuse reflection,
	refraction, and multiple light sources, animation, interference)
o Bistatic laser analysis
o A number of Synthetic Aperture Radar Codes (including codes due to ERIM)
o Acoustic model predictions
o High-Energy Laser Damage
o High-Power Microwave Damage
o An array of V/L Codes
o Neutron Transport Code
o Link to PATRAN [TM] and hence to ADINA, EPIC-2, NASTRAN, etc.
	for structural/stress analysis
o X-Ray calculation

For more details about what geometric models are useful for, see M.
Muuss, ``Understanding the Preparation and Analysis of Solid Models'',
in ``Techniques for Computer Graphics'', ed: Rogers & Earnshaw, Springer
Verlag, 1987.

To obtain a copy of the BRL CAD Package distribution, you must send
enough magnetic tape for 20 Mbytes of data. Standard nine-track
half-inch magtape is the strongly preferred format, and can be written
at either 1600 or 6250 bpi, in TAR format with 10k byte records. For
sites with no half-inch tape drives, Silicon Graphics and SUN tape
cartridges can also be accommodated. With your tape, you must also
enclose a letter indicating

(a) who you are,
(b) what the BRL CAD package is to be used for,
(c) the equipment and operating system(s) you plan on using,
(d) that you agree to the conditions listed below.

This software is an unpublished work that is not generally available to
the public, except through the terms of this limited distribution.
The United States Department of the Army grants a royalty-free,
nonexclusive, nontransferable license and right to use, free of charge,
with the following terms and conditions:

1.  The BRL CAD package source files will not be disclosed to third
parties.  BRL needs to know who has what, and what it is being used for.

2.  BRL will be credited should the software be used in a product or written
about in any publication.  BRL will be referenced as the original
source in any advertisements.

3.  The software is provided "as is", without warranty by BRL.
In no event shall BRL be liable for any loss or for any indirect,
special, punitive, exemplary, incidental, or consequential damages
arising from use, possession, or performance of the software.

4.  When bugs or problems are found, you will make a reasonable effort
to report them to BRL.

5.  Before using the software at additional sites, or for permission to
use this work as part of a commercial package, you agree to first obtain
authorization from BRL.

6.  You will own full rights to any databases or images you create with this
package.

All requests should be sent to:

	Keith Applin
	Ballistic Research Lab
	APG, MD  21005-5066



Best Wishes,
 -Mike Muuss

Advanced Computer Systems
InterNet E-mail:  <Mike @ BRL.MIL>
