/*
 *			C A K E F I L E . L I B
 *
 *  Cakefile for building a single library archive.
 *  Symbol PRODUCTS should be name of a single library
 *
 *  Author -
 *	Michael John Muuss
 *  
 *  Source -
 *	SECAD/VLD Computing Consortium, Bldg 394
 *	The U. S. Army Ballistic Research Laboratory
 *	Aberdeen Proving Ground, Maryland  21005-5066
 *  
 *  Distribution Status -
 *	Public Domain, Distribution Unlimitied.
 *
 *  $Header: /usr/Backup/upgrade/brlcad/brlcad/Cakefile.lib,v 11.3 1997-05-06 17:32:49 mike Exp $
 */

#ifndef PRODUCTS
#	include "Symbol PRODUCTS required to make library"
#endif
#ifndef SRCDIR
#	include "Symbol SRCDIR required"
#endif
#ifndef OBJ
#	include "Symbol OBJ required"
#endif
#ifndef VERSION_VARIABLE
#	include "Symbol VERSION_VARIABLE required"
#endif
#ifndef LIBRARY_TITLE
#	include "Symbol LIBRARY_TITLE required"
#endif

#ifndef EXTRA_OBJ
#	define	EXTRA_OBJ	/**/
#endif

#ifndef EXTRA_SETUP
#	define	EXTRA_SETUP	/**/
#endif

#ifndef EXTRA_OBJ_NO_DEPENDENCY
#	define EXTRA_OBJ_NO_DEPENDENCY	/**/
#endif

#ifndef LIB_FLAGS
#	define LIB_FLAGS	/**/
#endif

#if !defined(BUILD_LIBRARY)
PRODUCTS::	EXTRA_OBJ OBJ
	newvers.sh VERSION_VARIABLE LIBRARY_TITLE
	CC CFLAGS -c vers.c
	rm -f PRODUCTS; EXTRA_SETUP
	AR r PRODUCTS OBJ EXTRA_OBJ EXTRA_OBJ_NO_DEPENDENCY vers.o
	RANLIB PRODUCTS

	/* If RANLIB is specified, be sure it is run when installing libraries */
#	define AFTERINSTALL	RANLIB
#else
	/* Shared library support.
	 * Can't list SHARED_PRODUCT in 'rm' command, or will get wrong version number.
	 * (Cake won't re-run the LIBVERS shell script)
	 * If non-null, LIBVERS will start with a leading dot.
	 * Note that the RM command zaps anything starting with PRODUCTS.
	 * Note that if you have shared libraries, you better have symlinks.
	 */
#	define	SHARED_PRODUCT	PRODUCTS''[[LIBVERS]]
PRODUCTS&::	EXTRA_OBJ OBJ
	newvers.sh VERSION_VARIABLE LIBRARY_TITLE
	CC CFLAGS -c vers.c
	rm -f PRODUCTS*; EXTRA_SETUP
	BUILD_LIBRARY SHARED_PRODUCT OBJ EXTRA_OBJ EXTRA_OBJ_NO_DEPENDENCY vers.o
	sharedliblink.sh SHARED_PRODUCT

	/* Use this opportunity to build the symlink on install */
#	define AFTERINSTALL	sharedliblink.sh
#endif

%.o&:	SRCDIR,,%,o			if exist ../SRCDIR/%.c

%9,,%,o&:	../%9/%.c HDR_DEP9	if exist ../%9/%.c
	CC CFLAGS LIB_FLAGS -c ../%9/%.c

/* Override the modes for the library */
#undef	BINDIR
#undef	BINPERM
#define	BINDIR	LIBDIR
#define	BINPERM	LIBPERM
