/*
 *				C M D H I S T . C
 *
 * A cmdhist object contains the attributes and
 * methods for maintaining command history.
 *
 *
 *  Author -
 *	  Robert G. Parker
 *
 *  Source -
 *	The U. S. Army Research Laboratory
 *	Aberdeen Proving Ground, Maryland  21005-5068  USA
 *  
 *  Distribution Notice -
 *	Re-distribution of this software is restricted, as described in
 *	your "Statement of Terms and Conditions for the Release of
 *	The BRL-CAD Package" license agreement.
 *
 *  Copyright Notice -
 *	This software is Copyright (C) 1998 by the United States Army
 *	in all countries except the USA.  All rights reserved.
 *
 */

#include "conf.h"
#include "tcl.h"

#include "machine.h"
#include "externs.h"
#include "cmd.h"

/* bu_cmdhist routines are defined in libbu/cmdhist.c */
extern int bu_cmdhist_history();
extern int bu_cmdhist_add();
extern int bu_cmdhist_curr();
extern int bu_cmdhist_next();
extern int bu_cmdhist_prev();

int cho_open_tcl();

static struct bu_cmdhist_obj HeadCmdHistObj;		/* head of command history object list */

static struct bu_cmdtab ch_cmds[] = 
{
	"add",		bu_cmdhist_add,
	"curr",		bu_cmdhist_curr,
	"next",		bu_cmdhist_next,
	"prev",		bu_cmdhist_prev,
	(char *)NULL,	CMD_NULL
};

int
cho_hist(clientData, interp, argc, argv)
     ClientData clientData;
     Tcl_Interp *interp;
     int argc;
     char **argv;
{
	return bu_cmd(clientData, interp, argc, argv, ch_cmds, 1);
}

static struct bu_cmdtab cho_cmds[] = 
{
	"add",		bu_cmdhist_add,
	"curr",		bu_cmdhist_curr,
	"history",	bu_cmdhist_history,
	"next",		bu_cmdhist_next,
	"prev",		bu_cmdhist_prev,
	(char *)NULL,	CMD_NULL
};

static int
cho_cmd(clientData, interp, argc, argv)
     ClientData	clientData;
     Tcl_Interp	*interp;
     int		argc;
     char		**argv;
{
	return bu_cmd(clientData, interp, argc, argv, cho_cmds, 1);
}

int
Cho_Init(interp)
     Tcl_Interp *interp;
{
	BU_LIST_INIT(&HeadCmdHistObj.l);
	(void)Tcl_CreateCommand(interp, "ch_open", cho_open_tcl,
				(ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);

	return TCL_OK;
}

static void
cho_deleteProc(clientData)
     ClientData clientData;
{
	struct bu_cmdhist_obj *chop = (struct  bu_cmdhist_obj *)clientData;
	struct bu_cmdhist *curr, *next;

	/* free list of commands */
	curr = BU_LIST_NEXT(bu_cmdhist, &chop->cho_head.l);
	while(BU_LIST_NOT_HEAD(curr,&chop->cho_head.l)) {
		curr = BU_LIST_NEXT(bu_cmdhist, &chop->cho_head.l);
		next = BU_LIST_PNEXT(bu_cmdhist, curr);

		bu_vls_free(&curr->h_command);

		BU_LIST_DEQUEUE(&curr->l);
		bu_free((genptr_t)curr, "cho_deleteProc: curr");
		curr = next;
	}

	bu_vls_free(&chop->cho_name);
	bu_vls_free(&chop->cho_head.h_command);

	BU_LIST_DEQUEUE(&chop->l);
	bu_free((genptr_t)chop, "cho_deleteProc: chop");
}

/*
 * Close a command history object.
 *
 * USAGE:
 *        procname close
 */
static int
cho_close_tcl(clientData, interp, argc, argv)
     ClientData      clientData;
     Tcl_Interp      *interp;
     int             argc;
     char            **argv;
{
	struct bu_cmdhist_obj *chop = (struct  bu_cmdhist_obj *)clientData;
	struct bu_vls vls;

	if (argc != 2) {
		bu_vls_init(&vls);
		bu_vls_printf(&vls, "helplib cho_close");
		Tcl_Eval(interp, bu_vls_addr(&vls));
		bu_vls_free(&vls);
		return TCL_ERROR;
	}

	/* Among other things, this will call cho_deleteProc. */
	Tcl_DeleteCommand(interp, bu_vls_addr(&chop->cho_name));

	return TCL_OK;
}

static struct bu_cmdhist_obj *
cho_open(clientData, interp, name)
     ClientData      clientData;
     Tcl_Interp      *interp;
     char            *name;
{
	struct bu_cmdhist_obj *chop;

	/* check to see if command history object exists */
	for (BU_LIST_FOR(chop, bu_cmdhist_obj, &HeadCmdHistObj.l)) {
		if (strcmp(name,bu_vls_addr(&chop->cho_name)) == 0) {
			Tcl_AppendResult(interp, "ch_open: ", name,
					 " exists.\n", (char *)NULL);
			return CMDHIST_OBJ_NULL;
		}
	}

	BU_GETSTRUCT(chop, bu_cmdhist_obj);
	bu_vls_init(&chop->cho_name);
	bu_vls_strcpy(&chop->cho_name, name);
	BU_LIST_INIT(&chop->cho_head.l);
	bu_vls_init(&chop->cho_head.h_command);
	chop->cho_head.h_start.tv_sec = chop->cho_head.h_start.tv_usec =
		chop->cho_head.h_finish.tv_sec = chop->cho_head.h_finish.tv_usec = 0L;
	chop->cho_head.h_status = TCL_OK;
	chop->cho_curr = &chop->cho_head;

	BU_LIST_APPEND(&HeadCmdHistObj.l, &chop->l);
	return chop;
}

/*
 * Open a command history object.
 *
 * USAGE:
 *        ch_open name
 */
int
cho_open_tcl(clientData, interp, argc, argv)
     ClientData      clientData;
     Tcl_Interp      *interp;
     int             argc;
     char            **argv;
{
	struct bu_cmdhist_obj *chop;
	struct bu_vls vls;

	if (argc == 1) {
		/* get list of command history objects */
		for (BU_LIST_FOR(chop, bu_cmdhist_obj, &HeadCmdHistObj.l))
			Tcl_AppendResult(interp, bu_vls_addr(&chop->cho_name), " ", (char *)NULL);

		return TCL_OK;
	}

	if (argc == 2) {
		if ((chop = cho_open(clientData, interp, argv[1])) == CMDHIST_OBJ_NULL)
			return TCL_ERROR;

		(void)Tcl_CreateCommand(interp,
					bu_vls_addr(&chop->cho_name),
					cho_cmd,
					(ClientData)chop,
					cho_deleteProc);

		/* Return new function name as result */
		Tcl_ResetResult(interp);
		Tcl_AppendResult(interp, bu_vls_addr(&chop->cho_name), (char *)NULL);
		return TCL_OK;
	}

	bu_vls_init(&vls);
	bu_vls_printf(&vls, "helplib ch_open");
	Tcl_Eval(interp, bu_vls_addr(&vls));
	bu_vls_free(&vls);
	return TCL_ERROR;
}
