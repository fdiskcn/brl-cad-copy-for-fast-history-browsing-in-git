BRL-CAD on Windows README
=========================

Compilation of BRL-CAD under Windows is performed in one of three
ways.  The first and usually most readily attainable build process is
to install BRL-CAD under the Mingw or Cygwin compatibility
environments where the build steps are the same as on the other
platforms (documented in the INSTALL file).

The second approach is to compile using MSVC 8 (2005) using the build
files in the misc/win32-msvc8 directory.  The main brlcad.sln solution
file is in the brlcad directory.  This approach will compile the vast
majority of the package and is the method utilized when making a
binary release of BRL-CAD for Windows.  This will compile 'mged',
'rt', most of the converters, many of the other geometry analysis
tools, and more.  In all, it compiles all of the BRL-CAD libraries and
about half of the available BRL-CAD utilities.

The third approach is to compile using CMake using the build files
in the misc/win32-msvc directory.  This approach, however, will ONLY
currently build the core libraries together with their C++ interface
in rt^3 and is intended for 3rd party applications that link against
BRL-CAD as a geometry engine or ray-trace library.

Special thanks to Jonathan Bonomo for the Windows installer that uses
the Nullsoft Scriptable Install System (i.e. NSIS) that replaces the
former InstallShield installer.  The NSIS scripts files are in
misc/nsis/.
