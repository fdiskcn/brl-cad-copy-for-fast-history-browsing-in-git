<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2009 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='anim_orient1'>
  <refmeta>
    <refentrytitle>ANIM_ORIENT</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  <refnamediv id='name'>
    <refname>anim_orient</refname>
    <refpurpose>convert between orientation formats</refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>anim_orient</command>
      <group choice='opt'>
	<arg choice='plain'>q</arg>
	<arg choice='plain'>y</arg>
	<arg choice='plain'>a</arg>
	<arg choice='plain'>z</arg>
	<arg choice='plain'>m</arg>
      </group>
      <arg choice='opt'><replaceable>vri</replaceable></arg>
      <group choice='opt'>
	<arg choice='plain'>q</arg>
	<arg choice='plain'>y</arg>
	<arg choice='plain'>a</arg>
	<arg choice='plain'>z</arg>
	<arg choice='plain'>m</arg>
      </group>
      <arg choice='opt'><replaceable>vriu</replaceable></arg>
      <arg choice='plain'><replaceable>in.table</replaceable></arg>
      <arg choice='plain'><replaceable>out.table</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>


  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <emphasis remap='I'>Anim_orient</emphasis> is a filter which translates a table of orientation
      information from one format to another. The method used is to convert
      the input format to a matrix, then convert the matrix to the desired
      output format. The input format is specified in the first argument, and
      the output format is specified in the second. The orientation formats must be one of the following:
      <emphasis remap='B' role='bold'>q</emphasis> (quaternion), <emphasis remap='B' role='bold'>y</emphasis>
      (yaw,pitch, and roll), <emphasis remap='B' role='bold'>a</emphasis> (azimuth, elevation, and twist),
      <emphasis remap='B' role='bold'>z</emphasis> (x-y-z angles), or <emphasis remap='B' role='bold'>m</emphasis>
      (rotation matrix).  In addition, the output format may be modified by the 
      <emphasis remap='B' role='bold'>u</emphasis> option, and the input and output formats may be modified by the
      <emphasis remap='B' role='bold'>r</emphasis>, <emphasis remap='B' role='bold'>i</emphasis>,
      and <emphasis remap='B' role='bold'>v</emphasis> options. The <emphasis remap='B' role='bold'>r</emphasis>
      option specifies that angles are read or written in radians, rather than degrees. The
      <emphasis remap='B' role='bold'>i</emphasis>,<emphasis remap='B' role='bold'>v</emphasis>,
      and <emphasis remap='B' role='bold'>u</emphasis> options are explained below.
    </para>

    <para>
      To clearly understand the operation of anim_orient, it is important to
      examine the difference between orientation and rotation.
      The matrices, quaternions, and angles used by anim_orient specify rotations. These are transformations
      which, when applied to an object, preserve its shape but change its
      orientation with respect to a fixed set of axes. Given the initial
      orientation of an object, a rotation defines the final orientation, but
      different initial orientations will yield different final orientations.
      In order to represent orientations with rotations, we
      must define a default initial orientation for the object.
      Then a given matrix, quaternion, or set of angles represents the orientation
      resulting from applying the rotation to the initial object orientation.
    </para>

    <para>
      If no modifying codes are specified, anim_orient assumes that the
      default orientation of the object in question faces the positive
      x-direction, with the world y-axis pointing to the object's left and the
      z-axis pointing up. This is the orientation in which many <emphasis remap='I'>brlcad</emphasis>
      objects are modeled, and the default assumed by other animation routines such as
      anim_script. If the <emphasis remap='B' role='bold'>v</emphasis> code is specified, then the default orientation
      is taken to be facing the negative z-direction, with the world x-axis
      pointing to the object's right and the y-axis pointing up. This is the default orientation of the eye in
      <emphasis remap='I'>mged</emphasis> and <emphasis remap='I'>rt</emphasis>. Some common uses of the v
      option are demonstrated in the examples section.
    </para>

    <para>The input and output format codes are:</para>
    
    <variablelist remap='TP'>
      <varlistentry>
	<term><emphasis remap='B' role='bold'>q:</emphasis></term>
	<listitem>
	  <para>Quaternions, in the order x,y,z,w. This is the default input and
	  output format. The output will
	  always be unit quaternions. For a unit quaternion, the
	  xyz-vector represents an axis of rotation and and w represents
	  cos(omega/2), where
	  omega is the angle to be rotated about the given axis.</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><emphasis remap='B' role='bold'>z</emphasis>:</term>
	<listitem>
	  <para>x-y-z angles, in the order x, y, z. They represent a rotation
	  of z degrees around the z-axis, followed by y degrees around the y-axis,
	  followed by x degrees around the x-axis. If the code
	  <emphasis remap='B' role='bold'>zr</emphasis>
	  is used,
	  then the angles will be read or written in radians
	  rather than degrees.</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><emphasis remap='B' role='bold'>y</emphasis>:</term>
	<listitem>
	  <para>yaw-pitch-roll format, in that order. They represent
	  a rotation of +roll degrees about the x-axis, -pitch degrees about
	  the y-axis, followed by +yaw degrees about the z-axis. If the code
	  <emphasis remap='B' role='bold'>yr</emphasis>
	  is used, angles are in radians rather than degrees.</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><emphasis remap='B' role='bold'>a</emphasis>:</term>
	<listitem>
	  <para>azimuth, elevation, twist, in that order. They represent a rotation of
	  -twist degrees about the x-axis, +elevation degrees about the y-axis,
	  and +azimuth + 180 degrees about the z-axis. Alternatively, they can
	  be thought of as a 180-degree rotation about the z-axis followed by a
	  rotation of +twist degrees about the x-axis, -elevation degrees about
	  the y-axis, and +azimuth degrees about the z-axis.</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><emphasis remap='B' role='bold'>m</emphasis>:</term>
	<listitem>
	  <para>pre-multiplication rotation matrix. Sixteen
	  numbers form a 4x4 matrix, although only the nine elements in the upper
	  left corner are actually used. This is the format used at the interior of
	  the conversion routine. It is a transformation from initial orientation to
	  a new orientation.</para>
	</listitem>
      </varlistentry>
    </variablelist>
    
    <para>The input and ouput modification codes are:</para>
    <variablelist remap='TP'>
      <varlistentry>
	<term><emphasis remap='B' role='bold'>i</emphasis></term>
	<listitem>
	  <para>This code is used in conjunction with any of the format codes to represent
	  the inverse transformation. Under the
	  <emphasis remap='B' role='bold'>i</emphasis>
	  code, a matrix, quaternion, or
	  set of angles represents the inverse of the rotation that it normally
	  represents.
	  For example, normally, the orientation
	  corresponding to a given matrix is the orientation that the default
	  object ends up in after the matrix is applied to it. With the
	  <emphasis remap='B' role='bold'>i</emphasis>
	  code,
	  a given matrix represents the orientation an object should be placed
	  in so that applying the matrix puts it back in the default
	  orientation.</para>
	  
	  <para>One common use of the
	  <emphasis remap='B' role='bold'>i</emphasis>
	  option is for handling "Viewrot" matrices used
	  by
	  <emphasis remap='I'>mged</emphasis>
	  and
	  <emphasis remap='I'>rt</emphasis>.
	  The viewrot matrix is a model-to-view matrix, which is
	  the transpose (inverse) of the matrix which would conceptually be
	  applied to an eye object to put it in the desired place in the model.</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><emphasis remap='B' role='bold'>v</emphasis></term>
	<listitem>
	  <para>When this option is specified, the corresponding rotation is considered
	  applied to the default view orientation, rather than the default object
	  orientation, as explained above.</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><emphasis remap='B' role='bold'>u</emphasis></term>
	<listitem>
	  <para>This option forces the output orientation to be upright, or "right-side
	  up," where the z-axis is the up direction. In terms of yaw, pitch, and
	  roll, this means that the roll is forced to zero. Because this is a
	  one-way operation, it can only be applied to the output orientation, not
	  the input.</para>
	</listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
  
  <refsect1 id='examples'>
    <title>EXAMPLES</title>
  
    <example>
      <title>Normalize Quaternions to Unit Magnitude</title>
      <para>
	<userinput>anim_orient &lt; in.table &gt; out.table</userinput>
      </para>
      <para>
	Normalize a table of quaternions to unit magnitude. (Quaternions are the
	default format).
      </para>
    </example>

    <example>
      <title>XYZ Angles -&gt; Rotation Matrix</title>
      <para>
	<userinput>anim_orient z m &lt; in.table &gt; out.table</userinput>
      </para>
      <para>
	change xyz angles to a rotation matrix
      </para>
    </example>
  
    <example>
      <title>az/el/tw -&gt; viewrot</title>
      <para>
	<userinput>anim_orient a miv &lt; in.table &gt; out.table</userinput>
      </para>
      <para>
	Change azimuth,elevation, and twist to a matrix suitable for the
	<emphasis remap='I'>rt</emphasis> viewrot command.
      </para>
    </example>
    
    <example>
      <title>Transpose Matricies</title>
      <para>
	<userinput>anim_orient m mi &lt; in.table &gt; out.table</userinput>
      </para>
    </example>

    
    <example>
      <title>xyz angles -&gt; yaw-pitch-roll</title>
      <para>
	<userinput>anim_orient zr y &lt; in.table &gt; out.table</userinput>
      </para>
      <para>
	change radian xyz angles to yaw-pitch-roll in degrees
      </para>
    </example>
    
    <example>
      <title>Camera Turning Quaternion</title>
      <para>
	<userinput>anim_orient q qv &lt; in.table &gt; out.table</userinput>
      </para>
      <para>
	given a quaternion being applied to an object facing the x-axis,
	find the quaternion needed to turn the virtual camera in the same direction using
	<emphasis remap='I'>rt</emphasis>'s orientation command
      </para>
    </example>
    
    <example>
      <title>Default Restoring yaw-pitch-roll</title>
      <para>
	<userinput>anim_orient y yi &lt; in.table &gt; out.table</userinput>
      </para>
      <para>
	Given the yaw, pitch, and roll of an object, find the yaw, pitch, and
	roll which would bring it back to the default position.
      </para>
    </example>
    
    <example>
      <title>Orient "right-side "</title>
      <para>
	<userinput>anim_orient qv qvu &lt; in.table &gt; out.table</userinput>
      </para>
      <para>
	Force a table of view quaternions to represent "right-side up"
	orientations.
      </para>
    </example>
  </refsect1>

  <refsect1 id='bugs'>
    <title>BUGS</title>
    <para>
      There are built-in trouble spots which occur, in ypr, xyz, or aet
      formats, when the pitch, y-rotation, or elevation is an odd multiple of 90
      degrees, because at this point the other two angles become
      indistinguishable. The program makes the arbitrary assumption that the
      roll is zero or that the x-rotation is zero, depending on the format,
      and gives a warning to that effect on stderr.
    </para>
  </refsect1>
  
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>Carl J. Nuzman</para>
  </refsect1>
  
  <refsect1 id='copyright'>
    <title>COPYRIGHT</title>
    <para>This software is Copyright (c) 1993-2009 by the United States
    Government as represented by U.S. Army Research Laboratory.</para>
  </refsect1>
  
  <refsect1 id='bug_reports'><title>BUG REPORTS</title>
  <para>Reports of bugs or problems should be submitted via electronic
  mail to &lt;devs@brlcad.org&gt;.</para>
  </refsect1>
</refentry>

