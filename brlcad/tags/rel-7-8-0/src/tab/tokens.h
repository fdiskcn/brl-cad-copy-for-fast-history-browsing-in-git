/*                        T O K E N S . H
 * BRL-CAD
 *
 * Copyright (c) 2004-2006 United States Government as represented by
 * the U.S. Army Research Laboratory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this file; see the file named COPYING for more
 * information.
 */
/** @file tokens.h
 */
#define FLOAT	1
#define INT	2
#define	SHELL	3
#define COMMENT	4
#define	START	5
#define	VIEWSIZE 6
#define EYEPT	7
#define	LOOKATPT 8
#define VIEWROT	9
#define ORIENTATION 10
#define END	11
#define TREE	12
#define MULTIVIEW	13
#define ANIM	14
#define MATRIX	15
#define RMUL	16
#define LMUL	17
#define RBOTH	18
#define RSTACK	19
#define RARC	20
#define CLEAN	21
#define SET	22
#define WIDTH	23
#define HEIGHT	24
#define PERSPECTIVE 25
#define ANGLE	26
#define	AE	27
#define OPT	28
#define STRING	29
#define SEMI	30
#define PATH	31

/*
 * Local Variables:
 * mode: C
 * tab-width: 8
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 * ex: shiftwidth=4 tabstop=8
 */
