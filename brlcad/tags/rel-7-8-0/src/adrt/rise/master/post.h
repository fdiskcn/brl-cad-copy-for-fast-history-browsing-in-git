#ifndef _RISE_POST_H
#define _RISE_POST_H

#include "cdb.h"

extern	void	rise_post_process(tfloat *image, int w, int h);

#endif
