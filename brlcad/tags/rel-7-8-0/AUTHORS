                                                 -*- coding: utf-8 -*-
BRL-CAD Contributors
====================

The BRL-CAD development team would like to take this opportunity to
thank the many people and organizations who have contributed to make
BRL-CAD the package that it is today.

BRL-CAD was originally conceived and written for the most part by the
late Michael John Muuss.  He is the original primary architect and
author.  The package has since been improved upon over the years by
Mike and the Advanced Computer Systems Team at the U.S. Army Research
Laboratory (* see note), as well as by many others around the world.
His work lives on in testament to his intellect and indomitable spirit
through the contributions and improvements by others.  We strive to
recognize those groups and individuals that have helped make BRL-CAD
the package that it is today here.

Contributions of this large collective work are separated into a few
major sections depending on the level and type of involvement that the
individual maintained.  Contributors are roughly ordered
chronologically for each section, depending upon when involvement with
BRL-CAD began.  The earliest contributors are named first; likewise,
the most recent contributors are listed at the bottom.

Names are optionally followed by the organizations that may have
supported their involvement to some degree, also listed in
chronological order.  Many individuals including many with affiliated
organizations have also supported the package of their own accord and
on their own time.  Participation and contributions are graciously
appreciated.

Active developers are denoted with an "**", meaning that they have
been actively involved within the past 12 months.


ORIGINAL ARCHITECT AND AUTHOR
-----------------------------

"The future exists first in the imagination,
then in the will, then in reality."
                - Mike Muuss


PROJECT ADMINISTRATORS
----------------------

Open Source Point of Contact
  Morrison, Christopher Sean  <morrison@brlcad.org>

U.S. Army Research Laboratory Point of Contact
  Butler, Lee  <butler@arl.army.mil>


DEVELOPERS
----------

Muuss, Michael John
U.S. Army Research Laboratory

Weaver, Earl P.
U.S. Army Ballistic Research Laboratory

Moss, Gary S.
U.S. Army Research Laboratory

Dykstra, Phillip
U.S. Army Research Laboratory

Applin, Keith A.
U.S. Army Research Laboratory

Kennedy, Charles Michael  <kermit@brlcad.org>  **
U.S. Army Research Laboratory

Gwyn, Douglas A.
U.S. Army Research Laboratory

Stay, Paul Randall
U.S. Army Research Laboratory

Hanes, Jeff
U.S. Army Research Laboratory

Butler, Lee A.  <butler@arl.army.mil>  **
U.S. Army Research Laboratory

Johnson, Christopher T.  <cjohnson@cyberpaladin.com>
Cray Research, Inc.
Geometric Solutions, Inc.
Paladin Software
JRMTech, Inc.

Ross, Adam
Geometric Solutions, Inc.

Markowski, Michael J.  <michael.markowski@arl.army.mil>
U.S. Army Ballistic Research Laboratory

Anderson, John R.  <jra@arl.army.mil>  **
U.S. Army Research Laboratory

Tanenbaum, Paul J.
U.S. Army Research Laboratory

Durfee, Glenn
ARL Student Hire / Carnegie Mellon University

Parker, Robert G.  **
U.S. Army Research Laboratory
Survice Engineering, Inc.

Morrison, Christopher Sean  <morrison@brlcad.org>  **
ARL Student Hire / Johns Hopkins University
Quantum Research International, Inc.

Bowers, Ronald A.
U.S. Army Research Laboratory

Hartley, Robert  <hartley@vt.edu>
ARL Student Hire / Virginia Tech

Christy, TraNese Shantell
U.S. Army Research Laboratory

Shumaker, Justin  **
Quantum Research International, Inc.
U.S. Army Research Laboratory

Greenwald, Erik  <erikg@arl.army.mil>  **
U.S. Army Research Laboratory


MATHEMATICAL, ALGORITHMIC, AND GEOMETRIC SUPPORT
------------------------------------------------

Davisson, Edwin  **
U.S. Army Research Laboratory

Rodgers, David
U.S. Naval Academy

Reed, Harry
Geometric Solutions, Inc.
U.S. Army Ballistic Research Laboratory

Applin, Keith A.
U.S. Army Research Laboratory

Kregel, Dwayne  **
Survice Engineering, Inc.


CODE CONTRIBUTIONS
------------------

Hartwig, Jr., George W.
U.S. Army Ballistic Research Laboratory

Pistritto, Joseph C.
U.S. Army Ballistic Research Laboratory

Natalie, Ronald B.
U.S. Army Ballistic Research Laboratory

Toth, George E.
U.S. Army Ballistic Research Laboratory

Mermagen, Jr., William
U.S. Army Research Laboratory

Barker, Natalie L.
U.S. Army Research Laboratory

Becker, David
Cray Research

Hunt, Jim  <jhunt@ara.com>
Applied Research Associates, Inc.
U.S. Army Research Laboratory

Dender, Daniel C
U.S. Army Research Laboratory
Survice Engineering, Inc.

Coates, Sue
U.S. Army Research Laboratory (??? only brl)

Ross, Brant A
General Motors, Inc.

Lindemann, William
Sun Microsystems, Inc.

Smith, Timothy G.
Sun Microsystems, Inc.

Jackson, Christopher J.
Sun Microsystems, Inc.

Fiori, James D.
Sun Microsystems, Inc.

DiGiacinto, Tom
U.S. Army Ballistic Research Laboratory

Bowden, Mark Huston
University of Alabama

Suckling, Bob
U.S. Army Ballistic Research Laboratory

Reschly, Jr., Robert J.
U.S. Army Ballistic Research Laboratory

Gigante, Mike
Royal Melbourne Institute of Technology

Muuss, Susanne L.
U.S. Army Research Laboratory

Dray, Adam
Geometric Solutions, Inc.

Nuzman, Carl J.
ARL Student Hire / Princeton University

Laut, Bill
Gull Island Consultants, Inc.

Hewitt, Terry
Manchester-Computing Centre, UK

Brand, Mario

McCarty, Corey
ARL Student Hire / Johns Hopkins University

Owens, Jason
ARL Student Hire / Johns Hopkins University

Yemc, David J.
Johns Hopkins University Applied Physics Laboratory

Bowman, Keith
Survice Engineering, Inc.

Anderson, David
SGI

Ro�berg, Daniel
Industrieanlagen-Betriebsgesellschaft mbH (IABG)


SPECIAL THANKS
--------------

Henrikson, Bruce
U.S. Army Ballistic Research Laboratory

Wolff, Stephen
U.S. Army Ballistic Research Laboratory

Stanley, Charles
U.S. Army Ballistic Research Laboratory

Deitz, Paul
U.S. Army Ballistic Research Laboratory

Miles, Robert S.
U.S. Army Ballistic Research Laboratory

Martin, Glenn E.
Northrup Research and Technology Center

Willson, Stephen Hunter
Northrup Research and Technology Center

Merrit, Don
U.S. Army Research Laboratory

zum Brunnen, Rick
U.S. Army Research Laboratory

Smith, Jill
U.S. Army Research Laboratory

Monk, Mary
U.S. Army Research Laboratory

Spencer, Thomas W.
University of Utah

McKie, Jim

Davies, Steve

Turkowski, Ken

Woods, James A.

Orost, Joseph

Lorenzo, Max
CECOM Night Vision & Electronic Sensors Directorate

Dome, John
CECOM Night Vision & Electronic Sensors Directorate

Florek, David
CECOM Night Vision & Electronic Sensors Directorate

Stoudenmire, Eugene A.
CECOM Night Vision & Electronic Sensors Directorate

Kekesi, Alex
CECOM Night Vision & Electronic Sensors Directorate

Pronk, Sander
TNO Prins Maurits Laboratory

Moulton, Jr., Russ
EOSoft, Inc. (CECOM)

Reed, Jr., Harry
U.S. Army Ballistic Research Laboratory

Satterfield, Steve
U.S. Naval Academy

Johnson, Joe
U.S. Naval Academy

Woo, Alex
University of Illinois

Musgrave, F. Kenton

Skinner, Robert

Sebok, William L.

Haywood, Heather
U.S. Army Research Laboratory

Ousterhout, John

Edwards, Eric
Survice Engineering, Inc.

Bokkers, Wim
TNO Prins Maurits Laboratory

Yapp, Cliff

Giffuni, Pedro

Thomas, Balbir

Remenak, Daniel T.
California Institute of Technology

Caruso, Mike
Qryon LLC

Arunaogun, Razak
Qryon LLC


CORPORATE AND ORGANIZATION SUPPORT
----------------------------------

U.S. Army Research Laboratory
  formerly named the U.S. Army Ballistic Research Laboratory
Survivability and Lethality Analysis Directorate

Sun Microsystems, Inc.

University of Alabama in Huntsville

U.S. Army Communications-Electronics Command (CECOM)
Night Vision & Electronic Sensors Directorate

U.S. Naval Academy

Northrup, Inc.
Norhtrup Research and Technology Center

SGI
  formerly named Silicon Graphics, Inc.

Geometric Solutions, Inc.

JRMTech, Inc.

Survice Engineering, Inc.

Quantum Research Laboratory, Inc.

Paladin Software, Inc.

Manchester-Computing-Centre, UK
Computer Graphics Unit, MCC

Industrieanlagen-Betriebsgesellschaft mbH (IABG)

TNO Prins Maurits Laboratory

Free Software Foundation

Open Source Technology Group
  SourceForge.net, freshmeat.net

Qryon LLC


NEWS ARTICLES
-------------

Muuss, Mike
"The BRL-CAD Package." Ray Tracing News. Vol 2, No 2. 1989/02/20

"Designing with BRL-CAD." ITWorld.com, Linux Desktop Applications. 2000/12/14

Wasmund, Thomas L.
"New Model to Evaluate Weapon Effects and Platform Vulnerability: AJEM." WSTIAC NEWSLETTER. Vol 2, No 4. 2001/09

Jackson, Joab
"Open for business." Government Computer News. Vol 24, No 13. 2005/06/06


---

* The U.S. Army Research Laboratory (ARL) was formerly named the
U.S. Army Ballistic Research Laboratory (BRL).  Persons who
contributed entirely before the name change reflect the original name,
whereas persons who continued or began contributions after the name
change reflect the new name.

** The contributor is an active developer, meaning that they have been
actively involved with the BRL-CAD development within the past 12 months.

If any corrections or additions need to be made, please contact one of
the active core developers.  Additionally, you may send inquiries,
suggestions, and comments to the developer mailing list:
brlcad-devel@lists.sourceforge.net
