<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='ls'>

<refmeta>
  <refentrytitle>LS</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>ls</refname>
  <refpurpose>The command with no <emphasis>object</emphasis> argument lists the name of every object in the
database (in alphabetical order) except for those marked as hidden with the <command>hide</command>
command.  If the object argument is supplied, only those objects are listed.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>ls</command>    
      <arg>-A -o -a -c -r -s -p -l</arg>
    	<arg><replaceable>objects</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>The command with no <emphasis>object</emphasis> argument lists the name of every object in the
	database (in alphabetical order) except for those marked as hidden with the <command>hide</command>
	command.  If the <emphasis>object</emphasis> argument is supplied, only those <emphasis>objects</emphasis> 	are listed.  The <emphasis>object</emphasis> argument may include regular expressions. If the <emphasis>-A	</emphasis> option is used, then the arguments are expected to be a list of attribute name/value pairs, and 	objects having attributes that match the provided list are listed. By default, an object must match all
	the specified attributes in order to be listed; however, the <emphasis>-o</emphasis> flag indicates that an
	object matching at least one attribute name/value pair should be listed. See the <command>attr</command>
	command for information on how to set or get attributes. Regular expressions are not
	supported for attributes. The following options are also allowed:
   </para>
<itemizedlist mark='bullet'>
<listitem>
 <para>a -- List all objects in the database.
 </para>
</listitem>
<listitem>
 <para>c -- List all non-hidden combinations in the database.
 </para>
</listitem>
<listitem>
 <para>r -- List all non-hidden regions in the database.
 </para>
</listitem>
<listitem>
 <para>s -- List all non-hidden primitives in the database.
 </para>
</listitem>
<listitem>
 <para>p -- List all non-hidden primitives in the database.
 </para>
</listitem>
<listitem>
 <para>l -- Use long format showing object name, object type, major type, minor type, and
   length.
 </para>
</listitem>
</itemizedlist>

<para>The <command>ls</command> command is a synonym for the <command>t</command> command.  Note that when any of 	the above options are used, the output is not formatted.
</para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The examples show the use of the <command>ls</command> command and its options to list objects, primitives, regions, or combinations having a specific object. 
  </para>
  <example>
    <title>List all objects beginning with a particular name.</title>
    <variablelist>
      <varlistentry>
	   <term><prompt>mged></prompt> <userinput>ls shape*</userinput></term>
	   <listitem>
	     <para>Lists all objects with names beginning with "shape" (output is formatted).
	      	     </para>
	   </listitem>
      </varlistentry>
    </variablelist>
  </example>
       
  <example>
    <title>List all objects beginning with a particular name.</title>
    <para>
      <prompt>mged></prompt><userinput>ls -a shape*</userinput>
    </para>
    <para>Lists all objects with names beginning with "shape."
    </para>
  </example>
  <example>
    <title>List all primitives beginning with a particular name.</title>
    <para>
      <prompt>mged></prompt><userinput>ls -p wheel*</userinput>
    </para>
    <para>Lists all primitives with names beginning with "wheel."
    </para>
  </example>
 <example>
    <title>List all regions beginning with a particular name.</title>
    <para>
      <prompt>mged></prompt><userinput>ls -r wheel*</userinput>
    </para>
    <para>Lists all regions with names beginning with "wheel."
    </para>
  </example>
 <example>
    <title>List all combinations beginning with a particular name.</title>
    <para>
      <prompt>mged></prompt><userinput>ls -c suspension*</userinput>
    </para>
    <para>Lists all combinations with names beginning with "suspension."
    </para>
  </example>
  <example>
    <title>List all regions having particular attributes.</title>
    <para>
      <prompt>mged></prompt><userinput>ls -A -o -r Comment{First comment} Comment{Second comment}</userinput>
    </para>
    <para>Lists all regions that have a "Comment" attribute that is set to either "First comment" or "Second comment."
    </para>
  </example>

</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

