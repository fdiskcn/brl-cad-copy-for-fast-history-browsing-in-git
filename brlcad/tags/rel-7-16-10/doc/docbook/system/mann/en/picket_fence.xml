<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2008-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR AS IS'' AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='picket_fence1'>

<refmeta>
  <refentrytitle>PICKET_FENCE</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>picket_fence</refname>
  <refpurpose>
    Creates a BRL-CAD .g file containing picket fences.
  </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>picket_fence</command>
    <arg><replaceable>filename</replaceable></arg> 
    <arg><replaceable>prefix</replaceable></arg> 
    <arg><replaceable>height</replaceable></arg> 
    <arg><replaceable>spacing</replaceable></arg> 
    <arg><replaceable>x0</replaceable></arg> 
    <arg><replaceable>y0</replaceable></arg> 
    <arg><replaceable>z0</replaceable></arg> 
    <arg rep='repeat'></arg> 
    <arg><replaceable>xn</replaceable></arg> 
    <arg><replaceable>yn</replaceable></arg> 
    <arg><replaceable>zn</replaceable></arg> 
    <arg choice='opt' ><replaceable>-r</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>
    <command>picket_fence</command> creates a geometry file in
    <emphasis>filename</emphasis> of a picket fence.  All  parameters
    for the fence are in mm.  If more than two points are specified 
    it will generate a fence with multiple sections connecting the 
    points.  Parts of the fence are created starting at the origin.  The
    two backing boards are created in the negative x halfspace.  The pickets are
    created in the positive x halfspace and translated down the y-axis to their
    proper positions.  The entire segment is then rotated and translated to the
    position specified by the user.  To create a box of fence with the
    pickets on the outside, the points must be specified in a counter-clockwise
    order.  Parts of the model are named according to the format
    <literallayout>
&lt;prefix&gt;&lt;partname&gt;&lt;sec_name&gt;&lt;[&gt;part_num&lt;].&lt;obj_type&gt;
    </literallayout>
    The <emphasis>&lt;prefix&gt;</emphasis> above is the second argument to the 
    program.  The <emphasis>spacing</emphasis> parameter specifies the amound of
    space (in mm) between pickets.  The <emphasis>-r</emphasis> option specifies round
    fronts for the pickets.
 </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>
  Creates an example fence two meters high and six meters long with
  fifteen millimeter spaces between the pickets.  It will start the
  fence at the origin and extend it to (100,6000,0).
  </para>
  <example>
    <title><command>picket_fence</command> Example</title>
    <para>
      <userinput>picket_fence fence.g Imy2000 15 0 0 0 100 6000 0</userinput>
    </para>
  </example>

</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='copyright'>
  <title>COPYRIGHT</title>
  <para>
    This software is Copyright (c) 2005-2010 United States Government as
    represented by the U.S. Army Research Laboratory. All rights reserved.
  </para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

