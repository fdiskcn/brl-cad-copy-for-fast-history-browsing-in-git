<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='mater'>

<refmeta>
  <refentrytitle>MATER</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>mater</refname>
  <refpurpose>Assigns shader parameters, RGB color, and inheritance to an
	existing combination.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>mater</command>    
    
    <arg choice='req'><replaceable>combination</replaceable></arg>
    <arg>shader_parameters <arg>RGB <arg>inheritance</arg></arg></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Assigns shader parameters, RGB color, and inheritance to an
existing combination. The information may be included on the command line;
otherwise the user will be prompted for it. Some available shaders are:
  </para>
<itemizedlist mark='bullet'>
<listitem>
<para>bump -- bump maps.
</para>
</listitem>
<listitem>
<para>bwtexture -- black and white texture maps.
</para>
</listitem>
<listitem>
<para>camo -- camouflage.
</para>
</listitem>
<listitem>
<para>checker -- checkerboard design.
</para>
</listitem>
<listitem>
<para>cloud -- 2D Geoffrey Gardner style cloud texture map.
</para>
</listitem>
<listitem>
<para>envmap -- environment mapping.
</para>
</listitem>
<listitem>
<para>fakestar -- a fake star pattern.
</para>
</listitem>
<listitem>
<para>fbmbump -- fbm noise applied to surface normal.
</para>
</listitem>
<listitem>
<para>fbmcolor -- fbm noise applied to color.
</para>
</listitem>
<listitem>
<para>fire -- flames simulated with turbulence noise.
</para>
</listitem>
<listitem>
<para>glass -- Phong shader with values set to simulate glass.
</para>
</listitem>
<listitem>
<para>gravel -- turbulence noise applied to color and surface normal.
</para>
</listitem>
<listitem>
<para>light -- light source.
</para>
</listitem>
<listitem>
<para>marble -- marble texture.
</para>
</listitem>
<listitem>
<para>mirror -- Phong shader with values set to simulate mirror.
</para>
</listitem>
<listitem>
<para>plastic -- Phong shader with values set to simulate plastic.
</para>
</listitem>
<listitem>
<para>rtrans -- random transparency.
</para>
</listitem>
<listitem>
<para>scloud -- 3D cloud shader.
</para>
</listitem>
<listitem>
<para>spm -- spherical texture maps.
</para>
</listitem>
<listitem>
<para>stack -- allows stacking of shaders.
</para>
</listitem>
<listitem>
<para>stxt -- shape texture mapping.
</para>
</listitem>
<listitem>
<para>texture -- full color texture mapping.
</para>
</listitem>
<listitem>
<para>turbump -- turbulence noise applied to surface normals.
</para>
</listitem>
<listitem>
<para>turcolor -- turbulence noise applied to color.
</para>
</listitem>
<listitem>
<para>wood -- wood texture.
</para>
</listitem>
</itemizedlist>

</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The example shows the use of the <command>mater</command> command to assign shader parameters, color, and inheritance to a region.
    
  </para>
  <example>
    <title>Assigning shader parameters, color, and inheritance to a region.</title>
    <variablelist>
      <varlistentry>
	   <term><prompt>mged></prompt> <userinput>mater region1 "plastic {tr 0.5 re 0.2}" 210 100 100 0		</userinput></term>
	   <listitem>
	     <para>Sets <emphasis>region1</emphasis> to use the plastic shader with 50% transparency, 20% 			reflectivity, a base color of (210 100 100), and inheritance set to 0.
	      	     </para>
	   </listitem>
      </varlistentry>
    </variablelist>
  </example>
       
  </refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

