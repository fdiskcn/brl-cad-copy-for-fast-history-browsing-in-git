<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR AS IS'' AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='all_sf1'>

<refmeta>
  <refentrytitle>ALL_SF</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>all_sf</refname>
  <refpurpose>
    obtain shape factors between named regions of an entire mged database
  </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>all_sf</command>    
    <arg choice='plain'><replaceable>model.g</replaceable></arg>
    <arg choice='plain' rep='repeat'><replaceable>objects</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='descriptions'>
  <title>DESCRIPTIONS</title>
  <para>
    <command>all_sf</command> is an interactive program used to create 
    a file containing shape factors between all regions of the named objects.  
    The shape factor from region i to region j is the fraction of total energy 
    radiated from region i that is intercepted by region j.  This program finds
    the shapefactors for all regions in an mged model not just shapefactors
    between engine regions.
  </para>

  <para>
    All_sf uses a Monte Carlo simulation, so increasing the number of
    significant digits in the answer by one requires about a 100-fold 
    increase in the number of rays fired.
  </para>

  <para>
    Three different files are created by <command>all_sf</command>: an output file, a 
    longwave radiation exchange file, and an error file.  The output file lists the
    shapefactors between each region.  The longwave radiation exchange
    file is for use with PRISM.  This file is not quite PRISM ready so the
    user must fill in the missing parameters if it is to be used with
    PRISM.  The error file is basically used for checking errors in the
    program and probably will not be used by the average user.
  </para>
</refsect1>

<refsect1 id='example'>
  <title>EXAMPLE</title>
  <para>
    The following is an example from an interactive session.
  </para>
  
  <programlisting remap='.nf'>
	$ all_sf con.sph.g all.air
	Enter name of output file (25 char max).
		cs.out
	Enter name of longwave radiation exchange file (25 char max).
		cs.lwx
	Enter the name of the error file (25 char max).
		cs.err
	Database Title:  concentric spheres for use with
	Number of regions:  3
	Min &amp; max for entire model.
		X:  -2300.000000 - 2300.000000
		Y:  -2300.000000 - 2300.000000
		Z:  -2300.000000 - 2300.000000
	Center:  0.000000, 0.000000, 0.000000

	Radius:  3984.216857
	Surface Area:  199478365.653926

	Enter the number of rays to be fired.
		50000000
	Region names in structure.
	Do you wish to enter your own seed (0) or use the default of 1 (1)?
		1
	Seed initialized
	$
   </programlisting> <!-- .fi -->
</refsect1>

<refsect1 id='see_also'>
  <title>SEE ALSO</title>
  <para>
    <citerefentry>
      <refentrytitle>firpass</refentrytitle>
      <manvolnum>1</manvolnum>
   </citerefentry>, 
   <citerefentry>
     <refentrytitle>secpass</refentrytitle>
     <manvolnum>1</manvolnum>
   </citerefentry>, 
   <citerefentry>
     <refentrytitle>showtherm</refentrytitle>
     <manvolnum>1</manvolnum>
   </citerefentry>, 
   <citerefentry>
     <refentrytitle>ir-X</refentrytitle>
     <manvolnum>1</manvolnum>
   </citerefentry>,
   <citerefentry>
     <refentrytitle>ir-sgi</refentrytitle>
     <manvolnum>1</manvolnum>
   </citerefentry>, 
   <citerefentry>
     <refentrytitle>pictx</refentrytitle>
     <manvolnum>1</manvolnum>
   </citerefentry>,
   <citerefentry>
     <refentrytitle>pictsgi</refentrytitle>
     <manvolnum>1</manvolnum>
   </citerefentry>,
   <citerefentry>
     <refentrytitle>shapefact</refentrytitle>
     <manvolnum>1</manvolnum>
   </citerefentry>, 
   User's Manual for IRPREP (BRL-SP-96), Computer Programs
   for Generating an Input File for PRISM and Displaying PRISM Results
   (BRL report in progress)
  </para>
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>Susan A. Coates</para>
</refsect1>

</refentry>

