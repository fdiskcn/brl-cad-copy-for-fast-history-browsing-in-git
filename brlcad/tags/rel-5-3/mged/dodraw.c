/*
 *			D O D R A W . C
 *
 * Functions -
 *	drawtrees	Add a set of tree hierarchies to the active set
 *	drawHsolid	Manage the drawing of a COMGEOM solid
 *	pathHmat	Find matrix across a given path
 *	replot_original_solid	Replot vector list for a solid
 *	replot_modified_solid	Replot solid, given matrix and db record.
 *	invent_solid		Turn list of vectors into phony solid
 *  
 *  Author -
 *	Michael John Muuss
 *  
 *  Source -
 *	SECAD/VLD Computing Consortium, Bldg 394
 *	The U. S. Army Ballistic Research Laboratory
 *	Aberdeen Proving Ground, Maryland  21005
 *  
 *  Copyright Notice -
 *	This software is Copyright (C) 1985 by the United States Army.
 *	All rights reserved.
 */
#ifndef lint
static char RCSid[] = "@(#)$Header$ (BRL)";
#endif

#include "conf.h"

#include <stdio.h>
#include "machine.h"
#include "bu.h"
#include "vmath.h"
#include "bn.h"
#include "nmg.h"
#include "raytrace.h"
#include "rtgeom.h"		/* for ID_POLY special support */
#include "./ged.h"
#include "externs.h"
#include "./mged_solid.h"
#include "./mged_dm.h"

#include "../librt/debug.h"	/* XXX */

void		cvt_vlblock_to_solids();
void		drawH_part2();
extern void	(*nmg_plot_anim_upcall)();
extern void	(*nmg_vlblock_anim_upcall)();
extern void	(*nmg_mged_debug_display_hack)();
long	nvectors;	/* number of vectors drawn so far */

unsigned char geometry_default_color[] = { 255, 0, 0 };

/*
 *  This is just like the rt_initial_tree_state in librt/tree.c,
 *  except that the default color is red instead of white.
 *  This avoids confusion with illuminate mode.
 *  Red is a one-gun color, avoiding convergence problems too.
 */
struct db_tree_state	mged_initial_tree_state = {
	0,			/* ts_dbip */
	0,			/* ts_sofar */
	0, 0, 0,		/* region, air, gmater */
	100,			/* GIFT los */
#if __STDC__
	{
#endif
		/* struct mater_info ts_mater */
		1.0, 0.0, 0.0,		/* color, RGB */
		-1.0,			/* Temperature */
		0,			/* ma_color_valid=0 --> use default */
		0,			/* color inherit */
		0,			/* mater inherit */
		(char *)NULL		/* shader */
#if __STDC__
	}
#endif
	,
	1.0, 0.0, 0.0, 0.0,
	0.0, 1.0, 0.0, 0.0,
	0.0, 0.0, 1.0, 0.0,
	0.0, 0.0, 0.0, 1.0,
};

static int		mged_draw_nmg_only;
static int		mged_nmg_triangulate;
static int		mged_draw_wireframes;
static int		mged_draw_normals;
static int		mged_draw_solid_lines_only=0;
static int		mged_draw_no_surfaces = 0;
static int		mged_shade_per_vertex_normals=0;
int			mged_wireframe_color_override;
int			mged_wireframe_color[3];
static struct model	*mged_nmg_model;
struct rt_tess_tol	mged_ttol;	/* XXX needs to replace mged_abs_tol, et.al. */

extern struct bn_tol		mged_tol;	/* from ged.c */

/*
 *		M G E D _ P L O T _ A N I M _ U P C A L L _ H A N D L E R
 *
 *  Used via upcall by routines deep inside LIBRT, to have a UNIX-plot
 *  file dyanmicly overlaid on the screen.
 *  This can be used to provide a very easy to program diagnostic
 *  animation capability.
 *  Alas, no wextern keyword to make this a little less indirect.
 */
void
mged_plot_anim_upcall_handler( file, us )
char	*file;
long	us;		/* microseconds of extra delay */
{
	char *av[3];

	/* Overlay plot file */
	av[0] = "overlay";
	av[1] = file;
	av[2] = NULL;
	(void)f_overlay((ClientData)NULL, interp, 2, av);

	do {
		event_check( 1 );	/* Take any device events */
		refresh();		/* Force screen update */
		us -= frametime * 1000000;
	} while (us > 0);

#if 0
	/* Extra delay between screen updates, for more viewing time */
	/* Use /dev/tty to select on, because stdin may be a file */
	if(us)  {
		int	fd;

		if( (fd = open("/dev/tty", 2)) < 0 )  {
			perror("/dev/tty");
		} else {
			struct timeval tv;
			fd_set readfds;

			FD_ZERO(&readfds);
			FD_SET(fd, &readfds);
			tv.tv_sec = 0L;
			tv.tv_usec = us;

			select( fd+1, &readfds, (fd_set *)0, (fd_set *)0, &tv );
			close(fd);
		}
	}
#endif
}

/*
 *		M G E D _ V L B L O C K _ A N I M _ U P C A L L _ H A N D L E R
 *
 *  Used via upcall by routines deep inside LIBRT, to have a UNIX-plot
 *  file dyanmicly overlaid on the screen.
 *  This can be used to provide a very easy to program diagnostic
 *  animation capability.
 *  Alas, no wextern keyword to make this a little less indirect.
 */
void
mged_vlblock_anim_upcall_handler( vbp, us, copy )
struct bn_vlblock	*vbp;
long		us;		/* microseconds of extra delay */
int		copy;
{

	cvt_vlblock_to_solids( vbp, "_PLOT_OVERLAY_", copy );


	do  {
		event_check( 1 );	/* Take any device events */
		refresh();		/* Force screen update */
		us -= frametime * 1000000;
	} while (us > 0);
#if 0
	/* Extra delay between screen updates, for more viewing time */
	/* Use /dev/tty to select on, because stdin may be a file */
	if(us)  {
		int	fd;
		if( (fd = open("/dev/tty", 2)) < 0 )  {
			perror("/dev/tty");
		} else {
			struct timeval tv;
			fd_set readfds;

			FD_ZERO(&readfds);
			FD_SET(fd, &readfds);
			tv.tv_sec = 0L;
			tv.tv_usec = us;

			select( fd+1, &readfds, (fd_set *)0, (fd_set *)0, &tv );
			close(fd);
		}
	}
#endif
}
static void
hack_for_lee()
{
	event_check( 1 );	/* Take any device events */

	refresh();		/* Force screen update */
}

/*
 *			M G E D _ W I R E F R A M E _ R E G I O N _ E N D
 *
 *  This routine must be prepared to run in parallel.
 */
HIDDEN union tree *mged_wireframe_region_end( tsp, pathp, curtree, client_data )
register struct db_tree_state	*tsp;
struct db_full_path	*pathp;
union tree		*curtree;
genptr_t		client_data;
{
	return( curtree );
}

/*
 *			M G E D _ W I R E F R A M E _ L E A F
 *
 *  This routine must be prepared to run in parallel.
 */
HIDDEN union tree *mged_wireframe_leaf( tsp, pathp, ep, id, client_data )
struct db_tree_state	*tsp;
struct db_full_path	*pathp;
struct bu_external	*ep;
int			id;
genptr_t		client_data;
{
	struct rt_db_internal	intern;
	union tree	*curtree;
	int		dashflag;		/* draw with dashed lines */
	struct bu_list	vhead;

	RT_CK_TESS_TOL(tsp->ts_ttol);
	BN_CK_TOL(tsp->ts_tol);

	BU_LIST_INIT( &vhead );

	if(rt_g.debug&DEBUG_TREEWALK)  {
	  char	*sofar = db_path_to_string(pathp);

	  Tcl_AppendResult(interp, "mged_wireframe_leaf(", rt_functab[id].ft_name,
			   ") path='", sofar, "'\n", (char *)NULL);
	  bu_free((genptr_t)sofar, "path string");
	}

	if( mged_draw_solid_lines_only )
		dashflag = 0;
	else
		dashflag = (tsp->ts_sofar & (TS_SOFAR_MINUS|TS_SOFAR_INTER) );

    	RT_INIT_DB_INTERNAL(&intern);
	if( rt_functab[id].ft_import( &intern, ep, tsp->ts_mat, dbip ) < 0 )  {
	  Tcl_AppendResult(interp, DB_FULL_PATH_CUR_DIR(pathp)->d_namep,
			   ":  solid import failure\n", (char *)NULL);

	  if( intern.idb_ptr )  rt_functab[id].ft_ifree( &intern );
	  return(TREE_NULL);		/* ERROR */
	}
	RT_CK_DB_INTERNAL( &intern );

	if( rt_functab[id].ft_plot(
	    &vhead,
	    &intern,
	    tsp->ts_ttol, tsp->ts_tol ) < 0 )  {
	  Tcl_AppendResult(interp, DB_FULL_PATH_CUR_DIR(pathp)->d_namep,
			   ": plot failure\n", (char *)NULL);
	  rt_functab[id].ft_ifree( &intern );
	  return(TREE_NULL);		/* ERROR */
	}

	/*
	 * XXX HACK CTJ - drawH_part2 sets the default color of a
	 * solid by looking in tps->ts_mater.ma_color, for pseudo
	 * solids, this needs to be something different and drawH
	 * has no idea or need to know what type of solid this is.
	 */
	if (intern.idb_type == ID_GRIP) {
		int r,g,b;
		r= tsp->ts_mater.ma_color[0];
		g= tsp->ts_mater.ma_color[1];
		b= tsp->ts_mater.ma_color[2];
		tsp->ts_mater.ma_color[0] = 0;
		tsp->ts_mater.ma_color[1] = 128;
		tsp->ts_mater.ma_color[2] = 128;
		drawH_part2( dashflag, &vhead, pathp, tsp, SOLID_NULL );
		tsp->ts_mater.ma_color[0] = r;
		tsp->ts_mater.ma_color[1] = g;
		tsp->ts_mater.ma_color[2] = b;
	} else {
		drawH_part2( dashflag, &vhead, pathp, tsp, SOLID_NULL );
	}
	rt_functab[id].ft_ifree( &intern );

	/* Indicate success by returning something other than TREE_NULL */
	BU_GETUNION( curtree, tree );
	curtree->magic = RT_TREE_MAGIC;
	curtree->tr_op = OP_NOP;

	return( curtree );
}
/* XXX Grotesque, shameless hack */
static int mged_do_not_draw_nmg_solids_during_debugging = 0;
static int mged_draw_edge_uses=0;
static int mged_enable_fastpath = 0;
static int mged_fastpath_count=0;	/* statistics */
static struct bn_vlblock	*mged_draw_edge_uses_vbp;

/*
 *			M G E D _ N M G _ R E G I O N _ S T A R T
 *
 *  When performing "ev" on a region, consider whether to process
 *  the whole subtree recursively.
 *  Normally, say "yes" to all regions by returning 0.
 *
 *  Check for special case:  a region of one solid, which can be
 *  directly drawn as polygons without going through NMGs.
 *  If we draw it here, then return -1 to signal caller to ignore
 *  further processing of this region.
 *  A hack to view polygonal models (converted from FASTGEN) more rapidly.
 */
mged_nmg_region_start( tsp, pathp, combp, client_data )
struct db_tree_state	*tsp;
struct db_full_path	*pathp;
CONST struct rt_comb_internal *combp;
genptr_t client_data;
{
	union tree		*tp;
	struct directory	*dp;
	struct rt_db_internal	intern;
	mat_t			xform;
	matp_t			matp;
	struct bu_list		vhead;

	if(rt_g.debug&DEBUG_TREEWALK)  {
		char	*sofar = db_path_to_string(pathp);
		bu_log("mged_nmg_region_start(%s)\n", sofar);
		bu_free((genptr_t)sofar, "path string");
		rt_pr_tree( combp->tree, 1 );
		db_pr_tree_state(tsp);
	}

	BU_LIST_INIT( &vhead );

	RT_CK_COMB(combp);
	tp = combp->tree;
	if( !tp )
		return( -1 );
	RT_CK_TREE(tp);
	if( tp->tr_l.tl_op != OP_DB_LEAF )
		return 0;	/* proceed as usual */

	/* The subtree is a single node.  It may be a combination, though */

	/* Fetch by name, check to see if it's an easy type */
	dp = db_lookup( tsp->ts_dbip, tp->tr_l.tl_name, LOOKUP_NOISY );
	if( !dp )
		return 0;	/* proceed as usual */
	if( tsp->ts_mat )  {
		if( tp->tr_l.tl_mat )  {
			matp = xform;
			bn_mat_mul( xform, tsp->ts_mat, tp->tr_l.tl_mat );
		} else {
			matp = tsp->ts_mat;
		}
	} else {
		if( tp->tr_l.tl_mat )  {
			matp = tp->tr_l.tl_mat;
		} else {
			matp = (matp_t)NULL;
		}
	}
	if( rt_db_get_internal(&intern, dp, tsp->ts_dbip, matp) < 0 )
		return 0;	/* proceed as usual */

	switch( intern.idb_type )  {
	case ID_POLY:
		{
			struct rt_pg_internal	*pgp;
			register int	i;
			int		p;

			if(rt_g.debug&DEBUG_TREEWALK)  {
				bu_log("fastpath draw ID_POLY\n", dp->d_namep);
			}
			pgp = (struct rt_pg_internal *)intern.idb_ptr;
			RT_PG_CK_MAGIC(pgp);

			if( mged_draw_wireframes )  {
				for( p = 0; p < pgp->npoly; p++ )  {
					register struct rt_pg_face_internal	*pp;

					pp = &pgp->poly[p];
					RT_ADD_VLIST( &vhead, &pp->verts[3*(pp->npts-1)],
						BN_VLIST_LINE_MOVE );
					for( i=0; i < pp->npts; i++ )  {
						RT_ADD_VLIST( &vhead, &pp->verts[3*i],
							BN_VLIST_LINE_DRAW );
					}
				}
			} else {
				for( p = 0; p < pgp->npoly; p++ )  {
					register struct rt_pg_face_internal	*pp;
					vect_t aa, bb, norm;

					pp = &pgp->poly[p];
					if( pp->npts < 3 )  continue;
					VSUB2( aa, &pp->verts[3*(0)], &pp->verts[3*(1)] );
					VSUB2( bb, &pp->verts[3*(0)], &pp->verts[3*(2)] );
					VCROSS( norm, aa, bb );
					VUNITIZE(norm);
					RT_ADD_VLIST( &vhead, norm,
						BN_VLIST_POLY_START );

					RT_ADD_VLIST( &vhead, &pp->verts[3*(pp->npts-1)],
						BN_VLIST_POLY_MOVE );
					for( i=0; i < pp->npts-1; i++ )  {
						RT_ADD_VLIST( &vhead, &pp->verts[3*i],
							BN_VLIST_POLY_DRAW );
					}
					RT_ADD_VLIST( &vhead, &pp->verts[3*(pp->npts-1)],
						BN_VLIST_POLY_END );
				}
			}
		}
		goto out;
	case ID_COMBINATION:
	default:
		break;
	}
	rt_db_free_internal(&intern);
	return 0;

out:
	/* Successful fastpath drawing of this solid */
	db_add_node_to_full_path( pathp, dp );
	drawH_part2( 0, &vhead, pathp, tsp, SOLID_NULL );
	DB_FULL_PATH_POP(pathp);
	rt_db_free_internal(&intern);
	mged_fastpath_count++;
	return -1;	/* SKIP THIS REGION */
}

/*
 *			M G E D _ N M G _ R E G I O N _ E N D
 *
 *  This routine must be prepared to run in parallel.
 */
HIDDEN union tree *mged_nmg_region_end( tsp, pathp, curtree, client_data )
register struct db_tree_state	*tsp;
struct db_full_path	*pathp;
union tree		*curtree;
genptr_t client_data;
{
	struct nmgregion	*r;
	struct bu_list		vhead;
	int			failed;

	RT_CK_TESS_TOL(tsp->ts_ttol);
	BN_CK_TOL(tsp->ts_tol);
	NMG_CK_MODEL(*tsp->ts_m);

	BU_LIST_INIT( &vhead );

	if(rt_g.debug&DEBUG_TREEWALK)  {
	  char	*sofar = db_path_to_string(pathp);

	  Tcl_AppendResult(interp, "mged_nmg_region_end() path='", sofar,
			   "'\n", (char *)NULL);
	  bu_free((genptr_t)sofar, "path string");
	}

	if( curtree->tr_op == OP_NOP )  return  curtree;

	if ( !mged_draw_nmg_only ) {
		if( BU_SETJUMP )
		{
			char  *sofar = db_path_to_string(pathp);

			BU_UNSETJUMP;

			Tcl_AppendResult(interp, "WARNING: Boolean evaluation of ", sofar,
				" failed!!!\n", (char *)NULL );
			bu_free((genptr_t)sofar, "path string");
			if( curtree )
				db_free_tree( curtree );
			return (union tree *)NULL;
		}
		failed = nmg_boolean( curtree, *tsp->ts_m, tsp->ts_tol );
		BU_UNSETJUMP;
		if( failed )  {
			db_free_tree( curtree );
			return (union tree *)NULL;
		}
	}
	else if( curtree->tr_op != OP_NMG_TESS )
	{
	  Tcl_AppendResult(interp, "Cannot use '-d' option when Boolean evaluation is required\n", (char *)NULL);
	  db_free_tree( curtree );
	  return (union tree *)NULL;
	}
	r = curtree->tr_d.td_r;
	NMG_CK_REGION(r);

	if( mged_do_not_draw_nmg_solids_during_debugging && r )  {
		db_free_tree( curtree );
		return (union tree *)NULL;
	}

	if (mged_nmg_triangulate) {
		if( BU_SETJUMP )
		{
			char  *sofar = db_path_to_string(pathp);

			BU_UNSETJUMP;

			Tcl_AppendResult(interp, "WARNING: Triangulation of ", sofar,
				" failed!!!\n", (char *)NULL );
			bu_free((genptr_t)sofar, "path string");
			if( curtree )
				db_free_tree( curtree );
			return (union tree *)NULL;
		}
		nmg_triangulate_model(*tsp->ts_m, tsp->ts_tol);
		BU_UNSETJUMP;
	}

	if( r != 0 )  {
		int	style;
		/* Convert NMG to vlist */
		NMG_CK_REGION(r);

		if( mged_draw_wireframes )  {
			/* Draw in vector form */
			style = NMG_VLIST_STYLE_VECTOR;
		} else {
			/* Default -- draw polygons */
			style = NMG_VLIST_STYLE_POLYGON;
		}
		if( mged_draw_normals )  {
			style |= NMG_VLIST_STYLE_VISUALIZE_NORMALS;
		}
		if( mged_shade_per_vertex_normals )  {
			style |= NMG_VLIST_STYLE_USE_VU_NORMALS;
		}
		if( mged_draw_no_surfaces )  {
			style |= NMG_VLIST_STYLE_NO_SURFACES;
		}
		nmg_r_to_vlist( &vhead, r, style );

		drawH_part2( 0, &vhead, pathp, tsp, SOLID_NULL );

		if( mged_draw_edge_uses )  {
			nmg_vlblock_r(mged_draw_edge_uses_vbp, r, 1);
		}
		/* NMG region is no longer necessary, only vlist remains */
		db_free_tree( curtree );
		return (union tree *)NULL;
	}

	/* Return tree -- it needs to be freed (by caller) */
	return curtree;
}

/*
 *			D R A W T R E E S
 *
 *  This routine is MGED's analog of rt_gettrees().
 *  Add a set of tree hierarchies to the active set.
 *  Note that argv[0] should be ignored, it has the command name in it.
 *
 *  Kind =
 *	1	regular wireframes
 *	2	big-E
 *	3	NMG polygons
 *  
 *  Returns -
 *  	0	Ordinarily
 *	-1	On major error
 */
int
drawtrees( argc, argv, kind )
int	argc;
char	**argv;
int	kind;
{
	int		ret = 0;
	register int	c;
	int		ncpu;
	int		mged_nmg_use_tnurbs = 0;

	if(dbip == DBI_NULL)
	  return 0;

	RT_CHECK_DBI(dbip);

	if( argc <= 0 )  return(-1);	/* FAIL */

	/* Initial values for options, must be reset each time */
	ncpu = 1;
	mged_draw_nmg_only = 0;	/* no booleans */
	mged_nmg_triangulate = 1;
	mged_draw_wireframes = 0;
	mged_draw_normals = 0;
	mged_draw_edge_uses = 0;
	mged_draw_solid_lines_only = 0;
	mged_shade_per_vertex_normals = 0;
	mged_draw_no_surfaces = 0;
	mged_wireframe_color_override = 0;
	mged_fastpath_count = 0;
	mged_enable_fastpath = 0;

	/* Parse options. */
	bu_optind = 1;		/* re-init bu_getopt() */
	while( (c=bu_getopt(argc,argv,"dfnqrstuvwSTP:C:")) != EOF )  {
		switch(c)  {
		case 'u':
			mged_draw_edge_uses = 1;
			break;
		case 's':
			mged_draw_solid_lines_only = 1;
			break;
		case 't':
			mged_nmg_use_tnurbs = 1;
			break;
		case 'v':
			mged_shade_per_vertex_normals = 1;
			break;
		case 'w':
			mged_draw_wireframes = 1;
			break;
		case 'S':
			mged_draw_no_surfaces = 1;
			break;
		case 'T':
			mged_nmg_triangulate = 0;
			break;
		case 'n':
			mged_draw_normals = 1;
			break;
		case 'P':
			ncpu = atoi(bu_optarg);
			break;
		case 'q':
			mged_do_not_draw_nmg_solids_during_debugging = 1;
			break;
		case 'd':
			mged_draw_nmg_only = 1;
			break;
		case 'f':
			mged_enable_fastpath = 1;
			break;
		case 'C':
			{
				int		r,g,b;
				register char	*cp = bu_optarg;

				r = atoi(cp);
				while( (*cp >= '0' && *cp <= '9') )  cp++;
				while( *cp && (*cp < '0' || *cp > '9') ) cp++;
				g = atoi(cp);
				while( (*cp >= '0' && *cp <= '9') )  cp++;
				while( *cp && (*cp < '0' || *cp > '9') ) cp++;
				b = atoi(cp);

				if( r < 0 || r > 255 )  r = 255;
				if( g < 0 || g > 255 )  g = 255;
				if( b < 0 || b > 255 )  b = 255;

				mged_wireframe_color_override = 1;
				mged_wireframe_color[0] = r;
				mged_wireframe_color[1] = g;
				mged_wireframe_color[2] = b;
			}
			break;
		case 'r':
			/* Draw in all-red, as in Release 3 and earlier */
			/* Useful for spotting regions colored black */
			mged_wireframe_color_override = 1;
			mged_wireframe_color[0] = 255;
			mged_wireframe_color[1] = 0;
			mged_wireframe_color[2] = 0;
			break;
		default:
			{
				struct bu_vls vls;

				bu_vls_init(&vls);
				bu_vls_printf(&vls, "help %s", argv[0]);
				Tcl_Eval(interp, bu_vls_addr(&vls));
				bu_vls_free(&vls);

				return TCL_ERROR;
			}
#if 0
		  Tcl_AppendResult(interp, "Usage: ev [-dfnqstuvwST] [-P ncpu] object(s)\n\
	-d draw nmg without performing boolean operations\n\
	-f enable polysolid fastpath\n\
	-w draw wireframes (rather than polygons)\n\
	-n draw surface normals as little 'hairs'\n\
	-s draw solid lines only (no dot-dash for subtract and intersect)\n\
	-t Perform CSG-to-tNURBS conversion\n\
	-v shade using per-vertex normals, when present.\n\
	-u debug: draw edgeuses\n\
	-S draw tNURBs with trimming curves only, no surfaces.\n\
	-T debug: disable triangulator\n", (char *)NULL);
			break;
#endif
		}
	}
	argc -= bu_optind;
	argv += bu_optind;

	/* Establish upcall interfaces for use by bottom of NMG library */
	nmg_plot_anim_upcall = mged_plot_anim_upcall_handler;
	nmg_vlblock_anim_upcall = mged_vlblock_anim_upcall_handler;
	nmg_mged_debug_display_hack = hack_for_lee;

	/* Establish tolerances */
	mged_initial_tree_state.ts_ttol = &mged_ttol;
	mged_initial_tree_state.ts_tol = &mged_tol;

#if 0
	/* set default wireframe color */
	VMOVE(mged_initial_tree_state.ts_mater.ma_color, default_wireframe_color);
#endif

	mged_ttol.magic = RT_TESS_TOL_MAGIC;
	mged_ttol.abs = mged_abs_tol;
	mged_ttol.rel = mged_rel_tol;
	mged_ttol.norm = mged_nrm_tol;

	switch( kind )  {
	default:
	  Tcl_AppendResult(interp, "ERROR, bad kind\n", (char *)NULL);
	  return(-1);
	case 1:		/* Wireframes */
		ret = db_walk_tree( dbip, argc, (CONST char **)argv,
			ncpu,
			&mged_initial_tree_state,
			0,			/* take all regions */
			mged_wireframe_region_end,
			mged_wireframe_leaf, (genptr_t)NULL );
		break;
	case 2:		/* Big-E */
#	    if 0
		ret = db_walk_tree( dbip, argc, argv,
			ncpu,
			&mged_initial_tree_state,
			0,			/* take all regions */
			mged_bigE_region_end,
			mged_bigE_leaf, (genptr_t)NULL );
		break;
#	    else
		Tcl_AppendResult(interp, "drawtrees:  can't do big-E here\n", (char *)NULL);
		return(-1);
#	    endif
	case 3:
	  {
		/* NMG */
#if 0
	    Tcl_AppendResult(interp, "\
Please note that the NMG library used by this command is experimental.\n\
A production implementation will exist in the maintenance release.\n", (char *)NULL);
#endif
	  	mged_nmg_model = nmg_mm();
		mged_initial_tree_state.ts_m = &mged_nmg_model;
	  	if (mged_draw_edge_uses) {
		  Tcl_AppendResult(interp, "Doing the edgeuse thang (-u)\n", (char *)NULL);
		  mged_draw_edge_uses_vbp = rt_vlblock_init();
	  	}

		ret = db_walk_tree( dbip, argc, (CONST char **)argv,
			ncpu,
			&mged_initial_tree_state,
			mged_enable_fastpath ? mged_nmg_region_start : 0,
			mged_nmg_region_end,
	  		mged_nmg_use_tnurbs ?
	  			nmg_booltree_leaf_tnurb :
				nmg_booltree_leaf_tess,
			(genptr_t)NULL
			);

	  	if (mged_draw_edge_uses) {
	  		cvt_vlblock_to_solids(mged_draw_edge_uses_vbp, "_EDGEUSES_", 0);
	  		rt_vlblock_free(mged_draw_edge_uses_vbp);
			mged_draw_edge_uses_vbp = (struct bn_vlblock *)NULL;
 	  	}

		/* Destroy NMG */
		nmg_km( mged_nmg_model );
	  	break;
	  }
	}
	if(mged_fastpath_count)  {
		bu_log("%d region%s rendered through polygon fastpath\n",
			mged_fastpath_count, mged_fastpath_count==1?"":"s");
	}
	if( ret < 0 )  return(-1);
	return(0);	/* OK */
}

/*
 *  Compute the min, max, and center points of the solid.
 *  Also finds s_vlen;
 * XXX Should split out a separate bn_vlist_rpp() routine, for librt/vlist.c
 */
void
mged_bound_solid( sp )
register struct solid *sp;
{
	register struct bn_vlist	*vp;
	register double			xmax, ymax, zmax;
	register double			xmin, ymin, zmin;

	xmax = ymax = zmax = -INFINITY;
	xmin = ymin = zmin =  INFINITY;
	sp->s_vlen = 0;
	for( BU_LIST_FOR( vp, bn_vlist, &(sp->s_vlist) ) )  {
		register int	j;
		register int	nused = vp->nused;
		register int	*cmd = vp->cmd;
		register point_t *pt = vp->pt;
		for( j = 0; j < nused; j++,cmd++,pt++ )  {
			switch( *cmd )  {
			case BN_VLIST_POLY_START:
			case BN_VLIST_POLY_VERTNORM:
				/* Has normal vector, not location */
				break;
			case BN_VLIST_LINE_MOVE:
			case BN_VLIST_LINE_DRAW:
			case BN_VLIST_POLY_MOVE:
			case BN_VLIST_POLY_DRAW:
			case BN_VLIST_POLY_END:
				V_MIN( xmin, (*pt)[X] );
				V_MAX( xmax, (*pt)[X] );
				V_MIN( ymin, (*pt)[Y] );
				V_MAX( ymax, (*pt)[Y] );
				V_MIN( zmin, (*pt)[Z] );
				V_MAX( zmax, (*pt)[Z] );
				break;
			default:
			  {
			    struct bu_vls tmp_vls;

			    bu_vls_init(&tmp_vls);
			    bu_vls_printf(&tmp_vls, "unknown vlist op %d\n", *cmd);
			    Tcl_AppendResult(interp, bu_vls_addr(&tmp_vls), (char *)NULL);
			    bu_vls_free(&tmp_vls);
			  }
			}
		}
		sp->s_vlen += nused;
	}

	sp->s_center[X] = (xmin + xmax) * 0.5;
	sp->s_center[Y] = (ymin + ymax) * 0.5;
	sp->s_center[Z] = (zmin + zmax) * 0.5;

	sp->s_size = xmax - xmin;
	V_MAX( sp->s_size, ymax - ymin );
	V_MAX( sp->s_size, zmax - zmin );
}

/*
 *			D R A W h _ P A R T 2
 *
 *  Once the vlist has been created, perform the common tasks
 *  in handling the drawn solid.
 *
 *  This routine must be prepared to run in parallel.
 */
void
drawH_part2( dashflag, vhead, pathp, tsp, existing_sp )
int			dashflag;
struct bu_list		*vhead;
struct db_full_path	*pathp;
struct db_tree_state	*tsp;
struct solid		*existing_sp;
{
	register struct solid *sp;
	register int	i;

	if( !existing_sp )  {
		if (pathp->fp_len > MAX_PATH) {
		  char *cp = db_path_to_string(pathp);

		  Tcl_AppendResult(interp, "drawH_part2: path too long, solid ignored.\n\t",
				   cp, "\n", (char *)NULL);
		  bu_free((genptr_t)cp, "Path string");
		  return;
		}
		/* Handling a new solid */
		GET_SOLID(sp, &FreeSolid.l);
		/* NOTICE:  The structure is dirty & not initialized for you! */

		sp->s_dlist = BU_LIST_LAST(solid, &HeadSolid.l)->s_dlist + 1;
	} else {
		/* Just updating an existing solid.
		 *  'tsp' and 'pathpos' will not be used
		 */
		sp = existing_sp;
	}


	/*
	 * Compute the min, max, and center points.
	 */
	BU_LIST_APPEND_LIST( &(sp->s_vlist), vhead );
	mged_bound_solid( sp );
	nvectors += sp->s_vlen;

	/*
	 *  If this solid is new, fill in it's information.
	 *  Otherwise, don't touch what is already there.
	 */
	if( !existing_sp )  {
		/* Take note of the base color */
		if( mged_wireframe_color_override ) {
		        /* a user specified the color, so arrange to use it */
			sp->s_uflag = 1;
			sp->s_dflag = 0;
			sp->s_basecolor[0] = mged_wireframe_color[0];
			sp->s_basecolor[1] = mged_wireframe_color[1];
			sp->s_basecolor[2] = mged_wireframe_color[2];
		} else {
			sp->s_uflag = 0;
			if (tsp) {
			  if (tsp->ts_mater.ma_color_valid) {
			    sp->s_dflag = 0;	/* color specified in db */
			  } else {
			    sp->s_dflag = 1;	/* default color */
			  }
			  /* Copy into basecolor anyway, to prevent black */
			  sp->s_basecolor[0] = tsp->ts_mater.ma_color[0] * 255.;
			  sp->s_basecolor[1] = tsp->ts_mater.ma_color[1] * 255.;
			  sp->s_basecolor[2] = tsp->ts_mater.ma_color[2] * 255.;
			}
		}
		sp->s_cflag = 0;
		sp->s_iflag = DOWN;
		sp->s_soldash = dashflag;
		sp->s_Eflag = 0;	/* This is a solid */
		sp->s_last = pathp->fp_len-1;

		/* Copy path information */
		for( i=0; i<=sp->s_last; i++ ) {
			sp->s_path[i] = pathp->fp_names[i];
		}
		sp->s_regionid = tsp->ts_regionid;
	}

#ifdef DO_DISPLAY_LISTS
	createDListALL(sp);
#endif

	/* Solid is successfully drawn */
	if( !existing_sp )  {
		/* Add to linked list of solid structs */
		bu_semaphore_acquire( RT_SEM_MODEL );
		BU_LIST_APPEND(HeadSolid.l.back, &sp->l);
		bu_semaphore_release( RT_SEM_MODEL );
	} else {
		/* replacing existing solid -- struct already linked in */
		sp->s_iflag = UP;
	}
}

HIDDEN void
Do_getmat( dbip, comb, comb_leaf, user_ptr1, user_ptr2, user_ptr3 )
struct db_i             *dbip;
struct rt_comb_internal *comb;
union tree              *comb_leaf;
genptr_t                user_ptr1, user_ptr2, user_ptr3;
{
	matp_t	xmat;
	char	*kid_name;
	int	*found;

	RT_CK_DBI( dbip );
	RT_CK_TREE( comb_leaf );

	kid_name = (char *)user_ptr2;

	if( strncmp( comb_leaf->tr_l.tl_name, kid_name, NAMESIZE ) )
		return;

	xmat = (matp_t)user_ptr1;
	found = (int *)user_ptr3;

	(*found) = 1;
	if( comb_leaf->tr_l.tl_mat )
		bn_mat_copy( xmat, comb_leaf->tr_l.tl_mat );
	else
		bn_mat_idn( xmat );
}

/*
 *			F U L L _ P A T H _ F R O M _ S O L I D
 *
 *  Initializes a 'db_full_path' to correspond to sp->s_path.
 */
void
full_path_from_solid( pathp, sp )
struct db_full_path	*pathp;
register struct solid	*sp;
{
	pathp->fp_len = pathp->fp_maxlen = sp->s_last+1;
	pathp->fp_names = (struct directory **)bu_malloc(
		pathp->fp_maxlen * sizeof(struct directory *),
		"db_full_path array");
	pathp->magic = DB_FULL_PATH_MAGIC;

	bcopy( (char *)sp->s_path, (char *)pathp->fp_names,
		pathp->fp_len * sizeof(struct directory *) );
}

/*
 *  			P A T H h M A T
 *  
 *  Find the transformation matrix obtained when traversing
 *  the arc indicated in sp->s_path[] to the indicated depth.
 *
 *  Returns -
 *	matp is filled with values (never read first).
 *	sp may have fields updated.
 */
void
pathHmat( sp, matp, depth )
register struct solid *sp;
matp_t matp;
{
	struct db_tree_state	ts;
	struct db_full_path	null_path;
	struct db_full_path	path;

	RT_CHECK_DBI(dbip);

	full_path_from_solid( &path, sp );

	db_full_path_init( &null_path );
	ts = mged_initial_tree_state;		/* struct copy */
	ts.ts_dbip = dbip;

	(void)db_follow_path( &ts, &null_path, &path, LOOKUP_NOISY, depth+1 );
	db_free_full_path( &null_path );
	db_free_full_path( &path );

#if 0
	/*
	 *  Copy color out to solid structure, in case it changed.
	 *  This is an odd place to do this, but...
	 */
#if 0
	sp->s_color[0] = sp->s_basecolor[0] = ts.ts_mater.ma_color[0] * 255.;
	sp->s_color[1] = sp->s_basecolor[1] = ts.ts_mater.ma_color[1] * 255.;
	sp->s_color[2] = sp->s_basecolor[2] = ts.ts_mater.ma_color[2] * 255.;
#else
	if(!sp->s_uflag){
	  /* the user did not specify a color */
	  sp->s_basecolor[0] = ts.ts_mater.ma_color[0] * 255.;
	  sp->s_basecolor[1] = ts.ts_mater.ma_color[1] * 255.;
	  sp->s_basecolor[2] = ts.ts_mater.ma_color[2] * 255.;
	}
#endif
#endif

	bn_mat_copy( matp, ts.ts_mat );	/* implicit return */

	db_free_db_tree_state( &ts );

#if 0
	register struct directory *parentp;
	register struct directory *kidp;
	register int		j;
	struct rt_db_internal	intern;
	struct rt_comb_internal	*comb;
	auto mat_t		tmat;
	register int		i;

	if(dbip == DBI_NULL)
	  return;

	bn_mat_idn( matp );
	for( i=0; i <= depth; i++ )  {
		parentp = sp->s_path[i];
		kidp = sp->s_path[i+1];
		if( !(parentp->d_flags & DIR_COMB) )  {
		  Tcl_AppendResult(interp, "pathHmat:  ", parentp->d_namep,
				   " is not a combination\n", (char *)NULL);
		  return;		/* ERROR */
		}

		if( rt_db_get_internal( &intern, parentp, dbip, (fastf_t *)NULL ) < 0 )
			READ_ERR_return;
		comb = (struct rt_comb_internal *)intern.idb_ptr;
		if( comb->tree )
		{
			static mat_t xmat;	/* temporary fastf_t matrix */
			int found=0;

			db_tree_funcleaf( dbip, comb, comb->tree, Do_getmat,
				(genptr_t)xmat, (genptr_t)kidp->d_namep, (genptr_t)&found );
			rt_comb_ifree( &intern );

			if( found )
			{
				bn_mat_mul( tmat, matp, xmat );
				bn_mat_copy( matp, tmat );
			}
			else
			{
				Tcl_AppendResult(interp, "pathHmat: unable to follow ", parentp->d_namep,
						 "/", kidp->d_namep, "\n", (char *)NULL);
				return;			/* ERROR */
			}
		}
	}
#endif
}

/*
 *			R E P L O T _ O R I G I N A L _ S O L I D
 *
 *  Given an existing solid structure that may have been subjected to
 *  solid editing, recompute the vector list, etc, to make the solid
 *  the same as it originally was.
 *
 *  Returns -
 *	-1	error
 *	 0	OK
 */
int
replot_original_solid( sp )
struct solid	*sp;
{
	struct bu_external	ext;
	struct rt_db_internal	intern;
	struct directory	*dp;
	mat_t			mat;
	int			id;

	if(dbip == DBI_NULL)
	  return 0;

	dp = sp->s_path[sp->s_last];
	if( sp->s_Eflag )  {
	  Tcl_AppendResult(interp, "replot_original_solid(", dp->d_namep,
			   "): Unable to plot evaluated regions, skipping\n", (char *)NULL);
	  return(-1);
	}
	pathHmat( sp, mat, sp->s_last-1 );

	BU_INIT_EXTERNAL( &ext );
	if( db_get_external( &ext, dp, dbip ) < 0 )  return(-1);

	if( (id = rt_id_solid( &ext )) == ID_NULL )  {
	  Tcl_AppendResult(interp, "replot_original_solid() unable to identify type of solid ",
			   dp->d_namep, "\n", (char *)NULL);
	  db_free_external( &ext );
	  return(-1);
	}

    	RT_INIT_DB_INTERNAL(&intern);
	if( rt_functab[id].ft_import( &intern, &ext, mat, dbip ) < 0 )  {
	  Tcl_AppendResult(interp, dp->d_namep, ":  solid import failure\n", (char *)NULL);
	  if( intern.idb_ptr )  rt_functab[id].ft_ifree( &intern );
	  db_free_external( &ext );
	  return(-1);		/* ERROR */
	}
	RT_CK_DB_INTERNAL( &intern );

	if( replot_modified_solid( sp, &intern, bn_mat_identity ) < 0 )  {
	    	if( intern.idb_ptr )  rt_functab[id].ft_ifree( &intern );
		db_free_external( &ext );
		return(-1);
	}
	if( intern.idb_type > ID_NULL && intern.idb_ptr )
		rt_functab[id].ft_ifree( &intern );
	db_free_external( &ext );
	return(0);
}

/*
 *  			R E P L O T _ M O D I F I E D _ S O L I D
 *
 *  Given the solid structure of a solid that has already been drawn,
 *  and a new database record and transform matrix,
 *  create a new vector list for that solid, and substitute.
 *  Used for solid editing mode.
 *
 *  Returns -
 *	-1	error
 *	 0	OK
 */
int
replot_modified_solid( sp, ip, mat )
struct solid			*sp;
struct rt_db_internal		*ip;
CONST mat_t			mat;
{
	struct rt_db_internal	intern;
	struct bu_list		vhead;

	BU_LIST_INIT( &vhead );

	if( sp == SOLID_NULL )  {
	  Tcl_AppendResult(interp, "replot_modified_solid() sp==NULL?\n", (char *)NULL);
	  return(-1);
	}

	/* Release existing vlist of this solid */
	RT_FREE_VLIST( &(sp->s_vlist) );

	/* Draw (plot) a normal solid */
	RT_CK_DB_INTERNAL( ip );

	mged_ttol.magic = RT_TESS_TOL_MAGIC;
	mged_ttol.abs = mged_abs_tol;
	mged_ttol.rel = mged_rel_tol;
	mged_ttol.norm = mged_nrm_tol;

	transform_editing_solid( &intern, mat, ip, 0 );

	if( rt_functab[ip->idb_type].ft_plot( &vhead, &intern, &mged_ttol, &mged_tol ) < 0 )  {
	  Tcl_AppendResult(interp, sp->s_path[sp->s_last]->d_namep,
			   ": re-plot failure\n", (char *)NULL);
	  return(-1);
	}
    	if( intern.idb_ptr )  rt_functab[ip->idb_type].ft_ifree( &intern );

	/* Write new displaylist */
	drawH_part2( sp->s_soldash, &vhead,
		(struct db_full_path *)0,
		(struct db_tree_state *)0, sp );

#if 0
	/* Release previous chunk of displaylist. */
	if( bytes > 0 )
		rt_memfree( &(dmp->dm_map), bytes, (unsigned long)addr );
#endif

	view_state->vs_flag = 1;
	return(0);
}

/*
 *			C V T _ V L B L O C K _ T O _ S O L I D S
 */
void
cvt_vlblock_to_solids( vbp, name, copy )
struct bn_vlblock	*vbp;
char			*name;
int			copy;
{
	int		i;
	char		shortname[32];
	char		namebuf[64];
	char		*av[4];

	strncpy( shortname, name, 16-6 );
	shortname[16-6] = '\0';
	/* Remove any residue colors from a previous overlay w/same name */
	if( dbip->dbi_read_only )  {
		av[0] = "d";
		av[1] = shortname;
		av[2] = NULL;
		(void)f_erase((ClientData)NULL, interp, 2, av);
	} else {
		av[0] = "kill";
		av[1] = "-f";
		av[2] = shortname;
		av[3] = NULL;
		(void)f_kill((ClientData)NULL, interp, 3, av);
	}

	for( i=0; i < vbp->nused; i++ )  {
#if 0
		if( vbp->rgb[i] == 0 )  continue;
#endif
		if( BU_LIST_IS_EMPTY( &(vbp->head[i]) ) )  continue;

		sprintf( namebuf, "%s%lx",
			shortname, vbp->rgb[i] );
		invent_solid( namebuf, &vbp->head[i], vbp->rgb[i], copy );
	}
}

/*
 *			I N V E N T _ S O L I D
 *
 *  Invent a solid by adding a fake entry in the database table,
 *  adding an entry to the solid table, and populating it with
 *  the given vector list.
 *
 *  This parallels much of the code in dodraw.c
 */
int
invent_solid( name, vhead, rgb, copy )
char		*name;
struct bu_list	*vhead;
long		rgb;
int		copy;
{
	struct directory	*dp;
	struct directory	*dpp[2] = {DIR_NULL, DIR_NULL};
	register struct solid	*sp;

	if(dbip == DBI_NULL)
	  return 0;

	if( (dp = db_lookup( dbip,  name, LOOKUP_QUIET )) != DIR_NULL )  {
	  if( dp->d_addr != RT_DIR_PHONY_ADDR )  {
	    Tcl_AppendResult(interp, "invent_solid(", name,
			     ") would clobber existing database entry, ignored\n", (char *)NULL);
	    return(-1);
	  }
	  /* Name exists from some other overlay,
	   * zap any associated solids
	   */
	  dpp[0] = dp;
	  eraseobjall(dpp);
	}
	/* Need to enter phony name in directory structure */
	dp = db_diradd( dbip,  name, RT_DIR_PHONY_ADDR, 0, DIR_SOLID, NULL );

#if 0
	/* XXX need to get this going. */
	path.fp_names[0] = dp;
	state.ts_mater.ma_color[0] = ((rgb>>16) & 0xFF) / 255.0
	state.ts_mater.ma_color[1] = ((rgb>> 8) & 0xFF) / 255.0
	state.ts_mater.ma_color[2] = ((rgb    ) & 0xFF) / 255.0
	drawH_part2( 0, vhead, path, &state, SOLID_NULL );
#else

	/* Obtain a fresh solid structure, and fill it in */
	GET_SOLID(sp,&FreeSolid.l);

	if( copy )  {
		BU_LIST_INIT( &(sp->s_vlist) );
		rt_vlist_copy( &(sp->s_vlist), vhead );
	} else {
		BU_LIST_INIT(&(sp->s_vlist));
		BU_LIST_APPEND_LIST(&(sp->s_vlist), vhead);
	}
	mged_bound_solid( sp );
	nvectors += sp->s_vlen;

	/* set path information -- this is a top level node */
	sp->s_last = 0;
	sp->s_path[0] = dp;

	sp->s_iflag = DOWN;
	sp->s_soldash = 0;
	sp->s_Eflag = 1;		/* Can't be solid edited! */
	sp->s_color[0] = sp->s_basecolor[0] = (rgb>>16) & 0xFF;
	sp->s_color[1] = sp->s_basecolor[1] = (rgb>> 8) & 0xFF;
	sp->s_color[2] = sp->s_basecolor[2] = (rgb    ) & 0xFF;
	sp->s_regionid = 0;
	sp->s_dlist = BU_LIST_LAST(solid, &HeadSolid.l)->s_dlist + 1;

	/* Solid successfully drawn, add to linked list of solid structs */
	BU_LIST_APPEND(HeadSolid.l.back, &sp->l);

#ifdef DO_DISPLAY_LISTS
	createDListALL(sp);
#endif
#endif
	return(0);		/* OK */
}

static union tree	*mged_facetize_tree;

/*
 *			M G E D _ F A C E T I Z E _ R E G I O N _ E N D
 *
 *  This routine must be prepared to run in parallel.
 */
HIDDEN union tree *mged_facetize_region_end( tsp, pathp, curtree, client_data )
register struct db_tree_state	*tsp;
struct db_full_path	*pathp;
union tree		*curtree;
genptr_t		client_data;
{
	struct bu_list		vhead;

	BU_LIST_INIT( &vhead );

	if(rt_g.debug&DEBUG_TREEWALK)  {
	  char	*sofar = db_path_to_string(pathp);

	  Tcl_AppendResult(interp, "mged_facetize_region_end() path='", sofar,
			   "'\n", (char *)NULL);
	  bu_free((genptr_t)sofar, "path string");
	}

	if( curtree->tr_op == OP_NOP )  return  curtree;

	bu_semaphore_acquire( RT_SEM_MODEL );
	if( mged_facetize_tree )  {
		union tree	*tr;
		tr = (union tree *)bu_calloc(1, sizeof(union tree), "union tree");
		tr->magic = RT_TREE_MAGIC;
		tr->tr_op = OP_UNION;
		tr->tr_b.tb_regionp = REGION_NULL;
		tr->tr_b.tb_left = mged_facetize_tree;
		tr->tr_b.tb_right = curtree;
		mged_facetize_tree = tr;
	} else {
		mged_facetize_tree = curtree;
	}
	bu_semaphore_release( RT_SEM_MODEL );

	/* Tree has been saved, and will be freed later */
	return( TREE_NULL );
}

/* facetize [opts] new_obj old_obj(s) */
int
f_facetize(clientData, interp, argc, argv)
ClientData clientData;
Tcl_Interp *interp;
int	argc;
char	**argv;
{
	int			i;
	register int		c;
	int			ncpu;
	int			triangulate;
	char			*newname;
	struct rt_db_internal	intern;
	struct directory	*dp;
	int			failed;
	int			mged_nmg_use_tnurbs = 0;
	int			make_bot = 0;

	CHECK_DBI_NULL;

	if(argc < 3 || MAXARGS < argc){
	  struct bu_vls vls;

	  bu_vls_init(&vls);
	  bu_vls_printf(&vls, "help facetize");
	  Tcl_Eval(interp, bu_vls_addr(&vls));
	  bu_vls_free(&vls);
	  return TCL_ERROR;
	}

	RT_CHECK_DBI(dbip);

	/* Establish tolerances */
	mged_initial_tree_state.ts_ttol = &mged_ttol;
	mged_initial_tree_state.ts_tol = &mged_tol;

	mged_ttol.magic = RT_TESS_TOL_MAGIC;
	mged_ttol.abs = mged_abs_tol;
	mged_ttol.rel = mged_rel_tol;
	mged_ttol.norm = mged_nrm_tol;

	/* Initial vaues for options, must be reset each time */
	ncpu = 1;
	triangulate = 0;

	/* Parse options. */
	make_bot = 1;
	bu_optind = 1;		/* re-init bu_getopt() */
	while( (c=bu_getopt(argc,argv,"ntTP:")) != EOF )  {
		switch(c)  {
		case 'P':
			ncpu = atoi(bu_optarg);
			break;
		case 'T':
			triangulate = 1;
			break;
		case 't':
			mged_nmg_use_tnurbs = 1;
			break;
		case 'n':
			make_bot = 0;
			break;
		default:
		  {
		    struct bu_vls tmp_vls;

		    bu_vls_init(&tmp_vls);
		    bu_vls_printf(&tmp_vls, "option '%c' unknown\n", c);
		    Tcl_AppendResult(interp, bu_vls_addr(&tmp_vls),
				     "Usage: facetize [-tT] [-P ncpu] object(s)\n",
				     "\t-t Perform CSG-to-tNURBS conversion\n",
				     "\t-T enable triangulator\n", (char *)NULL);
		    bu_vls_free(&tmp_vls);
		  }
		  break;
		}
	}
	argc -= bu_optind;
	argv += bu_optind;
	if( argc < 0 ){
	  Tcl_AppendResult(interp, "facetize: missing argument\n", (char *)NULL);
	  return TCL_ERROR;
	}

	newname = argv[0];
	argv++;
	argc--;
	if( argc < 0 ){
	  Tcl_AppendResult(interp, "facetize: missing argument\n", (char *)NULL);
	  return TCL_ERROR;
	}

	if( db_lookup( dbip, newname, LOOKUP_QUIET ) != DIR_NULL )  {
	  Tcl_AppendResult(interp, "error: solid '", newname,
			   "' already exists, aborting\n", (char *)NULL);
	  return TCL_ERROR;
	}

	{
	  struct bu_vls tmp_vls;

	  bu_vls_init(&tmp_vls);
	  bu_vls_printf(&tmp_vls,
			"facetize:  tessellating primitives with tolerances a=%g, r=%g, n=%g\n",
			mged_abs_tol, mged_rel_tol, mged_nrm_tol );
	  Tcl_AppendResult(interp, bu_vls_addr(&tmp_vls), (char *)NULL);
	  bu_vls_free(&tmp_vls);
	}
	mged_facetize_tree = (union tree *)0;
  	mged_nmg_model = nmg_mm();
	mged_initial_tree_state.ts_m = &mged_nmg_model;

	i = db_walk_tree( dbip, argc, (CONST char **)argv,
		ncpu,
		&mged_initial_tree_state,
		0,			/* take all regions */
		mged_facetize_region_end,
  		mged_nmg_use_tnurbs ?
  			nmg_booltree_leaf_tnurb :
			nmg_booltree_leaf_tess,
		(genptr_t)NULL
		);


	if( i < 0 )  {
	  Tcl_AppendResult(interp, "facetize: error in db_walk_tree()\n", (char *)NULL);
	  /* Destroy NMG */
	  nmg_km( mged_nmg_model );
	  return TCL_ERROR;
	}

	if( mged_facetize_tree )
	{
		/* Now, evaluate the boolean tree into ONE region */
		Tcl_AppendResult(interp, "facetize:  evaluating boolean expressions\n", (char *)NULL);

		if( BU_SETJUMP )
		{
			BU_UNSETJUMP;
			Tcl_AppendResult(interp, "WARNING: facetization failed!!!\n", (char *)NULL );
			if( mged_facetize_tree )
				db_free_tree( mged_facetize_tree );
			mged_facetize_tree = (union tree *)NULL;
			nmg_km( mged_nmg_model );
			mged_nmg_model = (struct model *)NULL;
			return TCL_ERROR;
		}

		failed = nmg_boolean( mged_facetize_tree, mged_nmg_model, &mged_tol );
		BU_UNSETJUMP;
	}
	else
		failed = 1;

	if( failed )  {
	  Tcl_AppendResult(interp, "facetize:  no resulting region, aborting\n", (char *)NULL);
	  if( mged_facetize_tree )
		db_free_tree( mged_facetize_tree );
	  mged_facetize_tree = (union tree *)NULL;
	  nmg_km( mged_nmg_model );
	  mged_nmg_model = (struct model *)NULL;
	  return TCL_ERROR;
	}
	/* New region remains part of this nmg "model" */
	NMG_CK_REGION( mged_facetize_tree->tr_d.td_r );
	Tcl_AppendResult(interp, "facetize:  ", mged_facetize_tree->tr_d.td_name,
			 "\n", (char *)NULL);

	/* Triangulate model, if requested */
	if( triangulate && !make_bot )
	{
		Tcl_AppendResult(interp, "facetize:  triangulating resulting object\n", (char *)NULL);
		if( BU_SETJUMP )
		{
			BU_UNSETJUMP;
			Tcl_AppendResult(interp, "WARNING: triangulation failed!!!\n", (char *)NULL );
			if( mged_facetize_tree )
				db_free_tree( mged_facetize_tree );
			mged_facetize_tree = (union tree *)NULL;
			nmg_km( mged_nmg_model );
			mged_nmg_model = (struct model *)NULL;
			return TCL_ERROR;
		}
		nmg_triangulate_model( mged_nmg_model , &mged_tol );
		BU_UNSETJUMP;
	}

	if( make_bot ) {
		struct rt_bot_internal *bot;
		struct nmgregion *r;
		struct shell *s;

		r = BU_LIST_FIRST( nmgregion, &mged_nmg_model->r_hd );
		s = BU_LIST_FIRST( shell, &r->s_hd );
		bot = (struct rt_bot_internal *)nmg_bot( s, &mged_tol );
		nmg_km( mged_nmg_model );
		mged_nmg_model = (struct model *)NULL;

		/* Export BOT as a new solid */
		RT_INIT_DB_INTERNAL(&intern);
		intern.idb_type = ID_BOT;
		intern.idb_meth = &rt_functab[ID_BOT];
		intern.idb_ptr = (genptr_t) bot;
	}
	else {

		Tcl_AppendResult(interp, "facetize:  converting NMG to database format\n", (char *)NULL);

		/* Export NMG as a new solid */
		RT_INIT_DB_INTERNAL(&intern);
		intern.idb_type = ID_NMG;
		intern.idb_meth = &rt_functab[ID_NMG];
		intern.idb_ptr = (genptr_t)mged_nmg_model;
		mged_nmg_model = (struct model *)NULL;
	}

	if( (dp=db_diradd( dbip, newname, -1L, 0, DIR_SOLID, NULL)) == DIR_NULL )
	{
		Tcl_AppendResult(interp, "Cannot add ", newname, " to directory\n", (char *)NULL );
		return TCL_ERROR;
	}

	if( rt_db_put_internal( dp, dbip, &intern ) < 0 )
	{
		rt_db_free_internal( &intern );
		TCL_WRITE_ERR_return;
	}
	
	mged_facetize_tree->tr_d.td_r = (struct nmgregion *)NULL;

	/* Free boolean tree, and the regions in it */
	db_free_tree( mged_facetize_tree );
    	mged_facetize_tree = (union tree *)NULL;

	return TCL_OK;					/* OK */
}

/* bev [opts] new_obj obj1 op obj2 op obj3 ...
 *
 *	tesselates each operand object, then performs
 *	the Boolean evaluation, storing result in
 *	new_obj
 */
int
f_bev(clientData, interp, argc, argv )
ClientData clientData;
Tcl_Interp *interp;
int	argc;
char	**argv;
{
	int			i;
	register int		c;
	int			ncpu;
	int			triangulate;
	char			*newname;
	struct rt_db_internal	intern;
	struct directory	*dp;
	union tree		*tmp_tree;
	char			op;
	int			failed;

	CHECK_DBI_NULL;
	CHECK_READ_ONLY;

	if(argc < 2 || MAXARGS < argc){
	  struct bu_vls vls;

	  bu_vls_init(&vls);
	  bu_vls_printf(&vls, "help bev");
	  Tcl_Eval(interp, bu_vls_addr(&vls));
	  bu_vls_free(&vls);
	  return TCL_ERROR;
	}

	RT_CHECK_DBI( dbip );

	/* Establish tolerances */
	mged_initial_tree_state.ts_ttol = &mged_ttol;
	mged_initial_tree_state.ts_tol = &mged_tol;

	mged_ttol.magic = RT_TESS_TOL_MAGIC;
	mged_ttol.abs = mged_abs_tol;
	mged_ttol.rel = mged_rel_tol;
	mged_ttol.norm = mged_nrm_tol;

	/* Initial vaues for options, must be reset each time */
	ncpu = 1;
	triangulate = 0;

	/* Parse options. */
	bu_optind = 1;		/* re-init bu_getopt() */
	while( (c=bu_getopt(argc,argv,"tP:")) != EOF )  {
		switch(c)  {
		case 'P':
			ncpu = atoi(bu_optarg);
			break;
		case 't':
			triangulate = 1;
			break;
		default:
		  {
		    struct bu_vls tmp_vls;

		    bu_vls_init(&tmp_vls);
		    bu_vls_printf(&tmp_vls, "option '%c' unknown\n", c);
		    Tcl_AppendResult(interp, bu_vls_addr(&tmp_vls), (char *)NULL);
		    bu_vls_free(&tmp_vls);
		  }

		  break;
		}
	}
	argc -= bu_optind;
	argv += bu_optind;

	newname = argv[0];
	argv++;
	argc--;

	if( db_lookup( dbip, newname, LOOKUP_QUIET ) != DIR_NULL )  {
	  Tcl_AppendResult(interp, "error: solid '", newname,
			   "' already exists, aborting\n", (char *)NULL);
	  return TCL_ERROR;
	}

	if( argc < 1 )
	{
	  Tcl_AppendResult(interp, "Nothing to evaluate!!!\n", (char *)NULL);
	  return TCL_ERROR;
	}

	{
	  struct bu_vls tmp_vls;

	  bu_vls_init(&tmp_vls);
	  bu_vls_printf(&tmp_vls,
			"bev:  tessellating primitives with tolerances a=%g, r=%g, n=%g\n",
			mged_abs_tol, mged_rel_tol, mged_nrm_tol);
	  Tcl_AppendResult(interp, bu_vls_addr(&tmp_vls), (char *)NULL);
	  bu_vls_free(&tmp_vls);
	}

	mged_facetize_tree = (union tree *)0;
  	mged_nmg_model = nmg_mm();
	mged_initial_tree_state.ts_m = &mged_nmg_model;

	op = ' ';
	tmp_tree = (union tree *)NULL;

	while( argc )
	{
		i = db_walk_tree( dbip, 1, (CONST char **)argv,
			ncpu,
			&mged_initial_tree_state,
			0,			/* take all regions */
			mged_facetize_region_end,
			nmg_booltree_leaf_tess,
			(genptr_t)NULL );

		if( i < 0 )  {
		  Tcl_AppendResult(interp, "bev: error in db_walk_tree()\n", (char *)NULL);
		  /* Destroy NMG */
		  nmg_km( mged_nmg_model );
		  return TCL_ERROR;
		}
		argc--;
		argv++;

		if( tmp_tree && op != ' ' )
		{
			union tree *new_tree;

			BU_GETUNION( new_tree, tree );

			new_tree->magic = RT_TREE_MAGIC;
			new_tree->tr_b.tb_regionp = REGION_NULL;
			new_tree->tr_b.tb_left = tmp_tree;
			new_tree->tr_b.tb_right = mged_facetize_tree;

			switch( op )
			{
				case 'u':
				case 'U':
					new_tree->tr_op = OP_UNION;
					break;
				case '-':
					new_tree->tr_op = OP_SUBTRACT;
					break;
				case '+':
					new_tree->tr_op = OP_INTERSECT;
					break;
				default:
				  {
				    struct bu_vls tmp_vls;

				    bu_vls_init(&tmp_vls);
				    bu_vls_printf(&tmp_vls, "Unrecognized operator: (%c)\n" , op );
				    Tcl_AppendResult(interp, bu_vls_addr(&tmp_vls),
						     "Aborting\n", (char *)NULL);
				    bu_vls_free(&tmp_vls);
				    db_free_tree( mged_facetize_tree );
				    nmg_km( mged_nmg_model );
				    return TCL_ERROR;
				  }
			}

			tmp_tree = new_tree;
			mged_facetize_tree = (union tree *)NULL;
		}
		else if( !tmp_tree && op == ' ' )
		{
			/* just starting out */
			tmp_tree = mged_facetize_tree;
			mged_facetize_tree = (union tree *)NULL;
		}

		if( argc )
		{
			op = *argv[0];
			argc--;
			argv++;
		}
		else
			op = ' ';

	}

	if( tmp_tree )
	{
		/* Now, evaluate the boolean tree into ONE region */
		Tcl_AppendResult(interp, "bev:  evaluating boolean expressions\n", (char *)NULL);

		if( BU_SETJUMP )
		{
			BU_UNSETJUMP;

			Tcl_AppendResult(interp, "WARNING: Boolean evaluation failed!!!\n", (char *)NULL );
			if( tmp_tree )
				db_free_tree( tmp_tree );
			tmp_tree = (union tree *)NULL;
			nmg_km( mged_nmg_model );
			mged_nmg_model = (struct model *)NULL;
			return TCL_ERROR;
		}

		failed = nmg_boolean( tmp_tree, mged_nmg_model, &mged_tol );
		BU_UNSETJUMP;
	}
	else
		failed = 1;

	if( failed )  {
	  Tcl_AppendResult(interp, "bev:  no resulting region, aborting\n", (char *)NULL);
	  if( tmp_tree )
		db_free_tree( tmp_tree );
	  tmp_tree = (union tree *)NULL;
	  nmg_km( mged_nmg_model );
	  mged_nmg_model = (struct model *)NULL;
	  return TCL_ERROR;
	}
	/* New region remains part of this nmg "model" */
	NMG_CK_REGION( tmp_tree->tr_d.td_r );
	Tcl_AppendResult(interp, "facetize:  ", tmp_tree->tr_d.td_name, "\n", (char *)NULL);

	nmg_vmodel( mged_nmg_model );

	/* Triangulate model, if requested */
	if( triangulate )
	{
		Tcl_AppendResult(interp, "bev:  triangulating resulting object\n", (char *)NULL);
		if( BU_SETJUMP )
		{
			BU_UNSETJUMP;
			Tcl_AppendResult(interp, "WARNING: Triangulation failed!!!\n", (char *)NULL );
			if( tmp_tree )
				db_free_tree( tmp_tree );
			tmp_tree = (union tree *)NULL;
			nmg_km( mged_nmg_model );
			mged_nmg_model = (struct model *)NULL;
			return TCL_ERROR;
		}
		nmg_triangulate_model( mged_nmg_model , &mged_tol );
		BU_UNSETJUMP;
	}

	Tcl_AppendResult(interp, "bev:  converting NMG to database format\n", (char *)NULL);

	/* Export NMG as a new solid */
	RT_INIT_DB_INTERNAL(&intern);
	intern.idb_type = ID_NMG;
	intern.idb_meth = &rt_functab[ID_NMG];
	intern.idb_ptr = (genptr_t)mged_nmg_model;
	mged_nmg_model = (struct model *)NULL;

	if( (dp=db_diradd( dbip, newname, -1L, 0, DIR_SOLID, NULL)) == DIR_NULL )
	{
		Tcl_AppendResult(interp, "Cannot add ", newname, " to directory\n", (char *)NULL );
		return TCL_ERROR;
	}

	if( rt_db_put_internal( dp, dbip, &intern ) < 0 )
	{
		rt_db_free_internal( &intern );
		TCL_WRITE_ERR_return;
	}

	tmp_tree->tr_d.td_r = (struct nmgregion *)NULL;

	/* Free boolean tree, and the regions in it. */
	db_free_tree( tmp_tree );


	{
	  char *av[3];

	  av[0] = "e";
	  av[1] = newname;
	  av[2] = NULL;

	  /* draw the new solid */
	  return f_edit( clientData, interp, 2, av );
	}
}

/*
 *			A D D _ S O L I D _ P A T H _ T O _ R E S U L T
 */
void
add_solid_path_to_result( interp, sp )
Tcl_Interp *interp;
register struct solid	*sp;
{
	register int	i;

	for( i = 0; i <= sp->s_last; i++ )  {
		Tcl_AppendResult( interp, sp->s_path[i]->d_namep,
			i == sp->s_last ? NULL : "/", NULL );
	}
	Tcl_AppendResult( interp, " ", NULL );
}

/*
 *			R E D R A W _ V L I S T
 *
 *  Given the name(s) of database objects, re-generate the vlist
 *  associated with every solid in view which references the
 *  named object(s), either solids or regions.
 *  Particularly useful with outboard .inmem database modifications.
 */
int
cmd_redraw_vlist( clientData, interp, argc, argv )
ClientData clientData;
Tcl_Interp *interp;
int	argc;
char	**argv;
{
	struct directory	*dp;
	int		i;

	CHECK_DBI_NULL;

	if( argc < 2 )  {
		struct bu_vls vls;

		bu_vls_init(&vls);
		bu_vls_printf(&vls, "help redraw_vlist");
		Tcl_Eval(interp, bu_vls_addr(&vls));
		bu_vls_free(&vls);
		return TCL_ERROR;
	}

	for( i = 1; i < argc; i++ )  {
		register struct solid	*sp;

		if( (dp = db_lookup( dbip, argv[i], LOOKUP_NOISY )) == NULL )
			continue;

		FOR_ALL_SOLIDS(sp, &HeadSolid.l)  {
			register int j;
			for( j = sp->s_last; j >= 0; j-- )  {
				if( sp->s_path[j] != dp )
					continue;
#if 0
				add_solid_path_to_result(interp, sp);
#endif
				(void)replot_original_solid( sp );
				sp->s_iflag = DOWN;	/* It won't be drawn otherwise */
				break;
			}
		}
	}


	update_views = 1;
	return TCL_OK;
}
