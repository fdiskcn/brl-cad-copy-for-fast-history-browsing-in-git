/*
 *			libtcl/Cakefile
 */
#define SRCDIR	[[echo libtcl''TCLTK_VERS]]
#define	SRCSUFF	.c
#define MANSECTION	3

#include "../Cakefile.defs"

/* Since we're building a library, we can't also list a regular
 * program (like tclsh) in the PRODUCTS rule, it will confuse
 * the BUILD_LIBRARY rule in Cakefile.defs.
 * Hence the EXTRA_PRODUCTS dodge
 */

#if !defined(BUILD_LIBRARY)
#define	BUILD_LIBRARY	ar r
#define TCL_OPTS	--srcdir=[[pwd]]/../SRCDIR/"unix" --quiet --cache-file cache.MTYPE \
			 --exec-prefix=BASEDIR --prefix=BASEDIR --disable-shared
#define PRODUCTS	[[echo libtcl''TCLTK_VERS.a]]
#define	AFTER_MAKE	rm -f libtcl.a ; ln -s PRODUCTS libtcl.a
#else
#define TCL_OPTS	--srcdir=[[pwd]]/../SRCDIR/"unix" --quiet --enable-shared \
			 --cache-file cache.MTYPE --exec-prefix=BASEDIR --prefix=BASEDIR
#define PRODUCTS	[[ echo libtcl''TCLTK_VERS.so]]
#define SHARED_PRODUCT	libtcl''TCLTK_VERS.so''[[LIBVERS]]
#define	AFTER_MAKE	mv -f libtcl''TCLTK_VERS.so SHARED_PRODUCT ; sharedliblink.sh SHARED_PRODUCT ; rm -f libtcl.so ; ln -s SHARED_PRODUCT libtcl.so
#endif
#define EXTRA_PRODUCTS	tclsh

/* Rule to see if cake is running with -s flag */
#define MINUS_S	[[echo CAKEFLAGS | tr '\040' '\012' | sed -n -e /-s/p ]]

PRODUCTS: Makefile.MTYPE cache.MTYPE
	make MINUS_S -f Makefile.MTYPE CAKEFLAGS EXTRA_PRODUCTS
	AFTER_MAKE

EXTRA_PRODUCTS: Makefile.MTYPE cache.MTYPE
	make MINUS_S -f Makefile.MTYPE CAKEFLAGS EXTRA_PRODUCTS
	AFTER_MAKE


Makefile.MTYPE cache.MTYPE:		if not exist Makefile.MTYPE or not exist cache.MTYPE
	( \C\F\L\A\G\S=\"GFLAG OPTIMIZER PROFILER\"; \
		\A\F\T\E\R\_\M\A\K\E=\"AFTER_MAKE\"; \
		\R\A\N\L\I\B=\"RANLIB\"; \
		\L\D\F\L\A\G\S=\"LDFLAGS\"; \
		\B\U\I\L\D\_\L\I\B\R\A\R\Y=\"BUILD_LIBRARY\"; \
		\C\C=\"CC\"; \
		 export \C\F\L\A\G\S \R\A\N\L\I\B \L\D\F\L\A\G\S \B\U\I\L\D\_\L\I\B\R\A\R\Y \C\C \A\F\T\E\R\_\M\A\K\E; \
		 ../SRCDIR/"unix"/configure TCL_OPTS; \
		../SRCDIR/fix_makefile.sh > Makefile.MTYPE)


clean&:	Makefile.MTYPE
	make -f Makefile.MTYPE clean

noprod&:
	rm -rf libtcl''TCLTK_VERS.*

clobber&: Makefile.MTYPE
	make -f Makefile.MTYPE clean
	rm -f *.MTYPE *.o libtcl* config.log config.status tclConfig.sh tclsh Makefile
	#ifdef NFS
	rm -rf dltest
	#endif

install&: Makefile.MTYPE
	make -f Makefile.MTYPE install
	( cd BASEDIR/lib; AFTER_MAKE )

install-nobak&: Makefile.MTYPE
	make -f Makefile.MTYPE install
	( cd BASEDIR/lib; AFTER_MAKE )

tclsh: Makefile.MTYPE
	make -f Makefile.MTYPE tclsh

tcltest: Makefile.MTYPE
	make -f Makefile.MTYPE tcltest

test&: Makefile.MTYPE
	make -f Makefile.MTYPE test

/* dltest:			if not exist dltest
	cp -r ../SRCDIR/"unix"/dltest .
*/
