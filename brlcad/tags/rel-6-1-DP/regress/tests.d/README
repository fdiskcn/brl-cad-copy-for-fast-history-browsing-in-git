#
# t e s t s . d
#
###
# Introduction
#
# The scripts in this directory are intended to be used in conjunction with 
# the regression suite.  More specifically, these scripts are normally
# invoked by a 'regression' tool in the '..' directory.  These tests may,
# however, be run by themselves as well (perhaps with a little massaging).
#
# For most tests, running './TESTNAME start' should be enough to get the test
# to begin working.  Some tests, however, are dependant upon others or may
# rely on certain variables to be settable prior to running.
#
# If a test is waiting for some other test to complete, create the
# appropriate semaphore file that it is waiting for and it should continue
# normally.  If the test is dependant upon variables being set, it should 
# complain and/or give an error message -- see the file for details on what
# and appropriate value will be.
#
###
# Creating new tests
#
# All new tests must implement at least one principal function, namely the
# start ( ) function.  More correctly, every test must run when it's first
# argument is "start".  In addition to start, there is a stop and status
# function that may be defined to complement or override the default behaviour.
#
# By default status returns only whether a script has 1) started,
# 2) completed and failed, or 3) completed and succeeded.  No return message
# is given if the test never ran.
#
# By default the stop command will attempt to interrupt and kill any 
# presently running test it finds.  When tests begin running, they register
# themselves using lock variables.  These lock variables make a note of the
# locking process' PID that may be later killed.  The lock also serves to 
# prevent multple invokations of the same tests in the same directory
# (which could cause clobbering of temporary files).
#
# Two other optional commands that may be added to the tests are restart
# and clean.  Restart is, by default, simply a call to the stop and start
# commands.  Clean should be set up to remove any files that get created by 
# a start command.  
#
###
# Conventions
#
# There are a few conventions to consider to maintain consistency and common
# behaviour with other test scripts.  When creating files, they should be
# created in the current directory (whatever that may be) or a subdirectory
# therein (that the script tests for and creates (and cleans out later..)).
# All output should go to stdout and not logged to any particular file.
# Fatal errors and unexpected or bad behaviour should call bomb.  Caution
# messages should be logged using warn.  Echo and the library's log function
# are equivalent, though the latter is preferred for consistency (and for
# escape support detection).  Clean should remove any and all files that the
# script generates.  Any and all files generated should be prefixed with a
# '$TEST.' (e.g. geometry.output.log from the geometry test) where $TEST is
# the name of the test being run.
# 
###