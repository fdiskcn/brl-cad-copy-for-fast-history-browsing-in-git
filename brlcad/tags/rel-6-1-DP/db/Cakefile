/*
 *			db/Cakefile
 *
 *  This Cakefile is for a recipient machine.
 *  The function is to take the distributed .asc files, and
 *  create the local binary form in .g files.
 *
 *  The STATIC_PRODUCTS definition will cause special things to
 *  happen in the "inst-dist" rule, so that the ASCII files
 *  get into the distribution directory.
 *  What is still missing is some automation to generate the
 *  .asc files from updated binary .g files on the master machine,
 *  but of course, it is hard to know which directory to look in,
 *  so for now this will remain a manual process.
 */

#define PRODUCTS	\
	boolean-ops.g \
	bldg391.g crod.g demo.g \
	ktank.g moss.g prim.g \
	star.g truck.g world.g \
	cube.g axis.g woodsman.g \
	m35.g lgt-test.g havoc.g \
	tank_car.g sphflake.g xmp.g \
	wave.g pic.g galileo.g castle.g cray.g kman.g terra.g

#define STATIC_PRODUCTS		\
	../SRCDIR/boolean-ops.asc \
	../SRCDIR/bldg391.asc ../SRCDIR/crod.asc ../SRCDIR/demo.asc \
	../SRCDIR/ktank.asc ../SRCDIR/moss.asc ../SRCDIR/prim.asc \
	../SRCDIR/star.asc ../SRCDIR/truck.asc ../SRCDIR/world.asc \
	../SRCDIR/cube.asc ../SRCDIR/axis.asc ../SRCDIR/woodsman.asc \
	../SRCDIR/m35.asc ../SRCDIR/lgt-test.asc ../SRCDIR/lgt-test.ldb \
	../SRCDIR/lgt-test.mdb ../SRCDIR/lgt-test.key ../SRCDIR/havoc.asc \
	../SRCDIR/tank_car.asc ../SRCDIR/sphflake.asc ../SRCDIR/xmp.asc \
	../SRCDIR/wave.asc ../SRCDIR/pic.asc ../SRCDIR/galileo.asc \
	../SRCDIR/castle.asc ../SRCDIR/cray.asc  ../SRCDIR/kman.asc \
	../SRCDIR/terra.asc \


#define SRCDIR	db
#define FILES	/**/
#define	SRCSUFF	.asc
#define MANSECTION	9

#include "../Cakefile.defs"

/* Prevent actual installation */
#define DONT_INSTALL	1
#undef BINDIR
#define BINDIR	/tmp
#define AFTERINSTALL	rm

/* Default rule */
everything&::	PRODUCTS

/*
 *  Because it may be necessary to run this command long before
 *  a "cake install" could be done, on an SGI the "rld" loader has
 *  no idea where to find the shared libraries necessary.
 *  So we tell it.  Ugh.  Stupid SGI software.  Sun got it right.
 */
#if defined(NFS)
#	if defined(__CAKE__sun3) || defined(__CAKE__sun4)
#		define	CONV	../.conv.MTYPE/asc2g
#	else
#		if defined(__CAKE__sp3)
#			define	CONV	\
			LIBPATH=LIBWDB_DIR:LIBRT_DIR:LIBBN_DIR:LIBBU_DIR:LIBTCL_DIR:LIBITCL_DIR:LIBSYSV_DIR:$LIBPATH \
			../.conv.MTYPE/asc2g
#		else
#			define	CONV	\
			LD_LIBRARY_PATH=LIBWDB_DIR:LIBRT_DIR:LIBBN_DIR:LIBBU_DIR:LIBTCL_DIR:LIBITCL_DIR:LIBSYSV_DIR:$LD_LIBRARY_PATH \
			../.conv.MTYPE/asc2g
#		endif
#	endif
#	define	CONV_EX	../.conv.MTYPE/asc2g
#else
#	if defined(__CAKE__sun3) || defined(__CAKE__sun4)
#		define	CONV	../conv/asc2g
#	else
#		if defined(__CAKE__sp3)
#			define	CONV	\
			LIBPATH=LIBWDB_DIR:LIBRT_DIR:LIBBN_DIR:LIBBU_DIR:LIBTCL_DIR:LIBITCL_DIR:LIBSYSV_DIR:$LIBPATH \
			../.conv.MTYPE/asc2g
#		else
#			define	CONV	\
			LD_LIBRARY_PATH=LIBWDB_DIR:LIBRT_DIR:LIBBN_DIR:LIBBU_DIR:LIBTCL_DIR:LIBITCL_DIR:LIBSYSV_DIR:$LD_LIBRARY_PATH \
			../conv/asc2g
#		endif
#	endif
#	define	CONV_EX	../conv/asc2g
#endif

/* If converter program changed, be sure to re-convert the .g files */
%.g:	../SRCDIR/%.asc CONV_EX			if exist ../SRCDIR/%.asc
	rm -f %.g; CONV ../SRCDIR/%.asc %.g

#include "../Cakefile.rules"
