#ifdef WIN32
#include "common.h"

#include <stdio.h>
#include <math.h>
#include "machine.h"
#include "bu.h"
#include "vmath.h"
#include "bn.h"
#endif
const char bn_version[] = "\
@(#) BRL-CAD Release 7.6.7 with SURVICE Modifications   The BRL-CAD Numerical Computation Library\n\
    Mon Sep 12 16:00:00 EDT 2005, Compilation 1\n\
    bob@mako:/home/bob/src/brlcad/src/libbn\n";
