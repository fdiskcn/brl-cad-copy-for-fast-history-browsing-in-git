<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='build_region'>
  <refmeta>
    <refentrytitle>BUILD_REGION</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>build_region</refname>
    <refpurpose>
      Builds a region from existing solids that have
      specifically formatted names based on the provided tags and numbers.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>build_region</command>    
      <arg choice='opt'>-a <replaceable>region_num</replaceable></arg>
      <arg choice='req'><replaceable>tag start_num end_num</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      Builds a region from existing solids that have specifically formatted names 
      based on the provided tags and numbers. The created region will be named "tag.rx,"
      where "x" is the first number (starting from 1) that produces an unused region 
      name. If the <emphasis>-a</emphasis> option is used, then the specified
      "region_num" will be used for "x." If that region already exists, this operation will
      append the new geometry to the existing region definition. If that region does not 
      exist, a new one will be created. The solids that will be involved in this operation 
      are those with names of the form "tag.s#" or "tag.s#o@," where "#" is a number 
      between <emphasis>start_num</emphasis> and <emphasis>end_num</emphasis> inclusive, 
      "o" is either "u," "-," or "+," and "@" is any number.
      The operators and numbers coded into the solid names are used to build the region.
    </para>
  </refsect1>
  
  <refsect1 id='examples'>
    <title>EXAMPLES</title>
    <para>The example demonstrates how to use the <command>build_region</command> command to 
    build a region from existing solids.
    </para>
    <example>
      <title>Build a region from existing solids</title>
      <variablelist>
	<varlistentry>
	  <term><prompt>mged></prompt> <userinput>buid_region abc 1 2</userinput></term>
	  <listitem>
	    <para>Creates a region named "abc.r1" consisting of: 
	    <simplelist type='vert'>
	      <member>u abc.s1</member>
	      <member>u abc.s2</member>
	      <member>+ abc.s2+1</member>
	      <member>- abc.s2-1</member>
	    </simplelist>
	    provided that the above shapes already exist in the database.
	    </para>
	  </listitem>
	</varlistentry>
      </variablelist>
    </example>
  </refsect1>
  
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>BRL-CAD Team</para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
    </para>
  </refsect1>
</refentry>

