<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2008-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR AS IS'' AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='gastank1'>

<refmeta>
  <refentrytitle>GASTANK</refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>gastank</refname>
  <refpurpose>
    Creates a BRL-CAD .g file containing gas tanks.
  </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>gastank</command>    
    <arg choice='opt' rep='repeat'><replaceable>options</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>
    <command>gastank</command> is a program to create a BRL-CAD data base of 
    gas tanks. Up to twenty-six gas tanks of the same size may be created.  
    These gas tanks are solid with rounded corners and edges.  They are 
    appropriate for use with millimeter wave signature analysis.  They are not 
    appropriate for infra  red or vulnerability analysis.  <command>gastank</command>
    uses libwdb to create a BRL-CAD database.  The gas tanks are composed of three
    arb8s, eight spheres, and twelve cylinders.  This program may be run interactively
    or the user may specify options on a command line.  If the user chooses to run
    the program interactively he answers the questions as the program prompts him.  
    Below are the options that can be used on the command line.
  </para>
</refsect1>


<refsect1 id='options'>
  <title>OPTIONS</title>
  <variablelist remap='TP'>
    <varlistentry>
      <term><option>-fname.g</option></term>
      <listitem>
	<para>
         BRL-CAD file name.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-n#</option></term>
      <listitem>
	<para>
         The number of gas tanks to be created (must be less than or equal to 26.)
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-h#</option></term>
      <listitem>
	<para>
         Height of gas tank in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-w#</option></term>
      <listitem>
	<para>
         Width of gas tank in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-d#</option></term>
      <listitem>
	<para>
         Depth of gas tank in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-r#</option></term>
      <listitem>
	<para>
         Radius of the corner in millimeters.
        </para>
      </listitem>
    </varlistentry>
 </variablelist>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <example>
    <title>Interactive <command>gastank</command> Session</title>
    <para>
    <literallayout>
$ gastank

This program constructs a solid gas tank with all
edges and corners rounded.

Enter the mged file to be created (25 char max).
        gastank.g
Enter the number of gas tanks to create (26 max).
        3
Enter the height, width, and depth of the gas tank.
        300 200 400
Enter the radius of the corners.
        30

mged file:  gastank.g
height of gas tank:  300.000000 mm
width of gas tank:  200.000000 mm
depth of gas tank:  400.000000 mm
radius of corner:  30.000000 mm
number of gas tanks:  3
   </literallayout>
    </para>
  </example>

  <example>
    <title>Single-Line <command>gastank</command> Command</title>
    <para>
      <userinput>gastank -fgastank.g -n3 -h300 -w200 -d400 -r30</userinput>
    </para>
  </example>

  <para>
   Both of these examples produce the same result - 3 gas tanks
   with a height of 300mm, a width of 200mm, a depth of 400mm, and
   a radius for the corner of 30mm.  The BRL-CAD database file
   created in each case is gastank.g
  </para>

</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>Susan A. Coates</para>
</refsect1>

<refsect1 id='copyright'>
  <title>COPYRIGHT</title>
  <para>
    This software is Copyright (c) 2005-2010 United States Government as
    represented by the U.S. Army Research Laboratory. All rights reserved.
  </para>
</refsect1>

<refsect1 id='see_also'>
  <title>SEE ALSO</title>
  <para>
   bolt(1), handle(1), window(1), window_frame(1)
  </para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

