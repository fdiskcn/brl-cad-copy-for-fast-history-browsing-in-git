<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='rtcheck1'>
  <refmeta>
    <refentrytitle>RTCHECK</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  <refnamediv id='name'>
    <refname>rtcheck</refname>
    <refpurpose>Make UNIX-Plot of overlaps in an mged model, using raytracing</refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>rtcheck</command>    
      <arg choice='opt' rep='repeat'><replaceable>options</replaceable></arg>
      <arg choice='plain'><replaceable>model.g</replaceable></arg>
      <arg choice='plain' rep='repeat'><replaceable>objects</replaceable></arg> &gt; <arg choice='plain'><replaceable>overlaps.pl</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <emphasis remap='I'>Rtcheck</emphasis> operates on the indicated
      <emphasis remap='I'>objects</emphasis> in the input
      <emphasis remap='I'>model.g</emphasis> and produces a floating point 3-D BRL-style
      <citerefentry><refentrytitle>plot</refentrytitle><manvolnum>5</manvolnum></citerefentry>
      format file on standard output, in millimeters.
    </para>
    
    <para>
      The orientation of the rays to be fired may be specified by the
      <option>-a</option> and <option>-e</option> options, in which case the 
      model will be autosized, and the grid will be centered on the centroid of the 
      model, with ray spacing chosen to span the entire set of <emphasis remap='I'>objects.</emphasis>
      Alternatively, with the <option>-M</option> option, a transformation matrix may 
      be provided on standard input which maps model-space to view-space.
      In this case, the grid ranges from -1.0 &lt; = X,Y &lt; = +1.0 in view space,
      with the size of the grid (number of rays fired) specified with the
      <option>-s</option> option. The <option>-M</option> option is most useful when
      <command>rtcheck</command> is being automatically invoked from within 
      <citerefentry><refentrytitle>mged</refentrytitle><manvolnum>1</manvolnum></citerefentry>
      using the <command>rtcheck</command> command in <emphasis remap='I'>mged</emphasis>.
    </para>

    <para>
      The following options are recognized.
    </para>
    <variablelist remap='TP'>
      <varlistentry>
	<term><option>-g#</option></term>
	<listitem>
	  <para>
	    The grid cell width, in millimeters.  Ordinarily, the cell width is
	    computed by dividing the view size by the number of pixels of width.
	    This option provides an alternate direct means to set the grid cell
	    width.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-s#</option></term>
	<listitem>
	  <para>
	    Number of rays to fire in X and Y directions (square grid).
	    Default is 512 (512 x 512).
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-r</option></term>
	<listitem>
	  <para>
	    Report unique overlaps. At the end of a view, a list of unique pairs
	    of overlapping regions is printed, along with a count of the number
	    of detected overlaps for each pair. Not all detected
	    overlap combinations are necessarily listed, as the overlap
	    partition may mask a second overlap that is a continuation of an
	    already detected overlap.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-a#</option></term>
	<listitem>
	  <para>
	    Select azimuth in degrees.  Used with <option>-e</option>
	    and conflicts with <option>-M</option>
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-e#</option></term>
	<listitem>
	  <para>Select elevation in degrees.  Used with <option>-a</option>
	  and conflicts with <option>-M</option>
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-G</option></term>
	<listitem>
	  <para>Sets the grid cell height, in millimeters.</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-M</option></term>
	<listitem>
	  <para>
	    Read model2view matrix from standard input. Conflicts with
	    <option>-a</option> and <option>-e</option>
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-P#</option></term>
	<listitem>
	  <para>
	    Specify the maximum number of processors (in a multi-processor system) to be
	    used for this execution.  The default is 1.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-+ t</option></term>
	<listitem>
	  <para>
	    Specify that rtcheck should output plot file data in text format.
	    Default is binary plot data.
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
    
    <para>
      <command>rtcheck</command> also accepts all other options that the
      <citerefentry><refentrytitle>rt</refentrytitle><manvolnum>1</manvolnum></citerefentry>
      program implements, such as for non-square views, perspective, etc.
    </para>
    
    <para>
      The <command>rtcheck</command> program is a simple front-end to
      <citerefentry><refentrytitle>librt</refentrytitle><manvolnum>3</manvolnum></citerefentry>
      which is most useful when used from
      <citerefentry><refentrytitle>mged</refentrytitle><manvolnum>1</manvolnum></citerefentry>.
      The output can be independently viewed using the UNIX-plot viewers like
      <citerefentry><refentrytitle>pl-fb</refentrytitle><manvolnum>1</manvolnum></citerefentry>,
      <citerefentry><refentrytitle>pl-sgi</refentrytitle><manvolnum>1</manvolnum></citerefentry>,
      <citerefentry><refentrytitle>tplot</refentrytitle><manvolnum>1</manvolnum></citerefentry>,
      etc.
    </para>
  </refsect1>
  
  <refsect1 id='see_also'>
    <title>SEE ALSO</title>
    <para>
      <citerefentry><refentrytitle>mged</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>rt</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>rtray</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>rtpp</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>pl-fb</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>pl-sgi</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>tplot</refentrytitle><manvolnum>1</manvolnum></citerefentry>,
<citerefentry><refentrytitle>libplot3</refentrytitle><manvolnum>3</manvolnum></citerefentry>, <citerefentry><refentrytitle>librt</refentrytitle><manvolnum>3</manvolnum></citerefentry>, <citerefentry><refentrytitle>plot</refentrytitle><manvolnum>5</manvolnum></citerefentry>.
    </para>
  </refsect1>
  
  <refsect1 id='diagnostics'>
    <title>DIAGNOSTICS</title>
    <para>
      Numerous error conditions are possible. Descriptive messages are printed on stderr 
      (file descriptor 2).
    </para>
  </refsect1>
  
  <refsect1 id='authors'>
    <title>AUTHORS</title>
    <para>Michael John Muuss, Gary S. Moss</para>
  </refsect1>
  
  <refsect1 id='copyright'>
    <title>COPYRIGHT</title>
    <para>
      This software is Copyright (c) 1988-2010 United States Government as
      represented by the U.S. Army Research Laboratory. All rights reserved.
    </para>
  </refsect1>
  
  <refsect1 id='bugs'><title>BUGS</title>
  <para>
    Most deficiencies observed while using the <command>rtcheck</command>
    program are a consequence of problems in 
    <citerefentry><refentrytitle>librt.</refentrytitle><manvolnum>3</manvolnum></citerefentry>
  </para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;.</para>
</refsect1>
</refentry>

