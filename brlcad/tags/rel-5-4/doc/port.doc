So you've decided that you want to port BRLCAD to a new operating system....

Well, this is my record of porting BRLCAD to FreeBSD.  These are pretty
much the same steps I used for porting BRLCAD to A/UX and LINUX.

The first step, strange as it might seem, is to find out if your
system is already known to the BRL-CAD team.  It is sometimes the case
that a system is not in the list of official ports but is in the software
because somebody was interested but did not have enough time to 
get everything done.

So the first thing to do is to try:
	machinetype.sh -v

If that works, the chances are that somebody has already installed brlcad
on your machine.

If it does not work, then go to the base of the brlcad tree and type:
	sh/machinetype.sh -v

Here is the results that I got:

17 neunacht> ./machinetype.sh -v
MACHINE=at; UNIXTYPE=SYSV; HAS_TCP=1; HAS_SYMLINKS=0; BASEDIR=/usr/brlcad


According to this, my FreeBSD box is of type SYSV and does not have symbolic
links.  It is also an "at" machine.  This is not correct, so we need to
modify machinetype.sh to recongize our machine.  

Here are the things we will need to know before we can make that modification:
	1) MACHINE 	This is a short, unique, identifier for this type of
			machine.
	2) UNIXTYPE	BSD or System V
	3) HAS_TCP	Does this box have TCP/IP networking?
	4) HAS_SYMLINKS	Does this box have symbolic links?

For FreeBSD the answers are:
	1) fbsd	
	2) BSD
	3) YES
	4) YES

