                             BRL-CAD
                         Release 7.4.0
                       http://brlcad.org/

BRL-CAD is a powerful Combinatorial/Constructive Solid Geometry (CSG)
solid modeling system that includes an interactive geometry editor,
ray-tracing support for rendering and geometric analysis,
network-distributed framebuffer support, image and signal-processing
tools, realistic image synthesis path-tracing, and an embedded
scripting language.

The package is a collection of over 400 tools and utilities across
over 750,000 lines of source code.  Included is support for various
geometric data format conversions, image and signal processing
capabilities, sophisticated ray-tracing based lighting models, network
distributed ray-tracing, massively parallel ray-tracing,
high-performance path-tracing, animation capabilities, data
compression, image handling, and interactive 3-D geometric editing.
Included is an implementation of Weiler's n-Manifold Geometry (NMG)
data structures for surface-based solid models and photon mapping.

Since 1979, the U.S. Army Research Laboratory has been developing and
distributing the BRL-CAD solid modeling package for a wide range of
military and industrial applications.  The package is primarily used
for the design and analysis of vehicles, mechanical parts,
architecture, and various signature analysis work.  BRL-CAD is also used in
radiation dose planning and education where it's used to to aid
teaching fundamental computer graphics and CSG modeling concepts to
primary, secondary, and higher education students.

BRL-CAD was released as open source in December 2004 with portions
licensed under the GPL, LGPL, GFDL, and BSD license.  See the COPYING
file for more details.


CONTENTS
--------
  Introduction
  Contents
  Getting Started
  Compile and Install
  Benchmark
  Communication
  Bug Reports/Feature Requests
  Distribution Details
  Background Material
  Future Events
  Getting Help


GETTING STARTED
----------------

This README file exists only to provide you a brief roadmap to the
contents of the BRL-CAD distribution.

The information on how to install and operate the software, to become
involved with the development of the package, or to run the benchmark
performance test suite have each become sufficiently large that their
details are covered in more detail in standalone documents.  For more
details on what BRL-CAD provides, see the documentation provided on
the website at http://brlcad.org/.

Information relevant to those interested in the source code is
contained in the HACKING and COPYING files.  See doc/benchmark.tr file
for benchmark suite information.  There is a variety of other
documentation and information also available in the doc/ directory.

Please note that this distribution package does not include the
various military-specific model analysis tools such as MUVES, GIFT,
SAR, SLAVE, VAST, etc., nor does it include any military geometry
databases.  If you desire to have access to this material, please
contact one of the BRL-CAD developers.


COMPILE AND INSTALL
-------------------

See the INSTALL file for details on configuring, compiling, and
installing BRL-CAD.  In order to compile and install BRL-CAD from a
source distribution, the following steps may be taken for a full
default install:

  ./configure
  make
  make install

If you do not have a configure script in your source tree (at the same
level as this README file), you will need a relatively up-to-date
version of the GNU Build System (autoconf, automake, & libtool)
installed.  The autogen.sh script will need to be run successfully in
order to generate the configure script first:

  sh autogen.sh
  ./configure
  make
  make install

There are various options available to both configure and make.  Run
"./configure --help" for a list of available configuration options.
Users on multiprocessor systems will be able to speed up their builds
by passing the -j option to make (e.g. make -j4, assuming GNU make).

Once the build completes and assuming you have proper filesystem
permissions, you should be able to begin the installation by running
the "make install" .  By default, the package is configured to install
entirely into /usr/brlcad/.

You will need to add /usr/brlcad/bin to your path (or whatever was
used as a --prefix during configure) in order for the BRL-CAD binaries
to be in your path.

Warning: Libtool 1.5.x may be incompatible due to changes the libtool
project made that were backwards-incompatible.  Using an earlier 1.4.x
version is recommended for the time being.


BENCHMARK
---------

After the build successfully completes, you will be able to verify
basic functionality by running the BRL-CAD Benchmark.  Running the
benchmark suite is integrated into the build system such that any time
after configure, you may run:

  make benchmark

If the build is successful, you will see "CORRECT" numbers and a
performance summary at the end.  The "vgr" line effectively shows you
approximately how much faster your machine is to a VAX 11/780.  After
installing BRL-CAD, you may run the 'benchmark' tool as well to run
the BRL-CAD Benchmark.

See the paper in doc/benchmark.tr for information on analyzing and
comparing the results of the benchmark tests in more detail.


COMMUNICATION
-------------

You are invited to participate in the InterNet (aka "ARPANET")
electronic mailing list on the BRL-CAD software.  There are various
BRL-CAD mailing lists available on the project website at
http://sourceforge.net/mail/?group_id=105292 including three
developer-oriented lists, a user list, and a read-only news
announcement list.  See the posted archives to get a feel for the
volume of list traffic for each list and to read/search past
discussions.

On-line web forums are likewise available on the project site at
http://sourceforge.net/forum/?group_id=105292

A variety of information on BRL-CAD is available via the
World-Wide-Web.  Papers, documentation, and the software repository
may be found at:

  http://brlcad.org

In addition to the above, there is an official BRL-CAD IRC channel on
the Freenode network that may be reached by joining #brlcad on
irc.freenode.net connecting via port 6667.  For first-time users,
there is a web-based IRC client available for certain browsers at:

  http://irc.brlcad.org

IRC is the preferred method of communication to interact with the
BRL-CAD developers, to get involved with the project, obtain support,
or provide direct feedback.


BUG REPORTS AND FEATURE REQUESTS
--------------------------------

Please report any bugs encountered to the project bug tracker at
http://sourceforge.net/tracker/?group_id=105292&atid=640802

Similarly, please post any request for feature enhancements or support
request to http://sourceforge.net/tracker/?group_id=105292&atid=640805
and http://sourceforge.net/tracker/?group_id=105292&atid=640803
respectively.


DISTRIBUTION DETAILS
--------------------

BRL-CAD is freely available and distributed in both binary and source
code form through the BRL-CAD project website on Sourceforge:

  http://sf.net/projects/brlcad/

The package is similarly available via the Concurrent Versions System
(CVS) software version control repository.  To obtain the latest,
follow the directions for anonymous checkout at:

  http://sourceforge.net/cvs/?group_id=105292

There is also a paid-for full-service support distribution available
for those interested through the Survivability/Vulnerability
Information Analysis Center (SURVIAC).

SURVIAC administers the supported BRL-CAD distributions and
information exchange programs for the U.S. Army Research Laboratory.
Full service distributions include a copy of the full distribution
materials on your choice of magnetic or tape media.  One printed set
of BRL-CAD documentation will be mailed to you.  Software upgrades
will also be provided at no additional charge, and you will have
access to full technical assistance by phone, FAX, letter, or E-mail.
Agencies of the Federal Government may acquire the full service
distribution with a simple MIPR or OGA funds transfer.

For further details, contact:

BRL-CAD Distribution
SURVIAC Aberdeen Satellite Office
4695 Millennium Drive
Belcamp, MD 210017-1505  USA

or call USA telephone (410) 273-7794, send E-mail to
<cad-dist@arl.army.mil>, FAX your letter to USA (410) 272-6763, or
write to the above address.


BACKGROUND MATERIAL
-------------------

For overview information on the whole package, consult the website at
http://brlcad.org/ and the brlcad(1) manual page in the file
src/util/brlcad.1.  For a discussion of the significance of this
software, read the paper "Understanding the Preparation and Analysis
of Solid Models".


FUTURE EVENTS
-------------

It is expected that new releases of BRL-CAD will be issued
approximately once a month.  A new release is scheduled for
distribution at the beginning of every month, usually being posted
sometime within the first week of every month.

Depending on need, demand, and activity, additional releases may also
be made.  The additional releases will be heavily dependant upon the
level of user involvement and contributions in particular.
Information about new releases will be routinely provided by
electronic mail to recipients of this software as well as to the
BRL-CAD NEWS mailing list:

  http://sourceforge.net/mail/?group_id=105292

Periodically, training and user group meetings are provided with
announcements similarly posted to the BRL-CAD NEWS mailing list.
Several BRL-CAD developers also regularly attend the ACM Siggraph
conference, the ACM Solid and Physical Modeling conference, and other
similar conferences, conventions, and symposiums as well.


GETTING HELP
------------

Feel free to direct any unanswered questions relating to BRL-CAD via
e-mail to 'devs@brlcad.org' or to any of the other contact means
described above in the communications section of this document.

Best Wishes,
The BRL-CAD Development Team
devs@brlcad.org

