/*
  *			fb/Cakefile
 */

#define SRCDIR	fb
#define PRODUCTS	\
	bw-fb cat-fb cell-fb cmap-fb fbgammamod \
	fb-bw fb-cmap fb-fb fb-orle fb-pix fb-rle \
	fbanim fbcbars fbclear fbcmap fbcmrot fbcolor \
	fbfade fbframe fbfree fbgamma fbgrid fbhelp \
	fblabel fbline fbpoint fbscanplot fbstretch fbzoom \
	gif-fb gif2fb orle-fb pix-fb pixflip-fb \
	pl-fb polar-fb pp-fb rle-fb spm-fb pixautosize png-fb fb-png

#define	SRCSUFF	.c
#define MANSECTION	1

#include "../Cakefile.defs"
#include "../Cakefile.prog"

/* Explicit composition of each product */

fbanim:		fbanim.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbanim fbanim.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

pl-fb:		pl-fb.o LIBFB_DEP LIBBU_DEP LIBBN_DEP
	CC LDFLAGS -o pl-fb pl-fb.o LIBFB LIBFB_LIBES LIBBN LIBBU LIBBU_LIBES LIBES

fbcbars:	fbcbars.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbcbars fbcbars.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

png-fb:		png-fb.o LIBFB_DEP LIBPNG_DEP LIBBU_DEP LIBZ_DEP
	CC LDFLAGS -o png-fb png-fb.o LIBBU LIBBU_LIBES LIBPNG LIBFB LIBFB_LIBES LIBZ LIBES

fb-png:		fb-png.o cmap-crunch.o LIBPNG_DEP LIBFB_DEP LIBBU_DEP LIBZ_DEP
	CC LDFLAGS -o fb-png fb-png.o cmap-crunch.o LIBBU LIBBU_LIBES LIBPNG LIBFB LIBFB_LIBES LIBZ LIBES

pix-fb:		pix-fb.o LIBFB_DEP LIBRT_DEP LIBBU_DEP LIBBN_DEP
	CC LDFLAGS -o pix-fb pix-fb.o LIBFB LIBFB_LIBES LIBRT LIBBU LIBBU_LIBES LIBBN LIBES

fb-pix:		fb-pix.o cmap-crunch.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fb-pix fb-pix.o cmap-crunch.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

fb-fb:		fb-fb.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fb-fb fb-fb.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

pixflip-fb:	pixflip-fb.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o pixflip-fb pixflip-fb.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

pp-fb:		pp-fb.o LIBFB_DEP LIBTERMIO_DEP LIBBU_DEP
	CC LDFLAGS -o pp-fb pp-fb.o LIBFB LIBFB_LIBES LIBTERMIO LIBBU LIBBU_LIBES LIBES

fbclear:	fbclear.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbclear fbclear.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

fbfree:	fbfree.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbfree fbfree.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

fbhelp:	fbhelp.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbhelp fbhelp.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

fbgrid:		fbgrid.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbgrid fbgrid.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

pixautosize:	pixautosize.o LIBFB_DEP LIBRT_DEP LIBBN_DEP LIBBU_DEP
	CC LDFLAGS -o pixautosize pixautosize.o LIBFB LIBFB_LIBES LIBRT LIBBN LIBBU LIBBU_LIBES LIBES

/* Old RLE Programs (Utah format Edition 2) */

orle-fb:	orle-fb.o LIBFB_DEP LIBORLE_DEP LIBBU_DEP
	CC LDFLAGS -o orle-fb orle-fb.o LIBFB LIBFB_LIBES LIBORLE LIBBU LIBBU_LIBES LIBES

fb-orle:	fb-orle.o cmap-crunch.o LIBFB_DEP LIBORLE_DEP LIBBU_DEP
	CC LDFLAGS -o fb-orle fb-orle.o cmap-crunch.o LIBFB LIBFB_LIBES LIBORLE LIBBU LIBBU_LIBES LIBES

/* New RLE Programs (Utah format Edition 3) -- matches Utah Raster Toolkit */

/*** oldtonewrle.c ? */

rle-fb:		rle-fb.o LIBFB_DEP LIBRLE_DEP LIBBU_DEP
	CC LDFLAGS -o rle-fb rle-fb.o LIBFB LIBFB_LIBES LIBRLE LIBBU LIBBU_LIBES LIBES

fb-rle:		fb-rle.o cmap-crunch.o LIBFB_DEP LIBRLE_DEP LIBBU_DEP
	CC LDFLAGS -o fb-rle fb-rle.o cmap-crunch.o LIBFB LIBFB_LIBES LIBRLE LIBBU LIBBU_LIBES LIBES

fbcmap:		fbcmap.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbcmap fbcmap.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

fb-cmap:	fb-cmap.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fb-cmap fb-cmap.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

cmap-fb:	cmap-fb.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o cmap-fb cmap-fb.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

fbgamma:	fbgamma.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbgamma fbgamma.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

fbgammamod:	fbgammamod.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbgammamod fbgammamod.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

fbscanplot:	fbscanplot.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbscanplot fbscanplot.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

fbzoom:		fbzoom.o LIBFB_DEP LIBTERMIO_DEP LIBBU_DEP
	CC LDFLAGS -o fbzoom fbzoom.o LIBFB LIBFB_LIBES LIBTERMIO LIBBU LIBBU_LIBES LIBES 

fbpoint:	fbpoint.o LIBFB_DEP LIBTERMIO_DEP LIBBU_DEP
	CC LDFLAGS -o fbpoint fbpoint.o LIBFB LIBFB_LIBES LIBTERMIO LIBBU LIBBU_LIBES LIBES 

fblabel:	fblabel.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fblabel fblabel.o LIBBU LIBBU_LIBES LIBFB LIBFB_LIBES LIBES 

fbline:		fbline.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbline fbline.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES 

fbcolor:	fbcolor.o LIBFB_DEP LIBTERMIO_DEP  LIBBU_DEP
	CC LDFLAGS -o fbcolor fbcolor.o LIBFB LIBFB_LIBES LIBTERMIO LIBBU LIBBU_LIBES LIBES 

fbcmrot:	fbcmrot.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbcmrot fbcmrot.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

fbframe:	fbframe.o LIBFB_DEP  LIBBU_DEP
	CC LDFLAGS -o fbframe fbframe.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

bw-fb:		bw-fb.o LIBFB_DEP LIBRT_DEP LIBBU_DEP LIBBN_DEP
	CC LDFLAGS -o bw-fb bw-fb.o LIBFB LIBFB_LIBES LIBRT LIBBU LIBBU_LIBES LIBBN LIBES

fb-bw:		fb-bw.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fb-bw fb-bw.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

cat-fb:		cat-fb.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o cat-fb cat-fb.o LIBBU LIBBU_LIBES LIBFB LIBFB_LIBES LIBES

/* "gif2fb" is an alternative implementation of "gif-fb"; use either or both */

gif2fb:		gif2fb.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o gif2fb gif2fb.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

fbfade:		fbfade.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbfade fbfade.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

fbstretch:	fbstretch.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o fbstretch fbstretch.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

gif-fb:		gif-fb.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o gif-fb gif-fb.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

spm-fb:		spm-fb.o LIBFB_DEP LIBBU_DEP LIBBN_DEP LIBRT_DEP
	CC LDFLAGS -o spm-fb spm-fb.o LIBRT LIBFB LIBFB_LIBES LIBBN LIBBU LIBBU_LIBES LIBES

polar-fb:		polar-fb.o LIBFB_DEP LIBBU_DEP
	CC LDFLAGS -o polar-fb polar-fb.o LIBFB LIBFB_LIBES LIBBU LIBBU_LIBES LIBES

cell-fb:		cell-fb.o LIBFB_DEP LIBBU_DEP LIBBN_DEP
	CC LDFLAGS -o cell-fb cell-fb.o LIBFB LIBFB_LIBES LIBBN LIBBU LIBBU_LIBES LIBES

#include "../Cakefile.rules"
