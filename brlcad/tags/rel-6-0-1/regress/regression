#!/bin/sh
#
#  r e g r e s s i o n
#
###
#
#  This is the regression testing script suite from the BRL-CAD package.  This
#  script is run from a machine that has access to a BRL-CAD source tree (or 
#  any other for that matter) and a set of default regression test scripts 
#  (i.e. a test bucket).  The regression suite may either be run locally 
#  (default) or across multiple systems.
#
#  The source tree is obtained either via a source directory (default), cvs, 
#  a tar file, or a compressed tar file (tar.gz, tar.bz, tar.Z).  That source
#  may then be optionally merged with another source tree, tar, or tar.* of 
#  "update" patches to apply [unimplemented].
#
#  The resulting "source package" may then be optionally regression tested on
#  a host machine (localhost is default) or on the host systems listed in the 
#  systems.d directory.  Each system defines all of the options and 
#  specifications on how to get the source package to the regressing host as 
#  well as which tests should be run.
#
#  The source package is transferred to remote hosts and tests are remotely 
#  invoked via SSH.  SSH must be properly set up with rsa or dsa key
#  management to avoid password prompting.
#
#  Filesystem changes should all only occur within the REGRESS_DIR directory,
#  e.g. the REGRESS_DIR directory is the working directory.
#
###
#
#  GETTING STARTED
#
#  Run:
#  ./regression
#  ./regression --help-options
#  ./regression --help-commands
#  to get help with command line options for modifying run-time behaviour.
#
#  Run:
#  ./regression start
#  to begin runnning all regression tests listed in the tests.d directory on 
#  the local host.
#
#  The default behavior of the regression script is to regression test the
#  brlcad package that this regression testing suite resides with and run on 
#  the local host pulling any options listed in the systems.d directory or 
#  from a local .regressionrc file.  By default, the only defined system
#  should be "localhost" and the default testing directive is to run all of 
#  the tests found in the tests.d directory (the test bucket).  See the 
#  systems.d/localhost script and/or the systems.d/README for an example on 
#  how to set up your own systems to automatically regression test the brlcad
#  package.  Run './regression settings' or use a file in the systems.d 
#  directory as a starting point for defining new systems.
#
###############################################################################

#
# include standard utility functions
# dirname is needed so that we will still be able to find the library file
# regardless of where or how this script was invoked
. `dirname $0`/tests.d/library

###
# DEFAULT PUBLIC VARIABLE INITIALIZATION
########################################

# default name of the regression source (for various usages)
initializeVariable PACKAGE_NAME brlcad

# default directory for source package to regress
initializeVariable SOURCE ..

# default working directory for checkouts and (un)packing
initializeVariable REGRESS_DIR /tmp/`whoami`

# default system to regression test
# all is a special keyword to regression test all of the systems, otherwise
# the tests must manually be run one at a time through the shell. 
# localhost is a special keyword to regression test the local system.
initializeVariable REGRESS_SYSTEM localhost

# default test(s) to run
# all is a special keyword to perform all tests
# otherwise the tests must manually be run one at a time through the shell (!)
initializeVariable REGRESS_TEST all

# fail on warnings (0==do not fail, 1==fail)
initializeVariable NO_WARNINGS 0

# clean out the regression directory before starting
initializeVariable CLOBBER_FIRST 0

# name of log file for output
initializeVariable LOGFILE .regression-${HOSTNAME}-$$.log

# number of minutes (greater than zero) to wait on the regression lock
initializeVariable WAIT_FOR_LOCK_TIMEOUT 3

# whether to link to or copy source archives into the regression directory
initializeVariable LINK_OR_COPY copy

# CVS
###

# cvs binary to run
initializeVariable CVS cvs

# location of the cvs repository
initializeVariable CVS_ROOT /c/CVS

# name of package to pull from cvs repository
initializeVariable CVS_TAG brlcad

# cvs log file (unset to not log)
initializeVariable CVS_LOGFILE .cvs-${HOSTNAME}.log

# SSH
###

# ssh binary to run
initializeVariable SSH ssh

# TAR
###

# tar binary to run
initializeVariable TAR tar

# GZIP
###

# gzip compressor binary to run
initializeVariable GZIP gzip

# gzip decompressor binary to run
initializeVariable GUNZIP gunzip

# COMMAND
###

# name of default command to run 
# (no COMMAND set or invalid command prompts a help message)
#initializeVariable COMMAND settings

# TRANSPORT
###

# transport is the mechanism that gets a file from the localhost to remote machines
# available options are ssh, rsh, and nfs
initializeVariable TRANSPORT ssh

###
# PRIVATE VARIABLE INITIALIZATION
#################################

# version of this brlcad regression suite (library version is separate)
initializeVariable VERSION 1.0

# if we have a gen.sh around, grab the brlcad release version
if [ -r "${LPWD}/../gen.sh" ] ; then
    initializeVariable BRLCAD_VERSION "`grep '^RELEASE=' \"${LPWD}/../gen.sh\" | sed 's/^RELEASE=[\ \t]*\([^\ \;]*\).*$/\1/'`"
elif [ -r "${LPWD}/gen.sh" ] ; then
    initializeVariable BRLCAD_VERSION "`grep '^RELEASE=' \"${LPWD}/gen.sh\" | sed 's/^RELEASE=[\ \t]*\([^\ \;]*\).*$/\1/'`"
fi

# name of systems directory
initializeVariable SYSTEMS_D systems.d
initializeVariable SYSTEMS_D_WITH_PATH "${LPWD}/${SYSTEMS_D}"

# name of tests directory (must update library inclusion to match if this changes)
initializeVariable TESTS_D tests.d
initializeVariable TESTS_D_WITH_PATH "${LPWD}/${TESTS_D}"

# name of the remote architecture regress directories
initializeVariable REMOTE_REGRESS_DIR "${REGRESS_DIR}/.regress.${HOSTNAME}"

# resource file loading
###

# the resource file may be used to override any of the default public or
# private settings.  look for the rc file first in the current directory 
# and then in the same directory as the regression script
initializeVariable RESOURCE_FILE ${LPWD}/.regressionrc
# go ahead and load the resource file so command-line options will override
# we only perform the readability check to avoid warning output
[ -r "$RESOURCE_FILE" ] && loadResourceFile "$RESOURCE_FILE"

# help
###

INFO="The BRL-CAD Regression Suite is a tool for regression testing.\n"
INFO="${INFO}  For updates to this tool and the BRL-CAD software package,\n"
INFO="${INFO}  visit the BRL-CAD website at http://web.arl.army.mil/software/brlcad/"

# build up the usage and help incrementally (it is easier to modify later)
USAGE="Usage: $0 [regression-options] command [command-options]\n"
USAGE="${USAGE}  where regression-options are -w, -S, etc.\n"
USAGE="${USAGE}    (specify --help-options for a list of options)\n"
USAGE="${USAGE}  where command is start, stop, status, etc.\n"
USAGE="${USAGE}    (specify --help-commands for a list of commands)\n"
USAGE="${USAGE}  where command-options is any command specific option\n"
USAGE="${USAGE}    (specify -H [command] for help on a particular command)\n"

HELP_OPTIONS="Options help\n"
HELP_OPTIONS="${HELP_OPTIONS}  -H\t\tHelp on a particular command\n"
HELP_OPTIONS="${HELP_OPTIONS}  -w\t\tFail on warnings\n"
HELP_OPTIONS="${HELP_OPTIONS}  -S hosts\tSystem(s) to regression test (default is [$REGRESS_SYSTEM])\n"
HELP_OPTIONS="${HELP_OPTIONS}  -T tests\tTest(s) to run (default is [$REGRESS_TEST])\n"
HELP_OPTIONS="${HELP_OPTIONS}  -d regdir\tWhere to perform testing (default is [$REGRESS_DIR])\n"
HELP_OPTIONS="${HELP_OPTIONS}  -l logfile\tName of log file for script output (default is [$LOGFILE])\n"
HELP_OPTIONS="${HELP_OPTIONS}  -t timeout\tMinutes to wait for nfs lock (default is [$WAIT_FOR_LOCK_TIMEOUT])\n"
HELP_OPTIONS="${HELP_OPTIONS}\n"
HELP_OPTIONS="${HELP_OPTIONS}Start command options\n"
HELP_OPTIONS="${HELP_OPTIONS}  -C\t\tClean out the regression directory before starting\n"
HELP_OPTIONS="${HELP_OPTIONS}  -s src\tWhere/How to find the source archive (default is [$SOURCE])\n"
HELP_OPTIONS="${HELP_OPTIONS}  -c cvsbin\tcvs command to run, including args (default is [$CVS])\n"
HELP_OPTIONS="${HELP_OPTIONS}  -r cvsroot\tLocation of cvs root (default is [$CVS_ROOT])\n"
HELP_OPTIONS="${HELP_OPTIONS}  -m module\tName of cvs module to export (default is [$CVS_TAG])\n"
HELP_OPTIONS="${HELP_OPTIONS}  -l\t\tInstead of copying the source archive, create a soft link\n"
HELP_OPTIONS="${HELP_OPTIONS}\n"

HELP_COMMANDS="Commands help\n"
HELP_COMMANDS="${HELP_COMMANDS}  start\t\tStart the regression testing\n"
HELP_COMMANDS="${HELP_COMMANDS}  stop\t\tStop the regression testing\n"
HELP_COMMANDS="${HELP_COMMANDS}  restart\t\tRestart the regression testing\n"
HELP_COMMANDS="${HELP_COMMANDS}  status\t\tGet status of the regression testing\n"
HELP_COMMANDS="${HELP_COMMANDS}  settings\t\tDisplay current setting values\n"
HELP_COMMANDS="${HELP_COMMANDS}\n"
HELP_COMMANDS="${HELP_COMMANDS}Specify -H [command] for more detailed help on a particular command\n"
HELP_COMMANDS="${HELP_COMMANDS}  (e.g. $0 -H start)\n"


HELP_start_label="Start command help"
HELP_start="The start command is what will invoke the regression suite testing.  After validating any regression and command options, the existance of the source archive is ensured on each of the regressing systems.  Then each of the regressing systems will commence testing the source by running the requested regression scripts from the ${TESTS_D} directory on each host."
#HELP_start="${HELP_start}\n\nAvailable Options:\n  -s src\tWhere/How to find the source archive (default is [$SOURCE])\n"

HELP_stop_label="Stop command help"
HELP_stop="This command will halt any specified systems that are presently in the process of running the regression suite.  Depending on the nature of what the regressing machine is doing at the time (and whether the machine is remote, or not), all test processes will be halted as soon as possible."

HELP_restart_label="Restart command help"
HELP_restart="This command will restart regression suite testing for any systems specified.  Restarting the suite consists of stopping any hosts that are presently running any tests, and then commencing the regression suite testing for each host regardless of whether they were running prior to the stop or not."

HELP_status_label="Status command help"
HELP_status="This command will return a regression suite status as to whether a certain host has not run the test, is presently running the test, has completed the test and failed, or has completed the test and passed.\n  Optionally, the results from the test may be displayed using terminal \"colorized\" ansi output, text output, and/or results may be sent out via e-mail."

HELP_settings_label="Settings command help"
HELP_settings="This command is useful in generating a default resource file (i.e. a default $RESOURCE_FILE file) that may be used for future regression test runs or for debugging."

COMMANDS="start stop restart status settings"


###
# HANDLE OPTIONS
################

#
# handle regression options
#
for i in $* ; do
    case "$i" in
        # handle help options.  a command name may be given as an argument
        # to any of the help options to get command-specific help
        -\? | -h | -H | -help | --help | --h | --H | help )
            helped="no"
            for i in $COMMANDS ; do
                hlp_msg_lbl="echo \"\$HELP_${i}_label\""
                hlp_msg="echo \"\$HELP_$i\""
                if [ "HELP_$i" = "HELP_$2" ] ; then
		    log "`eval $hlp_msg_lbl`"
                    plog "`eval $hlp_msg`\n" "$LOG" 80 "  "
                    helped="yes"
                    break
                fi
            done
            [ "x$helped" = "xno" ] && log "$USAGE"
            log "$INFO"
            exit;;
        --help-options | --help_options | -help-options )
            log "$HELP_OPTIONS" ; log "$INFO" ; exit;;
        --help-commands | --help_commands | -help-commands )
            log "$HELP_COMMANDS" ; log "$INFO" ; exit;;
        -v | --version | -version )
            log "BRL-CAD Regression Suite version $VERSION"
            if [ ! "x$BRLCAD_VERSION" = "x" ] ; then
                log "  (BRL-CAD v$BRLCAD_VERSION, script library version $LIBRARY_VERSION)\n"
            else
                log "  (script library version $LIBRARY_VERSION)\n"
            fi
            log "$INFO" ; exit;;
	-S | --system | --regress-system | --regress_system | -system | -regress-system )
	    REGRESS_SYSTEM=$2; shift 2;;
	-T | --test | --regress-test | --regress_test | -test | -regress-test )
	    REGRESS_TEST=$2; shift 2;;
	-d  | --destination | --regress-dir | --regress_dir | -destination | -regress-dir )
	    REGRESS_DIR=$2; shift 2;;
	-t | --timeout | --wait-for-lock-timeout | --wait_for_lock_timeout | -timeout | -wait-for-lock-timeout )
	    WAIT_FOR_LOCK_TIMEOUT=$2; shift 2;;
	-w | --no-warnings | --no_warnings | -no-warnings )
	    NO_WARNINGS=1; shift 1;;
	-l | --log | --logfile | -log | -logfile )
	    LOGFILE=$2; shift 2;;
        # once we reach a command, we are performing command options
        start | stop | restart | status | settings )
            COMMAND=$1; shift 1; break;;
        -C | -s | -c | -r | -m )
            log "Must specify command name before command option: $1"
            log "$USAGE"
            log "$INFO"
            exit ;;
        -* )
            log "Illegal option: $1"
            log "$HELP_OPTIONS"
            log "$INFO"
            exit ;;
    esac
done
VALID_CMD="no"
for cmd in $COMMANDS ; do
    if [ "x$cmd" = "x$COMMAND" ] ; then
        VALID_CMD="yes"
        break
    fi
done
if [ ! "x$VALID_CMD" = "xyes" ] ; then
    log "$USAGE"
    log "$INFO"
    exit
fi

#
# handle command options
#
for i in $* ; do
    if [ "x$COMMAND" = "xstart" ] || [ "x$COMMAND" = "restart" ] ; then
        case "$i" in 
            -s | --source | -source )
                SOURCE=$2; shift 2;;
            -c | --cvs | -cvs )
                CVS=$2; shift 2;;
            -r | --repository | -repository ) 
                CVS_ROOT=$2; shift 2;;
            -m | --cvs-tag | -cvs-tag )
                CVS_TAG=$2; shift 2;;
            -C | --clobber | -clobber )
                CLOBBER_FIRST=1; shift 1;;
            -l | --link | -link )
                LINK_OR_COPY="link"; shift 1;;
        esac
    elif [ "x$COMMAND" = "xstop" ] ; then
        case "$i" in 
        esac
    elif [ "x$COMMAND" = "xstatus" ] ; then
        case "$i" in 
        esac
    elif [ "x$COMMAND" = "xsettings" ] ; then
        case "$i" in
        esac
    fi
done


# export for debugging purposes
# no need to export since initVar does it for us
# export PACKAGE_NAME REGRESS_DIR CVS CVS_ROOT CVS_TAG WAIT_FOR_LOCK_TIMEOUT NO_WARNINGS CLOBBER_FIRST LOGFILE


###
# HANDLE SETTINGS COMMAND (simplest case)
#########################################
regressionSettings ( ) {
    if [ "x$1" = "x0" ] ; then comment=0 ; else comment=1 ; fi

    logfile=$2

    if [ "x$comment" = "x1" ] ; then
    log "#" $logfile
    log "# R E G R E S S I O N   S U I T E   R E S O U R C E   S E T T I N G S" $logfile
    log "#####################################################################" $logfile
    log "#" $logfile
    log "# The following are settings to be used in conjunction with the" $logfile
    log "# BRL-CAD regression suite.  This file may be used as either the" $logfile
    log "# start for a default .regressionrc file or as a template for a" $logfile
    log "# system file that would go in the $SYSTEMS_D directory." $logfile
    log "#" $logfile
    log "# As few or many of the options below may be modified to enforce" $logfile
    log "# behaviour different from the default settings." $logfile
    log "#" $logfile
    log "#####################################################################" $logfile
    log " " $logfile
    log "#" $logfile
    log "# NAME should be a valid domain name or ip address for the host that"  $logfile
    log "# corresponds with these settings." $logfile
    log "###" $logfile
    fi
    log "NAME=\"${HOSTNAME}\" ; export NAME" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "#" $logfile
    log "# PACKAGE_NAME is a name \"label\" of the source repository being" $logfile
    log "# tested." $logfile
    log "###" $logfile
    fi
    log "PACKAGE_NAME=\"${PACKAGE_NAME}\" ; export PACKAGE_NAME" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "#" $logfile
    log "# SOURCE is what or where the source for this build is.  By default" $logfile
    log "# the source is usually the entire contents of the directory named" $logfile
    log "# ../../PACKAGE_NAME given as either a tar.gz file or a tar.bz2 file." $logfile
    log "###" $logfile
    fi
    log "SOURCE=\"${SOURCE}\" ; export SOURCE" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "#" $logfile
    log "# REGRESS_DIR is the location of where the testing/writing will occur." $logfile
    log "# Write permission is only assumed in that directory." $logfile
    log "###" $logfile
    fi
    log "REGRESS_DIR=\"${REGRESS_DIR}\" ; export REGRESS_DIR" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "#" $logfile
    log "# WAIT_FOR_LOCK_TIMEOUT is the number of minutes for a client to wait " $logfile
    log "# until the regressing client either acquires a lock on the" $logfile
    log "# REGRESS_DIR directory or times out waiting.." $logfile
    log "###" $logfile
    fi
    log "WAIT_FOR_LOCK_TIMEOUT=\"${WAIT_FOR_LOCK_TIMEOUT}\" ; export WAIT_FOR_LOCK_TIMEOUT" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "#" $logfile
    log "# NO_WARNINGS puts the regressing client into a strict behaviour mode." $logfile
    log "# This has the effect of the scripts to terminate if they hit a warning" $logfile
    log "# condition, instead of just warning and continuing." $logfile
    log "###" $logfile
    fi
    log "NO_WARNINGS=\"${NO_WARNINGS}\" ; export NO_WARNINGS" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "#" $logfile
    log "# CLOBBER_FIRST is a yes/no value as to whether or not the" $logfile
    log "# the regressing client should empty out REGRESS_DIR" $logfile
    log "# first." $logfile
    log "###" $logfile
    fi
    log "CLOBBER_FIRST=\"${CLOBBER_FIRST}\" ; export CLOBBER_FIRST" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "#" $logfile
    log "# LOGFILE is simply the name of the log file to use for status output." $logfile
    log "# The output does not include the particular status of an individual" $logfile
    log "# test, athough completion status and other similar notices may output." $logfile
    log "###" $logfile
    fi
    log "LOGFILE=\"${LOGFILE}\" ; export LOGFILE" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "#" $logfile
    log "# REGRESS_SYSTEM is a list of other hosts that will perform regression" $logfile
    log "# testing.  If the systems listed have a file of the same name in the" $logfile
    log "# SYSTEMS_D directory, and they are not localhost, the settings there" $logfile
    log "# will override the default settings. "all" is a keyword value that" $logfile
    log "# indicates that all systems listed in the SYSTEMS_D dir should be run." $logfile
    log "###" $logfile
    fi
    log "REGRESS_SYSTEM=\"${REGRESS_SYSTEM}\" ; export REGRESS_SYSTEM" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "#" $logfile
    log "# REGRESS_TEST is a list of the tests that should get run by default" $logfile
    log "# when the regression suite runs.  By default, all tests will be run." $logfile
    log "# Modifying this value changes the default to be something other than" $logfile
    log "# all tests.  "all" is a keyword value that indicates that all tests" $logfile
    log "# listed in the TESTS_D directory should be run." $logfile
    log "###" $logfile
    fi
    log "REGRESS_TEST=\"${REGRESS_TEST}\" ; export REGRESS_TEST" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "###" $logfile
    log "# cvs-specific settings" $logfile
    log "###" $logfile
    log " " $logfile
    log "# " $logfile
    log "# CVS is the name/location of the cvs application.  Options to cvs" $logfile
    log "# may optionally be included, so long as they do not conflict with" $logfile
    log "# running cvs [whatever-options] -v" $logfile
    log "###" $logfile
    fi
    log "CVS=\"${CVS}\" ; export CVS" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "# " $logfile
    log "# CVS_ROOT is the location of the cvs repository (i.e. CVS_ROOT)" $logfile
    log "###" $logfile
    fi
    log "CVS_ROOT=\"${CVS_ROOT}\" ; export CVS_ROOT" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "# " $logfile
    log "# CVS_TAG is the name of the cvs package to checkout." $logfile
    log "###" $logfile
    fi
    log "CVS_TAG=\"${CVS_TAG}\" ; export CVS_TAG" $logfile
    if [ "x$comment" = "x1" ] ; then
    log " " $logfile
    log "# " $logfile
    log "# CVS_LOGFILE is the name of the cvs log file to write.  Set" $logfile
    log "# this value to empty to not log cvs exports." $logfile
    log "###" $logfile
    fi
    log "CVS_LOG=\"${CVS_LOG}\" ; export CVS_LOG" $logfile
}

if [ "x$COMMAND" = "xsettings" ] ; then
    regressionSettings
    exit
fi

# for any other command, we need to validate our settings

###
# VALIDATE COMMON SETTINGS
##########################

# check our regression directory settings for useability
# sanity check -- make sure someone didn't request ""
# not a good idea to use "." either, but we do not check
[ "x$REGRESS_DIR" = "x" ] && bomb "Must specify regression directory (use --regress-dir option)"

#
# make sure the SYSTEMS_D and TESTS_D directories exist
#
if [ ! -d "$SYSTEMS_D_WITH_PATH" ] ; then
    warn "Systems directory [$SYSTEMS_D_WITH_PATH] does not exist"
elif [ ! -r "$SYSTEMS_D_WITH_PATH" ] ; then
    warn "Systems directory [$SYSTEMS_D_WITH_PATH] is not readable"
fi
if [ ! -d "$TESTS_D_WITH_PATH" ] ; then
    warn "Tests directory [$TESTS_D_WITH_PATH] does not exist"
elif [ ! -r "$TESTS_D_WITH_PATH" ] ; then
    warn "Tests directory [$TESTS_D_WITH_PATH] is not readable"
fi

# 
# warn if a localhost systems file exists (it never gets used)
#
if [ -f "${SYSTEMS_D_WITH_PATH}/localhost" ] && [ ! "x$REGRESS_SYSTEM" = "xlocalhost" ] ; then
    warn "Found localhost file in $SYSTEMS_D directory (ignoring)"
fi

#
# if a specified system's name is the same as the localhost, 
# convert it to localhost to avoid packaging up the archive
# and ssh'ing to same host.  This option is off by default.
#
# if [ "x$REGRESS_SYSTEM" = "x$HOSTNAME" ] ; then
#     REGRESS_SYSTEM="localhost"
# fi

#
# expand "all" keyworded system and test lists
#
if [ "x$REGRESS_SYSTEM" = "xall" ] ; then
    systems="`ls -A $SYSTEMS_D_WITH_PATH`"
    REGRESS_SYSTEM=""
    for SYS in $systems ; do
        case $SYS in
            *~ | *.bak )
                # ignore backup files
                ;;
            * )
                # do not have to be executable
                [ -f "${SYSTEMS_D_WITH_PATH}/${SYS}" ] && REGRESS_SYSTEM="$REGRESS_SYSTEM $SYS"
        esac
    done
fi
if [ "x$REGRESS_TEST" = "xall" ] ; then
    tests="`ls -A $TESTS_D_WITH_PATH`"
    REGRESS_TEST=""
    for TST in $tests ; do
        case $SYS in
            *~ | *.bak )
                # ignore backup files
                ;;
            * )
                # have to be executable
                if [ -f "${TESTS_D_WITH_PATH}/${TST}" ] && [ -x "${TESTS_D_WITH_PATH}/${SYS}" ] ; then
                    REGRESS_TEST="$REGRESS_TEST $TST"
                fi
        esac
    done
fi

# 
# make sure there is something left to do
#
[ "x$REGRESS_SYSTEM" = "x" ] && bomb "No systems are specified\n  (use --system or set REGRESS_SYSTEM var)"
[ "x$REGRESS_TEST" = "x" ] && bomb "No tests are specified\n  (use --test or set REGRESS_TEST var)"


###
# VALIDATE COMMAND-SPECIFIC SETTINGS
####################################
#
# validate start command settings
#
if [ "x$COMMAND" = "xstart" ] ; then
    #
    #  Make sure the regression directory exists and is writeable
    #
    if [ ! -d "$REGRESS_DIR" ] ; then
        warn "Regression directory [$REGRESS_DIR] does not exist"
        log "Creating [$REGRESS_DIR]"
        mkdir -m 700 -p $REGRESS_DIR
        if [ ! -d "$REGRESS_DIR" ] ; then
            bomb "Unable to create [$REGRESS_DIR]"
        fi
    fi
    [ ! -w "${REGRESS_DIR}/." ] && bomb "Unable to write to [$REGRESS_DIR]"

    #
    # acquire the precious regression directory lock.  we will release at
    # the end of remote running or after localhost, whichever is later.
    #
    if ! eval "acquireLock regression $WAIT_FOR_LOCK_TIMEOUT 60 $REGRESS_DIR" ; then
        bomb "Unable to obtain regression.lock" $TLOG
    fi

    #
    # clean out the contents of the directory
    # delete everything except the current log file
    #
    if [ x$CLOBBER_FIRST = x1 ] ; then
        log "Cleaning out $REGRESS_DIR"
        for ENTRY in `ls -A $REGRESS_DIR` ; do
            if [ ! "x$ENTRY" = "x$LOGFILE" ] && [ ! "x$ENTRY" = "xregression.lock" ] ; then
                [ ! "x$DEBUG" = "x" ] && log "Deleting [${REGRESS_DIR}/${ENTRY}]"
                rm -rf "${REGRESS_DIR}/${ENTRY}"
                [ $? != 0 ] && bomb "Failure cleaning out regression directroy $REGRESS_DIR"
            fi
        done
    fi

    #
    # Verify that the source exists and is readable
    #
    if [ "x$SOURCE" = "x" ] ; then
        bomb "Must give a source archive (use --source option or set SOURCE var)"
    else
        #
        # figure out what kind of source we are dealing with
        #
        case $SOURCE in
            *.tar | *.zip | *.tar.Z | *.tar.gz | *.tgz | *.tar.bz2 )
                initializeVariable SRC_TYPE "archive" ;;
            cvs )
                initializeVariable SRC_TYPE "cvs" ;;
            # default is to assume we are using a source directory
            * )
                initializeVariable SRC_TYPE "dir" ;;
        esac
    fi
    if [ "x$SRC_TYPE" = "xcvs" ] ; then
        
        # save any other cvs export log files
        if [ -f "${REGRESS_DIR}/$CVS_LOGFILE" ] ; then
            warn "Moving previous cvs log file [${REGRESS_DIR}/$CVS_LOGFILE] to [${REGRESS_DIR}/${CVS_LOGFILE}.$$]"
            mv "${REGRESS_DIR}/$CVS_LOGFILE" "${REGRESS_DIR}/${CVS_LOGFILE}.$$"
            touch "${REGRESS_DIR}/$CVS_LOGFILE"
        fi
        
        # Perform the cvs export
        if ! eval "cvsExport \"$CVS_TAG\" \"$CVS_ROOT\" \"$REGRESS_DIR\" \"$CVS\" \"${REGRESS_DIR}/${CVS_LOGFILE}\"" ; then
            bomb "cvs export failed"
        fi

        # now we behave just like if we were given a directory
        SRC_TYPE="dir"
        SOURCE="$CVS_TAG"
    elif [ ! "x$SRC_TYPE" = "xdir" ] ; then
        unpackArchive "$SOURCE" "${REGRESS_DIR}/${PACKAGE_NAME}"
        SRC_TYPE="dir"
        SOURCE="$CVS_TAG"
    fi

    # everything should be 
    if [ "x$SRC_TYPE" = "xdir" ] ; then
        if [ ! -d "$SOURCE" ] ; then
            if [ ! -d "${LPWD}/${SOURCE}" ] ; then
                bomb "Source directory [$SOURCE] does not exist"
            else
                SOURCE="${LPWD}/${SOURCE}"
            fi
        fi
        [ ! -r "$SOURCE" ] && bomb "Unable to read source directory [$SOURCE]"

        # link or copy the source into the regression directory if we didn't unarchive there.
        # this is only significant for sources that are not archived initially since we unpack
        # into the regression directory
        if [ ! "x$SOURCE" = "x${REGRESS_DIR}/${PACKAGE_NAME}" ] ; then
            if [ "x$LINK_OR_COPY" = "xlink" ] ; then
                ln -s "${SOURCE}" "${REGRESS_DIR}/${PACKAGE_NAME}"
            else
                cp -R "${SOURCE}" "${REGRESS_DIR}/${PACKAGE_NAME}"
            fi
            [ $? != 0 ] && bomb "Source preparation failed (for ${REGRESS_DIR}/${PACKAGE_NAME})"
        fi
    else
        bomb "Should never reach this line (verifying that the source exists and is readable)"
    fi

#
# validate stop command settings
#
elif [ "x$COMMAND" = "xstop" ] ; then
    log "stop command unimplemented"
#
# validate restart command settings
#
elif [ "x$COMMAND" = "xrestart" ] ; then
    log "restart command unimplemented"
#
# validate status command settings
#
elif [ "x$COMMAND" = "xstatus" ] ; then
    log "status command unimplemented"
else
    bomb "Should never reach this line (we lost a valid command when validating command settings)"
fi


###
# HANDLE REMOTE ACTIONS
#######################
#
# for non-localhost run, we log to the master logfile.  instead we log
# to a file in the .regress dir per convention (we can override with param)
if [ ! "x$REGRESS_SYSTEM" = "xlocalhost" ] ; then
    # save variable values that may get overridden with a systems file
    if [ ! "x$NAME" = "x" ] ; then __NAME="$NAME" ; fi
    if [ ! "x$USER" = "x" ] ; then __USER="$USER" ; fi
    if [ ! "x$TRANSPORT" = "x" ] ; then __TRANSPORT="$TRANSPORT" ; fi
    if [ ! "x$REGRESS_DIR" = "x" ] ; then __REGRESS_DIR="$REGRESS_DIR" ; fi

    #
    # iterate over the systems list
    #
    alreadyBuilt="no"
    for sys in $REGRESS_SYSTEM ; do
        
        #
        # pull any override values from a systems file if one exists
        #
        unset NAME
        unset USER
        unset TRANSPORT
        unset REGRESS_DIR
        [ -r "${SYSTEMS_D_WITH_PATH}/${sys}" ] && loadResourceFile "${SYSTEMS_D_WITH_PATH}/$sys"
        initializeVariable NAME "$sys"
        initializeVariable USER "$__USER"
        initializeVariable TRANSPORT "$__TRANSPORT"
        initializeVariable REGRESS_DIR "$__REGRESS_DIR"

        if [ "x$NAME" = "xlocalhost" ] ; then
            FOUND_LOCALHOST=1
            continue
        fi

        #
        # validate our systems list (that hosts are alive)
        #
        if ! hostIsAlive $NAME ; then
            warn "Hostname $NAME did not respond, trying $sys"
        elif hostIsAlive $sys ; then
            warn "Hostname $sys is responding, using $sys instead of $NAME"
            NAME=$sys
        else
            warn "host $sys is also not responding, skipping"
            continue
        fi


        # 
        # handle the start command special
        ###
        if [ "x$COMMAND" = "xstart" ] ; then
            #
            # get the regression scripts we are running to the remote host
            #
            if [ "x$TRANSPORT" = "xnfs" ] ; then
                bomb "unimplemented"
            else
                ssh ${USER}@${HOST} "mkdir \"${REGRESS_DIR}/.regress-${NAME}\""
                ssh ${USER}@${HOST} "ln -s \"${REGRESS_DIR}/${PACKAGE_NAME}\" \"${REGRESS_DIR}/.regress-${NAME}/${PACKAGE_NAME}\""
                ssh ${USER}@${HOST} "mkdir \"${REGRESS_DIR}/.regress-${NAME}/${PACKAGE_NAME}/.regress.`uname -s`\""
                scp -R ./* "${USER}@${HOST}:${REGRESS_DIR}/.regress-${NAME}/${PACKAGE_NAME}/.regress.`uname -s`"
            fi

            #
            # if the transport is nfs, we can invoke as a directory building in place
            # otherwise we need to package up the archive and send it as a tar.gz via ssh
            #
            if [ "x$TRANSPORT" = "xnfs" ] ; then
                bomb "unimplemented"
            else
                if [ "x$alreadyBuilt" = "xno" ] ; then
                    # archiveDirectory ${REGRESS_DIR}/${PACKAGE_NAME} ${REGRESS_DIR}/${PACKAGE_NAME}.tar.gz
#                    tar cvf "${REGRESS_DIR}/${PACKAGE_NAME}.tar.gz" "${REGRESS_DIR}/${PACKAGE_NAME}"
                    scp -R "${REGRESS_DIR}/${PACKAGE_NAME}" "${USER}@${HOST}:${REGRESS_DIR}/.regress-${NAME}/${PACKAGE_NAME}"
                    alreadyBuilt="yes"
                fi
            fi

        #
        # handle stop|restart|settings command
        ###
        else
            bomb "spawn regression test on remote host"
        fi

        bomb !!!
    done

    # restore the previous variable values
    if [ ! "x${__NAME}" = "x" ] ; then NAME="${__NAME}" ; unset __NAME ; fi
    if [ ! "x${__USER}" = "x" ] ; then USER="${__USER}" ; unset __USER ; fi
    if [ ! "x${__TRANSPORT}" = "x" ] ; then TRANSPORT="${__TRANSPORT}" ; unset __TRANSPORT ; fi
    if [ ! "x${__REGRESS_DIR}" = "x" ] ; then REGRESS_DIR="${__REGRESS_DIR}" ; unset __REGRESS_DIR ; fi
fi



###
# HANDLE LOCALHOST ACTIONS
##########################
#
# localhost behaviour is, in a sense, a special case as it does not involve
# any of the management intrinsic to a remote run.  It basically means 
# perform the requested action now.
if [ "x$REGRESS_SYSTEM" = "xlocalhost" ] || [ "x$FOUND_LOCALHOST" = "x1" ] ; then

    # move the tests.d contents to the regression dir
    [ ! "x${TESTS_D_WITH_PATH}" = "x${REGRESS_DIR}/$TESTS_D" ] && cp -R "${TESTS_D_WITH_PATH}" "${REGRESS_DIR}/$TESTS_D"
    # validate that the tests are in place
    [ ! -r "${REGRESS_DIR}/${TESTS_D}/library" ] && bomb "Unable to validate that tests are in place (no library file)"

    # save our previous working directory for the sake of the scripts that may misbehave or leave junk
    __PPWD="`pwd`"
    cd "${REGRESS_DIR}"
    for tst in $REGRESS_TEST ; do
        log "Running $tst $COMMAND"
        log `${REGRESS_DIR}/${TESTS_D}/${tst} $COMMAND` "${REGRESS_DIR}/${tst}.log"
    done
    # restore the previous working directory 
    cd "$__PPWD"

# end localhost run
fi


###
# COMMAND CLEAN-UP
##################
#
# handle start
#
if [ "x$COMMAND" = "xstart" ] ; then
    #
    # release that precious lock that we acquired earlier
    #
    releaseLock regression $REGRESS_DIR
else
    bomb "other commands unimplemented"
fi



log "OK Done $0"
exit 0


bomb "previous remote implementation of localhost running
    #
    # creation of the .regress dir kind of insinuates that the tests.d was copied, make sure
    #
    if [ ! -d "${REGRESS_DIR}/.regress-${HOSTNAME}/${TESTS_D}" ] ; then

        #
        # copy the regression scripts into a runnable location in the regression directory
        #
        if [ ! -d "${REGRESS_DIR}/.regress-$HOSTNAME" ] ; then
            mkdir "${REGRESS_DIR}/.regress-$HOSTNAME"
            if [ $? != 0 ] ; then bomb "Unable to create directory [$REGRESS_DIR/.regress-$HOSTNAME]" ; fi
        fi
        if [ ! -w "${REGRESS_DIR}/.regress-$HOSTNAME" ] ; then
            bomb "Unable to write to directory [$REGRESS_DIR/.regress-$HOSTNAME]"
        fi
        if [ ! -d "${REGRESS_DIR}/.regress-${HOSTNAME}/$TESTS_D" ] ; then
            mkdir "${REGRESS_DIR}/.regress-${HOSTNAME}/$TEST_D"
            if [ $? != 0 ] ; then bomb "Unable to create directory [$REGRESS_DIR/.regress-$HOSTNAME/$TESTS_D]" ; fi
        fi
        if [ ! -w "${REGRESS_DIR}/.regress-${HOSTNAME}/$TESTS_D" ] ; then
            bomb "Unable to write to directory [$REGRESS_DIR/.regress-$HOSTNAME/$TESTS_D]"
        fi
        
        ln -s "${REGRESS_DIR}/${PACKAGE_NAME}" "${REGRESS_DIR}/.regress-${HOSTNAME}/$PACKAGE_NAME"
        # move the tests.d contents to here 
        if [ ! "x${TESTS_D_WITH_PATH}" = "x${REGRESS_DIR}/.regress-${HOSTNAME}/$TESTS_D" ] ; then
            cp -R "${TESTS_D_WITH_PATH}" "${REGRESS_DIR}/.regress-${HOSTNAME}/$TESTS_D"
        fi

    # we already did the prepwork earlier
    else

        # save our previous working directory for the sake of the scripts that may misbehave or leave junk
        __PPWD="`pwd`"
        cd "${REGRESS_DIR}/.regress-$HOSTNAME"
        for tst in $REGRESS_TEST ; do
            "./${TESTS_D}/$tst" $COMMAND > "${tst}.log" 2>&1 &
            log "Running $tst $COMMAND"
        done
        # restore the previous working directory 
        cd "$__PPWD"
    fi
