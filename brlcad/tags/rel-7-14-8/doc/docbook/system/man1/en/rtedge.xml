<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2009 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='rtedge1'>
  <refmeta>
    <refentrytitle>RTEDGE</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>rtedge</refname>
    <refpurpose>ray-traces a model, detects edges, and writes a BRL-CAD color image (.pix) file</refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>rtedge</command>    
      <arg choice='opt' rep='repeat'><replaceable>options</replaceable></arg>
      <arg choice='plain'><replaceable>model.g</replaceable></arg>
      <arg choice='plain' rep='repeat'><replaceable>objects</replaceable></arg>
      <arg choice='plain'><replaceable>&lt; model.pix</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <emphasis remap='I'>Rtedge</emphasis> operates on the indicated
      <emphasis remap='I'>objects</emphasis> in the input
      <emphasis remap='I'>model.g</emphasis> and produces a BRL-CAD 
      .pix file that indicates the 'edges' in the model file.
      <emphasis remap='I'>Rtedge</emphasis> produces images by drawing a
      boundary whenever a change in region_id is detected.  It also recognizes
      and portrays abrupt changes in surface curvature and changes in surface
      height.  This permits the recognition of pits, protrusions, and changes in
      surface curvature.
    </para>

    <para>
      The orientation of the rays to be fired may be specified by
      the <option>-a</option> and <option>-e</option>
      options, in which case the model will be autosized, and the grid
      will be centered on the centroid of the model, with ray spacing
      chosen to span the entire set of <emphasis remap='I'>objects.</emphasis>
      Alternatively, with the <option>-M</option> option, a transformation 
      matrix may be provided on standard input which maps model-space to view-space.
      In this case, the grid ranges from -1.0 &lt; = X,Y &lt; = +1.0 in view space,
      with the size of the grid (number of rays fired) specified with the
      <option>-s</option> option. This option is most useful when
      <command>rtedge</command> is being invoked from a shell script created by an
      <citerefentry><refentrytitle>mged</refentrytitle><manvolnum>1</manvolnum></citerefentry> <emphasis remap='I'>saveview</emphasis> command.
    </para>
    
    <para>
      The following options are recognized.
    </para>
    <variablelist remap='TP'>
      <varlistentry>
	<term><option>-s#</option></term>
	<listitem>
	  <para>
	    Number of rays to fire in X and Y directions (square grid).
	    Default is 512 (512 x 512).
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-F framebuffer</option></term>
	<listitem>
	  <para>
	    Indicates that the output should be sent to the
	    indicated framebuffer. See
	    <citerefentry><refentrytitle>libfb</refentrytitle><manvolnum>3</manvolnum></citerefentry> for more details on the specification of a framebuffer.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-a#</option></term>
	<listitem>
	  <para>
	    Select azimuth in degrees.  Used with <option>-e</option> option
	    and conflicts with <option>-M</option> option 
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-A#</option></term>
	<listitem>
	  <para>
	    Select angle for shading.  Default is 5.0 degrees; 89.0 will produce
	    an image where only steep drops and rises are shaded.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-e#</option></term>
	<listitem>
	  <para>
	    Select elevation in degrees.  Used with <option>-a</option> option
	    and conflicts with <option>-M</option> option. 
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-M</option></term>
	<listitem>
	  <para>
	    Read model2view matrix from standard input.  Conflicts with
	    <option>-a</option> and <option>-e</option> options 
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-g#</option></term>
	<listitem>
	  <para>Select grid cell width.</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-G#</option></term>
	<listitem>
	  <para>
	    Select grid cell height.  If <emphasis remap='I'>not</emphasis> 
	    specified, cell height equals cell width.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-P#</option></term>
	<listitem>
	  <para>
	    Specify the maximum number of processors (in a multi-processor
	    system) to be used for this execution.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-U#</option></term>
	<listitem>
	  <para>
	    sets the Boolean variable <emphasis remap='I'>use_air</emphasis>
	    to the given value.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-o output.pix</option></term>
	<listitem>
	  <para>
	    specifies a named file for output. Note that using -o disables 
	    parallel processing.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-W</option></term>
	<listitem>
	  <para>
	    Inverts the foreground and background colors changing the default from
	    white lines on a black background to black lines on a white
	    background.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-x#</option></term>
	<listitem>
	  <para>Set librt debug flags to (hexadecimal) number.</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c</option></term>
	<listitem>
	  <para>
	    Set special <command>rtedge</command> configuration variables.
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
    
    <para>
      The <command>rtedge</command> program is a simple front-end to
    <citerefentry><refentrytitle>librt</refentrytitle><manvolnum>3</manvolnum></citerefentry>.
    </para>
  </refsect1>
  
  <refsect1 id='additional_options'>
    <title>ADDITIONAL OPTIONS</title>
    <para>
      <emphasis remap='I'>Rtedge</emphasis> replaces <citerefentry><refentrytitle>lgt</refentrytitle><manvolnum>1</manvolnum></citerefentry>.
      In addition to the simple white edges on a black background (or black on white) 
      provided by <emphasis remap='I'>lgt,</emphasis> <command>rtedge</command>
      allows arbitrary color combinations. It also allows rendering into an 
      existing framebuffer and occlusion detection between two pieces of geometry.
      To configure its behavior, <command>rtedge</command> makes copious use of 
      the <option>-c</option> option.
    </para>
    
    <para>
      Note that this approach is necessitated by the fact that 
      <emphasis remap='I'>librt (3)</emphasis> has used up nearly the entire 
      alphabet. A transition to GNU-style long option names is planned.
    </para>
    <variablelist remap='TP'>
      <varlistentry>
	<term><option>-c"set foreground=#,#,#</option></term>
	<listitem>
	  <para>and</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set fg=#,#,#</option></term>
	<listitem>
	  <para>
	    set the color of the foreground (edges) to the
	    given  r,g,b triple. The default is white
	    (255,255,255).
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set background=#,#,#</option></term>
	<listitem>
	  <para>and</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set bg=#,#,#</option></term>
	<listitem>
	  <para>
	    set the color of the background to the r,g,b
	    triple. The default is near-black (0,0,1).
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set region_colors=#</option></term>
	<listitem>
	  <para>and</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set rc=#</option></term>
	<listitem>
	  <para>
	    determine whether to use the material color
	    assigned to an object as the edge color. Valid
	    values are 1 (on) and 0 (off). The default is
	    off. Using region colors overides any
	    foreground color setting.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set detect_regions=#</option></term>
	<listitem>
	  <para>and</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set dr=#</option></term>
	<listitem>
	  <para>
	    determine whether the boundary between two BRL-CAD regions
	    is considered to be an edge. Valid values are 1 (on) and
	    0 (off). The default is off.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set detect_distance=#</option></term>
	<listitem>
	  <para>and</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set dd=#</option></term>
	<listitem>
	  <para>
	    determine whether a difference in hit distance between
	    adjacent pixels defines an edge. Valid values are 1 (on)
	    and 0 (off). The default is on.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set detect_normals</option></term>
	<listitem>
	  <para>and</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set dn=#</option></term>
	<listitem>
	  <para>
	    determine whether a difference in surface normals
	    between adjacent pixels defines an edge. Valid values
	    are 1 (on) and 0 (off). The default is on.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set detect_ids</option></term>
	<listitem>
	  <para>and</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set di=#</option></term>
	<listitem>
	  <para>
	    determine whether a difference in component id numbers
	    between adjacent pixels defines an edge. Valid values
	    are 1 (on) and 0 (off). The default is on.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set overlaymode=#</option></term>
	<listitem>
	  <para>and</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set ov=#</option></term>
	<listitem>
	  <para>
	    configure the overlay mode to be either on (1) or off (0).
	    Overlay mode allows <command>rtedge</command>
	    to write into an existing framebuffer. The framebuffer is
	    specified with the -F option.
	  </para>
	</listitem>
	<listitem>
	  <para>and</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set max_dist=#</option></term>
	<listitem>
	  <para>
	    over-ride the maximum hit distance of neighboring pixels before being 
	    declared an edge. The default value is computed based on the size of 
	    the output image, approximately 1000 for the default image/fb size.
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
  
  <refsect1 id='occlusion_checking'>
    <title>OCCLUSION CHECKING</title>
    <para>
      <emphasis remap='I'>Rtedge</emphasis> has the ability to perform occlusion 
      checking between two sets of BRL-CAD objects from the same database. 
      Occlusion checking determines which set of geometry is closer
      to the eyepoint and thus should be rendered. The first
      set is that specified in the basic object list (after the
      database). This set will be rendered using edge detection.
      The second is specified using a <option>-c</option> option.
    </para>
    <para>
      Occlusion checking is most handy when combining <command>rtedge</command>
      renderings with <emphasis remap='I'>rt</emphasis> renderings. The syntax 
      for such compositions is daunting, but the results are cool.
    </para>
    <variablelist remap='TP'>
      <varlistentry>
	<term><option>-c"set occlusion_objects=\"obj1 obj2 ... objN\ </option></term>
	<listitem>
	  <para>and</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set oo=\"obj1 obj2 ... objN\ </option></term>
	<listitem>
	  <para>specify the second set of geometry.</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set occlusion_mode=#</option></term>
	<listitem>
	  <para>and</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c"set om=#</option></term>
	<listitem>
	  <para>
	    determine how <command>rtedge</command> behaves when 
	    performing occlusion checking. There are three
	    valid modes. Mode 2 is the default.
	  </para>
	  <para>
	    <emphasis remap='B' role='B'>mode 1 -</emphasis>
	    An edge detected in the first set of geometry is written to
	    the framebuffer if and only if it occludes the second set of
	    geometry. The edge is colored according to the foreground or
	    region colors options.
	  </para>
	  <para>
	    <emphasis remap='B' role='B'>mode 2 -</emphasis>
	    All pixels that hit the first set of geometry that are not
	    occluded by the second set are written to the framebuffer.
	    Edges are rendered in the foreground or region color; non-edges
	    are rendered in the background color.
	  </para>
	  <para>
	    <emphasis remap='B' role='B'>mode 3 -</emphasis>
	    Like mode 2 except non-edge pixels are dithered to make the
	    geometry semi-transparent.
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
  
  <refsect1 id='examples'>
    <title>EXAMPLES</title>
    <para>
      <emphasis remap='I'>Rtedge</emphasis> can be somewhat complicated 
      to configure. These examples illustrate both simple and complex examples. 
      Hopefully they will be enough to get you started. These examples use
      the havoc.g target description that may be found in the
      source distribution.
    </para>

    <para>
      Note that in practice it will usually be easiest to use the
      <emphasis remap='I'>saveview</emphasis> command in
      <emphasis remap='I'>mged</emphasis>
      to produce a script and then modify that script to run
      <emphasis remap='I'>rtedge.</emphasis>
    </para>

    <example>
      <title>Rtedge rendering of havoc.g</title>
      <para>
      <userinput>rtedge -s 1024 -Fnew.pix havoc.g havoc</userinput>
      </para>
      <para>
	results in a 1024 by 1024 BRL-CAD image file showing the
	edges on a Havoc helicopter. The default settings are used,
	so the image has white edges on a black background. Edges
	exist where there are differences in component id, hit
	distance, surface normal, or between hits and misses.
      </para>
      
      <para>
	The command	
	<userinput>rtedge -s1024 -Fnew.pix -c"set fg=0,255,0 bg=255,0,0" havoc.g havoc</userinput> 
	changes the edge color to be green and the background color to be red.
      </para>

      <para>
	The command
	<userinput>rtedge -s1024 -Fnew.pix -c"set rc=1 dr=1" havoc.g havoc</userinput>
	activates region detection and uses the region colors for the edges.
      </para>

      <para>
	The remaining examples will illustrate how to use <command>rtedge</command>
	in concert with <emphasis remap='I'>rt</emphasis> to produce interesting 
	images. When merging <command>rtedge</command> and 
	<emphasis remap='I'>rt</emphasis> images, it is best to use
	<emphasis remap='I'>saveview</emphasis> scripts. In 
	<emphasis remap='I'>mged</emphasis>, configure your view, save it, 
	and then open the file in an editor. Dupilcate the
	<emphasis remap='I'>rt</emphasis> command block. After duplicating 
	the block, change the second to <emphasis remap='I'>rtedge.</emphasis></para>

	<para>
	  This script will overlay bright orange edges on the Havoc.
	</para>

	<literallayout remap='.nf'>
#!/bin/sh
rt -M -s1280 -Fnew.pix -C255/255/255\
 $*\
 havoc.g\
 havoc\
 2&gt;&gt; example1.log\
 &lt;&lt;EOF
viewsize 8.000e+03;
orientation 2.4809e-01 4.7650e-01 7.4809e-01 3.8943e-01;
eye_pt 2.2146e+04 7.1103e+03 7.1913e+03;
start 0; clean;
end;

EOF

rtedge -M -s1280 -Fnew.pix \
 -c"set dr=1 dn=1 ov=1"\
 -c"set fg=255,200,0" \
 $*\
 havoc.g\
 havoc\
 2&gt;&gt; example1.log\

 &lt;&lt; EOF
viewsize 8.000e+03;
orientation 2.4809e-01 4.7650e-01 7.4809e-01 3.8943e-01;
eye_pt 2.2146e+04 7.1103e+03 7.1913e+03;
start 0; clean;
end;

EOF
	</literallayout> 
	
	<para>
	  Finally, this script will render the Havoc weapon systems in
	  full color, render the edges on the remainder of the aircraft
	  in black, and render the non-edges in dithered gray to make
	  them semi-transparent. Note that when specifying the occlusion
	  objects, the delimiting quotes must be escaped.
	</para>

	<literallayout remap='.nf'>
#!/bin/sh

rt -M -s1280 -Fnew.pix -C255/255/255 \
 $*\
 havoc.g\
 weapons\
 2&gt;&gt; example2.log\ 
 &lt;&lt; EOF
viewsize 8.000e+03;
orientation 2.4809e-01 4.7650e-01 7.4809e-01 3.8943e-01;
eye_pt 2.2146e+04 7.1103e+03 7.1913e+03;
start 0; clean;
end;

EOF

rtedge -M -s1280 -Fnew.pix \
 -c"set dr=1 dn=1 om=3"\
 -c"set fg=0,0,0 bg=200,200,200"\
 -c"set oo=\"weapons\" "\
 $*\
 havoc.g\
 havoc_front havoc_middle havoc_tail landing_gear main_rotor\
 2&gt;&gt; example2.log\  

 &lt;&lt; EOF
viewsize 8.000e+03;
orientation 2.4809e-01 4.7650e-01 7.4809e-01 3.8943e-01;
eye_pt 2.2146e+04 7.1103e+03 7.1913e+03;
start 0; clean;
end;

EOF
	</literallayout> 
	
	<para>
	  In general, if you are mixing <emphasis remap='I'>rt</emphasis>
	  and <command>rtedge</command> renderings, and the objects being 
	  rendered are different, occlusion checking should be used.
	</para>
    </example>
  </refsect1>
  
  <refsect1 id='see_also'>
    <title>SEE ALSO</title>
    <para><citerefentry><refentrytitle>mged</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>rt</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>fbserv</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>librt</refentrytitle><manvolnum>3</manvolnum></citerefentry></para>
  </refsect1>

  <refsect1 id='diagnostics'>
    <title>DIAGNOSTICS</title>
    <para>
      Numerous error conditions are possible, usually due to errors in
      the geometry database. Descriptive messages are printed on stderr 
      (file descriptor 2).
    </para>
  </refsect1>

  <refsect1 id='author'>
    <title>AUTHORS</title>
    <para>Ronald Anthony Bowers, Michael John Muuss</para>
  </refsect1>
  
  <refsect1 id='copyright'>
    <title>COPYRIGHT</title>
    <para>This software is Copyright (c) 2001-2009 United States Government as
    represented by the U.S. Army Research Laboratory. All rights reserved.</para>
  </refsect1>
  
  <refsect1 id='bugs'>
    <title>BUGS</title>
    <para>Most deficiencies observed while using the <command>rtedge</command>
    program are a consequence of problems in <citerefentry><refentrytitle>librt</refentrytitle><manvolnum>3</manvolnum></citerefentry>.</para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;.</para>
  </refsect1>
</refentry>

