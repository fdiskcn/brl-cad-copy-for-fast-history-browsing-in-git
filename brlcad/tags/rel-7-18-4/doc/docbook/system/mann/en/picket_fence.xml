<!-- Converted by db4-upgrade version 1.0 -->

<refentry xmlns="http://docbook.org/ns/docbook" version="5.0" xml:id="picket_fence1">

<refmeta>
  <refentrytitle>PICKET_FENCE</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class="source">BRL-CAD</refmiscinfo>
  <refmiscinfo class="manual">BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv xml:id="name">
  <refname>picket_fence</refname>
  <refpurpose>
    Creates a BRL-CAD .g file containing picket fences.
  </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv xml:id="synopsis">
  <cmdsynopsis sepchar=" ">
    <command>picket_fence</command>
    <arg choice="opt" rep="norepeat"><replaceable>filename</replaceable></arg> 
    <arg choice="opt" rep="norepeat"><replaceable>prefix</replaceable></arg> 
    <arg choice="opt" rep="norepeat"><replaceable>height</replaceable></arg> 
    <arg choice="opt" rep="norepeat"><replaceable>spacing</replaceable></arg> 
    <arg choice="opt" rep="norepeat"><replaceable>x0</replaceable></arg> 
    <arg choice="opt" rep="norepeat"><replaceable>y0</replaceable></arg> 
    <arg choice="opt" rep="norepeat"><replaceable>z0</replaceable></arg> 
    <arg rep="repeat" choice="opt"/> 
    <arg choice="opt" rep="norepeat"><replaceable>xn</replaceable></arg> 
    <arg choice="opt" rep="norepeat"><replaceable>yn</replaceable></arg> 
    <arg choice="opt" rep="norepeat"><replaceable>zn</replaceable></arg> 
    <arg choice="opt" rep="norepeat"><replaceable>-r</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsection xml:id="description"><info><title>DESCRIPTION</title></info>
  
  <para>
    <command>picket_fence</command> creates a geometry file in
    <emphasis>filename</emphasis> of a picket fence.  All  parameters
    for the fence are in mm.  If more than two points are specified 
    it will generate a fence with multiple sections connecting the 
    points.  Parts of the fence are created starting at the origin.  The
    two backing boards are created in the negative x halfspace.  The pickets are
    created in the positive x halfspace and translated down the y-axis to their
    proper positions.  The entire segment is then rotated and translated to the
    position specified by the user.  To create a box of fence with the
    pickets on the outside, the points must be specified in a counter-clockwise
    order.  Parts of the model are named according to the format
    <literallayout class="normal">
&lt;prefix&gt;&lt;partname&gt;&lt;sec_name&gt;&lt;[&gt;part_num&lt;].&lt;obj_type&gt;
    </literallayout>
    The <emphasis>&lt;prefix&gt;</emphasis> above is the second argument to the 
    program.  The <emphasis>spacing</emphasis> parameter specifies the amound of
    space (in mm) between pickets.  The <emphasis>-r</emphasis> option specifies round
    fronts for the pickets.
 </para>
</refsection>

<refsection xml:id="examples"><info><title>EXAMPLES</title></info>
  
  <para>
  Creates an example fence two meters high and six meters long with
  fifteen millimeter spaces between the pickets.  It will start the
  fence at the origin and extend it to (100,6000,0).
  </para>
  <example><info><title><command>picket_fence</command> Example</title></info>
    
    <para>
      <userinput>picket_fence fence.g Imy2000 15 0 0 0 100 6000 0</userinput>
    </para>
  </example>

</refsection>

<info><corpauthor>BRL-CAD Team</corpauthor></info>

<refsection xml:id="copyright"><info><title>COPYRIGHT</title></info>
  
  <para>
    This software is Copyright (c) 2005-2011 United States Government as
    represented by the U.S. Army Research Laboratory. All rights reserved.
  </para>
</refsection>

<refsection xml:id="bug_reports"><info><title>BUG REPORTS</title></info>
  
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsection>
</refentry>
