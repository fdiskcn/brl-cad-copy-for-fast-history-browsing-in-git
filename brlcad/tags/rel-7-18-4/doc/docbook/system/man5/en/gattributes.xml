<!-- Converted by db4-upgrade version 1.0 -->

<refentry xmlns="http://docbook.org/ns/docbook" version="5.0" xml:id="gattributes">

<refmeta>
  <refentrytitle>Standard Attributes</refentrytitle>
  <manvolnum>5</manvolnum>
  <refmiscinfo class="source">BRL-CAD</refmiscinfo>
  <refmiscinfo class="manual">BRL-CAD Standard Conventions</refmiscinfo>
</refmeta>

<refnamediv xml:id="name">
  <refname>gattributes</refname>
  <refpurpose>
	standard attribute conventions in BRL-CAD databases
  </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsection xml:id="gift_background"><info><title>BACKGROUND</title></info>
  
  <para>
	Starting with BRL-CAD database format version 5, BRL-CAD supports
	the storage of arbitrary attribute/value data on any database object.
	Prior to the development of this generic ability, BRL-CAD supported a
	set of standard attributes used to hold key information about specific
	properties. These conventions predate not only the version 5
	database format but BRL-CAD itself.  The purpose of this man page
	is to identify the standard attributes, document situations where
	two or more attributes are used to identify the same property,
	and identify where appropriate what constitutes meaningful values
	for an attribute.
  </para>

  <para>
	The earliest ancestor of BRL-CAD was the MAGIC code, written by
	the Mathematical Applications Group, Inc in 1967 to implement geometric
	modeling ideas developed by BRL and AMSAA from 1958-1967.  That system,
	which was entirely non-graphical, was followed some years later by the GIFT program -
	Geometric Information From Targets.  LIBRT, appearing around 1983, became the successor to GIFT.
	Today it is the core library of the BRL-CAD solid modeling package.  (For more background
	see http://ftp.arl.army.mil/~mike/papers/96thebook/).  LIBRT inherited
	both geometric primitives and attributes from GIFT - this heritage
	is occasionally apparent in attribute names.
  </para>
  <para>
	Note that with the introduction of support for arbitrary attribute/value pairs,
	it becomes much simpler for both developers and users to formulate new standards of their
	own for storage of metadata in attributes.  Attributes should only
	be documented in this file if one or more core BRL-CAD tools has implemented
	one or more features that require specific attributes to be present.  Conventions
	for attribute names that do not involve tool support should be documented
	in appropriate user-level documentation.  In documenting attributes, one
	additional distinction is made.  Attributes that exist only as post-v5 attribute/value 
	pairs are referred to as non-core attributes; older
	attributes (which may also be represented in core data structures) are referred
	to as core attributes.
  </para>
</refsection>

<refsection xml:id="core_attributes"><info><title>CORE ATTRIBUTES</title></info>
  
  <para>
	The following table lists all known core BRL-CAD attributes and
	aliases.  Any code setting or reading the value of one of these attributes
	must handle all aliases, to ensure all functions asking for
	the value in question get a consistent answer.
  </para>

  <para>
	<table><info><title>Core Attributes</title></info>
	   
	   <tgroup cols="7">
	     <colspec colname="c1"/>
	     <colspec colname="c2"/>
	     <colspec colname="c3"/>
	     <colspec colname="c4"/>
	     <colspec colname="c5"/>
	     <colspec colname="c6"/>
	     <colspec colname="c7"/>
	     <tbody>
	       <row>
		 <entry>Property</entry>
	         <entry namest="c2" nameend="c6" align="center">Attribute Name(s)</entry>
	         <entry>Typical Value(s)</entry>
	       </row>
	       <row>
		 <entry>Region Flag</entry>
	         <entry>region</entry>
	         <entry>REGION</entry>
	         <entry/>
	         <entry/>
	         <entry/>
		 <entry>Yes, R or 1</entry>
               </row>
      	       <row>
		 <entry>Region Identifier Number</entry>
	         <entry>region_id</entry>
	         <entry>REGION_ID</entry>
	         <entry>id</entry>
	         <entry>ID</entry>
	         <entry/>
		 <entry>-1, 0, and positive integers</entry>
               </row>
               <row>
		 <entry>Material Identifier Number</entry>
	         <entry>material_id</entry>
	         <entry>MATERIAL_ID</entry>
	         <entry>GIFTmater</entry>
	         <entry>GIFT_MATERIAL</entry>
	         <entry>mat</entry>
		 <entry>0 and positive integers</entry>
               </row>
               <row>
		 <entry>Air Code</entry>
	         <entry>air</entry>
	         <entry>AIR</entry>
	         <entry>AIRCODE</entry>
	         <entry/>
	         <entry/>
		 <entry>0 or 1</entry>
               </row>
               <row>
		 <entry>Line Of Sight Thickness Equivalence</entry>
	         <entry>los</entry>
	         <entry>LOS</entry>
	         <entry/>
	         <entry/>
	         <entry/>
	         <entry/>
               </row>
               <row>
		 <entry>Color (Red Green Blue)</entry>
	         <entry>color</entry>
	         <entry>rgb</entry>
	         <entry>RGB</entry>
	         <entry>COLOR</entry>
	         <entry/>
		 <entry>###/###/###</entry>
               </row>
               <row>
		 <entry>Shader Name</entry>
	         <entry>oshader</entry>
	         <entry>SHADER</entry>
	         <entry/>
	         <entry/>
	         <entry/>
		 <entry>string</entry>
               </row>
               <row>
		 <entry>Inherit Properties</entry>
	         <entry>inherit</entry>
	         <entry>INHERIT</entry>
	         <entry/>
		 <entry/>
	         <entry/>
	         <entry>Yes or 1</entry>
               </row>
             </tbody>
	   </tgroup>
         </table>
   </para>

   <para>
     Given the importance of these attributes, it is appropriate to briefly outline the
     meaning and purpose of each of them:

     <variablelist remap="TP">
       <varlistentry>
	 <term><emphasis remap="B" role="bold">Region Flag:</emphasis></term>
	 <listitem>
	   <para>
	   The Region Flag identifies a particular geometric combination as being a solid material - 
	   in other words, any geometry below this combination in the tree can overlap without the
	   overlap being regarded as a non-physical description, since it is the combination of all
	   descriptions in the region object that define the physical volume in space.
	   </para>
	 </listitem>
       </varlistentry>
       <varlistentry>                                                                                              
         <term><emphasis remap="B" role="bold">Region ID Flag:</emphasis></term>                     
         <listitem>                                                                                                
           <para>                                                                                                  
           The Region ID Flag identifies a particular region with a unique number.  This allows
	   multiple region objects to be regarded as being the same type of region, without
	   requiring that they be included in the same combination object.
           </para>
	 </listitem>        
       </varlistentry>
        <varlistentry>
	 <term><emphasis remap="B" role="bold">Material ID Number:</emphasis></term>
	 <listitem>
	   <para>
	   The Material ID Number corresponds to an entry in a DENSITIES table, usually
	   contained in a text file.  This table associates numbers with material names and density
	   information, used by analytical programs such as rtweight.
	   </para>
	 </listitem>
       </varlistentry>
       <varlistentry>
	 <term><emphasis remap="B" role="bold">Air Flag:</emphasis></term>
	 <listitem>
	   <para>
	   The Air Flag alerts the raytracer that the region in question is modeling air,
	   which is handled by specialized rules in LIBRT.
	   </para>
	 </listitem>
       </varlistentry>
       <varlistentry>
	 <term><emphasis remap="B" role="bold">Line of Sight:</emphasis></term>
	 <listitem>
	   <para>
	   </para>
	 </listitem>
       </varlistentry>
       <varlistentry>
	 <term><emphasis remap="B" role="bold">Color:</emphasis></term>
	 <listitem>
	   <para>
	   </para>
	 </listitem>
       </varlistentry>
       <varlistentry>
	 <term><emphasis remap="B" role="bold">Shader Name:</emphasis></term>
	 <listitem>
	   <para>
	   LIBRT can utilize a variety of shaders when rendering - this attribute holds a
	   text string which corresponds to the name of the shader to be used.
	   </para>
	 </listitem>
       </varlistentry>
       <varlistentry>
	 <term><emphasis remap="B" role="bold">Inherit:</emphasis></term>
	 <listitem>
	   <para>
	   </para>
	 </listitem>
       </varlistentry>

     </variablelist>
   </para>
       

</refsection>

<refsection xml:id="examples"><info><title>EXAMPLES</title></info>
  
  <para>
  </para>
</refsection>

<refsection xml:id="see_also"><info><title>SEE ALSO</title></info>
<para><citerefentry><refentrytitle>attr</refentrytitle><manvolnum>n</manvolnum></citerefentry>.</para>
</refsection>

<info><corpauthor>BRL-CAD Team</corpauthor></info>

<refsection xml:id="bug_reports"><info><title>BUG REPORTS</title></info>
  
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsection>
</refentry>
