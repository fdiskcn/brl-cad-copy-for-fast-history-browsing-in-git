BRL-CAD Contributors
====================

The BRL-CAD development team would like to take this opportunity to
thank the many people and organizations who have contributed to make
BRL-CAD the package that it is today.

BRL-CAD was originally conceived and written for the most part by the
late Michael John Muuss.  He is the original primary architect and
author.  The package has since been improved upon over the years by
Mike and the Advanced Computer Systems Team at the U.S. Army Research
Laboratory (* see note), as well as by many others around the world.
His work lives on in testament to his intellect and indomitable spirit
through the contributions and improvements by others.  We strive to
recognize those groups and individuals that have helped make BRL-CAD
the package that it is today here.

Contributions of this large collective work are separated into a few
major sections depending on the level and type of involvement that the
individual maintained.  Contributors are roughly ordered
chronologically for each section, depending upon when involvement with
BRL-CAD began.  The earliest contributors are named first; likewise,
the most recent contributors are listed at the bottom.

Names are optionally followed by the organizations that may have
supported their involvement to some degree, also listed in
chronological order.  Many individuals including many with affiliated
organizations have also supported the package of their own accord and
on their own time.  Participation and contributions are graciously
appreciated.

Active developers are denoted with an "**", meaning that they have
been actively involved within the past 12 months.


ORIGINAL ARCHITECT AND AUTHOR
-----------------------------

"The future exists first in the imagination,
then in the will, then in reality."
                - Mike Muuss


DEVELOPERS
----------

Muuss, Michael John
U.S. Army Research Laboratory

Weaver, Earl P
U.S. Army Ballistic Research Laboratory

Moss, Gary S
U.S. Army Research Laboratory

Dykstra, Phillip
U.S. Army Research Laboratory

Applin, Keith A
U.S. Army Research Laboratory

Kennedy, Charles Michael  <kermit@brlcad.org>  **
U.S. Army Research Laboratory

Gwyn, Douglas A
U.S. Army Research Laboratory

Stay, Paul Randall
U.S. Army Research Laboratory

Hanes, Jeff
U.S. Army Research Laboratory

Butler, Lee A  <butler@arl.army.mil>  **
U.S. Army Research Laboratory

Johnson, Christopher T  <cjohnson@cyberpaladin.com>  **
Geometric Solutions, Inc.
JRMTech, Inc.
Paladin Software, Inc.

Markowski, Michael J
U.S. Army Research Laboratory (??? brl only)

Anderson, John R  <jra@arl.army.mil>  **
U.S. Army Research Laboratory

Tanenbaum, Paul J
U.S. Army Research Laboratory

Durfee, Glenn
ARL Student Hire / Stanford University  (??? carnegie-mellon)

Parker, Robert G  **
U.S. Army Research Laboratory
Survice Engineering, Inc.

Morrison, Christopher Sean  <morrison@brlcad.org>  **
ARL Student Hire / Johns Hopkins University
Quantum Research International, Inc.

Bowers, Ronald A
U.S. Army Research Laboratory

Hartley, Robert  <hartley@vt.edu>
ARL Student Hire / Virginia Tech

Christy, TraNese Shantell
U.S. Army Research Laboratory

Shumaker, Justin  **
Quantum Research International, Inc.

Greenwald, Erik  **
U.S. Army Research Laboratory


MATHEMATICAL, ALGORITHMIC, AND GEOMETRIC SUPPORT
------------------------------------------------

Davisson, Edwin
U.S. Army Research Laboratory

Reed, Jr., Harry
Geometric Solutions, Inc.
U.S. Army Ballistic Research Laboratory

Applin, Keith A
U.S. Army Research Laboratory

Kregel, Dwayne
Survice Engineering, Inc.


CODE CONTRIBUTIONS
------------------

Pistritto, Joseph C
U.S. Army Ballistic Research Laboratory

Natalie, Ronald B
U.S. Army Ballistic Research Laboratory

Mermagen, Jr., William
U.S. Army Research Laboratory

Barker, Natalie L
U.S. Army Research Laboratory

Hunt, Jim  <jhunt@ara.com>
Applied Research Associates, Inc.
U.S. Army Research Laboratory

Coates, Sue
U.S. Army Research Laboratory (??? only brl)

Lindemann, William
Sun Microsystems, Inc.

Smith, Timothy G.
Sun Microsystems, Inc.

Jackson, Christopher J.
Sun Microsystems, Inc.

Fiori, James D.
Sun Microsystems, Inc.

Bowden, Mark Huston
University of Alabama

Suckling, Bob
U.S. Army Ballistic Research Laboratory

Reschly, Jr., Robert J
U.S. Army Ballistic Research Laboratory

Gigante, Mike
Melbourne Institute of Technology

Muuss, Susanne L
U.S. Army Research Laboratory

Nuzman, Carl J
ARL Student Hire / Princeton University

Laut, Bill
Gull Island Consultants, Inc.

Hewitt, Terry
Manchester-Computing Centre, UK

Brand, Mario

McCarty, Corey
ARL Student Hire / Johns Hopkins University

Owens, Jason
ARL Student Hire / Johns Hopkins University

Yemc, David J.
Johns Hopkins University Applied Physics Laboratory

Bowman, Keith
Survice Engineering, Inc.

Anderson, David
SGI


SPECIAL THANKS
--------------

Deitz, Paul
U.S. Army Ballistic Research Laboratory

Lorenzo, Max
CECOM Night Vision & Electronic Sensors Directorate

Monk, Mary
U.S. Army Reserach Laboratory

Haywood, Heather
U.S. Army Research Laboratory

Ousterhout, John

Edwards, Eric
Survice Engineering, Inc.

Bokkers, Wim
TNO Prins Maurits Laboratory


CORPORATE AND ORGANIZATION SUPPORT
----------------------------------

U.S. Army Research Laboratory
  formerly named the U.S. Army Ballistic Research Laboratory
Survivability and Lethality Analysis Directorate

Sun Microsystems, Inc.

University of Alabama in Huntsville

U.S. Army Communications-Electronics Command (CECOM)
Night Vision & Electronic Sensors Directorate

U.S. Naval Academy

Northrup, Inc.
Norhtrup Research and Technology Center

SGI
  formerly named Silicon Graphics, Inc.

Geometric Solutions, Inc.

JRMTech, Inc.

Survice Engineering, Inc.

Quantum Research Laboratory, Inc.

Paladin Software, Inc.

Manchester-Computing-Centre, UK
Computer Graphics Unit, MCC

Industrieanlagen-Betriebsgesellschaft mbH (IABG)

TNO Prins Maurits Laboratory


TO BE FILED
-----------

Becker, David

Davies, Steve

Dender, Dan
U.S. Army Research Laboratory (chuck)
Survice Engineering, Inc.

DiGiacinto, Tom
U.S. Army Ballistic Research Laboratory

Dome, John
CECOM Night Vision & Electronic Sensors Directorate

Florek, David
CECOM Night Vision & Electronic Sensors Directorate

Hartwig, George W
U.S. Army Ballistic Research Laboratory

Henrikson, Bruce
U.S. Army Ballistic Research Laboratory

Johnson, Joe
U.S. Naval Academy

Kekesi, Alex
CECOM Night Vision & Electronic Sensors Directorate

Martin, Glenn E
Northrup Research and Technology Center

McKie, Jim

Merrit, Don
U.S. Army Research Laboratory

Miles, Robert S
U.S. Army Ballistic Research Laboratory (???NRTC)

Moulton, Jr., Russ
EOSoft, Inc.

Musgrave, F Kenton

Reed, Harry
U.S. Army Ballistic Research Laboratory

Ross, Brant A
General Motors, Inc.

Satterfield, Steve
U.S. Naval Academy

Sebok, William L

Skinner, Robert

Stoudenmire, Eugene A
CECOM Night Vision & Electronic Sensors Directorate

Toth, George E

Turkowski, Ken

Willson, Stephen Hunter
Northrup Research and Technology Center

Wolff, Stephen
U.S. Army Ballistic Research Laboratory

Woo, Alex
University of Illinois

zum Brunnen, Rick
U.S. Army Research Laboratory

Stanley, Charles
U.S. Army Ballistic Research Laboratory (??? maybe maybe didn't contribute)

--- 

* The U.S. Army Research Laboratory (ARL) was formerly named the
U.S. Army Ballistic Research Laboratory (BRL).  Persons who
contributed entirely before the name change reflect the original name,
whereas persons who continued contributions or began contributions
after the name change reflect the new name.

** The contributor is an active developer, meaning that they have been
actively involved with the BRL-CAD development within the past 12 months.

If any corrections or additions need to be made, please contact one of
the active core developers.  Additionally, you may send inquiries,
suggestions, and comments to the developer mailing list:
brlcad-devel@lists.sourceforge.net
