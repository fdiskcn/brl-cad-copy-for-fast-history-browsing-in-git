/*
 *  Authors -
 *	John R. Anderson
 *	Susanne L. Muuss
 *	Earl P. Weaver
 *
 *  Source -
 *	VLD/ASB Building 1065
 *	The U. S. Army Ballistic Research Laboratory
 *	Aberdeen Proving Ground, Maryland  21005
 *  
 *  Copyright Notice -
 *	This software is Copyright (C) 1990-2004 by the United States Army.
 *	All rights reserved.
 */

#include "common.h"



#include <stdio.h>

#include "machine.h"

fastf_t brlabs( a )
register const fastf_t a;
{
	register fastf_t b;

	if( a > 0 )
		b = a;
	else
		b = (-a);

	return( b );
}
