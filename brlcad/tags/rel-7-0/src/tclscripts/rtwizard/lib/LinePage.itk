#
# LinePage.itk
#
# This page supports selecting options for line renderings images
#

package require Itcl
package require Itk
package require Iwidgets

namespace eval RtWizard {

    package require MGEDpage 1.0
    package provide LinePage 1.0

    #
    # linepage - Provides a lowercased access method for the
    #            full-color image class
    #
    proc linepage {pathName args} {
	uplevel LinePage $pathName $args
    }

    #
    # Add options
    #

    # None!

    ::itcl::class LinePage {
	inherit ::RtWizard::MGEDpage

	public {
	    constructor {filename args}  {
		MGEDpage::constructor $filename
	    } {

		$itk_component(helpPanel) insert insert \
			"\nUse this page to select the items that you wish to render as a line drawing." \
			HelpTag

		$itk_component(helpPanel) tag configure HelpTag \
			-font {helvetica 10 bold}
		
		configure -onSelectCallback [itcl::code $this onSelect2]
		configure -onDeselectCallback [itcl::code $this onDeselect2]
		configure -statusCallback [itcl::code $this statusCallback]
		configure -labeltext "Configure the Line-Drawing Elements"
		configure -labelpos nw
		configure -stepName "Configure Line-Drawing Elements"

		eval itk_initialize $args

		$itk_component(helpPanel) configure -state disabled 
	    }
	}
    
	private {
	    method onSelect2 {optionsMenu}
	    method onDeselect2 {}
	    method statusCallback {}
	}		
    }

    #--------------------#
    #   Public Methods   #
    #--------------------#    

    #--------------------#
    #  Private Methods   #
    #--------------------#    
    #
    # onSelect2
    #
    # Derived class extension for the base class onSelect method.
    #    
    itcl::body LinePage::onSelect2 {optionsMenu} {
	#
	# Do the options menu
	#
	$::wizardInstance menubar add cascade .$optionsMenu.fgCasc \
		-label "Line Color"

	$::wizardInstance menubar add command .$optionsMenu.fgCasc.fgChoose \
		-label "Choose..." \
		-helpstr "Set the color of rendered lines" \
		-command "[itcl::code $this lineColorChooser]"

	$::wizardInstance menubar add checkbutton \
		.$optionsMenu.fgCasc.useRegCol \
		-label "Use Region Colors" \
		-helpstr "Use the colors assigned to individual regions" \
		-variable "[itcl::scope useRegionColors]" \
		-offvalue "false" \
		-onvalue "true"
	
	$::wizardInstance menubar add command .$optionsMenu.bg \
		-label "Non-Line Color..." \
		-helpstr "Set the Non-line color" \
		-command "[itcl::code $this nonLineColorChooser]"

	$::wizardInstance menubar add command .$optionsMenu.occMode \
		-label "Occlusion Mode..." \
		-helpstr \
		"Select how full-color objects and lines are mixed" \
		-command "[itcl::code $this occModeDialogReveal]"
    }

    #
    # onDeselect2
    #
    # Cleans up when a page is deselected
    #
    itcl::body LinePage::onDeselect2 {} {

    }   

    #
    # statusCallback
    #
    # Page-specific status method
    #
    itcl::body LinePage::statusCallback {} {
	return 1
    }
    
}; # end namespace















