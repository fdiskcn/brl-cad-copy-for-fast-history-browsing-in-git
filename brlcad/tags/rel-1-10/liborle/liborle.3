.TH LIBRLE 3V VMB
'\"	last edit:	85/08/15	G S Moss
'\"	SCCS ID:	@(#)librle.3	1.7
.SH NAME
librle \- run-length encoded image library
.SH SYNOPSIS
.nf
.B #include ``/vld/include/rle.h''
.B #include ``/vld/include/fb.h''
.P
.B "int rle_rhdr( stream, flags, bgpixel )"
.B FILE *stream;
.B int *flags;
.B Pixel *bgpixel;
.P
.B "rle_whdr( stream, ncolors, bgflag, cmflag, bgpixel )"
.B FILE *stream;
.B Pixel *bgpixel;
.P
.B "rle_rlen( xlen, ylen )
.B int *xlen, *ylen;
.P
.B "rle_rpos( xpos, ypos )
.B int *xpos, *ypos;
.P
.B "rle_wlen( xlen, ylen, mode )
.B int xlen, ylen, mode;
.P
.B "rle_wpos( xpos, ypos, mode )
.B int xpos, ypos, mode;
.P
.B int rle_rmap( stream, cmap )
.B FILE *stream;
.B ColorMap *cmap;
.P
.B rle_wmap( stream, cmap )
.B FILE *stream;
.B ColorMap *cmap;
.P
.B rle_decode_ln( stream, scanline_buffer )
.B FILE *stream;
.B Pixel *scanline_buffer;
.P
.B rle_encode_ln( stream, scanline_buffer )
.B FILE *stream;
.B Pixel *scanline_buffer;
.fi
.SH DESCRIPTION
These routines are designed to provide a means of decoding or encoding
University of Utah Alpha_1 format Run-Length Encoded (RLE) files
from a
C
program.
There are separate routines for reading or writing the
RLE
header, reading
or writing the encoded color map, and decoding or encoding a scanline of a
raster image.
The program that loads this library can be ignorant of the
particular encoding
scheme being used.
This library does not need to know anything about
the available frame buffer hardware.
The frame buffer library,
.IR libfb\^ (3V),
can also be used to free programs of hardware dependence.
.P
The following routines are used to decode an
RLE
file into a raster image:
.P
.I Rle_rhdr\^
will seek to the beginning of the given input
.I stream\^
and read the
setup information in the
RLE
header.
This routine
must be called first when decoding an
RLE
file.
If
.I bgpixel\^
is not
.BR \s-1NULL\s0 ,
the background color stored in the
RLE
header will be placed there.
.I Flags\^
is a flag word using a combination of single-bit flags
.BR \s-1NO_BOX_SAVE\s0 ,
.BR \s-1NO_COLORMAP\s0 ,
and
.B
.SM NO_IMAGE
(described in
.BR rle.h )
to indicate whether the image was saved with background,
whether the color map was saved,
and whether the image was saved or just the color map.
If the color map was saved,
.I rle_rmap\^
must be used next to read the color map from the
RLE
file and decode it into the buffer
pointed to by
.IR cmap\^ .
.I Rle_rlen
and
.I rle_rpos
are used after
.I rle_rhdr
to retrieve, from the header structure, the length and position of the image
in the locations specified by
.IR xlen\^ ,
.IR ylen\^ ,
.I xpos\^
and
.IR ypos\^ .
.P
.I Rle_decode_ln\^
is used to decode the next scanline (starting at the bottom) and
should be passed the address of a
.I scanline_buffer\^
large enough to store
.I xlen\^
plus
.I xpos\^
pixels.
Only foreground pixels are written;
it is the caller's responsibility
to fill the buffer first with the appropriate background.
If no
foreground pixels occur, zero is returned;
otherwise 1 is returned
to indicate that the buffer has been altered.
.P
The following routines are used to compress a raster image into an
RLE
file:
.P
.I Rle_whdr\^
is used first to specify the encoding scheme to use and to
write the
RLE
header which will contain this information
to the given output
.IR stream\^ .
The
.I ncolors\^
parameter specifies the number of color channels.
Normally, this should
be set to
.B \s-1\&3\s0
(for red, green, and blue channels).
If
.I ncolors\^
equals
.BR \s-1\&1\s0 ,
a monochrome (black and white) image is assumed, and if
it is
.BR \s-1\&0\s0 ,
only the color map will be saved.
.I Bgflag\^
specifies whether or not the image is saved with the background.
If
.I bgflag\^
is nonzero, only the foreground image is saved.
If
.I bgflag\^
is zero,
the entire image is encoded.
.I Rle_whdr\^
stores the background color along with the other setup information.
If a monochrome image has been specified, the
NTSC
standard is used to calculate the background color from
.IR bgpixel\^ ;
otherwise
.I bgpixel\^
is used as the background color.
.I Cmflag\^
is nonzero to indicate that the color map is to be saved, and this is
accomplished with
.I rle_wmap\^
which must be used next to encode the color map pointed to by
.I cmap\^
and write it to the
RLE
file.
If the value of
.I cmap\^
is
.BR \s-1NULL\s0 ,
a standard (linear) color map will be written.
.P
.I Rle_encode_ln\^
is used to encode the next scanline (starting at the bottom) and
should be passed the address of a
.I scanline_buffer\^
containing the pixels to be converted.
.P
The
.I Pixel\^
and
.I ColorMap\^
data types are defined in
.BR fb.h .
The color map and scanlines can be conveniently read from or written to
a file or supported frame buffer with compatible functions found in
.IR libfb\^ (3V).
.SH WARNINGS
The
RLE
files must be decoded seqentially from the bottom up, as it is the convention
that the origin be the lower left corner of the screen.
.I Rle_decode_ln\^
should fail if called more than
.I ylen\^
times, as it will attempt to read past the end of the input file.
Encoding an
RLE
file from the top down is considered anti-social behavior and will lead
to confusion if the installed frame buffer utilities are used.
The functions must be used in sequence to read/write the header, followed
by the color map (if one exists) and then the image.
It is safe to process multiple images as long as the proper sequence
is followed.
Both
.I rle_rhdr\^
and
.I rle_whdr\^
seek to the beginning of
.I stream\^
before performing any I/O, but all other functions rely on sequential
read/write operations on the
RLE
file.
.P
Processing two images asynchronously is not recommended;
that is,
you should finish one before you begin the next.
The problem stems from the fact that some global information is stored by
.IR rle_rhdr\^ ,
and this information is clobbered by subsequent calls.
Therefore,
when switching back to an image that is partially decoded, the file offset
must be determined with
.IR ftell\^ (3S);
then
.I rle_rhdr\^
can be invoked again to set up things, and finally
.IR fseek\^ (3S)
can be used to reposition the file pointer.
.SH EXAMPLE
The library may be loaded as follows:
.RS
$ \|\fIcc \|\-I/vld/include \|program.c \|\/vld/lib/librle.a\fP
.RE
.SH FILES
/vld/include/rle.h
.br
/vld/include/fb.h
.br
/vld/lib/librle.a
.SH "SEE ALSO"
libfb(3V).
.SH DIAGNOSTICS
Upon error, all functions print a message and return \-1.
.SH AUTHOR
Gary S. Moss, BRL/VLD-VMB
.br
Mike J. Muuss, BRL/SECAD
.br
Spencer W. Thomas, U. of Utah
.SH BUGS
It would be nice to fix the problem of asynchronous calls by passing a
pointer to storage for the setup structure to the read/write header and
read/write scanline routines so that there is no global information
to contend with.
