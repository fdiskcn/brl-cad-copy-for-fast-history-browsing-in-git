set(LIBRT_SOURCES
    bezier_2d_isect.c
    binary_obj.c
    bomb.c
    bool.c
    bundle.c
    cmd.c
    cut.c
    db5_alloc.c
    db5_bin.c
    db5_comb.c
    db5_io.c
    db5_scan.c
    db5_types.c
    db_alloc.c
    db_anim.c
    db_comb.c
    db_flags.c
    db_inmem.c
    db_io.c
    db_lookup.c
    db_match.c
    db_open.c
    db_path.c
    db_scan.c
    db_tree.c
    db_walk.c
    dir.c
    dspline.c
    fortray.c
    g_arb.c
    g_arbn.c
    g_ars.c
    g_bot.c
    g_brep.cpp
    g_cline.c
    g_dsp.c
    g_ebm.c
    g_ehy.c
    g_ell.c
    g_epa.c
    g_eto.c
    g_extrude.c
    g_grip.c
    g_half.c
    g_hf.c
    g_hyp.c
    g_metaball.c
    g_nmg.c
    g_nurb.c
    g_part.c
    g_pg.c
    g_pipe.c
    g_rec.c
    g_rhc.c
    g_rpc.c
    g_sketch.c
    g_sph.c
    g_submodel.c
    g_superell.c
    g_tgc.c
    g_torus.c
    g_vol.c
    globals.c
    htbl.c
    many.c
    mater.c
    memalloc.c
    mirror.c
    mkbundle.c
    opennurbs_ext.cpp
    nmg_bool.c
    nmg_ck.c
    nmg_class.c
    nmg_eval.c
    nmg_extrude.c
    nmg_fcut.c
    nmg_fuse.c
    nmg_index.c
    nmg_info.c
    nmg_inter.c
    nmg_manif.c
    nmg_mesh.c
    nmg_misc.c
    nmg_mk.c
    nmg_mod.c
    nmg_plot.c
    nmg_pr.c
    nmg_pt_fu.c
    nmg_rt_isect.c
    nmg_rt_segs.c
    nmg_tri.c
    nmg_visit.c
    nurb_basis.c
    nurb_bezier.c
    nurb_bound.c
    nurb_c2.c
    nurb_copy.c
    nurb_diff.c
    nurb_eval.c
    nurb_flat.c
    nurb_interp.c
    nurb_knot.c
    nurb_norm.c
    nurb_plot.c
    nurb_poly.c
    nurb_ray.c
    nurb_refine.c
    nurb_reverse.c
    nurb_solve.c
    nurb_split.c
    nurb_tess.c
    nurb_trim.c
    nurb_trim_util.c
    nurb_util.c
    nurb_xsplit.c
    oslo_calc.c
    oslo_map.c
    pr.c
    prep.c
    regionfix.c
    roots.c
    shoot.c
    spectrum.c
    storage.c
    table.c
    tcl.c
    transform.c
    tree.c
    vers.c
    vlist.c
    wdb.c
)

if(MSVC)
    set(LIBRT_SOURCES
        ${LIBRT_SOURCES}
        timer-nt.c
    )
endif(MSVC)

include_directories(
    ../../include
    ../other/libz
    ../other/openNURBS
    ../other/libregex
    ../other/tcl/generic
    ../other/tnt
)

add_definitions(
    -DHAVE_CONFIG_H
    -DBRLCADBUILD
    -DON_DLL_IMPORTS
)

add_library(librt ${LIBRT_SOURCES})
