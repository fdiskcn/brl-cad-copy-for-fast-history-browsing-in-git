.TH LIBRT 3 BRL/CAD
.SH NAME
librt \- library for raytracing an MGED database
.SH SYNOPSIS
.nf
\fB#include ``raytrace.h''
\fB#include ``vmath.h''
.sp
extern struct rt_functab rt_functab[];
extern struct rt_g rt_g;
.sp
struct rt_i *rt_dirbuild( mged_file_name, buf, len )
char *mged_file_name;
char *buf;
int len;
.sp
int rt_gettree( rtip, object_name )
struct rt_i *rtip;
char *object_name;
.sp
int rt_shootray( ap )
struct application *ap;
.sp
void rt_prep_timer()
.sp
double rt_read_timer( buf, len )
char *buf;
int len;
.sp
void rt_pr_partitions( phead, title )
struct partition *phead;
char *title;
.sp
void rt_pr_seg( segp );
struct seg *segp;
.sp
void rt_bomb( msg )
char *msg;
.sp
void rt_log( msg )
char *msg;
.sp
struct soltab *rt_find_solid( )
.sp
void mat_zero( m )		/* fill matrix m with zeros */
matp_t m;
.sp
void mat_idn( m )		/* fill matrix m with identity matrix */
matp_t m;
.sp
void mat_copy( o, i )		/* copy matrix i to matrix o */
matp_t o, i;
.sp
void mat_mul( o, i1, i2 )	/* multiply i1 by i2 and store in o */
matp_t o, i1, i2;
.sp
void matXvec( ov, m, iv )	/* multiply m by vector iv, store in ov */
matp_t ov, m, iv;
.sp
void mat_inv( o, i )		/* invert matrix i, store result in o */
matp_t o, i;
.sp
void mat_print( title, m )	/* print matrix m, (with title) on stdout */
char *title;
matp_t m;
.sp
void mat_trn( o, i )		/* transpose matrix i into matrix o */
matp_t o, i;
.sp
void mat_ae( o, a, e )		/* rotation matrix o from azimuth+elevation */
matp_t o;
double a, e;
.sp
void mat_angles( o, a, b, g )	/* rotation matrix o from angles a, b, g */
matp_t o;
double a, b, g;
.sp
void vtoh_move( v, h )		/* homogeneous vector from ordinary vector */
vectp_t v, h;
.sp
void htov_move( h, v )		/* ordinary vector from homogeneous vector */
vectp_t h, v;
.sp
.SH DESCRIPTION
.I rt_dirbuild\^
opens
.I mged_file_name\^
and builds a directory for quick lookup of objects.
.I rt_dirbuild\^
returns a pointer to a struct rt_i on success (often called ``rtip''),
or RTI_NULL on failure (such as being unable to open the named database).
This rt_i pointer must be saved, as it is a required parameter to
.I rt_gettree .
The user-supplied buffer ``buf'' is filled with up to ``len'' characters
of information from the first title record in the database.
.P
All objects (groups and regions) which are to be included in the description
to be raytraced must be preprocessed with
.IR rt_gettree\^ ,
which returns -1 for failure and 0 for success.
This function can be called as many times as required.
Be certain to pass the struct rt_i pointer from rt_dirbuild() each time.
.P
To fire a ray at the model, an application structure must be
prepared and its address passed to
.IR rt_shootray\^ .
Note that it is mandatory that you provide values for
a_ray.r_pt (the starting point of the ray to be fired),
a_ray.r_dir (a unit-length direction vector),
a_hit (address of user-supplied hit routine),
a_miss (address of user-supplised miss routine),
a_rt_i (struct rt_i pointer, from rt_dirbuild()),
a_onehit (flag controlling stop after first hit),
and
a_level (recursion level, just for diagnostic printing).
.P
To obtain a report of cpu usage for a portion or portions of your program,
frame the statements with calls to
.I rt_prep_timer
and
.IR rt_read_timer .
Each call to
.I rt_prep_timer
resets the timing process, after which
.I rt_read_timer
can be called to get
a double which is the elapsed CPU time in seconds since
.I rt_prep_timer\^
was last called.
In addition, up to ``len'' bytes of
system-specific detailing of resource consumption
is placed in the user-supplied buffer ``buf''.
.P
.I rt_bomb\^
can be used to exit your program with
.I msg\^
printed on the standard error output.
.SH "EXIT CODE"
All truely fatal errors detected by the library use \fIrtbomb\^
\fRto exit with a status of 12.
.SH DEFINITION
RPP \- Rectangular parallel-piped.
A region of space defined by minimum and maximum values in X, Y, and Z.
RPPs are used by librt as the bounding volume for solids.
.SH DISCUSSION
You should study the structures in
.IR raytrace.h\^ ,
in particular, the
.IR application\^ ,
the
.I partition\^
structure and its component structures to get an idea of what information
is handed to/from
.IR rt_shootray\^ .
.I rt_shootray\^
may be called recursively as from your
.I a_hit\^
routine (good for doing bounced rays).  If you only care about the first
object hit along the path of the ray, set the
.I a_onehit\^ 
flag in the application structure before calling
.IR rt_shootray\^ .
.I rt_shootray\^
returns the return value of the user-supplied hit or miss function called.
.P
If the ray intersects the model, the a_hit() routine is called
with a pointer to the application structure and a pointer to a
linked list of ray \fBpartitions\fR (struct partition).  Within each
partition are \fBsegment\fR and \fBhit\fR structure pointers
for the places that the ray enters and leaves this partition of space.
pt_inhit.hit_dist is the parametric distance at which the ray enters
the partition,
and pt_outhit.hit_dist is the parameteric distance the ray leaves.
Note that while the \fBhit\fR structure contains hit_point
and hit_norm elements, they are not computed by rt_shootray().
If these are needed, they can be filled in by using the RT_HIT_NORM()
macro.
.P
Helpful in generation a grid of ray origins, the bounding RPP of the
model is computed as
.I rt_gettree\^
is called and stored in
.I rtip->mdl_min\^
and
.IR rtip->mdl_max\^ .
.SH EXAMPLE(S)
.RS
A program can be loaded as follows:
.sp
$ \|\fIcc \|\-I?? \|main.c \|/usr/brl/lib/librt.a \|\-lm\fP
.sp
Here is a portion of a hypethetical program which uses the library:
.sp
.nf
#include "vmath.h"
#include "raytrace.h"
main( argc, argv )				/* m a i n ( ) */
int argc;
char *argv[];
{
	extern int optind;			/* Used by getopt(3C) */
	extern int do_if_hit(), do_if_miss();	/* Application routines */
	register int h, v;
	int grid_sz;
	struct application ap;		/* Set up for rt_shootray() */
	struct rt_i *rtip;
	...

	/* Build the directory.	*/
	rtip = rt_dirbuild( argv[optind++] );

	/* Load the desired portion of the model. */
	objects = &argv[optind];
	while( argv[optind] != NULL )  {
		rt_gettree( rtip, argv[optind++] );
	}
	ap.a_hit = do_if_hit;		/* Supply routine for hit */
	ap.a_miss = do_if_missed;	/* Supply routine for miss */
	ap.a_rt_i = rtip;
	ap.a_level = 0;
	ap.a_onehit = 1;		/* Return only closest object hit */

	for(	v = 0;			/* First scanline.	*/
		v < grid_sz;		/* Check for end.	*/
		++v			/* Next scanline.	*/
	    )  {
		for( h = 0; h < grid_sz; ++h )  {
			/* Set up ray origin. */
			VMOVE( ap.a_ray.r_pt, get_grid( h, v ) );
			/* Compute ray direction. */
			VMOVE( ap.a_ray.r_dir, get_dir( h, v ) );
			VUNITIZE( ap.a_ray.r_dir ); /* Must be unit vector. */
			(void) rt_shootray( &ap );
		}
	}
}

static int
do_if_hit( ap, PartHeadp )
register struct application *ap;
struct partition *PartHeadp;
{
	register struct partition	*pp = PartHeadp->pt_forw;

	RT_HIT_NORM( pp->pt_inhit, pp->pt_inseg->seg_stp, &(ap->a_ray) );
	RT_HIT_NORM( pp->pt_outhit, pp->pt_outseg->seg_stp, &(ap->a_ray) );
	/* Check for flipped normal and fix (if you intend to use it) */
	if( pp->pt_inflip )  {
		VREVERSE( pp->pt_inhit->hit_normal, pp->pt_inhit->hit_normal );
		pp->pt_inflip = 0;
	}
	if( pp->pt_outflip )  {
		VREVERSE( pp->pt_outhit->hit_normal, pp->pt_outhit->hit_normal );
		pp->pt_outflip = 0;
	}

	/* Do something based on information in partition structure
	 *	such as output a pixel to the frame buffer
         */
	light_model( pp->pt_inhit );
	...
	return	1; /* Report hit to main routine */
}

/*ARGSUSED*/
static int
do_if_miss( ap, PartHeadp )
register struct application *ap;
struct partition *PartHeadp;
{
	return	0; /* Report miss to main routine */
}
.RE
.sp
.SH "SEE ALSO"
mged(1B), rt(1B)
.SH DIAGNOSTICS
"rt_malloc: malloc failure",
if librt is unable to allocate memory will malloc().
"rt_XXX:  read error", if an error or EOF occures while
reading from the model database.
"unexpected SIGFPE!\n" when a floating point error occurs.
(The rootfinder traps SIGFPE, but SIGFPE elsewhere is unexpected).
"rt_shootray:  zero length dir vector\n" when the a_ray.r_dir
vector is not unit length.
"rt_gettree called again after rt_prep!" when an attempt is made
to add more sub-trees to the active model after calling rt_prep
(or after firing the first ray).
"rt_prep: re-invocation" when rt_prep called more than once.
"rt_prep:  no solids to prep" when there are no valid solids
in the model.
.SH AUTHOR(S)
Michael John Muuss
.SH BUGS
The rootfinder detects "hard" cases by taking a SIGFPE and retyring
with a slower but more stable algorithm.  This is unfortunate.
.SH SOURCE
 SECAD/VLD Computing Consortium, Bldg 394
 The U. S. Army Ballistic Research Laboratory
 Aberdeen Proving Ground, Maryland  21005
.SH COPYRIGHT
This software is Copyright (C) 1985 by the United States Army.
All rights reserved.
.SH "BUG REPORTS"
Reports of bugs or problems should be submitted via electronic
mail to <CAD@BRL.ARPA>.
