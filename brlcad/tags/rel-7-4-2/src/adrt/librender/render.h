#ifndef _RENDER_H
#define _RENDER_H

#include "component.h"
#include "depth.h"
#include "flat.h"
#include "grid.h"
#include "kelos.h"
#include "normal.h"
#include "path.h"
#include "phong.h"
#include "plane.h"
#include "spall.h"
#include "render_internal.h"

#endif
