A comprehensive solid modeling system, based on the Combinatorial
Solid Geometry (CSG) technique.  Primarily used for design and
analysis of vehicles, mechanical parts, and architecture. Also used in
radiation dose planning and education.

The distribution includes an interactive 3-D geometry editor,
ray-tracing library, sophisticated ray-tracing based lighting models,
a network-distributed ray-tracer, animation capabilities, a
network-distributed image-processing capability, image-handling, and
data-compression utilities.  Also included is an implementation of
Weiler's n-Manifold Geometry (NMG) data structures for surface-based
solid models and photon mapping.
