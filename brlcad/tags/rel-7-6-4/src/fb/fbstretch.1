.TH FBSTRETCH \*(ms "BRL-CAD package"
./"                    F B S T R E T C H . 1
./" BRL-CAD
./"
./" Copyright (c) 2005 United States Government as represented by
./" the U.S. Army Research Laboratory.
./"
./" This document is made available under the terms of the GNU Free
./" Documentation License or, at your option, under the terms of the
./" GNU General Public License as published by the Free Software
./" Foundation.  Permission is granted to copy, distribute and/or
./" modify this document under the terms of the GNU Free Documentation
./" License, Version 1.2 or any later version published by the Free
./" Software Foundation; with no Invariant Sections, no Front-Cover
./" Texts, and no Back-Cover Texts.  Permission is also granted to
./" redistribute this document under the terms of the GNU General
./" Public License; either version 2 of the License, or (at your
./" option) any later version.
./"
./" You should have received a copy of the GNU Free Documentation
./" License and/or the GNU General Public License along with this
./" document; see the file named COPYING for more information.
./"
./"./"./"
'\"	@(#)$Header$ (BRL)
'\" Edit the next two lines to configure for your system:
.ds ms 1\" utility manual section, normally 1B -- alternatives are 1, 1L, etc.
.ds ls 3\" library manual section, normally 3B -- alternatives are 3, 3L, etc.
.ds fs 5\" format manual section, normally 4B -- alternatives are 5, 4L, etc.
.ie t .ds pf B\" "printout" font, normally (CW -- use B if you don't have one
.el .ds pf 1
'\"
.de CW
.lg 0
\%\&\\$3\f\*(pf\\$1\fP\&\\$2
.lg
..
.SH NAME
fbstretch \- stretch a frame buffer image
.SH SYNOPSIS
.CW fbstretch
[
.CW -h
] [
.CW -f
.I "input image name"
] [
.CW -s
.I "input square size"
] [
.CW -w
.I "input width"
]
.br
	[
.CW -n
.I "input height"
] [
.CW -a
] [
.CW -x
.I "horizontal scale factor"
] [
.CW -y
.I "vertical scale factor"
]
.br
	[
.CW -v
] [
.CW -S
.I "output square size"
] [
.CW -W
.I "output width"
] [
.CW -N
.I "output height"
]
.br
	[ [
.CW -F
]
.I "output frame buffer name"
]
.SH DESCRIPTION
.CW fbstretch
expands or compresses a frame buffer image,
in either or both the horizontal and vertical directions.
The image scaling origin (fixed point) is
the lower left-hand corner of the display.
When expanding (scale factor greater than 1), pixels are simply replicated;
when compressing (scale factor less than 1), pixel averaging is performed
unless sampling is explicitly requested by the
.CW -a
option.
.SS Options
.TP "\w'\f\*(pf-F\fP \fIoutput frame buffer name\fP\ \ \ 'u"
.CW -h
(``high resolution'')\
Assumes 1024 by 1024 pixels for default input image size
instead of 512 by 512.
Explicitly specified sizes override the default,
and if actual input image size is smaller than requested,
the actual size will be used.
If not specified,
requested output frame buffer size will be the stretched revised input size.
If there are margins within the requested output beyond the stretched image,
they will be cleared to background;
if the stretched image would extend beyond the requested output size,
it will be clipped to fit,
even if there is room for it in the actual frame buffer.
Actual sizes depend on details of specific frame buffers.
.TP
\f\*(pf-f\fP \fIinput image name\fP
Inputs the image to be displayed
from the specified frame buffer or \fIpix\^\fP(\*(fs) file,
instead of modifying the output frame buffer in place.
This option may not work if input and output frame buffers are the same device.
.TP
\f\*(pf-s\fP \fIinput square size\fP
Specifies input image width and height.
.TP
\f\*(pf-w\fP \fIinput width\fP
Specifies input image width.
.TP
\f\*(pf-n\fP \fIinput height\fP
Specifies input image height.
.TP
.CW -a
(``no averaging'')\
Specifies that output pixels will be sampled from the input,
instead of being computed by averaging RGB values.
.TP
.CW -v
(``verbose'')\
Causes actual sizes and scale factors used
to be printed on the standard error output.
.TP
\f\*(pf-x\fP \fIhorizontal scale factor\fP
Scales the image by the specified factor in the horizontal direction;
the default is the ratio of requested output to input widths,
if specified,
otherwise 1.
.TP
\f\*(pf-y\fP \fIvertical scale factor\fP
Scales the image by the specified factor in the vertical direction;
the default is the ratio of requested output to input heights,
if specified,
otherwise 1.
.TP
\f\*(pf-S\fP \fIoutput square size\fP
Specifies output frame buffer width and height.
.TP
\f\*(pf-W\fP \fIoutput width\fP
Specifies output frame buffer width.
.TP
\f\*(pf-N\fP \fIoutput height\fP
Specifies output frame buffer height.
.TP
\f\*(pf-F\fP \fIoutput frame buffer name\fP
Outputs to the specified frame buffer
instead of the one specified by the
.CW FB_FILE
environment variable
(or the default, if
.CW FB_FILE
is not set).
If this is the last option specified, the
.CW -F
is optional.
.SH DISCUSSION
Pixel averaging may be meaningless for some images or color maps,
in which case sampling should be specified.
Images of the Mandelbrot set (see \fImandel\^\fP(\*(ms))
are a good example of this.
.SH EXAMPLE
The following procedure enlarges an image,
obtained from a personal computer
.I via
the
.CW gif-fb
utility,
to fill a 1280-by-1024 frame buffer.
.RS
\fI$\fP \|\f\*(pfgif-fb \|image.gif	# \fP\fIdisplays the 320x200 PC image\fP
.br
\fI$\fP \|\f\*(pffbstretch \|-w 320 \|-n 200 \|-W 1280 \|-N 1024\fP
.RE
.SH "SEE ALSO"
gif-fb(\*(ms), mandel(\*(ms), pixscale(\*(ms), libfb(\*(ls), pix(\*(fs).
.SH DIAGNOSTICS
Error messages are intended to be self-explanatory.
.SH AUTHOR
Douglas A.\& Gwyn, BRL/VLD-VMB
