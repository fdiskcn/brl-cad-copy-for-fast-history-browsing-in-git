/*                    A D A G E F R A M E . H
 * BRL-CAD
 *
 * Copyright (C) 2004-2005 United States Government as represented by
 * the U.S. Army Research Laboratory.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this file; see the file named COPYING for more
 * information.
 */
/** @file adageframe.h
 * Adage "Frame" cursor.
 */
11, 11,		/* size */
7, 7,		/* origin */
{
0x00, 0xE0,
0x00, 0xE0,
0x00, 0xE0,
0x00, 0xE0,
0x00, 0xE0,
0x00, 0xE0,
0x00, 0xE0,
0x00, 0xE0,
0xFF, 0xE0,
0xFF, 0xE0,
0xFF, 0xE0
}

/*
 * Local Variables:
 * mode: C
 * tab-width: 8
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 * ex: shiftwidth=4 tabstop=8
 */
