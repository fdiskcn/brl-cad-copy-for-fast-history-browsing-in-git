<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='facedef'>

<refmeta>
  <refentrytitle>FACEDEF</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>facedef</refname>
  <refpurpose>Allows the user to redefine any face of an ARB8 shape.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>facedef</command>    
    
    <arg choice='req'><replaceable>####</replaceable></arg>
    <group>
	<arg>a</arg>
	<arg>b</arg>
	<arg>c</arg>
	<arg>d</arg>
	<arg><replaceable>parameters</replaceable></arg>
    </group>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Allows the user to redefine any face of an ARB8 shape.  The
	user must be in Primitive Edit Mode with an <emphasis>ARB</emphasis> selected for editing. The optional 	parameters may be omitted, and MGED will prompt for the missing values. The
	options are as follows:</para>
        <variablelist>
          <varlistentry><term><emphasis>a</emphasis></term>
            <listitem>
              <para>
	          Specify the new location of this face by providing coefficients for its plane
	          equation:
                  <equation>Ax + By + Cz = D.</equation>
              </para>
            </listitem>
         </varlistentry>
        <varlistentry><term><emphasis>b</emphasis></term>
          <listitem>
             <para>Specify the new location of this face using three points.
		 </para>
	    </listitem>	
	  </varlistentry>
	 <varlistentry><term><emphasis>c</emphasis></term>
          <listitem>
             <para>
	         Specify the new location of this face using rotation and fallback angles. 
             </para>
           </listitem>
       </varlistentry>
       <varlistentry><term><emphasis>d</emphasis></term>
          <listitem>
             <para>Specify the new location of this face by changing the <emphasis>D</emphasis> value in the 		    plane equation.
		 </para>
	    </listitem>	
	 </varlistentry>
	 <varlistentry><term><emphasis>q</emphasis></term>
          <listitem>
             <para>Return to MGED prompt.
		 </para>
	    </listitem>	
	 </varlistentry>
       </variablelist>
  
 
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
     <para>The first example shows the use of the <command>facedef</command> command with the <emphasis>a 	 </emphasis> option to redefine a specified face so that it is in another plane.  The second example shows 	the use of the <command>facedef</command> command with the <emphasis>b</emphasis> option to redefine a 	face so that it is in another plane formed by three points.
     </para>
  <example>
    <title>Moving a specified face so that it is in another plane.</title>
    <variablelist>
      <varlistentry>
	   <term><prompt>mged></prompt> <userinput>facedef 1234 a 1 0 0 20</userinput></term>
	   <listitem>
	     <para>Moves face 1234 such that it is in the <emphasis>yz</emphasis> plane at <emphasis>x</emphasis>=		  20.
	     </para>
	   </listitem>
      </varlistentry>
    </variablelist>
   </example>
  <example>
   <title> Moving a specified face so that it is in another plane formed by three points.</title>  
    <variablelist>
      <varlistentry>
	   <term><prompt>mged</prompt> <userinput>facedef 5678 b 0 0 10 10 0 10 10 10 10</userinput></term>
	    <listitem>
	     <para>
		 Moves face 5678 such that it is in the plane formed by the three points (0 0 10), (10
		 0 10), and (10 10 10).
	     </para>
	   </listitem>
      </varlistentry>
    </variablelist>
  </example>
  
  
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

