<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2010 United States Government as represented by the  -->
<!-- U.S. Army Research Laboratory.                                     -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='put_comb'>

<refmeta>
  <refentrytitle>PUT_COMB</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD MGED Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>put_comb</refname>
  <refpurpose>Insert combinations and regions - allows definition of both
boolean expressions and standard attribute values.</refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>put_comb</command>    
    <arg choice='req'><replaceable>comb_name</replaceable></arg>
    <arg choice='opt'>is_Region
	<group>
	   <arg>id</arg>
	   <arg>air</arg>
	   <arg>material</arg>
	   <arg>los</arg>
	</group>
    </arg>
    <arg choice='req'><replaceable>color</replaceable></arg>
    <arg choice='req'><replaceable>shader</replaceable></arg>
    <arg choice='req'><replaceable>inherit</replaceable></arg>
    <arg choice='req'><replaceable>boolean_expr</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Command for inserting regions and combinations with attributes set using
a single line command - useful in scripting situations.	
    </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The <command>put_comb</command> expects one of two particular
forms of input - combination or region.</para>
         
  <example>
    <title>Specifying a combination using <command>put_comb</command></title>
    <para>
      <prompt>mged></prompt><userinput>put_comb test.c n 255/255/255 plastic n "u sph.s u arb8.s"</userinput>
    </para>
    <para>Creates a combination that is not a region, using the plastic shader, consisting of the union of sph.s and arb8.s".
    </para>
<literallayout>
mged> l test.c
test.c:  --
Shader 'plastic'
   u sph.s
   u arb8.s
mged> attr get test.c
oshader {plastic}
</literallayout>
  </example>
          
  <example>
    <title>Specifying a region using <command>put_comb</command></title>
    <para>
      <prompt>mged></prompt><userinput>put_comb test.r y 1001 0 32 1 255/255/255 plastic y "u sph.s u arb8.s"</userinput>
    </para>
    <para>Creates a combination that is a region, with region id 1001, air flag off, material 32, los 1, white in color, using the plastic shader, inheriting, consisting of the union of sph.s and arb8.s"
    </para>
<literallayout>
mged> l test.r
test.r:  REGION id=1001 (air=0, los=1, GIFTmater=32) --
Shader 'plastic'
(These material properties override all lower ones in the tree)
   u sph.s
   u arb8.s
mged> attr get test.r
region {R} inherit {1} oshader {plastic} region_id {1001} 
material_id {32} los {1} 
</literallayout>
  </example>
 
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

