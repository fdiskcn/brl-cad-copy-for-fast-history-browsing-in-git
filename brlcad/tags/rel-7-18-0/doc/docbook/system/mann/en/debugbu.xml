<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='debugbu'>
  
  <refmeta>
    <refentrytitle>DEBUGBU</refentrytitle>
    <manvolnum>nged</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>debugbu</refname>
    <refpurpose>Allows the user to set or check the debug flags used by
    <emphasis>libbu</emphasis>.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>debugbu</command>    
      <arg><replaceable>hex_code</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      Allows the user to set or check the debug flags used by <emphasis>libbu</emphasis>. 
      With no arguments, the <command>debugbu</command> command displays all the 
      possible settings for the <emphasis>bu_debug</emphasis> flag and the current 
      value. When a <emphasis>hex_code</emphasis> is supplied, that value is used 
      as the new value for <emphasis>bu_debug</emphasis>. Similar debug commands for 
      other <emphasis>BRL-CAD</emphasis> libraries are <command>debuglib</command> for
      <emphasis>librt</emphasis> and <command>debugnmg</command> for the NMG portion 
      of <emphasis>librt</emphasis>. Other debugging commands include 
      <command>debugmem</command> and <command>debugdir</command>.
    </para>
  </refsect1>
  
  <refsect1 id='examples'>
    <title>EXAMPLES</title>
    <para>
      The first example shows the use of the <command>debugbu</command> command 
      (without an argument) to display all possible settings for the 
      <emphasis>bu_debug</emphasis> flag and the current value. The second example 
      shows the use of the <command>debugbu</command> command with a hex code, which 
      is used as the new value for <emphasis>bu_debug</emphasis>.
  </para>
  
  <example>
    <title>Display a list of available <emphasis>bu_debug</emphasis> values and 
    the current value.</title>
    <para>
      <prompt>mged></prompt><userinput>debugbu</userinput>
    </para>
    <para>
      Lists available <emphasis>bu_debug</emphasis> values and the current value.
    </para>
  </example>
  
  <example>
    <title>Set a new value for <emphasis>bu_debug</emphasis>.</title>
    <para>
      <prompt>mged></prompt><userinput>debugbu 2</userinput>
    </para>
    <para>
      Sets <emphasis>bu_debug</emphasis> to &lt;MEM_CHECK&gt;.
    </para>
  </example>
  </refsect1>
  
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>BRL-CAD Team</para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
    </para>
  </refsect1>
</refentry>

