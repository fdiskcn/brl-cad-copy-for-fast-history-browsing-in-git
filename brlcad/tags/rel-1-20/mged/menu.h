/*
 *			M E N U . H
 *
 *  Authors -
 *	Bob Suckling
 *	Mike Muuss
 *  
 *  Source -
 *	SECAD/VLD Computing Consortium, Bldg 394
 *	The U. S. Army Ballistic Research Laboratory
 *	Aberdeen Proving Ground, Maryland  21005
 *  
 *  Copyright Notice -
 *	This software is Copyright (C) 1985 by the United States Army.
 *	All rights reserved.
 *
 *  $Header$
 */

/*
 * Each active menu is installed by haveing a non-null entry in
 * menu_array[] which is a pointer
 * to an array of menu items.  The first ([0]) menu item is the title
 * for the menu, and the remaining items are individual menu entries.
 */
struct	menu_item  {
	char	*menu_string;
	void	(*menu_func)();
	int	menu_arg;
};

#define MENU_NULL		((struct menu_item *)0)
	
#define NMENU	3
extern struct menu_item	*menu_array[NMENU];	/* Active menus (menu.c) */

#define MENU_L1		0	/* top-level solid-edit menu */
#define MENU_L2		1	/* second-level menu (unused) */
#define MENU_GEN	2	/* general features (mouse buttons) */
