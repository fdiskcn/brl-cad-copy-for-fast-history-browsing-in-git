						 -*- coding: utf-8 -*-
BRL-CAD Deprecation Log
=======================

Included below is a deprecation log and notes for BRL-CAD public
interface changes that potentially impact external developers and
users.  Public interfaces include developer application programming
interface (API) calls as well as user application command-line
interface (CLI) and graphical user interface (GUI) behaviors.

The general practice is that public interface changes may go from
deprecated to obsolete, whereby previous behavior is removed or
changed, during one of the following events:

a) during a major release (e.g. going from 7.*.* to 8.*.*)

b) during a minor release provided the change was publicly announced
   as deprecated beforehand AND sufficient deprecation warning was
   given via either i) three minor releases included the deprecation
   notice, or ii) three months passed since deprecation

c) during a minor release provided there still exists an equivalent
   alternative interface that will not impose an undue burden on users
   or developers (see "MINIMALLY IMPACTING API CHANGES" below)

d) during a patch release provided you can correctly prove P = NP

If a change will intentionally replace or remove functionality, then
they should be then marked as deprecated first.  Otherwise, changes to
interfaces that are never publicly announced, not installed, or are
not part of publicly available documentation are fair game to change
as needed.

Where feasible, interfaces should be visibly marked as deprecated
either during compile-time with #warning pre-processor declarations or
during run-time with print statements.

The items listed below are interfaces that are deprecated or obsolete,
categorized by the release in which the change was made.  If the
change involved a rename/removal of a routine, both the original and
new names should be listed for reference.


**************
* DEPRECATED *
**************

7.20
----

raytrace.h
	rt_load_attrs()

7.18
----

raytrace.h rt_comb_internal members
        region_id -> region_id attribute [deprecated 7.18]
        aircode -> air attribute [deprecated 7.18]
        GIFTmater -> material_id attribute [deprecated 7.18]
        los -> los attribute [deprecated 7.18]
bu.h xdr API
        BU_GLONGLONG() -> ntohll() [deprecated 7.18]
        BU_GLONG() -> ntohl() [deprecated 7.18]
        BU_GSHORT() -> ntohs() [deprecated 7.18]
        bu_gshort() -> ntohs() [deprecated 7.18]
        bu_glong() -> ntohl() [deprecated 7.18]
        bu_pshort() -> htons() [deprecated 7.18]
        bu_plong() -> htonl() [deprecated 7.18]
        bu_plonglong() -> htonll() [deprecated 7.18] 
db.h
        rt_fastf_float() -> ntohf() [deprecated 7.18]
        rt_mat_dbmat() -> htonf() [deprecated 7.18]
        rt_dbmat_mat() -> ntohf() [deprecated 7.18]
dbcp
        dbcp -> dd [deprecated 7.18]
mged
        ?(*) -> ? -(*) [deprecated 7.18]
        apropos(*) -> apropos -(*) [deprecated 7.18]
        dbconcat -> db concat [deprecated 7.18]
        dbupgrade -> db upgrade [deprecated 7.18]
        dbversion -> db version [deprecated 7.18]
        dbfind -> search [deprecated 7.18]
        debug* -> debug [deprecated 7.18]
        comb_std -> comb [deprecated 7.18]
        comb_color -> comb [deprecated 7.18]
        combmem -> comb [deprecated 7.18]
        kill(*) -> kill [deprecated 7.18]
        rotobj -> rot [deprecated 7.18]
        qvrot -> rot -q -v [deprecated 7.18]
        qorot -> rot -q [deprecated 7.18]
        ([amov])rot -> rot -([amv]) [deprecated 7.18]
        ([op])scale -> sca -(p) [deprecated 7.18]
        ([op])translate -> tra -(p) [deprecated 7.18]
        nmg_(*) -> nmg (*) [deprecated 7.18]
        bot_(*) -> bot (*) [deprecated 7.18]
        [so]ed -> edit [deprecated 7.18]
        [tr]ed -> edit -t [deprecated 7.18]
        ls -oA -> ls -O [deprecated 7.18]
        draw -oA -> draw -O [deprecated 7.18]
        erase -oA -> erase -O [deprecated 7.18]
        erase_all -> erase -r [deprecated 7.18]
        move_all -> move -r [deprecated 7.18]
        query_ray -> nirt [deprecated 7.18]
        vdraw -> draw -v [deprecated 7.18]
        vnirt -> nirt -v [deprecated 7.18]
        vquery_ray -> nirt -v [deprecated 7.18]
        xpush -> push -x [deprecated 7.18]

7.16
----
struct bu_structparse 'i' format
       'i' -> '%p' [deprecated 7.16]
SGI-specific tools
	pl-sgi -> pl-fb [ deprecated 7.16 ]
Dunn camera and Canon printer tools
	dunncolor [ deprecated 7.16 ]
	dunnsnap [ deprecated 7.16 ]
	canonize [ deprecated 7.16 ]
reserved attributes and case-sensitivity
	id -> region_id [deprecated 7.16]
	GIFTmater -> material_id [deprecated 7.16]
	GIFT_MATERIAL -> material_id [deprecated 7.16]
	mat -> material_id [deprecated 7.16]
	AIRCODE -> air [deprecated 7.16]
	rgb -> color [deprecated 7.16]
	oshader -> shader [deprecated 7.16]
genptr_t/GENPTR_NULL
	genptr_t -> void* [deprecated 7.16]
.pl file suffix
	.pl -> .plot3 [deprecated 7.16]
*-pl
	*-pl -> *-plot3 [deprecated 7.16]
pl*
	pl* -> plot3* [deprecated 7.16]
any-png.sh
        use pix tools [deprecated 7.16]
cadbug.sh
        use sf.net tracker [deprecated 7.16]
cray.sh
        obsolete hardware [deprecated 7.16]
pixwrite.sh
        obsolete hardware [deprecated 7.16]
pixread.sh
        obsolete hardware [deprecated 7.16]
sgisnap.sh
        obsolete hardware [deprecated 7.16]
include/bu.h
        bu_argv0() -> bu_basename(bu_argv0_full_path()); [deprecated 7.16]
include/raytrace.h
        rt_prep_timer() -> bu_timer() [deprecated 7.16]
        rt_get_timer() -> bu_elapsed() [deprecated 7.16]
mged
	make_bb -> bb [deprecated 7.16]

7.14
----
include/dm_color.h
	dm_color.h -> dm.h [deprecated 7.14]
include/fb.h
        FB_WPIXEL -> fb_wpixel [deprecated 7.14]
src/gtools
	g_[toolname] -> g[toolname] [deprecated 7.14]
src/other/jove
	jove -> emacs [deprecated 7.14]
include/raytrace.h
	rt_fdiff -> NEAR_ZERO [deprecated 7.14]
	rt_reldiff -> NEAR_ZERO [deprecated 7.14]
	DIR_* -> RT_DIR_* [deprecated 7.14]
include/bu.h
        bu_fopen_uniq -> fopen [deprecated 7.14]
	%S format specifier -> %V format specifier for bu_vls [deprecated 7.14]
mged
	tops -g -u -> now default behavior [deprecated 7.14]
	-n option -> -c option [deprecated 7.14]
        bots, per_line [deprecated 7.14]
include/vmath.h
	VADD2_2D -> V2ADD2 [deprecated 7.14]
	VSUB2_2D -> V2SUB2 [deprecated 7.14]
	MAGSQ_2D -> MAG2SQ [deprecated 7.14]
	VDOT_2D -> V2DOT [deprecated 7.14]
	VMOVE_2D -> V2MOVE [deprecated 7.14]
	VSCALE_2D -> V2SCALE [deprecated 7.14]
	VJOIN1_2D -> V2JOIN1 [deprecated 7.14]

7.12
----
include/raytrace.h
	rt_functab size -> growing ft_label and new callbacks [deprecated 7.12]
include/vmath.h
	ELEMENTS_PER_PT -> ELEMENTS_PER_POINT
	HVECT_LEN -> ELEMENTS_PER_HVECT
	HPT_LEN -> ELEMENTS_PER_HPOINT
srt/librt/cmd.c
	rt_split_cmd() -> bu_argv_from_string() [deprecated 7.12]
src/rt/viewarea.c
	terminology and output format [deprecated 7.12]
src/libbu/parallel.c
	bu_get_load_average(), remove [deprecated 7.12]
include/raytrace.h
	RT_HIT_NORM -> RT_HIT_NORMAL [deprecated 7.12]

pre 7.0
-------
include/raytrace.h
	struct hit.hit_point -> RT_HIT_POINT [deprecated pre-7.0]
	struct hit.hit_normal -> RT_HIT_NORMAL [deprecated pre-7.0]


************
* OBSOLETE *
************

7.20.0
------
	bu_mro and related macros -> bu_attribute_value_set

7.18.2
------
	RT_HIT_NORM -> RT_HIT_NORMAL [deprecated pre-7.0]
	g-shell.rect renamed to g-shell-rect [minor tool rename]

7.18.0
------
include/raytrace.h
	db_free_external -> bu_free_external [deprecated pre-7.0]
include/wdb.h
	mk_fastgen_region() -> mk_comb() [deprecated 7.12]
src/librt/bomb.c
	rt_bomb() -> bu_bomb() [deprecated 7.10]
include/machine.h
	machine.h -> common.h && bu.h [deprecated 7.10]
src/vas4
	vas4, remove [deprecated 7.12]

7.16.4
------
include/raytrace.h
        get_solidbitv() -> rt_get_solidbitv() [deprecated 7.16]

7.14.4
------
include/pkg.h
	uses size_t for lengths in API calls
include/bu.h
	bu_association() [unused]

7.14.0
------
include/raytrace.h
	GET_HITMISS -> NMG_GET_HITMISS
mged
	edcolor -> color -e
include/vmath.h
	PI -> M_PI
	M_SQRT2_DIV2 -> M_SQRT1_2

7.12.6
------
mged
	binary -> bo [rename]

7.12.4
------
include/mater.h
	rt_material_head -> rt_material_head()  [deprecated 7.12]
include/raytrace.h
	removed iterator from NMG_CK_HITMISS_LIST
include/bu.h
	BU_QFLSTR -> BU_FLSTR

7.12.2
------
include/wdb.h
	mk_trc() -> mk_trc_h() and mk_trc_top() [deprecated pre-7.0]
	mk_bsolid() [deprecated pre-7.0]
	mk_bsurf() [deprecated pre-7.0]
	mk_strsol() -> mk_dsp(), mk_ebm(), mk_vol(), mk_submodel() [deprecated pre-7.0]
	mk_rcomb(), mk_fcomb(), mk_memb() -> mk_lcomb() and mk_addmember() [deprecated pre-7.0]
	mk_fwrite_internal() [deprecated pre-7.0]

7.12.0
------
include/common.h
	NATURAL_IEEE -> bu_byteorder() == BU_BIG_ENDIAN || defined(WORDS_BIGENDIAN)
	REVERSE_IEEE -> bu_byteorder() == BU_LITTLE_ENDIAN
include/bu.h
	bu_log(char *, ...) -> bu_log(const char *, ...) [const]
	bu_flog(FILE *, char *, ...) -> bu_flog(FILE *, const char *, ...) [const]
	bu_vls_printf(struct bu_vls *, char *) -> bu_vls_printf(struct bu_vls *, const char *) [const]
	bu_vls_sprintf(struct bu_vls *, char *) -> bu_vls_sprintf(struct bu_vls *, const char *) [const]
include/wdb.h
	removed mk_poly() [deprecated pre-7.0]
	removed mk_polysolid() [deprecated pre-7.0]
	removed mk_fpoly() [deprecated pre-7.0]
	removed write_shell_as_polysolid() [deprecated 6.0]
include/bu.h
	char *bu_brlcad_path() -> const char *bu_brlcad_path() [const]
include/bn.h
	bn_mat_zero() -> MAT_ZERO() [deprecated pre-7.0]
	bn_mat_idn() -> MAT_IDN() [deprecated pre-7.0]
	bn_mat_copy() -> MAT_COPY() [deprecated pre-7.0]
include/compat4.h
	compat4.h -> bu.h && bn.h [deprecated 5.0]
include/raytrace.h
	rt_overlap_quietly() -> struct application.a_logoverlap = rt_silent_logoverlap [deprecated pre 7.0]
include/bu.h
	bu_brlcad_path() -> bu_brlcad_root() || bu_brlcad_data() [deprecated 7.4]
	bu_tcl_brlcad_path() -> bu_tcl_brlcad_root() || bu_tcl_brlcad_data() [deprecated 7.4]
include/fb.h
	fb_log(char *fmt) -> fb_log(const char *fmt) [const]
include/noalias-prag.h
	removed [non-stdc]
include/noalias.h
	removed [non-stdc]
src/librt/wdb_obj.c -> src/librt/db_obj.c
	removed wdb_tree_cmd(), added dgo_tree_cmd() [rename]
mged
	dbbinary -> binary [rename]

7.10.4
------
include/msr.h [pre 7.0]
	msr.h -> bu.h
include/rtlist.h [pre 7.0]
	rtlist.h -> bu.h
include/rtstring.h [pre 7.0]
	rtstring.h -> bu.h
include/shortvect.h [pre 7.0]
include/shortvect-pr.h [pre 7.0]

7.10.2
------
include/raytrace.h
	rt_version -> rt_version() [private]
include/[library].h (several)
	[library]_version -> [library]_version() [private]


***********************************
* MINIMALLY IMPACTING API CHANGES *
***********************************

If a change to an interface has a suitable and completely equivalent
alternative, the change is deemed to be "minimally impacting" and can
be made during any minor release.  Examples of such changes include
renaming an interface, reordering parameters, adding new parameters to
an interface for previously implicit or restricted behavior, and
removing unused parameters.

Basically, the impact can usually be considered minor if a static
regular expression search-and-replace is all that is needed to update.
Included in following are various minimally impacting API changes that
have been identified in reverse chronological order.  As expressions
are not extensively tested, use with caution.

s/V2APPROXEQUAL(/V2NEAR_EQUAL(/g
        V2APPROXEQUAL() renamed to V2NEAR_EQUAL() [7.18]

s/SMALL\([^_]\)/SMALL_FAST\1/g
        SMALL renamed to SMALL_FASTF [7.18]

s/db_shader_mat(/rt_shader_mat(/g
        db_shader_mat() renamed to rt_shader_mat() [7.18]

s/rt_get_seg(/rt_alloc_sed_block(/g
        rt_get_seg() renamed to rt_alloc_seg_block() [7.18]

s/db_get_directory(/db_alloc_directory_block(/g
        db_get_directory() renamed to db_alloc_directory_block() [7.18]

s/db_get_directory_size(/db_directory_size(/g
        db_get_directory_size() renamed to db_directory_size() [7.18]

s/db_get_version(/db_version(/g
        db_get_version() renamed to db_version() [7.18]

s/VAPPROXEQUAL(/VNEAR_EQUAL(/g
        VAPPROXEQUAL() renamed to VNEAR_EQUAL() [7.18]

s/bu_vlb_getBufferLength(/bu_vlb_buflen(/g
        bu_vlb_getBufferLength() remamed to bu_vlb_buflen() [7.18]

s/bu_vlb_getBuffer(/bu_vlb_addr(/g
        bu_vlb_getBuffer() remamed to bu_vlb_addr() [7.18]

s/nmg_struct_counts(/nmg_pr_m_struct_counts(/g
        nmg_struct_counts() renamed to nmg_pr_m_struct_counts() [7.18]

s/bu_ptbl(\([^,]*\),[[:space:]]*BU_PTBL_INIT\([^)]*\))/bu_ptbl_init(\1, 64, "init")/g
s/bu_ptbl(\([^,]*\),[[:space:]]*BU_PTBL_RST\([^)]*\))/bu_ptbl_reset(\1)/g
s/bu_ptbl(\([^,]*\),[[:space:]]*BU_PTBL_INS\([^)]*\))/bu_ptbl_ins(\1\2)/g
s/bu_ptbl(\([^,]*\),[[:space:]]*BU_PTBL_LOC\([^)]*\))/bu_ptbl_locate(\1\2)/g
s/bu_ptbl(\([^,]*\),[[:space:]]*BU_PTBL_ZERO\([^)]*\))/bu_ptbl_zero(\1\2/g
s/bu_ptbl(\([^,]*\),[[:space:]]*BU_PTBL_INS_UNIQUE\([^)]*\))/bu_ptbl_ins_unique(\1\2)/g
s/bu_ptbl(\([^,]*\),[[:space:]]*BU_PTBL_RM\([^)]*\))/bu_ptbl_rm(\1\2)/g
s/bu_ptbl(\([^,]*\),[[:space:]]*BU_PTBL_CAT\([^)]*\))/bu_ptbl_cat(\1\2)/g
s/bu_ptbl(\([^,]*\),[[:space:]]*BU_PTBL_FREE\([^)]*\))/bu_ptbl_free(\1)/g
        bu_ptbl() removed [7.18]

s/rt_ptalloc();/(struct rt_pt_node *)bu_malloc(sizeof(struct rt_pt_node), "rt_pt_node");/g
        rt_ptalloc() removed [7.16]

s/rt_db_free_internal(\([^,]+\),[^)]*)/rt_db_free_internal(\1)/g
	struct resource pointer parameter removed [7.14]
