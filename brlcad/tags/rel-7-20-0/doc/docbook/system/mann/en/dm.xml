<!-- Converted by db4-upgrade version 1.0 -->

<refentry xmlns="http://docbook.org/ns/docbook" version="5.0" xml:id="dm">
  
  <refmeta>
    <refentrytitle>DM</refentrytitle>
    <manvolnum>nged</manvolnum>
    <refmiscinfo class="source">BRL-CAD</refmiscinfo>
    <refmiscinfo class="manual">BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv xml:id="name">
    <refname>dm</refname>
    <refpurpose>
      Provides a means to interact with the display manager at a lower
      level.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv xml:id="synopsis">
    <cmdsynopsis sepchar=" ">
      <command>dm</command>    
      <arg choice="req" rep="norepeat"><replaceable>subcommand</replaceable></arg>
      <arg choice="opt" rep="norepeat"><replaceable>args</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsection xml:id="description"><info><title>DESCRIPTION</title></info>
    
    <para>
      Provides a means to interact with the display manager at a lower
      level. The <command>dm</command> command accepts the following subcommands:
    </para>
    <variablelist>
      <varlistentry>
	<term><command>set</command> 
	    <group choice="opt" rep="norepeat">   
	      <arg choice="opt" rep="norepeat"><replaceable>var</replaceable></arg>
	      <arg choice="opt" rep="norepeat"><replaceable>val</replaceable></arg>
	    </group>
	</term>
	<listitem>
	  <para>
	    The "set" subcommand provides a means to set or query display manager-specific
	    variables. Invoked without any arguments, the <emphasis>set</emphasis> subcommand 
	    will return a list of all available internal display manager variables. If only the 
	    <emphasis>var</emphasis> argument is specified, the value of that variable is 
	    returned. If both <emphasis>var</emphasis> and <emphasis>val</emphasis> are given, 
	    then <emphasis>var</emphasis> will be set to <emphasis>val</emphasis>.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><command>size</command><arg choice="opt" rep="norepeat"><replaceable>width height</replaceable></arg>
	</term>
	<listitem>
	  <para>
	    The "size" subcommand provides a means to set or query the window size. If no
	    arguments are given, the display manager's window size is returned. If 
	    <emphasis>width</emphasis> and <emphasis>height</emphasis> are specified, the 
	    display manager makes a request to have its window resized. Note that a size 
	    request is just that, a request, so it may be ignored, especially if the user 
	    has resized the window using the mouse.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><command>m</command>
	    <arg choice="req" rep="norepeat"><replaceable>button</replaceable></arg>
	    <arg choice="req" rep="norepeat"><replaceable>x y</replaceable></arg>
	</term>
	<listitem>
	  <para>
	    The "m" subcommand is used to simulate an <command>M</command> command. The 
	    <emphasis>button</emphasis> argument determines which mouse button is being 
	    used to trigger a call to this command. This value is used in the event 
	    handler to effect dragging the faceplate scrollbars. The <emphasis>x</emphasis> 
	    and <emphasis>y</emphasis> arguments are in X screen coordinates, which are 
	    converted to MGED screen coordinates before being passed to the 
	    <command>M</command> command.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term>
	    <command>am</command>
	    <arg choice="req" rep="norepeat"> 
	      <group choice="opt" rep="norepeat"> 
		<arg choice="opt" rep="norepeat">r</arg>
		<arg choice="opt" rep="norepeat">t</arg>
		<arg choice="opt" rep="norepeat">s</arg>
	      </group>
	    </arg>
	    <arg choice="req" rep="norepeat"><replaceable>xy</replaceable></arg>
	</term>
	<listitem>
	  <para>
	    The "am" subcommand effects <emphasis>mged's</emphasis> alternate mouse mode. The 
	    alternate mouse mode gives the user a different way of manipulating the view or 
	    an object. For example, the user can drag an object or perhaps rotate the view while 
	    using the mouse. The first argument indicates the type of operation to perform 
	    (i.e., <emphasis>r</emphasis> for rotation, <emphasis>t</emphasis> for translation, 
	    and <emphasis>s</emphasis> for scale). The <emphasis>x</emphasis> and 
	    <emphasis>y</emphasis> arguments are in X screen coordinates and are transformed 
	    appropriately before being passed to the knob command.
	  </para>
	</listitem>
      </varlistentry>
      
      <varlistentry>
	<term>
	    <command>adc</command>
	    <arg choice="req" rep="norepeat">
	      <group choice="opt" rep="norepeat">
		<arg choice="opt" rep="norepeat">1</arg>
		<arg choice="opt" rep="norepeat">2</arg>
		<arg choice="opt" rep="norepeat">t</arg>
		<arg choice="opt" rep="norepeat">d</arg>
	      </group>
	    </arg> 
	    <arg choice="req" rep="norepeat"><replaceable>xy</replaceable></arg>
	</term>
	<listitem>
	  <para>
	    The "adc" subcommand provides a way of manipulating the angle distance cursor
	    while using the mouse. The first argument indicates the type of operation to perform
	    (i.e., <emphasis>1</emphasis> for angle 1, <emphasis>2</emphasis> for angle 2, 
	    <emphasis>t</emphasis> for translate, and <emphasis>d</emphasis> for tick distance). 
	    The <emphasis>x</emphasis> and <emphasis>y</emphasis> arguments are in X screen 
	    coordinates and are transformed appropriately before being passed to the 
	    <command>adc</command> command (i.e., not "dm adc"). 
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term>
	    <command>con</command>
	    <arg choice="req" rep="norepeat">
	      <group choice="opt" rep="norepeat">
		<group choice="opt" rep="norepeat">
		  <arg choice="opt" rep="norepeat">r</arg>
		  <arg choice="opt" rep="norepeat">t</arg>
		  <arg choice="opt" rep="norepeat">s</arg>
		</group>
		<group choice="opt" rep="norepeat">     
		  <arg choice="opt" rep="norepeat">x</arg>
		  <arg choice="opt" rep="norepeat">y</arg>
		  <arg choice="opt" rep="norepeat">z</arg>
		</group>
	      </group>
	    </arg>
	    <arg choice="req" rep="norepeat"><replaceable>xpos ypos</replaceable></arg>
	</term>
	<listitem>
	  <para>
	    This form of the "con" subcommand provides a way to effect constrained
	    manipulation of the view or an object while using the mouse. This simulates the
	    behavior of sliders without taking up screen real estate. The first argument 
	    indicates the type of operation to perform (i.e., <emphasis>r</emphasis> for 
	    rotation, <emphasis>t</emphasis> for translation, and <emphasis>s</emphasis> for scale).
	    The &lt;<emphasis>x</emphasis>|<emphasis>y</emphasis>|<emphasis>z</emphasis>&gt; 
	    argument is the axis of rotation, translation, or scale. The <emphasis>xpos</emphasis> 
	    and <emphasis>ypos</emphasis> arguments are in X screen coordinates and are 
	    transformed appropriately before being passed to the knob command.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term>
	    <command>con</command>
	    <arg choice="req" rep="norepeat">a</arg>
	    <arg choice="req" rep="norepeat">
	      <group choice="opt" rep="norepeat">
		<arg choice="opt" rep="norepeat">x</arg>
		<arg choice="opt" rep="norepeat">y</arg>
		<arg choice="opt" rep="norepeat">1</arg>
		<arg choice="opt" rep="norepeat">2</arg>
		<arg choice="opt" rep="norepeat">d</arg>
	      </group>
	    </arg>
	    <arg choice="req" rep="norepeat"><replaceable>xpos ypos</replaceable></arg>
	</term>
	<listitem>
	  <para>
	    This form of the "con" subcommand provides a way to effect constrained
	    manipulation of the angle distance cursor while using the mouse. This simulates the
	    behavior of sliders without taking up screen real estate. The first argument indicates
	    that this is to be applied to the angle distance cursor. The next argument indicates the
	    type of operation to perform (i.e., <emphasis>x</emphasis> for translate in the 
	    <emphasis>x</emphasis> direction, <emphasis>y</emphasis> for translate in
	    the <emphasis>y</emphasis> direction, <emphasis>1</emphasis> for angle 1, 
	    <emphasis>2</emphasis> for angle 2, and <emphasis>d</emphasis>for tick distance). 
	    The <emphasis>xpos</emphasis> and <emphasis>ypos</emphasis> arguments are in X screen 
	    coordinates and are transformed appropriately before being passed to the knob 
	    command.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term>
	    <command>valid</command>
	    <arg choice="req" rep="norepeat">
	      <group choice="opt" rep="norepeat">
		<arg choice="opt" rep="norepeat">ogl</arg>
		<arg choice="opt" rep="norepeat">X</arg>
		<arg choice="opt" rep="norepeat">wgl</arg>
		<arg choice="opt" rep="norepeat">rtgl</arg>
		<arg choice="opt" rep="norepeat">...</arg>
	      </group>
	    </arg>
	</term>
	<listitem>
	  <para>
	    The "valid" subcommand provides a way to determine if a particular type of
	    display manager (X Windows, OpenGL, etc.) is available - if the display manager
            type is supported the string denoting that type (X, ogl, etc.) is returned back,
            otherwise nothing is returned.
  	  </para>
	</listitem>
      </varlistentry>
 
    </variablelist>
  </refsection>
   
    <refsection xml:id="examples"><info><title>EXAMPLES</title></info>
      
      <para>
	The examples show the use of the <command>dm</command> command with its various 
	subcommands as presented in the Description section.
      </para>
      <example><info><title>List the available display manager internal variables.</title></info>
	
	<para><prompt>mged&gt;</prompt> <userinput>dm set</userinput></para>
	<para>Lists the available display manager internal variables.</para>
      </example>
      
      <example><info><title>Turn on perspective projection in the display using 
	<emphasis>var</emphasis> and <emphasis>val</emphasis> arguments.</title></info>
	
	<para>
	  <prompt>mged&gt;</prompt><userinput>dm set perspective 1</userinput>
	</para>
	<para>Turns on perspective projection in the display.       
	</para>
      </example>
      
      <example><info><title>Query the display manager's window size.
	</title></info>
	
	<para>
	  <prompt>mged&gt;</prompt><userinput>dm size</userinput>
	</para>
	<para>The display manager's window size is returned.
	</para>
      </example>
      
      <example><info><title>Resize the display manager window.
	</title></info>
	
	<para>
	  <prompt>mged&gt;</prompt><userinput>dm size 900 900</userinput>
	</para>
	<para>The display manager window is resized to 900 x 900.
	</para>
      </example>
      
      <example><info><title>Simulate a button2 press at specific screen coordinates.
	</title></info>
	
	<para>
      <prompt>mged&gt;</prompt><userinput>dm m 2 100 200</userinput>
	</para>
	<para>Simulates a button2 press at (100, 200) in X screen coordinates.</para>
      </example>
      
      <example><info><title>Use the alternate mouse mode to rotate an image.
	</title></info>
	
	<para>
	  <prompt>mged&gt;</prompt><userinput>dm am r 400 100</userinput>
	</para>
	<para>Starts an alternate mouse mode rotation at (400, 100). </para>
      </example>
      
      <example><info><title>Start a tick distance manipulation with the mouse.
	</title></info>
	
	<para>
	  <prompt>mged&gt;</prompt><userinput>dm adc d 300 200</userinput>
	</para>
	<para>Starts a tick distance manipulation at (300, 200).</para>
      </example>
      
      <example><info><title>Start a constrained translation down the Z axis using the mouse.
	</title></info>
	
	<para>
	  <prompt>mged&gt;</prompt><userinput>dm con t z 200 200</userinput>
	</para>
	<para>Starts a constrained translation down the Z axis.
	</para>
      </example>
      
      <example><info><title>Start a constrained tick distance manipulation using the mouse.
	</title></info>
	
	<para>
	  <prompt>mged&gt;</prompt><userinput>dm con a d 200 100</userinput>
	</para>
	<para>Starts a constrained tick distance manipulation. 
	</para>
      </example>
      
      <example><info><title>End the mouse drag.
	</title></info>
	
	<para>
	  <prompt>mged&gt;</prompt><userinput>dm idle</userinput>
	</para>
	<para>Ends the drag.
	</para>
      </example>
      
    </refsection>
    
    <info><corpauthor>BRL-CAD Team</corpauthor></info>
    
    <refsection xml:id="bug_reports"><info><title>BUG REPORTS</title></info>
      
      <para>
	Reports of bugs or problems should be submitted via electronic
	mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
      </para>
    </refsection>
  </refentry>
