BRL-CAD Bugs
============

Bugs should be reported to the bug tracker on the project website at
http://sourceforge.net/tracker/?atid=640802&group_id=105292

The bugs and issues listed in here may or may not be the same as or
related to the bugs reported to the bug tracker.  In general, users
should not look to this file for information regarding the status of
bugs.  Informal bug-related information that is intended for
developers will generally be found here.  This may include short term
issues that are in active development as well as long term and
on-going issues.

Recent Bugs
-----------

* mged primitive editor doesn't accept/apply values to disk for
  certain primitives (e.g. sph)

* photon map cache file doesn't work (seems to crash rt on use)

* enabling perspective mode horks shaded mode, also Z clipping
  shouldn't be required to get the shading.

* an underlay framebuffer does not underlay with shaded mode

* bu_malloc() function and company take an unsigned int argument and
  will overflow without notification (since it is unsigned) resulting
  in less memory allocated than one requested.

* rtweight chokes on a .density file that has less than 3 values per
  line (infinite loop).  also gives infinite areas when presented with
  an empty .density file.

* libfb and libdm have hardcoded XMAXSCREEN and YMAXSCREEN values that
  are easily being surpassed on certain systems..

* setting src/librt/db5_io.c's AVS_ADD off in db5_import_attributes
  causes bad things to happen and it really shouldn't.

* vrml exporter (and probably others) doesn't export primitives
  directly as one might expect like the ray-tracers, only regions.

* if you turn off compilation of tcl/itcl, btclsh/mged fail to find
  their scripts.

* sketches extruded with non-square AB vectors results in inner sketch
  components not getting scaled properly.

* rtcheck is ignoring fastgen tagged geometry since no multioverlap
  handler is set.

* bot-bldxf fails in db_walk_tree(), interface may have changed

* bwmod gives a divide by zero error whenever you use the "-d" option

* you can't run a make benchmark unless you're compiling in place


Older Bugs
----------

* several manpages are missing or out of date for various tools

* X11 framebuffer often fails to display on certain middle bit depth
  displays. (e.g. 15 or 16 bit depth)


---
Bugs should be reported to the bug tracker on the project website at
http://sourceforge.net/tracker/?atid=640802&group_id=105292

BUGS items should be formated to column 70 (M-q in emacs), no tabs
