#ifndef _RENDER_HIT_H
#define _RENDER_HIT_H

#include "tie.h"

extern void* render_hit(tie_ray_t *ray, tie_id_t *id, tie_tri_t *tri, void *ptr);

#endif
