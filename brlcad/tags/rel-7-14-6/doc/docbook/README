This directory contains (as of Sept. 2008) the current docbook work on
Volume II and Volume II of the BRL-CAD Tutorial Series, as well as the oed
and tire articles.  It also holds the current test examples of the new
docbook based manual page conversions.

The default dtd and xsl stylesheets for docbook 4.5 are present
in the resources directory, and it is these copies that are
referenced in the docbook files.

The current structure is as follows:

articles/
	Individual articles on specific topics - oed and tire live here,
	as well as the articles that made up the appendices of VolIII.

books/
	Either compilations of smaller documents into large works, or
	individual self contained works.  Most of Volume III is in a
	single file in this subtree.

lessons/
	Individual documents intended to teach some aspect of BRL-CAD
	to new users/students.  At this time it contains the individual
	lessons that made up Volume II.

resources/
	The DocBook 4.5 dtd and xsl files are contained within the
	"standard" subdirectory.  Custom files will be organized under
	this directory in the future.

system/
	Documentation similar to traditional Unix "man" pages - focused
	documentation on specific parts of BRL-CAD.

English documents (the only language documentation is written in within
svn as of Sept. 2008) are in en subdirectories.  Other languages, when
implemented, will use standard two character sub directories.

These documents make extensive use of the XInclude modular documentation
specification, and as a consequence the contents of any one particular
"document" may be scattered over many other documents.  This design
direction is intended to allow re-use of standard definitions and
descriptions in multiple documents serving multiple purposes.  When
a definition is updated for one document, re-generation of other documents
will incorporate the update as well without requiring redundant editing. 

Notes:

* When rendered to html output and placed on a server, there may arise
  a problem where the server supplies the page as ISO-8859-1 encoded
  despite them actually using UTF-8.  To combat this problem in the
  case of the Apache web server, an .htaccess file in the directory 
  with the line:

  AddDefaultCharset UTF-8

  should let Apache know to use UTF-8 encoding for these pages.

