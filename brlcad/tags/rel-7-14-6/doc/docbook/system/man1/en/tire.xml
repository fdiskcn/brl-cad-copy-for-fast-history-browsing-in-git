<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2008-2009 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR AS IS'' AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='search1'>

<refmeta>
  <refentrytitle>TIRE</refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='tire_name'>
  <refname>tire</refname>
  <refpurpose>
    Generate a tire model, given standard tire dimensional information
  </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='tire_synopsis'>
  <cmdsynopsis>
    <command>tire</command>    
    <arg choice='opt' rep='repeat'><replaceable>options ...</replaceable></arg>
    <arg><replaceable>name</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='tire_description'>
  <title>DESCRIPTION</title>
  <para>
    <command>tire</command> creates a curved sidewall tire in either the current
    open database (when run from within MGED) or in <emphasis remap='I'>tire.g</emphasis>
    when run from the command line.  If no dimensions are supplied defaults are used.  The
    default is to <emphasis remap='I'>not</emphasis> model tread (due to performance 
    issues) but tread modeling is implemented and available as a user option with the
    <option>-t</option> flag.
  </para>
  <para>
    <option>-a #</option> instructs <command>tire</command> to automatically add 
    dimensional information to the toplevel object name used in the generated BRL-CAD 
    database tree.  Works with the <option>-n</option> option or standalone.  Output 
    format uses '-' instead of '/' because the latter has significance in BRL-CAD 
    object trees.
  </para>
</refsect1>

<refsect1 id='tire_options'>
  <title>OPTIONS:</title>
  <variablelist remap='TP'>
    <varlistentry>
      <term><option>-c count</option></term>
      <listitem>
	<para>
	  sets the number of tread <emphasis remap='I'>patterns</emphasis>
	  used when adding tread to a tire.  A tread pattern is the repeated shape
	  or combination of shapes used in tire tread definitions.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-d width/ratio/wheeldiam #</option></term>
      <listitem>
	<para>
	  This option accepts tire dimensions in the form of width of the tire
	  (in mm), ratio of sidewall height to tire width, and the diameter of
	  the wheel the tire is designed to fit over (in inches).  This is a
	  fairly standard method for specifying automotive tire dimensions,
	  and BRL-CAD can use these dimensions to create a model with approximately
	  those dimensions.  The '/' character will work but other options are
	  also allowed provided they don't have numeric significance - for example
	the common notation width/ratioRwheeldiam will work as-is.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-W width #</option></term>
      <listitem>
	<para>
	  Specify the maximum sidewall width of the tire in mm.  This option overrides
	  the -d option if used together, or the default if used alone.  Can be used
	  with the -R and -D flags.  Unlike the -d flag, this flag allows floating
	  point input - use this flag if you need to specify width more precisely
	  than 1mm.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-R ratio #</option></term>
      <listitem>
	<para>
	  Specify the ratio of the sidewall height to the maximum tire width.  This
	  option overrides the -d option if used together, or the default if used
	  alone.  Can be used with the -W and -D flags.  Unlike the -d flag, allows 
	floating point input for more precise definition of ratio.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-D rim diameter #</option></term>
      <listitem>
	<para>
	  Specify the diameter of the rim (a.k.a the steel wheel) in inches. This
	  option overrides the -d option if used together, or the default if used
	  alone.  Can be used with the -W and -R flags.  Unlike the -d flag, allows 
	  floating point input for more precise definition of rim diameter.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-g depth #</option></term>
      <listitem>
	<para>
	  Specify the tread depth in 32nds of an inch.  So, for example, -g 13 would
	  create a tread depth of 13/32in.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-j width #</option></term>
      <listitem>
	<para>Specify the rim width in inches.</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-n name #</option></term>
      <listitem>
	<para>
	  allows the user to specify a name other than 'tire' for the root name
	  of the toplevel database object.  Works with the
	  <emphasis remap='B'>\a</emphasis>
	  option or standalone.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-p type#</option></term>
      <listitem>
	<para>
	  Select tread pattern to model.  Note: tread modeling can SIGNIFICANTLY 
	  lengthen raytracing times, depending on the tread.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-s radius#</option></term>
      <listitem>
	<para>
	  Set the radius from the tire center of the maximum width point on the 
	  sidewall.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-t type#</option></term>
      <listitem>
	<para>
	  Select tread shape to model.  Note: tread modeling can SIGNIFICANTLY 
	  lengthen raytracing times, depending on the tread.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-u thickness#</option></term>
      <listitem>
	<para>Specify tire thickness in mm.</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-w #</option></term>
      <listitem>
	<para>
	  Specify wheel/rim type.  If 0, include no wheel.  As of 7.12.6 only
	  one wheel type is included by default.
	</para>
      </listitem>
    </varlistentry>
  </variablelist>
</refsect1>

<refsect1 id='tire_examples'>
  <title>EXAMPLE</title>
  <para>
    The following will create a tire with width=185mm, ratio=65mm, and wheel
    diameter = 15in.  The <option>-t</option> flag will instruct the procedure to 
    model using tread type one, the <option>-n</option> flag will set the root 
    name to 'autowheel' instead of tire, and the <option>-a</option>
    flag will append '-185-65R15' to 'autowheel' to create the final
    toplevel name 'autowheel-185-65R15' in the database rather than 'tire'.
  </para>
  <example>
    <title>Creating a 185/65R15 tire with wheel</title>
    <para>
      <userinput>tire -d 185/65R15 -t 1 -a -n autowheel</userinput>
    </para>
  </example>

</refsect1>

<refsect1 id='diagnostics'>
  <title>DIAGNOSTICS</title>
  <para>
    Not much error checking yet - need to add.
  </para>
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>Clifford Yapp</para>
</refsect1>

<refsect1 id='copyright'>
  <title>COPYRIGHT</title>
  <para>
    This software is Copyright (c) 2008-2009 United States Government as
    represented by the U.S. Army Research Laboratory. All rights reserved.
  </para>
</refsect1>

<refsect1 id='bugs'>
  <title>BUGS</title>
  <para>
    There is a known problem where specifying very thin tires with a large
    inner wheel will cause incorrect geometry to be produced - this has to do
    with how the elliptical torus primitive is handled.  Other extreme cases
    such as extremely thin tire walls would also be expected to cause problems.
  </para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

