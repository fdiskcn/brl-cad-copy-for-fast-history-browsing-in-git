/*
 *			D C O N V . C
 *
 *  Fast FFT based convolution
 *
 *  This uses the overlap-save method to achieve a linear convolution
 *  (straight FFT's give you a circular convolution).
 *  An M-point kernel is convolved with N-point sequences (in xform space).
 *  The first M-1 points are incorrect, while the remaining points yield
 *  a true linear convolution.  Thus the first M-1 points of each xform
 *  are thrown away, while the last M-1 points of each input section
 *  are saved for the next xform.
 */
#include <stdio.h>
#include <math.h>
#include "machine.h"

#define	MAXM	4096

double	savebuffer[MAXM-1];
double	xbuf[2*MAXM];
double	ibuf[2*MAXM];		/* impulse response */

static char usage[] = "\
Usage: dconv filter < doubles > doubles\n\
 XXX Warning: kernal size must be 2^i - 1\n";

main( argc, argv )
int	argc;
char	**argv;
{
	int	i;
	int	N, M, L;
	FILE	*fp;

	M = 128;	/* kernel size */
	N = 2*M;	/* input sub-section length (fft size) */
	L = N - M + 1;	/* number of "good" points per section */

	if( argc != 2 || isatty(fileno(stdin)) || isatty(fileno(stdout)) ) {
		fprintf( stderr, usage );
		exit( 1 );
	}

#ifdef never
	/* prepare the kernel(!) */
	/* this is either the direct complex response,
	 *  or the FT(impulse resp)
	 */
	for( i = 0; i < N; i++ ) {
		if( i <= N/2 )
			ibuf[i] = 1.0;	/* Real part */
		else
			ibuf[i] = 0.0;	/* Imag part */
	}
#endif /* never */

	if( (fp = fopen( argv[1], "r" )) == NULL ) {
		fprintf( stderr, "dconv: can't open \"%s\"\n", argv[1] );
		exit( 2 );
	}
	if( (M = fread( ibuf, sizeof(*ibuf), 2*MAXM, fp )) == 0 ) {
		fprintf( stderr, "dconv: problem reading filter file\n" );
		exit( 3 );
	}
	fclose( fp );
	if( M > MAXM ) {
		fprintf( stderr, "dconv: only compiled for up to %d sized filter kernels\n", MAXM );
		exit( 4 );
	}
/*XXX HACK HACK HACK HACK XXX*/
/* Assume M = 2^i - 1 */
M += 1;
	N = 2*M;	/* input sub-section length (fft size) */
	L = N - M + 1;	/* number of "good" points per section */

	if( N == 256 )
		rfft256( ibuf );
	else
		rfft( ibuf, N );

	while( (i = fread(&xbuf[M-1], sizeof(*xbuf), L, stdin)) > 0 ) {
		if( i < L ) {
			/* pad the end with zero's */
			bzero( &xbuf[M-1+i], (L-i)*sizeof(*savebuffer) );
		}
		bcopy( savebuffer, xbuf, (M-1)*sizeof(*savebuffer) );
		bcopy( &xbuf[L], savebuffer, (M-1)*sizeof(*savebuffer) );

		/*xform( xbuf, N );*/
		if( N == 256 )
			rfft256( xbuf );
		else
			rfft( xbuf, N );

		/* Mult */
		mult( xbuf, ibuf, N );

		/*invxform( xbuf, N );*/
		if( N == 256 )
			irfft256( xbuf );
		else
			irfft( xbuf, N );

		fwrite( &xbuf[M-1], sizeof(*xbuf), L, stdout );
	}
}

/*
 *  Multiply two "real valued" spectra of length n
 *  and put the result in the first.
 *  The order is: [Re(0),Re(1)...Re(N/2),Im(N/2-1),...,Im(1)]
 *    so for: 0 < i < n/2, (x[i],x[n-i]) is a complex pair.
 */
mult( o, b, n )
double	o[], b[];
int	n;
{
	int	i;
	double	r;

	/* do DC and Nyquist components */
	o[0] *= b[0];
	o[n/2] *= b[n/2];

	for( i = 1; i < n/2; i++ ) {
		r = o[i] * b[i] - o[n-i] * b[n-i];
		o[n-i] = o[i] * b[n-i] + o[n-i] * b[i];
		o[i] = r;
	}
}
