.TH RT 1 BRL/CAD
.SH NAME
rt \- raytrace an \fBmged\fP model, giving a shaded picture
.SH SYNOPSIS
.B rt
[ options ... ]
model.g
objects ...
.SH DESCRIPTION
.I Rt
operates on the indicated
.I objects
in the input
.I model.g
and produces a color shaded image as the output.
By default, the output is placed on the current framebuffer
as it is computed.  The environment variable FB_FILE specifies
the current framebuffer, see
.IR brlcad (1).
Alternatively, the output can be stored in a pixel file
.RI ( pix (5)
format)
by specifying
.B \-o
.B output.pix
in the option list.
The orientation of the rays to be fired may be specified by
the
.B \-a
and
.B \-e
options, in which case the model will be autosized, and the grid
will be centered on the centroid of the model, with ray spacing
chosen to span the entire set of
.I objects.
Alternatively,
with the
.B \-M
option, a transformation matrix may be provided on standard input
which maps model-space to view-space.
In this case, the grid ranges from -1.0 <= X,Y <= +1.0 in view space,
with the size of the grid (number of rays fired) specified with
.BR \-s .
The
.B \-M
option is most useful when
.B rt
is being invoked from a shell script created by an
.IR mged (1)
\fIsaveview\fR command.
.LP
The following options are recognized:
.TP
.B \-a#
selects auto-sizing, and gives the view azimuth in degrees.  Used with
.B \-e
and conflicts with
.B \-M
.TP
.B \-b ``# #''
is sometimes useful for debugging.  A single ray is fired at the pixel
coordinates specified.
This is best when accompanied by debugging options.  Note that the standard
.IR getopt (3)
routine only allows options to have one parameter, so the X and Y
pixel coordinates need to be combined into one string parameter by enclosing
them in double quotes.  This option must follow any viewing parameters.
.TP
.B \-c ``script_command''
is used to supply on the command line any command that can appear
in a
.B \-M
command script.
For example,
.B \-c ``set''
will print the values of all settable variables.
.TP
.B \-e#
selects auto-sizing, and gives the view elevation in degrees.  Used with
.B \-a
and conflicts with
.B \-M
.TP
.B \-g#
is used to set the grid cell width, in milimeters.
Ordinarily, the cell width is computed by dividing the view size by
the number of pixels of width, so this option need not be used.
To obtain exactly 1 inch ray spacing, specify
.B \-g25.4.
If the grid cell width is specified and
the grid cell height is \fInot\fR specified,
the grid cell height defaults to be the same as the width.
.TP
.B \-i
enables \fIincremental mode\fR processing.
First a single ray is fired.
Then, the screen is subdivided into four parts by evenly subdividing
in the vertical and horizontal directions.
The previously fired ray was the lower left corner;
three more rays are fired.
This process recurses until full resolution has been reached.
Assumes square image.
Also assumes fast path between CPU and display hardware.
.TP
.B \-l#
Select lighting model.  Default is 0.
Model 0 is a full lighting model with the ability to implement
Phong shading, transparant and reflective objects, shadow penumbras,
texture maps, etc.
In addition to ambient light, a small amount of light is
supplied from eye position.
All objects in the active model space with a material property
string of ``light'' represent additional light sources
(up to 16 are presently permitted),
and shadow computations will be initiated automaticly.
(This mechanism is subject to further change).
Model 1 is a diffuse-lighting model only, and is intended for
debugging.
Model 2 displays the surface normals as colors, and is useful
for examining curvature and surface orientation.
Model 3 is a three-light diffuse-lighting model, and is intended
for debugging.
Model 4 is a curvature debugging display, showing the inverse
radius of curvature.
Model 5 is a curvature debugging display, showing the principal
direction vector.
.TP
.B \-n#
defines the height of the image as a number of scanlines.
.TP
.B \-o
.B output.pix
Specify the name of a file to store the
.IR pix (5)
format output.
The
.IR pix-fb (1)
utility can be used to later display ``.pix'' files.
.TP
.B \-p#
Sets the perspective, in degrees, ranging  0 <= # < 180.
.TP
.B \-r#
requests that overlapping regions be reported.
.TP
.B \-s#
specifies the
number of rays to fire in the X and Y directions on a square grid.
The default size is 512 pixels.
There is no maximum size limit.
.TP
.B \-w#
defines the width of each scanline in pixels.
.TP
.B \-x#
sets the
.IR librt (3)
debug flags to the given hexadecimal bit vector.
See librt/debug.h for the bit definitions.
.TP
.B \-A#
sets the ambient light intensity,
as a fraction of the total light in the scene
in the range of 0.0 to 1.0.
.TP
.B \-B
The ``benchmark'' flag.  When specified, all intentional random effects
such as ray dither, color dither, etc, are turned off, to allow
benchmark timing comparison and benchmark result comparison.
.TP
.B \-C#/#/#
sets the background color to the RGB value #/#/#, where each #
is in the range of 0 to 255.
All non-background colors will be dithered away from this value.
.TP
.B \-D#
The "desired frame" flag, specifies frame number to start with.
.TP
.B \-E#
sets the distance from the eye point to the center of the model RPP.
Only useful with auto-sizing, conflicts with
.B \-M
.TP
.B \-F\ framebuffer
indicates that the output should be sent to the indicated framebuffer.
See
.IR libfb (3)
for more details on the specification of a framebuffer.
.TP
.B \-G#
sets the grid cell height, in milimeters.
.TP
.B \-H#
The "hypersample" flag, specifies number of extra rays to fire
for each pixel, to obtain more accurate results
without needing to store the extra pixels.
Causes
.B \-J1
to be set.
The final colors of all rays are averaged together.
Better results can be obtained by simply increasing the resolution,
and decimating the results with a filter such as
.IR pixhalve (1).
.B \-I
Turns on interactive mode.  If this flag is off, and your image is
"large" (>= 256x256) it will lower the priority of the rt process.
.TP
.B \-J#
The "jitter" flag, if nonzero randomizes the point from which the ray
is fired by +/- one half of the pixel size.  Useful if doing your own
post filtering for antialiasing, or to eliminate systematic
errors.  This has been useful in obtaining more accurate results
in a volume-calculating raytrace as well.
.TP
.B \-K#
sets the final (kill-after) frame number.
Used with animation scripts in conjunction with
.B \-D#
.TP
.B \-M
Read animation matrix or animation script from standard input.
Conflicts with
.B \-a
and
.B \-e
.TP
.B \-O
.B output.dpix
Specify the name of a file to store the double-precision floating-point
version of the image.
.IR dpix-pix (1)
can be used to later convert the file to
.IR pix (5)
format output.
(Deferred implementation).
.TP
.B \-P#
Specify the maximum number of processors (in a multi-processor system) to be
used for this execution.  The default is system dependent.
On ``dedicated'' multi-processors, such as workstations and super-minis,
the default is usually set for the maximum number of processors,
while on shared multi-processors, such as SuperComputers,
usually just one processor is used by default.
.TP
.B \-S
Turns on stereo viewing.
The left-eye image is drawn in red,
and the right-eye image is drawn in blue.
.TP
.B \-U#
Sets the "use_air" value.
Default is 0, which ignores regions that have non-zero aircode values.
.TP
.B \-V#
Sets the view aspect.  This is the ratio of width over height and can
be specified as either a fraction or a colon-separated ratio.
For example, the NTSC aspect ratio can be specified by either
.B \-V1.33
or
.B \V4:3
.TP
.B \-X#
Turns on the
.I rt (1)
program debugging flags.  See rt/rdebug.h for the meaning of these bits.
.TP
.LP
The
.B rt
program is a simple front-end to
.IR librt (3)
which can be invoked directly, or via the
.B rt
command in
.IR mged (1).
.SH "SEE ALSO"
brlcad(1), mged(1), lgt(1), pix-fb(1), rtray(1), rtpp(1),
librt(3), ray(5V), pix(5).
.SH DIAGNOSTICS
Numerous error conditions are possible.
Descriptive messages are printed on standard error.
.SH AUTHOR
Michael John Muuss
.SH SOURCE
SECAD/VLD Computing Consortium, Bldg 394
.br
The U. S. Army Ballistic Research Laboratory
.br
Aberdeen Proving Ground, Maryland  21005
.SH COPYRIGHT
This software is Copyright (C) 1985 by the United States Army.
All rights reserved.
.SH BUGS
Most deficiencies observed while using the
.B rt
program are usually with the
.IR librt (3)
package instead.
.SH "BUG REPORTS"
Reports of bugs or problems should be submitted via electronic
mail to <CAD@BRL.MIL>.
