/*
	Author:		Gary S. Moss
			U. S. Army Ballistic Research Laboratory
			Aberdeen Proving Ground
			Maryland 21005-5066
			(301)278-6651 or DSN 298-6651
*/
#if ! defined( INCL_FB )
#include "fb.h"
#endif
#if ! defined( _VLD_STD_H_ )
#include "./std.h"
#endif
#if ! defined( INCL_ASCII )
#include "./ascii.h"
#endif
#if ! defined( INCL_FONT )
#include "./font.h"
#endif
#if ! defined( INCL_TRY )
#include "./try.h"
#endif

/* Set pre-processor switch for getting signal() handler declarations right.
 */
#if defined(sun) && ! defined(SunOS4)
/* For Suns running older releases, compile with -DSunOS4=0 to suppress
	bogus warning messages. */
#define SunOS4	1
#endif
#if __STDC__ || (defined(SYSV) && ! defined(cray)) || SunOS4
#define STD_SIGNAL_DECLS 1
#else
#define STD_SIGNAL_DECLS 0
#endif

/* For production use, set to "static" */
#ifndef STATIC
#define STATIC static
#endif

typedef struct
	{
	int p_x;
	int p_y;
	}
Point;

typedef struct
	{
	Point r_origin;
	Point r_corner;
	}
Rectangle;

typedef struct
	{
	RGBpixel *n_buf;
	int n_wid;
	int n_hgt;
	}
Panel;

extern FBIO *fbp;
extern RGBpixel *menu_addr;
extern RGBpixel paint;
extern Point cursor_pos;
extern Point image_center;
extern Point windo_center;
extern Point windo_anchor;
extern Try *try_rootp;
extern char cread_buf[BUFSIZ*10], *cptr;
extern char macro_buf[];
extern char *macro_ptr;
extern int brush_sz;
extern int gain;
extern int pad_flag;
extern int remembering;
extern int report_status;
extern int reposition_cursor;
extern int tty;
extern int tty_fd;
extern int zoom_factor;
extern int LI, CO;

extern Func_Tab	*get_Func_Name();
extern RGBpixel *get_Fb_Panel();
extern char *char_To_String();
extern int add_Try();
extern int bitx();
extern int fb_Init_Menu();
extern int getpos();
extern int get_Input();
extern void fb_Get_Pixel();
extern void pos_close();
extern void init_Status();
extern void init_Tty(), restore_Tty();
extern void prnt_Status();
extern void prnt_Usage();
#if __STDC__
extern void prnt_Scroll( char * fmt, ... );
#else
extern void prnt_Scroll();
#endif
extern void prnt_Rectangle();
extern void do_Key_Cmd();

#define MAX_LN			81
#define Toggle(f)		(f) = ! (f)
#define De_Bounce_Pen()		while( do_Bitpad( &cursor_pos ) ) ;
#define BOTTOM_STATUS_AREA	2
#define TOP_SCROLL_WIN		(BOTTOM_STATUS_AREA-1)
#define PROMPT_LINE		(LI-2)
#define MACROBUFSZ		(BUFSIZ*10)
#define Malloc_Bomb() \
		fb_log(	"\"%s\"(%d) Malloc() no more space.\n", \
				__FILE__, __LINE__ \
				); \
		return 0;
