<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='e_muves'>

<refmeta>
  <refentrytitle>E_MUVES</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>e_muves</refname>
  <refpurpose>Displays the <emphasis>BRL-CAD</emphasis> regions that are part of the indicated
	MUVES components.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>e_muves</command>    
     <arg choice='req'><replaceable>MUVES_component1</replaceable></arg>
     <arg rep='repeat'><replaceable>MUVES_component2</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Displays the <emphasis>BRL-CAD</emphasis> regions that are part of the indicated
	MUVES components.  The internal list of MUVES components must have been
	created earlier by the <command>read_muves</command> command. The MUVES components listed on the
	command line must not use any wildcards (the expansion will result in <emphasis>BRL-CAD</emphasis>
	objects, not MUVES components).
   </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The example shows the use of the <command>e_muves</command> command to display the <emphasis>BRL-CAD</emphasis> regions that make up the specified MUVES components.
  </para>
  <example>
    <title>Display the <emphasis>BRL-CAD</emphasis> regions that make up the specified MUVES components.</title>
    <variablelist>
      <varlistentry>
	   <term><prompt>mged></prompt> <userinput>e_muves fuel transmission</userinput></term>
	   <listitem>
	     <para>Displays the BRL-CAD regions that make up the MUVES components named �fuel�
			and �transmission.�
	     </para>
	   </listitem>
      </varlistentry>
    </variablelist>
  </example>
       
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

