<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='adc'>
  
  <refmeta>
    <refentrytitle>adc</refentrytitle>
    <manvolnum>nged</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv id='name'>
    <refname>adc</refname>
    <refpurpose>This command controls the angle/distance cursor.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>adc</command>    
      <arg>-i</arg>
      <arg choice='plain'><replaceable>subcommand</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      This command controls the angle/distance cursor. The <command>adc</command> command with no
      arguments toggles the display of the angle/distance cursor (ADC). The <emphasis>-i</emphasis> option, if
      specified, causes the given value(s) to be treated as increments. Note that the <emphasis>-i</emphasis>
      option is ignored when getting values or when used with subcommands where this
      option makes no sense. You can also control the position, angles, and radius of the
      ADC using a knob or the knob command. This command accepts the following subcommands:
    </para>
    <variablelist>
      <varlistentry>
	<term><command>vars</command></term>
	<listitem>
	  <para>
	    Returns a list of all ADC variables and their values (i.e., var = val).
	  </para>
	</listitem>
      </varlistentry>
      
      <varlistentry>
	<term><command>draw [<emphasis>0|1</emphasis>]</command></term>
	<listitem>
	  <para>
	    Set or get the draw parameter.
	  </para>
	</listitem>
      </varlistentry>
      
      <varlistentry>
	<term><command>a1 [<emphasis>angle</emphasis>]</command></term>
	<listitem>
	  <para>
	    Set or get angle1 in degrees.
	  </para>
	</listitem>
     </varlistentry>

     <varlistentry>
       <term><command>a2 [<emphasis>angle</emphasis>]</command></term>
       <listitem>
	 <para>
	   Set or get angle2 in degrees.
	 </para>
       </listitem>
     </varlistentry>
     
     <varlistentry>
       <term><command>dist [<emphasis>distance</emphasis>]</command></term>
       <listitem>
	 <para>
	   Set or get radius (distance) of tick in local units.
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>odist [<emphasis>distance</emphasis>]</command></term>
       <listitem>
	 <para>
	   Set or get radius (distance) of tick (+-2047).
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>hv [<emphasis>position</emphasis>]</command></term>
       <listitem>
	 <para>
	   Set or get position (grid coordinates and local units).
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>xyz [<emphasis>position</emphasis>]</command></term>
       <listitem>
	 <para>
	   Set or get position (model coordinates and local units).
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>x [<emphasis>xpos</emphasis>]</command></term>
       <listitem>
	 <para>
	   Set or get horizontal position (+-2047).
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>y [<emphasis>ypos</emphasis>]</command></term>
       <listitem>
	 <para>
	   Set or get vertical position (+-2047).
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>dh <emphasis>distance</emphasis></command></term>
       <listitem>
	 <para>
	   Add to horizontal position (grid coordinates and local units).
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>dv <emphasis>distance</emphasis></command></term>
       <listitem>
	 <para>
	   Add to vertical position (grid coordinates and local units).
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>dx <emphasis>distance</emphasis></command></term>
       <listitem>
	 <para>
	   Add to <emphasis>x</emphasis> position (model coordinates and local units).
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>dy <emphasis>distance</emphasis></command></term>
       <listitem>
	 <para>
	   Add to <emphasis>y</emphasis> position (model coordinates and local units).
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>dz <emphasis>distance</emphasis></command></term>
       <listitem>
	 <para>
	   Add to <emphasis>z</emphasis> position (model coordinates and local units).
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>anchor_pos [<emphasis>0|1</emphasis>]</command></term>
       <listitem>
	 <para>
	   Anchor ADC to current position in model coordinates.
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>anchor_a1 [<emphasis>0|1</emphasis>]</command></term>
       <listitem>
	 <para>
	   Anchor angle1 to go through anchorpoint_a1.
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>anchor_a2 [<emphasis>0|1</emphasis>]</command></term>
       <listitem>
	 <para>
	   Anchor angle2 to go through anchorpoint_a2.
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>anchor_dst [<emphasis>0|1</emphasis>]</command></term>
       <listitem>
	 <para>
	   Anchor tick distance to go through anchorpoint_dst.
	 </para>
       </listitem>
     </varlistentry>

     <varlistentry>
       <term><command>anchorpoint_a1[<emphasis>x y z</emphasis>]</command></term>
       <listitem>
	 <para>
	   Set or get anchor point for angle1 (model coordinates and local units).
	 </para>
       </listitem>
     </varlistentry>
     
     <varlistentry>
       <term><command>anchorpoint_a2 [<emphasis>x y z</emphasis>]</command></term>
       <listitem>
	 <para>
	   Set or get anchor point for angle2 (model coordinates and local units).
	 </para>
       </listitem>
     </varlistentry>
     
     <varlistentry>
       <term><command>anchorpoint_dst [<emphasis>x y z</emphasis>]</command></term>
       <listitem>
	 <para>
	   Set or get anchor point for tick distance (model coordinates and local units). 
	 </para>
       </listitem>
     </varlistentry>
     
     <varlistentry>
       <term><command>reset</command></term>
       <listitem>
	 <para>
	   Reset all values to their defaults.
	 </para>
       </listitem>
     </varlistentry>
     
     <varlistentry>
       <term><command>help</command></term>
       <listitem>
	 <para>
	   Print the help message.
	 </para>
       </listitem>
     </varlistentry>
   </variablelist>
 </refsect1>
 
 <refsect1 id='examples'>
   <title>EXAMPLES</title>
   <para>
     The examples show the use of the <command>adc</command> command with no arguments, 
     with <emphasis>i</emphasis>, and with subcommands.
   </para>
   <example>
      <variablelist>
       <varlistentry>
	 <term><prompt>mged></prompt> <userinput>adc</userinput></term>
	 <listitem>
	   <para>
	     Toggle display of angle/distance cursor.
	   </para>
	 </listitem>
       </varlistentry>
      </variablelist>
   </example>
   <example>
     <variablelist>
       <varlistentry>
	 <term><prompt>mged></prompt> <userinput>adc a1 37.5</userinput></term>
	 <listitem>
	   <para>
	     Sets angle1 to 37.5 degrees.
	   </para>
	 </listitem>
       </varlistentry>
      </variablelist>
   </example>
   <example>
     <variablelist>
       <varlistentry>
	 <term><prompt>mged></prompt> <userinput>adc a1 37.5</userinput></term>
	 <listitem>
	   <para>
	     Gets angle1.
	   </para>
	 </listitem>
       </varlistentry>
      </variablelist>
   </example>
   <example>
     <variablelist>
       <varlistentry>
	 <term><prompt>mged></prompt> <userinput>adc xyz 100 0 0</userinput></term>
	 <listitem>
	   <para>
	     Moves ADC position to (100,0,0), model coordinates and local units.
	   </para>
	 </listitem>
       </varlistentry>
     </variablelist>
   </example>
 </refsect1>

 <refsect1 id='author'>
   <title>AUTHOR</title>
   <para>BRL-CAD Team</para>
 </refsect1>
 
 <refsect1 id='bug_reports'>
   <title>BUG REPORTS</title>
   <para>
     Reports of bugs or problems should be submitted via electronic
     mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
   </para>
 </refsect1>
</refentry>

