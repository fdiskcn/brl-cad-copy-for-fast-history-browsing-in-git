<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='r'>

<refmeta>
  <refentrytitle>R</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>r</refname>
  <refpurpose>Creates a region with the specified <emphasis>region_name</emphasis>.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>r</command>    
    
    <arg choice='req'><replaceable>region_name</replaceable></arg>
    <arg choice='req'><replaceable>operation object</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Creates a region with the specified <emphasis>region_name</emphasis>.
	The <emphasis>region</emphasis> is constructed using the list of Boolean <emphasis>operations</emphasis> 	and <emphasis>object</emphasis>pairs. The operators are represented by the single characters �+,� �-,� 	and �u� for intersection, subtraction, and union, respectively. The <emphasis>object</emphasis> 	associated with each operator may be a combination or a primitive shape. No parentheses or any grouping 	indication is allowed in the <command>r</command> command. The operator hierarchy for the <command>r 	</command> command has been established through the ancestry of BRL-CAD and does not conform to accepted 	standards (see the <command>c</command> command for a more standard implementation). Intersection and 	subtraction operations are performed first, proceeding left to right; then union operations are 	performed. BRL-CAD regions are special cases of BRL-CAD <emphasis>combinations</emphasis> and include 	special attributes. Default values for these attributes may be set using the <command>regdef</command> 	command. As new <emphasis>regions</emphasis> are built, the default ident number gets incremented. If 	<emphasis>region_name</emphasis> already exists, then the <emphasis>operation/object</emphasis> pairs 	get appended to its end.
  </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The example shows the use of the <command>r</command> command to create a region consisting of two parts 	unioned together.
   </para>
  
  <example>
    <title>Create a region using operations and object pairs.</title>
    <para>
      <prompt>mged></prompt><userinput>r new_region u shape1 - shape2 u shape3 + group4</userinput>
    </para>
    <para>Creates a region named <emphasis>new_region</emphasis> that consists of two parts unioned together. The
	first part is <emphasis>shape1</emphasis> with <emphasis>shape2</emphasis> subtracted. The second part is 	the intersection of <emphasis>shape3</emphasis> and the combination <emphasis>group4</emphasis>.       
    </para>
  </example>
 
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

