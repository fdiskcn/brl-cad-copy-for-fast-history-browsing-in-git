<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
               "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->


<refentry id='rtarea1'>
  <refmeta>
    <refentrytitle>RTAREA</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
    <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  <refnamediv id='name'>
    <refname>rtarea</refname>
    <refpurpose>Calculate exposed and presented surface areas</refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>rtarea</command>    
      <arg choice='opt' rep='repeat'><replaceable>options</replaceable></arg>
      <arg choice='plain'><replaceable>model.g</replaceable></arg>
      <arg choice='plain' rep='repeat'><replaceable>objects</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      The <emphasis remap='I'>rtarea</emphasis> command calculates the exposed, presented and centers of 
      surface areas of specified geometry objects from a particular view.  The summary 
      output includes details for individual regions, assemblies (combinations that contain
      regions), and summarized totals.  
    </para>
    <para>
      The <emphasis remap='B'>exposed </emphasis> area corresponds to what a human observer would see looking at the object(s)
      in question, if all the objects were non-transparent.  For example, say rays were cast towards two cubes, each one of which
      is a region, one in front of the other, grouped into an assembly, with the ray direction pointed into the page:
      <screen>
|----------------| Rear Cube
|                |
|                |
|        |--------------| Front Cube
|        |       |      |
|        |       |      |
|--------|-------|      |
         |              |
         |              |
         |--------------|
      </screen>

      The exposed area of the Front Cube corresponds to the total area of the sheet, since it is not
      occluded by any other object:
       <screen>
|----------------| Rear Cube
|                |
|                |
|        |--------------| Front Cube
|        |**************|
|        |**************|
|--------|**************|
         |**************|
         |**************|
         |--------------|
      </screen>

     

      The Rear Cube, on the other hand, IS occluded by the Front Cube and would thus report only a fraction
      of its area as exposed:
       <screen>
|----------------| Rear Cube
|****************|
|****************|
|********|--------------| Front Cube
|********|       |      |
|********|       |      |
|--------|-------|      |
         |              |
         |              |
         |--------------|
      </screen>

     

      An object that combines both of these sheets would thus report as its exposed area the
      sum of the exposed areas of the individual sheets:
      <screen>
|----------------| Rear Cube
|****************|
|****************|
|****************|------| Front Cube
|***********************|
|***********************|
|--------|**************|
         |**************|
         |**************|
         |--------------|
      </screen>


   </para>
   <para>
      <emphasis remap='B'>Presented</emphasis> area, unlike exposed area, does not take occlusion into account.  It is the
      equalivent of taking each object and considering it in isolation.  So for the previous
      case of two sheets of metal, the Front Cube would report the same area:

       <screen>
|----------------| Rear Cube
|                |
|                |
|        |--------------| Front Cube
|        |**************|
|        |**************|
|--------|**************|
         |**************|
         |**************|
         |--------------|
      </screen>



      the Rear Cube, on the other hand, ALSO reports its full area:
      <screen>
|----------------| Rear Cube
|****************|
|****************|
|****************-------| Front Cube
|****************|      |
|****************|      |
|--------|-------|      |
         |              |
         |              |
         |--------------|
      </screen>


      When reporting the presented area of the group, there is a subtle point - rtarea does not count area covered
      by multiple regions multiple times when totaling the presented area.  So for the group containing both cubes,
      the presented area is NOT the cumulative area of both regions' presented areas but the area of the union of those
      two areas.  In this case, that works out to be the same as the exposed area.
       <screen>
|----------------| Rear Cube
|****************|
|****************|
|****************|------| Front Cube
|***********************|
|***********************|
|--------|**************|
         |**************|
         |**************|
         |--------------|
      </screen>

          
    </para>

    <para>
      It should be noted that rtarea only counts surfaces with a surface normal that
      has a component looking back in the direction of the ray.  So, for example, a
      cube with only one face looking "back toward" the ray will not report area from
      other faces as "presented", since their surface normals will indicate they are
      not surfaces being presented to the ray.
    </para>

    <para>
      WARNING: <emphasis remap='I'>Rtarea</emphasis> may not correctly report area or 
      center when instancing is done at the group level. Using <command>xpush</command> 
      can be a workaround for this problem.
    </para>

    <para>
      The model geometry is the list of <emphasis remap='I'>objects</emphasis>
      in the input database <emphasis remap='I'>model.g</emphasis>.
    </para>
    
    <variablelist remap='TP'>
      <varlistentry>
	<term><option>-X #</option></term>
	<listitem>
	  <para>
	    Sets rtarea debug flags to a (hexadecimal) number.
	    A flag value of 1 causes 3-D plot output to be written to stdout.
	    All solid RPPs are plotted as grey boxes, and the walk path is
	    in alternating blue and green segments.
	    A flag value of 2 adds red segments for all attempted steps,
	    as well as the final steps.
	    A flag value of 3 adds some debugging prints to stderr in addition
	    to the plots.  This is useful for immediate path review, e.g., 
	    <userinput>rtarea -X 1 ... | tiris</userinput>
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-x #</option></term>
	<listitem>
	  <para>
	    Sets librt debug flags to a (hexadecimal) number.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-n #</option></term>
	<listitem>
	  <para>
	    Number of steps.
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
    
    <para>
      The <command>rtarea</command> program employs 
      <citerefentry><refentrytitle>librt</refentrytitle><manvolnum>3</manvolnum></citerefentry>
      for the geometry interrogation.
    </para>
    
  </refsect1>
 
  <refsect1 id='additional_options'>
    <title>ADDITIONAL OPTIONS</title>
    <para>
      To configure its behavior, <command>rtarea</command> makes use of 
      the <option>-c</option> option.
    </para>
    
    <para>
      Note that this approach is necessitated by the fact that 
      <emphasis remap='I'>librt (3)</emphasis> has used up nearly the entire 
      alphabet. A transition to GNU-style long option names is planned.
    </para>
    <variablelist remap='TP'>
      <varlistentry>
	<term><option>-c "set compute_centers=#"</option></term>
	<listitem>
	  <para>and</para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c "set cc=#"</option></term>
	<listitem>
	  <para>
	    determine whether area centers should be computed and reported.
	    Valid values are 1 (on) and 0 (off). The default is off.
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
 
  <refsect1 id='examples'>
    <title>EXAMPLES</title>

    <example>
      <title>Rtarea processing of example.g</title>
      
      <para>
	The command	
	<userinput>rtarea example.g all</userinput> 
	computes the area for model 'example.g' group 'all'.
      </para>

      <para>
	The command	
	<userinput>rtarea -c "set cc=1" example.g all</userinput> 
	computes the area and centers for model 'example.g' group 'all'.
      </para>
	
    </example>
  </refsect1>

  <refsect1 id='see_also'>
    <title>SEE ALSO</title>
    <para>
    <citerefentry><refentrytitle>mged</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>rt</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>pix-fb</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>librt</refentrytitle><manvolnum>3</manvolnum></citerefentry>, <citerefentry><refentrytitle>pix</refentrytitle><manvolnum>5</manvolnum></citerefentry>.
    </para>
  </refsect1>

  <refsect1 id='diagnostics'>
    <title>DIAGNOSTICS</title>
    <para>
      Numerous error conditions are possible. Descriptive messages are printed on stderr.
    </para>
  </refsect1>
  
  <refsect1 id='copyright'>
    <title>COPYRIGHT</title>
    <para>
      This software is Copyright (c) 1987-2010 United States Government as represented by the 
      U.S. Army Research Laboratory. All rights reserved.
    </para>
  </refsect1>
  
  <refsect1 id='bug_reports'>
    <title>BUG REPORTS</title>
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;bugs@brlcad.org&gt;.
    </para>
  </refsect1>
  
  <refsect1 id='author'>
    <title>AUTHOR</title>
    <para>BRL-CAD Team</para>
  </refsect1>
</refentry>


 
