TODO List for integrating MGED features into Archer

Intended as a "list of work to be done" for porting MGED features to Archer.  The absence of a feature
does not necessarily mean it is intended that this feature be added, particularly for "minor" features
or features that are currently not working well in MGED to start with - these are primarily discussion
points.

Starting with a "walkthrough" of the MGED menu:

     MGED Feature                   |         Archer Status of Feature or Alternative to Feature
------------------------------------------------------------------------------------------------------
Create New Database                 |                             DONE
Open Existing Database              |                             DONE (command line needs opendb)
Import Database	                    |    dbconcat available on command line, no menu options as in MGED
Export Database                     |    keep available on command line, no GUI menu options
Load Script                         |    no GUI dialog available, presumably command line works?
Raytrace Control Panel              |       DONE - reworked panel in Archer, improved on MGED version
Render View options                 |    Will become Export View - INCOMPLETE
Preferences->Units                  |   			  DONE
Preferences->Command Line Edit      |    Add to Preferences panel under the General tab
Preferences->Special Characters     |    Refers to globbing - per Bob this is currently being handled per
				    |    command in Archer, so a global setting may not make sense.  A
				    |    possible alternative is to have two lists of commands - globbing
				    |    and non-globbing - and the ability to select and move them from
				    |    one list to the other (kind of like traditional ftp clients) in
				    |    a Preferences tab
Preferences->Colors                 |    	    Handled in local per-topic preferences
Preferences->Fonts                  |                    INCOMPLETE, lower priority
Create/Update .mgedrc               |    .archerrc exists, needs to be expanded as more preferences are
				    |    implemented in Archer - ONGOING
Clear Command Window                |    clear command works on command line, Menu item judged not
				    |    necessary - DONE
Exit                                |                             DONE
Primitive Selection                 |    Need to integrate this functionality into the "component pick"
				    |    button - straightforward enough, button already reports what is
				    |    "picked" and just needs to do the internal logic to focus the
				    |    pick for editing operations
Matrix Selection                    |    Some work needed to do this correctly - implement a matrix
				    |    tab that is shown for every object in the right panel, which will
				    |    have controls to set things like translation vectors and rotations
				    |    to generate the matrices that hang "above" objects.  Do NOT, at
				    |    the GUI level, allow direct numerical editing of matrix values - that
				    |    is an invitation to invalid matrices and not especially intuitive.
				    |    Ensure working command line level functionality for direct matrix
				    |    manipulation if needed.  For items without a matrix show the tab with
				    |    either no values specified or the identity matrix as the starting
				    |    template.  Full functionality here will require "limited xpush"
				    |    abilities - xpushing only to a specified depth, and possibly some
				    |    awareness of full path targets vs. toplevel targets.
Primitive Editor                    |    Functionality covered elsewhere, can be dangerous in that it accepts
				    |    invalid inputs - remove
Combination Editor                  |    Functionality covered in General tab for objects in right panel,
				    |    except need color picker for rgb attribute - remove as separate entity
Attribute Editor                    |    This is a more general case of what is currently the General tab in
				    |    Archer for objects - make this into a CAD widget with the layout of
				    |    the General tab and the functionality of the Attribute editor -
				    |    General tab will become Attribute tab.  Will need to be able to
				    |    customize the standard attribute/value display to support things like
				    |    the color picker for rgb attribute
Geometry Browser                    |    Functionality should be covered by tree viewer - to ensure proper
				    |    ability to easily select objects, need tabbed tree view widget with
				    |    multiple trees - the current global tree in one tab, a flat list of
				    |    all objects in another (e.g. like the output of ls - select any item
				    |    as a tree root node), in another display tree(s) currently visible in
				    |    display (similar to what who outputs, just with the ability to expand
				    |    trees).  Probably some other possible modes to explore, even make
				    |    configurable.
Create Primitives                   |    This functionality will be handled by the graphical primitive buttons
				    |    make them into drop-down lists categorized like the MGED Create
				    |    Primitive menu, hopefully with example images for all types.
View                                |                             DONE  (duplicated elsewhere)
ViewRing			    |    Useful aspects handled by loadview and saveview - may be worth reworking
				    |    later but not a high priority currently.
Settings -> Mouse Behavior          |    Per item breakdown:
	 Pick Edit Primitive        |    Replaced by combination of "component pick" button and tree widget
	 Pick Edit Matrix           |    Replaced by matrix tab discussed under "Matrix Selection" item
	 Pick Edit Combination      |    Replaced by combination of "component pick" button and tree widget
	 Sweep Raytrace-Rect.       |    INCOMPLETE (will be handled differently in the UI as well, not under Settings)
	 Pick Raytrace Objects      |    			REMOVE
	 Query Ray                  |    INCOMPLETE (nirt functionality exists, need mouse mode)
	 Sweep Paint Rectangle      |    Incorporate into Sweep Raytrace-Rect. functionality
	 Sweep Zoom Rectangle       |    Need to see if this can be integrated as an option to the scale mode -
				    |    possibly as a left mouse button option to drag a rectangle to "zoom" to
Settings -> Transform               |    Angle/Distance Cursor not implemented yet - needs documentation as well
Settings -> Constraint Coords.      |    This is worth preserving, but needs some though on how to integrate it.
Settings -> Rotate About            |    Save ability to rotate around view and maybe a specified keypoint to allow
				    |    for flexibility
Settings -> Active Pane             |                             DONE
Settings -> Apply To                |    Not worth including unless specifically asked for - relates to multipane settings
Settings -> Query Ray Effects       |    Will be handled elsewhere - nirt/query ray settings panel
Settings -> Grid                    |                             DONE
Settings -> Grid Spacing            |                             DONE
Settings -> Framebuffer             |    Will be handled by raytrace control panel/other raytrace controls - DONE
Settings -> View Axes Position      |                             DONE (handled in preferences panel)
Modes -> Draw Grid                  |                           DONE (see Settings -> Grid)
Modes -> Snap to Grid               |                             DONE
Modes -> FrameBuffer Active         |              DONE - Archer toolbar button and raytrace control panel
Modes -> Listen for Clients         |             REMOVE - no useful user level functionality exposed
Modes -> Persistend Sweep Rect.     |             Roll this functionality into the Sweep Raytrace rectangle functionality
Modes -> Angle/Dist. Cursor         |   INCOMPLETE (duplicated in MGED Settings->Transform?)
Modes -> Faceplate                  |                             DONE
Modes -> Axes                       |   Archer has more advanced axes abilities - done
Modes -> Display Manager            |   Archer is ogl only at the moment need to support other display managers and
				    |   conditionalize the features that rely on OpenGL to only be active when ogl is.
Modes -> Multipane                  |                             DONE
Modes -> Edit Info                  |                             DONE (part of edit panel)
Modes -> Status Bar                 |             DONE (in geometry display area, bottom)
Modes -> Collaborate                |                            REMOVE
Modes -> Rate Knobs                 |                            Low priority
Modes -> Display Lists              |             Remove - no need to expose this to the users
Misc -> Z Clipping                  |             DONE - Wrapped up in Lighting setting
Misc -> Perspective                 |                           INCOMPLETE
Misc -> Faceplate                   |                            DONE
Misc -> Faceplate GUI               |             Maintain for classic mode, don't expose in Archer GUI
Misc -> Keystroke Forwarding        |                           REMOVE
Tools -> ADC Control Panel          |             INCOMPLETE - Include, but only using the controls in the Tool panel
				    |                          don't integrate in-display-manager mouse bindings
				    |                          new measurement tool more usable in most cases
Tools -> AnimMate                   |     Low priority - better approach might be to export to Blender for animation
Tools -> Grid Control Panel         |                            DONE
Tools -> Query Ray                  |             INCOMPLETE - convert to preferences tab or tool
Tools -> Raytrace Control Panel     |                            DONE
Tools -> Build Pattern Tool         |             INCOMPLETE - convert to Archer wizard
Tools -> Color Selector             |             Not needed as a separate tool - launch for appropriate preferences
Tools -> Geometry Browser           |             Not needed, make sure tree widget covers functionality
Tools -> Overlap Tool               |             INCOMPLETE - expose in Archer as a tool
Tools -> Upgrade Database...        |             Check that Archer prompts to upgrade when loading old .g
Tools -> Command Window             |             Investigate tabbed command windows?
Tools -> Graphics Window            |             Investigate tabbed graphics windows/views?
Help -> Dedication                  |                           DONE
Help -> About MGED                  |   Dialog available, contents need to be merged in
Help -> Command Manual Pages        |                           DONE
Help -> Shift Grips                 |                           DONE
Help -> Apropos                     |   INCOMPLETE, not working well in MGED either?
Help -> Manual                      |   INCOMPLETE, even in MGED needs web browser, should use docbook/tkhtml


Other Issues
------------

* archer html documentation refers to an 'ae' command, yet only 'aet'
  exists.  either the docs need updating or ae added.

* opendb command doesn't seem to be available.

* archer reports "invalid command" on 'aet' and other commands until a
  database is opened.  it shouldn't.

* archer doesn't have the -c option to run command-line or sans Tk -  need to "port"
  archer to the C code startup that MGED uses - some preliminary work has been done,
  but needs to be made more robust and tested.

* add ability to undo transaction sets

* handle situation where undo history uses up available memory (delete oldest entries?)

* move undo transaction support to libged level

Tests to Pass
-------------

* The merged Archer/MGED app needs to be able to pass all the regression
  tests currently passed by MGED


New Features
------------

* undo ability for selection sequences, other actions currently assumed to not change
  the db geometry (may be a natural consequence of transactional libged commands?)
