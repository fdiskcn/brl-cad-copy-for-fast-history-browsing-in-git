<!-- Converted by db4-upgrade version 1.0 -->

<refentry xmlns="http://docbook.org/ns/docbook" version="5.0" xml:id="arb">
  
  <refmeta>
    <refentrytitle>ARB</refentrytitle>
    <manvolnum>nged</manvolnum>
    <refmiscinfo class="source">BRL-CAD</refmiscinfo>
    <refmiscinfo class="manual">BRL-CAD User Commands</refmiscinfo>
  </refmeta>
  
  <refnamediv xml:id="name">
    <refname>arb</refname>
    <refpurpose> Creates a new ARB shape with the specified <emphasis>arb_name</emphasis>.
    </refpurpose>
  </refnamediv>
  
  <!-- body begins here -->
  <refsynopsisdiv xml:id="synopsis">
    <cmdsynopsis sepchar=" ">
      <command>arb</command>    
      <arg choice="req" rep="norepeat"> <replaceable>arb_name</replaceable>&gt;</arg>
      <arg choice="req" rep="norepeat"> <replaceable>rotation</replaceable></arg>
      <arg choice="req" rep="norepeat"> <replaceable>fallback</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsection xml:id="description"><info><title>DESCRIPTION</title></info>
    
    <para>
      The <command>arb</command> command creates a new ARB shape with the 
      specified <emphasis>arb_name</emphasis>. The new ARB will be 20 inches by 
      20 inches and 2 inches thick. The square faces will be perpendicular to the 
      direction defined by the rotation and fallback angles. This direction can be 
      determined by interpreting the rotation angle as an azimuth and the
      fallback angle as an elevation as in the <command>ae</command> command.
    </para>
  </refsection>
  
  <refsection xml:id="examples"><info><title>EXAMPLES</title></info>
    
    <para>
      The first example shows the use of the <command>arb</command> command to create a 
      new ARB shape with a specified name.  The second example shows the use of the 
      <command>ae</command> command to determine the view orientation of the arb.
    </para>
    <example><info><title>Create <emphasis>new_arb</emphasis> with a specific rotation angle and fallback angle.</title></info>
      
	 <para>
	   <prompt>mged&gt; </prompt> <userinput>arb new_arb 35 25</userinput>
	 </para>
	 <para>
	   Create new_arb with a rotation angle of 35° and a fallback angle of 25°.
	 </para>
    </example>
    <example><info><title>Using the <command>ae</command> command to determine the view orientation of the arb</title></info>
      
      <para>
	<prompt>mged&gt; </prompt><userinput>ae 35 25</userinput>
      </para>
      <para>
	Rotates view to look straight on at square face of <emphasis>new_arb</emphasis>.
      </para>
    </example>
  </refsection>
  
  <info><corpauthor>BRL-CAD Team</corpauthor></info>
  
  <refsection xml:id="bug_reports"><info><title>BUG REPORTS</title></info>
    
    <para>
      Reports of bugs or problems should be submitted via electronic
      mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
    </para>
  </refsection>
</refentry>
