---

This is a build of a Pro/ENGINEER plugin that exports geometry in
polygonal format for use with BRL-CAD.  The export plugin has a
variety of options, but should be relatively self-explanatory.

Builds are available for different versions of Pro/ENGINEER Wildfire
in 32-bit and 64-bit format.  The builds interface with the
Pro/Toolkit plugin interface.  The 64-bit builds were compiled on
Windows Vista.  See the INSTALL.txt file for more details and
assumptions for this particular build of the ProE-BRL plugin.

To inform Pro/ENGINEER about the Pro/Toolkit code that produces ASCII
file, you need to create a "protk.dat" file as follows:

line 1:name proe-brl
line 2:startup spawn
line 3:exec_file 'full path to wherever you install proe-brl'
line 4:text_dir 'full path to the "text" directory'
line 5:revision 2001
line 6:description Export facetization of displayed model to BRL-CAD
line 7:end

See "protk.dat" in this directory for an example.  The exec_file must
include the name of the executable file, and the text_dir must include
the name of the "text" directory.  In the "text" directory
Pro/ENGINEER will look for a subdirectory named "usascii" for a file
named "usermsg.txt".  The "text" directory in this directory should be
used or at least copied verbatim to where you want to use it.

The "protk.dat" file must be located where Pro/ENGINEER will look for
it.  Pro/ENGINEER will look in your current directory and in
Pro/ENGINEER's "text" directory (as well as a few other places).

The resource files (i.e. proe_brl.res, proe_brl_error.res and
proe_brl_gen_error.res) also need to be installed where Pro/ENGINEER
can find them.  Pro/ENGINEER will look in its text/resource directory.
It will also look in the resource directory that's in the text
directory specified by text_dir in the protk.dat file (see
line4:text_dir above).

If the "protk.dat" file, the "proe-brl" executable, and the
"usermsg.txt" files are installed properly, you will see the message
"Installation of Proe-BRL converter succeeded" in the Pro/ENGINEER
message window when Pro/E starts.

Once installed, the Pro/ENGINEER to BRL-CAD conversion is a two step
process.  First, in Pro/ENGINEER, you must select "Proe-brl" under the
"File" menu.  You will be prompted for an output file name, a starting
region ident number, a maximum allowed error for the facetization
approximation, and an angle control. The recomended value for the
angle control is 0.5.  After all prompts have been satisfied, the
currently displayed model will be output to a file in BRL-CAD ACSII
format (actually a Tcl script). The output file can then be converted
to BRL-CAD by using "asc2g" or by sourcing the file in MGED using the
"source" command.
