<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2009-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR AS IS'' AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='coil1'>

<refmeta>
  <refentrytitle>COIL</refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='coil_name'>
  <refname>coil</refname>
  <refpurpose>
    Generate various types of coils, using pipe control points calculated
    from coil dimensions.
  </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='coil_synopsis'>
  <cmdsynopsis>
    <command>coil</command>    
    <arg choice='opt' rep='repeat'><replaceable>options ...</replaceable></arg>
    <arg><replaceable>name</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='coil_description'>
  <title>DESCRIPTION</title>
  <para>
    <command>coil</command> creates a pipe shape within <emphasis remap='I'>coil.g</emphasis>
    or a user supplied file that implements a coil according to user provided dimensions.  
    If no dimensions are supplied defaults are used.  
  </para>
</refsect1>

<refsect1 id='coil_options'>
  <title>OPTIONS:</title>
  <variablelist remap='TP'>
    <varlistentry>
      <term><option>-n number_of_turns</option></term>
      <listitem>
	<para>
	  Sets the number of turns desired in the coil.  Must be an integer
	  value greater than zero - fractional turns are not supported.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-l</option></term>
      <listitem>
	<para>
	  Instructs the coil tool to create a "left handed" coil (default
	  is "right handed").  I.e. - from the starting point of the coil,
	  the coil winding will proceed in the -x direction for a left
	  handed coil, and in the +x direction for a right handed coil.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-d outer_diameter</option></term>
      <listitem>
	<para>
	  Sets the outer diameter of the coil in terms of millimeters.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-w wire_diameter</option></term>
      <listitem>
	<para>
	  Specify the diameter of the wire to be wound into a coil in terms
	  of millimeters.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-h helix_angle</option></term>
      <listitem>
	<para>
	  Specify the helix angle of the coil in degrees.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-p pitch</option></term>
      <listitem>
	<para>
	  Specify the pitch - distance between center of wire at beginning
	  and end of a single turn in mm.  Minimum value is equal to the
	  wire diameter.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-s cap_type</option></term>
      <listitem>
	<para>
	  Specify the capping style for the start of the coil.  The default is 0, with no
	  special styling.  1 is a squared off cap, 2 is a cap ground in the plane
	  normal to the vector of the coiling direction (the x-y plane), and 3
	  squares off the coil as well as grounding it.  Any non-default cap
	  will result in extra turns being added to the coil above those specified
	  with <option>-n</option>.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-e cap_type</option></term>
      <listitem>
	<para>
	  Specify the capping style for the end of the coil.  Default and options are
	  the same as for the starting cap.
	</para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-S number_of_turns,outer_diameter,wire_diameter,helix_angle,pitch,winding_direction</option></term>
      <listitem>
	<para>
	  An advanced option that allows specification of multiple sections, with settings specified
	  in a comma delimited list as above.  All settings are required for each section - multiple
	  sections are defined in left to right order with the left-most section being the starting
	  section at the origin.  Capping styles are supported for multiple section coils, but the
	  presence of one or more sections specified with the <option>-S</option> will cause any
	  supplied parameters other than capping styles to be ignored.  There is very little sanity
	  checking here - many inputs will result in invalid pipe primitives and generate only an
	  empty database file.
	</para>
      </listitem>
    </varlistentry>
  </variablelist>
</refsect1>

<refsect1 id='coil_examples'>
  <title>EXAMPLES</title>
  <para>
    The following will create coils exercising the various options.
  </para>
  <example>
    <title>Creating a tight coil with default dimensions and ground ends</title>
    <para>
      <userinput>coil -s 3 -e 3</userinput>
    </para>
  </example>
  <example>
    <title>Creating a coil with large helix angle and pitch</title>
    <para>
      <userinput>coil -h 40 -p 350</userinput>
    </para>
  </example>
  <example>
    <title>Creating a multi-section coil with right handed winding</title>
    <para>
      <userinput>coil -S 10,1000,50,60,800,1 -S 10,1000,50,0,100,1</userinput>
    </para>
  </example>
</refsect1>

<refsect1 id='diagnostics'>
  <title>DIAGNOSTICS</title>
  <para>
    Not much error checking yet - need to add.
  </para>
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>Clifford Yapp</para>
</refsect1>

<refsect1 id='copyright'>
  <title>COPYRIGHT</title>
  <para>
    This software is Copyright (c) 2009-2010 United States Government as
    represented by the U.S. Army Research Laboratory. All rights reserved.
  </para>
</refsect1>

<refsect1 id='bugs'>
  <title>BUGS</title>
  <para>
    There are many inputs that will be accepted but will attempt to generate
    invalid pipes.
  </para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

