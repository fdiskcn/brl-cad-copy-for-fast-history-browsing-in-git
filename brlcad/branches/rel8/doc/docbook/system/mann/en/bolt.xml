<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2008-2010 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR AS IS'' AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='bolt1'>

<refmeta>
  <refentrytitle>BOLT</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>bolt</refname>
  <refpurpose>
    Creates a BRL-CAD .g file containing bolts.
  </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>bolt</command>    
    <arg choice='opt' rep='repeat'><replaceable>options</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>
    <command>bolt</command> is a program to create a BRL-CAD data base of 
    one of four different kinds of bolts.  Up to twenty-six bolts of one type  
    and size may be created. <command>bolt</command> uses libwdb to create a 
    database file.  This program may be run interactively or the user may 
    specify options on a command line.   If the user chooses to run the program 
    interactively he answers the questions as the program prompts him.  Below 
    are the options that can be used on the command line.
  </para>
</refsect1>


<refsect1 id='options'>
  <title>OPTIONS</title>
  <variablelist remap='TP'>
    <varlistentry>
      <term><option>-o#</option></term>
      <listitem>
	<para>
	 Type of bolt to be created:  1=>bolt head; 2=>bolt head and washer;  3=>bolt
         head, washer, and bolt stem; and 4=>bolt head and bolt stem.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-fname.g</option></term>
      <listitem>
	<para>
         BRL-CAD file name.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-n#</option></term>
      <listitem>
	<para>
         The number of bolts to be created.  This number must be less than or equal to
         26 (or it will be set to  26).
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-hd#</option></term>
      <listitem>
	<para>
         Diameter of bolt head flat edge to flat edge in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-hh#</option></term>
      <listitem>
	<para>
	 Height of bolt head in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-wd#</option></term>
      <listitem>
	<para>
         Diameter of washer in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-wh#</option></term>
      <listitem>
	<para>
         Height of washer in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-sd#</option></term>
      <listitem>
	<para>
         Diameter of bolt stem in millimeters.
        </para>
      </listitem>
    </varlistentry>
    <varlistentry>
      <term><option>-sh#</option></term>
      <listitem>
	<para>
         Height of bolt stem in millimeters.
        </para>
      </listitem>
    </varlistentry>
 </variablelist>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <example>
    <title>Interactive <command>bolt</command> Session</title>
    <para>
    <literallayout>
$ bolt
Enter option:
     1 - bolt head
     2 - bolt head &amp; washer
     3 - bolt head, washer, &amp; stem
     4 - bolt head &amp; stem
1
Enter name of mged file to be created (25 char max).
     bolt.g
Enter the number of bolts to be created (26 max).
     3
Enter diameter (flat edge to flat edge) &amp; height of bolt head.
     30 10

option:  1 - bolt head
file:  bolt.g
head diameter:  30.000000, &amp; height:  10.000000
washer diameter:  0.000000, &amp; height:  0.000000
stem diameter:  0.000000, &amp; height:  0.000000
number of bolts:  3
    </literallayout>
    </para>
  </example>

  <example>
    <title>Single-Line <command>bolt</command> Command</title>
    <para>
      <userinput>bolt -o1 -fbolt.g -n3 -hd30 -hh10</userinput>
    </para>
  </example>

  <para>
   Both examples produce the same output - the bolt heads (option 1)
   are created with a diameter of 30mm and a height of 10mm in a database
   file called bolt.g.
  </para>

</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>Susan A. Coates</para>
</refsect1>

<refsect1 id='copyright'>
  <title>COPYRIGHT</title>
  <para>
    This software is Copyright (c) 2005-2010 United States Government as
    represented by the U.S. Army Research Laboratory. All rights reserved.
  </para>
</refsect1>

<refsect1 id='see_also'>
  <title>SEE ALSO</title>
  <para>
   handle(1), window(1), window_frame(1), gastank(1)
  </para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

