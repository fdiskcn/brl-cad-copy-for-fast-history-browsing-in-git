<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='press'>

<refmeta>
  <refentrytitle>PRESS</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>press</refname>
  <refpurpose>Simulates the pressing of a button.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>press</command>    
     <arg choice='req'><replaceable>button_label</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Simulates the pressing of a button. All of these button actions
	can be run directly as a command. The <emphasis>button_label</emphasis> indicates which button to
	simulate. The available buttons are as follows:
   </para>
<itemizedlist mark='bullet'>
<listitem>
<para>help -- Provides a list of the available <emphasis>button_labels</emphasis>.
</para>
</listitem>
<listitem>
<para>35,25 -- Switches to a view from an azimuth of 35� and an elevation of 25�.
</para>
</listitem>
<listitem>
<para>45,45 -- Switches to a view from an azimuth of 45� and an elevation of 45�.
</para>
</listitem>
<listitem>
<para>accept -- Simulates the <emphasis>accept</emphasis> button (accepts edits and writes the edited object to the database).
</para>
</listitem>
<listitem>
<para>reject -- Simulates the <emphasis>reject</emphasis> button (discards edits).
</para>
</listitem>
<listitem>
<para>reset -- Resets view to <emphasis>top</emphasis> and resizes such that all displayed objects are within
the view.
</para>
</listitem>
<listitem>
<para>save -- Remembers the current view aspect and size.
</para>
</listitem>
<listitem>
<para>restore -- Restores the most recently saved view.
</para>
</listitem>
<listitem>
<para>adc -- Toggles display of the adc.
</para>
</listitem>
<listitem>
<para>front -- Switches to view from the front (synonym for ae 0 0).
</para>
</listitem>
<listitem>
<para>left -- Switches to view from the left (synonym for ae 90 0).
</para>
</listitem>
<listitem>
<para>rear -- Switches to view from the rear (synonym for ae 180 0).
</para>
</listitem>
<listitem>
<para>right -- Switches to view from the right (synonym for ae 270 0).
</para>
</listitem>
<listitem>
<para>bottom -- Switches to view from the bottom (synonym for ae -90 -90).
</para>
</listitem>
<listitem>
<para>top -- Switches to view from the top (synonym for ae -90 90).
</para>
</listitem>
<listitem>
<para>oill -- Enters object illuminate mode.
</para>
</listitem>
<listitem>
<para>orot -- Enters object rotate mode (must already be in matrix edit mode).
</para>
</listitem>
<listitem>
<para>oscale -- Enters object scale mode (must already be in matrix edit mode).
</para>
</listitem>
<listitem>
<para>oxscale -- Enters object scale (x-direction only) mode (must already be in matrix
	edit mode).
</para>
</listitem>
<listitem>
<para>oyscale -- Enters object scale (y-direction only) mode (must already be in matrix
	edit mode).
</para>
</listitem>
<listitem>
<para>ozscale -- Enters object scale (z-direction only) mode (must already be in matrix
	edit mode).
</para>
</listitem>
<listitem>
<para>oxy -- Enters object translate mode (must already be in matrix edit mode).
</para>
</listitem>
<listitem>
<para>ox -- Enters object translate (horizontal only) mode (must already be in matrix edit
	mode).
</para>
</listitem>
<listitem>
<para>oy -- Enters object translate (vertical only) mode (must already be in matrix edit
	mode).
</para>
</listitem>
<listitem>
<para>sill -- Enters solid (i.e., primitive) illuminate mode.
</para>
</listitem>
<listitem>
<para>sedit -- (deprecated) Enters primitive edit mode.
</para>
</listitem>
<listitem>
<para>srot -- Enters solid (i.e., primitive) rotate mode (must be in primitive edit mode).
</para>
</listitem>
<listitem>
<para>sscale -- Enters solid (i.e., primitive) scale mode (must be in primitive edit mode).
</para>
</listitem>
<listitem>
<para>sxy -- Enters solid (i.e., primitive) translate mode (must be in primitive edit mode).
</para>
</listitem>
<listitem>
<para>zoomin -- Zooms in, synonym for zoom 2.
</para>
</listitem>
<listitem>
<para>zoomout -- Zooms out, synonym for zoom 0.5.</para>
</listitem>
<listitem>
<para>rate -- Toggles between rate and absolute mode for knobs and sliders.
</para>
</listitem>
<listitem>
<para>edit -- (deprecated) Toggles between edit and view modes for knobs and sliders
	(useful during editing to allow the knobs and sliders to be used for either editing
	operations (in edit mode) or to adjust the view without affecting the edited object
	(in view mode).
</para>
</listitem>
</itemizedlist> 

</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The example shows the use of the <command>press</command> command to view the object from the top 	direction.
  </para>
  <example>
    <title>View from the top direction.</title>
    <para>
      <prompt>mged></prompt><userinput>press top</userinput>
    </para>
    <para>Switches to view from the top direction.
    </para>
  </example>
 
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

