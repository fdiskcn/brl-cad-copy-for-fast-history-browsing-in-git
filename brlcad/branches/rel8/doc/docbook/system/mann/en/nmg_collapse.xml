<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='nmg_collapse'>

<refmeta>
  <refentrytitle>NMG_COLLAPSE</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>nmg_collapse</refname>
  <refpurpose>Simplifies an existing <emphasis>nmg_shape</emphasis> by a process of edge
	decimation.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>nmg_collapse</command>    
       <arg choice='req'><replaceable>old_nmg_shape new_nmg_shape maximum_error_dist</replaceable></arg>
   	 <arg><replaceable>minimum_angle</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Simplifies an existing <emphasis>nmg_shape</emphasis> by a process of edge
	decimation. Each edge in the <emphasis>old_nmg_shape</emphasis> is considered; if it can be deleted
	without creating an error greater than the specified <emphasis>maximum_error_dist,</emphasis> then that
	edge is deleted. If a <emphasis>minimum_angle</emphasis> is specified (degrees), then the edge will not 	be deleted if it would create a triangle with an angle less than <emphasis>minimum_angle</emphasis>. The
	resulting shape is saved in <emphasis>new_nmg_shape</emphasis>. The <emphasis>old_nmg_shape</emphasis>	must have been triangulated previous to using the <command>nmg_collapse</command> command. The resulting 	shape consists of all triangular faces.
   </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The example shows the use of the <command>nmg_collapse</command> command to simplify an existing nmg 	shape by decimating its edges. The new nmg shape will be within a maximum error distribution and will 	not have any triangles with angles less than a given minimum.
   </para>
   <example>
    <title>Create a new nmg shape by decimating the edges of an existing nmg shape.</title>
    <para>
      <prompt>mged></prompt><userinput>nmg_collapse nmg_old nmg_new 1.0 10.0</userinput>
    </para>
    <para>Decimates edges in <emphasis>nmg_old</emphasis> to produce an NMG with an error no greater than 1.0
	units. The process will not create any triangles with an angle less than 10�. The new
	NMG shape will be named <emphasis>nmg_new</emphasis>.
    </para>
  </example>
 
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

