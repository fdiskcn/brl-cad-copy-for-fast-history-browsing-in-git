<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='vdraw'>

<refmeta>
  <refentrytitle>VDRAW</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD MGED Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>vdraw</refname>
  <refpurpose>Allows drawing of lines and polygons (optionally with per
vertex normals) in the MGED graphics display. It is used to build a named list of
drawing commands for MGED, send the list to the MGED display, modify the list, or
delete all or part of the list.   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>vdraw</command>    
    
    <arg choice='req'><replaceable>command</replaceable></arg>
    <arg><replaceable>args</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Allows drawing of lines and polygons (optionally with per
vertex normals) in the MGED graphics display. It is used to build a named list of
drawing commands for MGED, send the list to the MGED display, modify the list, or
delete all or part of the list. All vertices in the <emphasis>vdraw</emphasis> command are in millimeters.
The MGED drawing commands are represented by integers in the <emphasis>vdraw</emphasis> command.
The MGED drawing commands and the integers that <emphasis>vdraw</emphasis> uses for them are:
   </para>
<table frame='all'>
<tgroup cols='3' align='center' colsep='1' rowsep='1'>
<colspec colname='c1'/>
<colspec colname='c2'/>
<colspec colname='c3'/>
<thead>
<row>
	<entry>MGED Drawing Command</entry>
	<entry>Vdraw integer</entry>
	<entry>MGED Action</entry>
</row>
</thead>
<tbody>
<row>
	<entry>RT_VLIST_LINE_MOVE</entry>
	<entry>0</entry>
	<entry>Begin a new line at this point</entry>
</row>
<row>
	<entry>RT_VLIST_LINE_DRAW</entry>
	<entry>1</entry>
	<entry>Draw line from previous point to this point</entry>
</row>
<row>
	<entry>RT_VLIST_POLY_START</entry>
	<entry>2</entry>
	<entry>Start polygon (argument is surface normal)</entry>
</row>
<row>
	<entry>RT_VLIST_POLY_MOVE</entry>
	<entry>3</entry>
	<entry>move to first polygon vertex </entry>
</row>
<row>
	<entry>RT_VLIST_POLY_DRAW</entry>
	<entry>4</entry>
	<entry>Subsequent polygon vertices </entry>
</row>
<row>
	<entry>RT_VLIST_POLY_END</entry>
	<entry>5</entry>
	<entry>Last polygon vertex (should be same as first)</entry>
</row>
<row>
	<entry>RT_VLIST_POLY_VERTNORM</entry>
	<entry>6</entry>
	<entry>Vertex normal (for shading interpolation)</entry>
</row>
</tbody>
</tgroup>
</table>

<para>
The <command>vdraw</command> commands are as follows:
</para>

<itemizedlist mark='bullet'>
<listitem>
	<para>open -- with no arguments, this returns �1� if there is a open list; �0� otherwise. If
	an argument is supplied, a command list is opened with the provided name.
	</para>
</listitem>

<listitem>
	<para>write-- with arguments of <emphasis> i c x y z</emphasis>, the MGED drawing command <command>#c</command> is placed in the <emphasis>ith</emphasis> position of the command list with the vertex as (<emphasis> x y z</emphasis>).
	</para>
	<para>-- with arguments of <emphasis> next c x y z</emphasis>, the command is placed at the end of the list.
	</para>
</listitem>

<listitem>
<para>insert -- with arguments of<emphasis> i c x y z</emphasis>, the MGED drawing command <emphasis>#c</emphasis> is inserted
just before the <emphasis>ith </emphasis>position of the command list.
</para>
</listitem>

<listitem>
<para>delete -- with an integer argument of i, the <emphasis>ith</emphasis> command is deleted.</para>
<para>-- with an argument of "last," the last command on the list is deleted.</para>
<para>-- with an argument of "all," all the commands on the list are deleted.</para>
</listitem>

<listitem>
<para>params -- with an argument of <emphasis> color rrggbb</emphasis>, the color of all objects on this list is
set. The <emphasis>rrggbb</emphasis> is a hex number representing the color, "ffffff" is white, "ff0000"
is red, "00ff00" is green, etc.</para>
<para>-- with a single string argument, the name of the current list is changed.
</para>
</listitem>
<listitem>
<para>read -- with an integer argument of <emphasis>i</emphasis>, the <emphasis>ith</emphasis> command is returned.</para>
<para>-- with an argument of "color," the current color is returned.</para>
<para>-- with an argument of "length," the number of commands in the current list is returned.</para>
<para>-- with an argument of "name," the name of the current command list is returned.</para>
</listitem>
<listitem>
<para>send -- send the current command list to the MGED display manager.
</para>
</listitem>

<listitem>
<para>vlist -- with an argument of "list," return a list of the names of all existing
command lists.</para>
<para>-- with an argument of <emphasis>delete list_name</emphasis>, delete the specified command list.
</para>
</listitem>
</itemizedlist>
<para>All textual arguments may be abbreviated by their first letter.</para>

</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The eight examples show the use of the <command>vdraw</command> command with four different commands and various arguments to build a list of commands to draw a square.
   </para>
  
       
  <example>
    <title>Open a specified list.</title>
    <para>
      <prompt>mged></prompt><userinput>vdraw open square</userinput>
    </para>
    <para>Opens a list named <emphasis>square</emphasis>.
    </para>
  </example>
 <example>
    <title>Set the color.</title>
    <para>
      <prompt>mged></prompt><userinput>vdraw params color ff00</userinput>
    </para>
    <para>Sets the color to green.
    
    </para>
  </example>
<example>
    <title>Start a line at the origin.</title>
    <para>
      <prompt>mged></prompt><userinput>vdraw write next 0 0 0 0</userinput>
    </para>
    <para>Starts a line at the origin.
       
    </para>
  </example>
<example>
    <title>Draw a line from the previous point to a specified point.</title>
    <para>
      <prompt>mged></prompt><userinput>vdraw write next 1 100 0 0</userinput>
    </para>
    <para>Draws a line from the origin to (100 0 0).
       
    </para>
  </example>
<example>
    <title>Draw a line from the previous point to a specified point.</title>
    <para>
      <prompt>mged></prompt><userinput>vdraw write next 1 100 100 0</userinput>
    </para>
    <para>Draws a line from the previous point to (100 100 0).
       
    </para>
  </example>
<example>
    <title>Draw a line from the previous point to a specified point.</title>
    <para>
      <prompt>mged></prompt><userinput>vdraw write next 1 0 100 0</userinput>
    </para>
    <para> Draws a line from the previous point to (0 100 0).
       
    </para>
  </example>
<example>
    <title>Draw a line from the previous point to the origin.</title>
    <para>
      <prompt>mged></prompt><userinput>vdraw write next 1 0 0 0</userinput>
    </para>
    <para>
       Draws a line from the previous point to the origin.
    </para>
  </example>
<example>
    <title>Send the command list to the display manager.</title>
    <para>
      <prompt>mged></prompt><userinput>vdraw send</userinput>
    </para>
    <para>Draws the square in the MGED display.
       
    </para>
  </example>


</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

