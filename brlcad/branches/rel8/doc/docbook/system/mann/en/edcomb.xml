<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "../../../resources/standard/dtd/docbookx.dtd">

<!-- Copyright (c) 2005-2008 United States Government as represented by -->
<!-- the U.S. Army Research Laboratory.                                 -->
<!--                                                                    -->
<!-- Redistribution and use in source (Docbook format) and 'compiled'   -->
<!-- forms (PDF, PostScript, HTML, RTF, etc), with or without           -->
<!-- modification, are permitted provided that the following conditions -->
<!-- are met:                                                           -->
<!--                                                                    -->
<!-- 1. Redistributions of source code (Docbook format) must retain the -->
<!-- above copyright notice, this list of conditions and the following  -->
<!-- disclaimer.                                                        -->
<!--                                                                    -->
<!-- 2. Redistributions in compiled form (transformed to other DTDs,    -->
<!-- converted to PDF, PostScript, HTML, RTF, and other formats) must   -->
<!-- reproduce the above copyright notice, this list of conditions and  -->
<!-- the following disclaimer in the documentation and/or other         -->
<!-- materials provided with the distribution.                          -->
<!--                                                                    -->
<!-- 3. The name of the author may not be used to endorse or promote    -->
<!-- products derived from this documentation without specific prior    -->
<!-- written permission.                                                -->
<!--                                                                    -->
<!-- THIS DOCUMENTATION IS PROVIDED BY THE AUTHOR "AS IS" AND ANY       -->
<!-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  -->
<!-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR -->
<!-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR -->
<!-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           -->
<!-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  -->
<!-- OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR -->
<!-- BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF         -->
<!-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          -->
<!-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  -->
<!-- USE OF THIS DOCUMENTATION, EVEN IF ADVISED OF THE POSSIBILITY OF   -->
<!-- SUCH DAMAGE.                                                       -->

<refentry id='edcomb'>

<refmeta>
  <refentrytitle>EDCOMB</refentrytitle>
  <manvolnum>nged</manvolnum>
  <refmiscinfo class='source'>BRL-CAD</refmiscinfo>
  <refmiscinfo class='manual'>BRL-CAD User Commands</refmiscinfo>
</refmeta>

<refnamediv id='name'>
  <refname>edcomb</refname>
  <refpurpose>Allows the user to modify the attributes of a combination.
   </refpurpose>
</refnamediv>

<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
  <cmdsynopsis>
    <command>edcomb</command>    
    
    <arg choice='req'><replaceable>combname</replaceable></arg>
    <arg><replaceable>regionflag regionid air_code los material_code</replaceable></arg>
  </cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'>
  <title>DESCRIPTION</title>
  <para>Allows the user to modify the attributes of a combination.
The <emphasis>combname</emphasis> is the name of the combination to be modified. The <emphasis>regionflag</emphasis> controls whether the combination is identified as a region - supplying either <emphasis>R</emphasis> or <emphasis>1</emphasis> for the <emphasis>regionflag</emphasis> indicates that the combination is a region; to indicate the combination is NOT a region simply supply any other character (<emphasis>C</emphasis> or <emphasis>0</emphasis> are useful conventions here.) If the region flag is not being set, then the remainder of the attributes are ignored.  However, the <command>edcomb</command> command requires that something is suppled for those arguments in order to work - so if using <command>edcomb</command> to turn off the region flag on a combination simply supply 0 for each of the remaining arguments to <command>edcomb</command>. If the region flag is being set, then the <emphasis>region_id, aircode, los,</emphasis> and/or <emphasis>material_code</emphasis> are set according to the arguments supplied.  
  </para>
</refsect1>

<refsect1 id='examples'>
  <title>EXAMPLES</title>
  <para>The first example shows the use of the <command>edcomb</command> command with the <emphasis>R</emphasis> flag (region) added along with the <emphasis>region_id, air_code, los</emphasis> and <emphasis>material_code</emphasis>. The second example shows how to unset the region flag for a particular combination. 
  </para>
  <example>
    <title>Make a combination a region having specific attributes.</title>
    <variablelist>
      <varlistentry>
	   <term><prompt>meged></prompt> <userinput>edcomb comb1 R 1001 0 50 8</userinput></term>
	   <listitem>
	     <para>Makes <emphasis>comb1</emphasis> a <emphasis>region</emphasis> and set its <emphasis>ident				   </emphasis> to 1001, its <emphasis>air code</emphasis> to 0, its <emphasis>los</emphasis> to 50, 			   and its <emphasis>material code</emphasis> to 8.
	     </para>
	   </listitem>
      </varlistentry>
     </variablelist>
  </example>
       
  <example>
    <title>Unset a <emphasis>region</emphasis> flag for a particular combination.</title>
    <para>
      <prompt>mged></prompt> <userinput>edcomb comb1 C 0 0 0 0</userinput>
    </para>
    <para>Unsets the <emphasis>region</emphasis> flag for combination <emphasis>comb1</emphasis>.
        </para>
  </example>
 
</refsect1>

<refsect1 id='author'>
  <title>AUTHOR</title>
  <para>BRL-CAD Team</para>
</refsect1>

<refsect1 id='bug_reports'>
  <title>BUG REPORTS</title>
  <para>
    Reports of bugs or problems should be submitted via electronic
    mail to &lt;devs@brlcad.org&gt;, or via the "cadbug.sh" script.
  </para>
</refsect1>
</refentry>

