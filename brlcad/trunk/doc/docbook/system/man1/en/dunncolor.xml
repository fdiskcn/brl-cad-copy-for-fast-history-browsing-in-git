<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- lifted from troff+man by doclifter -->
<refentry xmlns='http://docbook.org/ns/docbook' version='5.0' xml:lang='en' xml:id='dunncolor1'>
<refmeta>
    <refentrytitle>DUNNCOLOR
</refentrytitle>
<manvolnum>1</manvolnum>
<refmiscinfo class='source'>BRL-CAD</refmiscinfo>
<refmiscinfo class='manual'>BRL-CAD</refmiscinfo>
</refmeta>

<refnamediv>
<refname>dunncolor</refname>
<refpurpose>set exposure values for a Dunn Model 631 camera</refpurpose>
</refnamediv>
<!-- body begins here -->
<refsynopsisdiv xml:id='synopsis'>
<cmdsynopsis>
  <command>dunncolor</command>
    <arg choice='opt'>-p </arg>
    <arg choice='plain'><replaceable>baseval</replaceable></arg>
    <arg choice='plain'><replaceable>redval</replaceable></arg>
    <arg choice='plain'><replaceable>greenval</replaceable></arg>
    <arg choice='plain'><replaceable>blueval</replaceable></arg>
</cmdsynopsis>
</refsynopsisdiv>


<refsect1 xml:id='description'><title>DESCRIPTION</title>
<para><emphasis remap='I'>Dunncolor</emphasis>
sets the exposure values in the Dunn Instruments Model 631 camera
to the specified values.
If the
<option>-p</option>
flag is used, the Polaroid (8X10) exposure values are set.
Otherwise, the auxiliary camera exposure values are set.
<emphasis remap='I'>baseval,</emphasis>
<emphasis remap='I'>redval,</emphasis>
<emphasis remap='I'>greenval,</emphasis>
and
<emphasis remap='I'>blueval,</emphasis>
are values between 0 and 255 used to calculate the exposure times for
the red, green, and blue filters. The final value for each color is the
product of
<emphasis remap='I'>baseval</emphasis>
and the value for that color
(<emphasis remap='I'>redval</emphasis>,
<emphasis remap='I'>greenval,</emphasis>
or
<emphasis remap='I'>blueval</emphasis>).
Exposure timing is based on the counting of vertical SYNC pulses.
The final exposure value indicates the number of SYNC pulses for
which the camera's monitor should be unblanked. At a 60 Hz rate,
this means an exposure time of 1 second for every 60 SYNC pulses.
<emphasis remap='I'>Dunncolor</emphasis>
reads the old values from the camera and prints them before setting the new values.
The new values are then read from the camera and printed to verify that they have
been changed. The five values printed are the exposure values for base, red,
green, blue and black/white.</para>
</refsect1>

<refsect1 xml:id='examples'><title>EXAMPLES</title>
<para>35mm Kodachrome 64 at f/4, shaded images</para>
<literallayout remap='RS'>
<emphasis remap='I'>dunncolor 2 80 16 16</emphasis>
</literallayout> <!-- remap='RE' -->
<para>35mm Kodachrome 64 at f/4, single-pixel wireframes, hires</para>
<literallayout remap='RS'>
<emphasis remap='I'>dunncolor 14 70 16 18</emphasis>
</literallayout> <!-- remap='RE' -->
<para>16mm Video News Film 7250, f/8</para>
<literallayout remap='RS'>
<emphasis remap='I'>dunncolor 1 56 14 14</emphasis>
</literallayout> <!-- remap='RE' -->
<para>Typically, red is 4X green and blue.</para>
</refsect1>

<refsect1 xml:id='diagnostics'><title>DIAGNOSTICS</title>
<para>Diagnostics are intended to be self-explanatory.</para>
</refsect1>

<refsect1 xml:id='files'><title>FILES</title>
<variablelist remap='TP'>
  <varlistentry>
  <term><emphasis remap='I'> /dev/camera </emphasis></term>
  <listitem>
<para>camera device supported by the operating system</para>
  </listitem>
  </varlistentry>
</variablelist>
</refsect1>

<refsect1 xml:id='see_also'><title>SEE ALSO</title>
<para><citerefentry><refentrytitle>brlcad</refentrytitle><manvolnum>1</manvolnum></citerefentry>, <citerefentry><refentrytitle>dunnsnap</refentrytitle><manvolnum>1</manvolnum></citerefentry></para>
</refsect1>

<refsect1 xml:id='author'><title>AUTHOR</title>
<para>BRL-CAD Team</para>
</refsect1>

<refsect1 xml:id='copyright'><title>COPYRIGHT</title>
<para>This software is Copyright (c) 1989-2013 by the United States
Government as represented by U.S. Army Research Laboratory.</para>
</refsect1>

<refsect1 xml:id='bug_reports'><title>BUG REPORTS</title>
<para>Reports of bugs or problems should be submitted via electronic
mail to &lt;devs@brlcad.org&gt;.</para>
</refsect1>
</refentry>

