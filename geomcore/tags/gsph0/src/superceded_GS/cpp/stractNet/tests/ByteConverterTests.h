///////////////////////////////////////////////////////////
//  ByteConverterTests.h
//  Implementation of the Class ByteConverterTests
//  Created on:      06-Aug-2008 8:03:44 AM
//  Original author: david.h.loman
///////////////////////////////////////////////////////////

#if !defined(EA_E2034AB8_C755_4162_B98E_D39686920CE2__INCLUDED_)
#define EA_E2034AB8_C755_4162_B98E_D39686920CE2__INCLUDED_

class ByteConverterTests
{

public:
	ByteConverterTests();
	virtual ~ByteConverterTests();

	static void main(String[] args);

};
#endif // !defined(EA_E2034AB8_C755_4162_B98E_D39686920CE2__INCLUDED_)
