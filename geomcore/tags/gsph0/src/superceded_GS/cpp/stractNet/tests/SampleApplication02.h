///////////////////////////////////////////////////////////
//  SampleApplication02.h
//  Implementation of the Class SampleApplication02
//  Created on:      06-Aug-2008 8:03:37 AM
//  Original author: david.h.loman
///////////////////////////////////////////////////////////

#if !defined(EA_827E5D64_2B3F_4743_A13B_C31489BD01BD__INCLUDED_)
#define EA_827E5D64_2B3F_4743_A13B_C31489BD01BD__INCLUDED_

class SampleApplication02
{

public:
	SampleApplication02();
	virtual ~SampleApplication02();

	SampleApplication02(InetAddress host, int port);
	static void main(String[] args);

};
#endif // !defined(EA_827E5D64_2B3F_4743_A13B_C31489BD01BD__INCLUDED_)
