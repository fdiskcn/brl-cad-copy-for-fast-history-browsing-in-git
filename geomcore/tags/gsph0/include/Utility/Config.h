///////////////////////////////////////////////////////////
//  Config.h
//  Implementation of the Class Config
//  Created on:      04-Dec-2008 8:26:37 AM
//  Original author: Dave Loman
///////////////////////////////////////////////////////////

#if !defined(__CONFIG_H__)
#define __CONFIG_H__

/**
 * Singleton Class.
 * Holds all application Preferences read in from config file.
 */
class Config
{

public:
	Config();
	virtual ~Config();

};
#endif // !defined(__CONFIG_H__)

// Local Variables: ***
// mode: C++ ***
// tab-width: 8 ***
// c-basic-offset: 2 ***
// indent-tabs-mode: t ***
// End: ***
// ex: shiftwidth=2 tabstop=8
