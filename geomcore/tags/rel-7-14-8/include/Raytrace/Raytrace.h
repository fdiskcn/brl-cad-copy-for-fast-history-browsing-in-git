#ifndef __RAYTRACE_H__
#define __RAYTRACE_H__

namespace Raytrace {
  int init();
}

#include "Raytrace/Ray.h"


#endif  /* __RAYTRACE_H__ */

// Local Variables: ***
// mode: C++ ***
// tab-width: 8 ***
// c-basic-offset: 2 ***
// indent-tabs-mode: t ***
// End: ***
// ex: shiftwidth=2 tabstop=8
