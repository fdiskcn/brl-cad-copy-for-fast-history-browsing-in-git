About
-----

widgEditor is an easily installed, easily customizable WYSIWYG editor for simple content. It replaces node create/edit, comment create/edit textareas with an improved editing pane using JavaScript, therefore if you don't have JavaScript (or your browser doesn't support HTML editing) it degrades gracefully. Makes jQuery and fQuery usage, so Drupal 5.x only.
Instructions:

Install
-----

 * Enable the module.
 * Set 'use widgEditor' permission on user access control setup.
 * Use the widgEditor in your comments, content, etc forms by clicking 'Enable WYSIWYG' link under the respective textarea.
 * Or you can prefer widgEditor to be enabled by default in your widgEditor settings.

Notes
-----

 * Optionally install fQuery library to enable widgEditor for all the textareas in a form. Works well, but only for comments body and node body textareas, without fQuery.
 * Note that the module requires PHP 5.

Maintainer
-----
Gurpartap Singh <http://drupal.org/user/41470/contact>