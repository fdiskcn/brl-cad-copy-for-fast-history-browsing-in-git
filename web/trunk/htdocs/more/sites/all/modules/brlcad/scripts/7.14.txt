# The metadata script is outputing information about the model
# This information is parsed by the PHP script and stored in node fields 
# set globing mode; sequence with concat to suppress output of set
set glob_compat_mode 0; concat

#
# get parameters sent from Drupal
#
set views $::env(views); concat
set objects $::env(objects); concat

if {$objects == "*"} then {
  # get top objects for default drawing 
  # tops -un lists unhidden objects without their prefix
  set objects [string map {"_GLOBAL" "" "\n" " "} [tops -un]]; concat
}

#
# Output database metadata 
#

# Output the database version
puts DATABASE:VERSION
dbversion
puts ENDDATABASE

# Output the one-line text of title
puts DATABASE:TITLE 
title
puts ENDDATABASE

# Output the units text
puts DATABASE:UNITS
regexp "You are editing in '(\\w+)'." [units] out units; puts $units
puts ENDDATABASE

# Output the summary
puts DATABASE:SUMMARY
regsub -all "\n\ +" [string trim [string map {Summary: ""} [summary]]] "; "
puts ENDDATABASE

# Output objects that should be rendered
puts DATABASE:RENDER
puts $objects
puts ENDDATABASE

# Output the list of all objects (in a csv format)
puts DATABASE:OBJECTS
regsub -all -line "^(.+?) +(\\w+) +(\\d+) +(\\d+) +(\\d+)$" [ls -l] "\\1;\\2;\\3;\\4\\5"
puts ENDDATABASE


#
# Write out the rendering script
#

# zapp the drawing area
Z
# draw objects
foreach x $objects { 
  draw $x
}

units mm; concat
puts DATABASE:RAYTRACE
foreach view [split $views ":"] {
  # set the view 
  ae $view

  # auto set the viewing size 
  autoview

  # zoom in a bit
  zoom 1.5

  # raytrace the model
  puts "viewsize [view size]; orientation [view quat]; eye_pt [view eye]; clean; end;"
}
puts ENDDATABASE
