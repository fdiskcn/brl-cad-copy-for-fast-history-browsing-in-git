"FireflyStream.com", theme for Drupal 5.x.

This theme was developed by http://www.fireflystream.com/

Based on Garland theme, it has a three-column.

List of features:

Standards-compliant XHTML 1.0 Strict and CSS (No CSS hacks).

Liquid CSS layout (tableless) - the whole layout increases or decreases proportionally as dimensions are specified in ems. Try changing the font size to see this working.

Supports one, two and three columns (Left Sidebar | Content | Right Sidebar).

Cross-browser compatible.


Hope you like it.

http://www.fireflystream.com/
