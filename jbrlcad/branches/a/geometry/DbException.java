package geometry;
/**
 * DbException.java
 *
 * @author Created by Omnicore CodeGuide
 */


public class DbException extends Exception
{
	public DbException( String msg )
	{
		super( msg );
	}
}

